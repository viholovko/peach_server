source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.2'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'pg'
gem 'slim'
gem 'slim-rails'
gem 'angular-rails-templates'
gem 'bootstrap-sass'
gem 'bootstrap-sass-extras'
gem 'will_paginate-bootstrap', '~> 1.0.1'
gem 'font-awesome-rails'
gem 'cancancan'
gem 'paperclip'
gem 'i18n-js', '>= 3.0.0.rc8'
gem 'ladda-rails'
gem 'figaro'
gem 'faker'
gem 'try_api'
gem 'ruby-hmac'
gem 'rest-client'
gem 'quick_blox', git: 'https://github.com/mskubenich/quick-blox.git'
gem 'fb_graph2'
gem 'rack-cors'
gem 'stripe'
gem 'ruby-push-notifications'
gem 'sidekiq'
gem 'whenever'
gem 'custom_error_message', '~> 1.1', '>= 1.1.1'
gem 'aws-sdk', '~> 2.3'

group :test do
  gem 'rails-controller-testing'
  gem 'webmock'
  gem 'database_cleaner'
  gem 'rspec-rails', '~> 3.5'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov', :require => false
  gem 'rspec-json_expectations'
  gem 'timecop'
end