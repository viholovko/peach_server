--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: gc_dist(double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gc_dist(_lat1 double precision, _lon1 double precision, _lat2 double precision, _lon2 double precision) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $_$
      select 2 * 3961 * asin(sqrt((sin(radians(($3 - $1) / 2))) ^ 2 + cos(radians($1)) * cos(radians($3)) * (sin(radians(($4 - $2) / 2))) ^ 2));
      $_$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE admin_notifications (
    id integer NOT NULL,
    user_id integer,
    notify_type integer,
    readed boolean DEFAULT false,
    data text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: admin_notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admin_notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admin_notifications_id_seq OWNED BY admin_notifications.id;


--
-- Name: announcements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE announcements (
    id integer NOT NULL,
    title character varying,
    subtitle character varying,
    body text,
    notify_trainers boolean,
    notify_clients boolean,
    notified_times integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone
);


--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE announcements_id_seq OWNED BY announcements.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attachments (
    id integer NOT NULL,
    entity_id integer,
    entity_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone
);


--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attachments_id_seq OWNED BY attachments.id;


--
-- Name: bank_accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bank_accounts (
    id integer NOT NULL,
    name character varying,
    number character varying,
    bank character varying,
    sort_code character varying,
    user_id integer
);


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bank_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bank_accounts_id_seq OWNED BY bank_accounts.id;


--
-- Name: block_client_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE block_client_reasons (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: block_client_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE block_client_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: block_client_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE block_client_reasons_id_seq OWNED BY block_client_reasons.id;


--
-- Name: block_trainer_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE block_trainer_reasons (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: block_trainer_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE block_trainer_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: block_trainer_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE block_trainer_reasons_id_seq OWNED BY block_trainer_reasons.id;


--
-- Name: blockings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE blockings (
    id integer NOT NULL,
    from_user_id integer,
    to_user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    comment text
);


--
-- Name: blockings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE blockings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: blockings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE blockings_id_seq OWNED BY blockings.id;


--
-- Name: blockings_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE blockings_reasons (
    blocking_id integer,
    reason_id integer
);


--
-- Name: booking_change_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE booking_change_requests (
    id integer NOT NULL,
    booking_id integer,
    from_user_id integer,
    to_user_id integer,
    booking_sessions_attributes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    active boolean DEFAULT true,
    applied boolean DEFAULT false,
    date date
);


--
-- Name: booking_change_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE booking_change_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: booking_change_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE booking_change_requests_id_seq OWNED BY booking_change_requests.id;


--
-- Name: booking_session_days; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE booking_session_days (
    id integer NOT NULL,
    booking_id integer,
    day integer,
    from_time character varying,
    to_time character varying
);


--
-- Name: booking_session_days_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE booking_session_days_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: booking_session_days_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE booking_session_days_id_seq OWNED BY booking_session_days.id;


--
-- Name: booking_sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE booking_sessions (
    id integer NOT NULL,
    booking_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    from_time timestamp without time zone,
    to_time timestamp without time zone,
    rate integer,
    feeling_id integer,
    payment_id integer,
    paid_up boolean DEFAULT false,
    notified_about_rate boolean DEFAULT false
);


--
-- Name: booking_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE booking_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: booking_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE booking_sessions_id_seq OWNED BY booking_sessions.id;


--
-- Name: bookings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bookings (
    id integer NOT NULL,
    client_id integer,
    trainer_id integer,
    booking_type integer,
    status integer,
    latitude double precision,
    longitude double precision,
    session_price double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address character varying,
    training_place_id integer,
    estimated_sessions_count_per_month integer DEFAULT 0,
    estimated_first_session_date_in_month timestamp without time zone,
    estimated_last_session_date_in_month timestamp without time zone,
    last_payment_date date,
    notified_about_payment_problems boolean DEFAULT false,
    notified_about_payment_problems_date date,
    next_booking_id integer
);


--
-- Name: bookings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bookings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bookings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bookings_id_seq OWNED BY bookings.id;


--
-- Name: bookings_reject_booking_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bookings_reject_booking_reasons (
    id integer NOT NULL,
    booking_id integer,
    reject_booking_reason_id integer
);


--
-- Name: bookings_reject_booking_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bookings_reject_booking_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bookings_reject_booking_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bookings_reject_booking_reasons_id_seq OWNED BY bookings_reject_booking_reasons.id;


--
-- Name: credit_cards; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE credit_cards (
    id integer NOT NULL,
    customer_id character varying,
    email character varying,
    brand character varying,
    country character varying,
    exp_month character varying,
    exp_year character varying,
    last4 character varying,
    active boolean DEFAULT false,
    user_id integer,
    name character varying
);


--
-- Name: credit_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credit_cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credit_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credit_cards_id_seq OWNED BY credit_cards.id;


--
-- Name: email_senders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE email_senders (
    id integer NOT NULL,
    address character varying,
    port character varying,
    domain character varying,
    authentication character varying,
    user_name character varying,
    password character varying,
    enable_starttls_auto boolean
);


--
-- Name: email_senders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE email_senders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_senders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE email_senders_id_seq OWNED BY email_senders.id;


--
-- Name: favorite_trainers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE favorite_trainers (
    id integer NOT NULL,
    client_id integer,
    trainer_id integer
);


--
-- Name: favorite_trainers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE favorite_trainers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: favorite_trainers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE favorite_trainers_id_seq OWNED BY favorite_trainers.id;


--
-- Name: feedbacks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE feedbacks (
    id integer NOT NULL,
    stars integer,
    comment character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: feedbacks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE feedbacks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: feedbacks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE feedbacks_id_seq OWNED BY feedbacks.id;


--
-- Name: feelings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE feelings (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: feelings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE feelings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: feelings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE feelings_id_seq OWNED BY feelings.id;


--
-- Name: goals; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goals (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: goals_clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goals_clients (
    id integer NOT NULL,
    goal_id integer,
    client_id integer
);


--
-- Name: goals_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE goals_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: goals_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE goals_clients_id_seq OWNED BY goals_clients.id;


--
-- Name: goals_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE goals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: goals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE goals_id_seq OWNED BY goals.id;


--
-- Name: goals_specialities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goals_specialities (
    id integer NOT NULL,
    goal_id integer,
    speciality_id integer
);


--
-- Name: goals_specialities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE goals_specialities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: goals_specialities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE goals_specialities_id_seq OWNED BY goals_specialities.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE notifications (
    id integer NOT NULL,
    user_id integer,
    notification_type integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    data text,
    booking_id integer,
    action_required boolean DEFAULT false,
    session_change_request_id integer,
    booking_change_request_id integer,
    read boolean DEFAULT false,
    booking_session_id integer,
    announcement_id integer
);


--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payments (
    id integer NOT NULL,
    booking_id integer,
    user_id integer,
    session_price integer,
    fee integer,
    stripe_response text,
    date date,
    sessions_count integer,
    transaction_type integer,
    created_at timestamp without time zone,
    payment_method_last_four character varying,
    card_brand character varying
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payments_id_seq OWNED BY payments.id;


--
-- Name: privacy_policy_pages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE privacy_policy_pages (
    id integer NOT NULL,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: privacy_policy_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE privacy_policy_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privacy_policy_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE privacy_policy_pages_id_seq OWNED BY privacy_policy_pages.id;


--
-- Name: problem_report_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE problem_report_reasons (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: problem_report_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE problem_report_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: problem_report_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE problem_report_reasons_id_seq OWNED BY problem_report_reasons.id;


--
-- Name: problem_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE problem_reports (
    id integer NOT NULL,
    comment text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer,
    problem_report_reason_id integer
);


--
-- Name: problem_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE problem_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: problem_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE problem_reports_id_seq OWNED BY problem_reports.id;


--
-- Name: reject_booking_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE reject_booking_reasons (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: reject_booking_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reject_booking_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reject_booking_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE reject_booking_reasons_id_seq OWNED BY reject_booking_reasons.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schedule_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schedule_histories (
    id integer NOT NULL,
    user_id integer,
    value text,
    created_at timestamp without time zone
);


--
-- Name: schedule_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE schedule_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: schedule_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE schedule_histories_id_seq OWNED BY schedule_histories.id;


--
-- Name: schedules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schedules (
    id integer NOT NULL,
    day integer,
    user_id integer,
    from_time timestamp without time zone,
    to_time timestamp without time zone
);


--
-- Name: schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE schedules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE schedules_id_seq OWNED BY schedules.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: session_change_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE session_change_requests (
    id integer NOT NULL,
    booking_session_id integer,
    booking_id integer,
    from_time timestamp without time zone,
    to_time timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    to_user_id integer,
    from_user_id integer,
    active boolean DEFAULT true
);


--
-- Name: session_change_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE session_change_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: session_change_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE session_change_requests_id_seq OWNED BY session_change_requests.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sessions (
    id integer NOT NULL,
    token character varying,
    user_id integer,
    push_token character varying,
    device_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- Name: specialities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE specialities (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: specialities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE specialities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: specialities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE specialities_id_seq OWNED BY specialities.id;


--
-- Name: specialities_trainers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE specialities_trainers (
    id integer NOT NULL,
    trainer_id integer,
    speciality_id integer
);


--
-- Name: specialities_trainers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE specialities_trainers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: specialities_trainers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE specialities_trainers_id_seq OWNED BY specialities_trainers.id;


--
-- Name: system_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE system_settings (
    id integer NOT NULL,
    fee integer,
    min_price double precision,
    max_price double precision,
    min_radius double precision,
    max_radius double precision,
    ios_push_apns_host character varying,
    ios_push_password character varying,
    ios_push_environment_sandbox boolean DEFAULT true,
    ios_push_certificate_file_name character varying,
    ios_push_certificate_content_type character varying,
    ios_push_certificate_file_size integer,
    ios_push_certificate_updated_at timestamp without time zone,
    package_expiration_time integer,
    min_sessions integer,
    max_sessions integer,
    stripe_secret_key character varying,
    stripe_publishable_key character varying,
    qb_host character varying,
    qb_application_id character varying,
    qb_auth_key character varying,
    qb_auth_secret character varying,
    trainer_bio_max_lenght integer,
    trainer_qualification_max_lenght integer
);


--
-- Name: system_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE system_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE system_settings_id_seq OWNED BY system_settings.id;


--
-- Name: terms_n_conditions_pages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE terms_n_conditions_pages (
    id integer NOT NULL,
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: terms_n_conditions_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE terms_n_conditions_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: terms_n_conditions_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE terms_n_conditions_pages_id_seq OWNED BY terms_n_conditions_pages.id;


--
-- Name: trainer_notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE trainer_notes (
    id integer NOT NULL,
    client_id integer,
    trainer_id integer,
    text character varying
);


--
-- Name: trainer_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE trainer_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: trainer_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE trainer_notes_id_seq OWNED BY trainer_notes.id;


--
-- Name: training_places; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE training_places (
    id integer NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: training_places_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE training_places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: training_places_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE training_places_id_seq OWNED BY training_places.id;


--
-- Name: training_places_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE training_places_users (
    id integer NOT NULL,
    user_id integer,
    training_place_id integer
);


--
-- Name: training_places_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE training_places_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: training_places_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE training_places_users_id_seq OWNED BY training_places_users.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    encrypted_password character varying,
    salt character varying,
    email character varying,
    confirmed boolean,
    confirmation_token character varying,
    role_id integer,
    qb_token character varying,
    qb_login character varying,
    qb_password character varying,
    qb_user_id integer,
    facebook_id character varying,
    social boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    avatar_file_name character varying,
    avatar_content_type character varying,
    avatar_file_size integer,
    avatar_updated_at timestamp without time zone,
    reset_password_token character varying,
    first_name character varying,
    last_name character varying,
    gender integer,
    address character varying,
    post_code character varying,
    latitude double precision,
    longitude double precision,
    location_radius double precision,
    registration_step integer DEFAULT 0,
    bio text,
    email_confirmed boolean,
    training_place integer,
    client_gender integer,
    deleted_at timestamp without time zone,
    deactivate_message text,
    notifications_enabled boolean,
    qualification text,
    trainers_gender integer,
    working_hours text,
    sessions_price double precision,
    time_zone integer,
    qb_app_id character varying,
    average_response_time integer DEFAULT 30,
    average_response_time_calculated_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: what_is_pages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE what_is_pages (
    id integer NOT NULL,
    content text
);


--
-- Name: what_is_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE what_is_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: what_is_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE what_is_pages_id_seq OWNED BY what_is_pages.id;


--
-- Name: admin_notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_notifications ALTER COLUMN id SET DEFAULT nextval('admin_notifications_id_seq'::regclass);


--
-- Name: announcements id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcements ALTER COLUMN id SET DEFAULT nextval('announcements_id_seq'::regclass);


--
-- Name: attachments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments ALTER COLUMN id SET DEFAULT nextval('attachments_id_seq'::regclass);


--
-- Name: bank_accounts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_accounts ALTER COLUMN id SET DEFAULT nextval('bank_accounts_id_seq'::regclass);


--
-- Name: block_client_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY block_client_reasons ALTER COLUMN id SET DEFAULT nextval('block_client_reasons_id_seq'::regclass);


--
-- Name: block_trainer_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY block_trainer_reasons ALTER COLUMN id SET DEFAULT nextval('block_trainer_reasons_id_seq'::regclass);


--
-- Name: blockings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY blockings ALTER COLUMN id SET DEFAULT nextval('blockings_id_seq'::regclass);


--
-- Name: booking_change_requests id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_change_requests ALTER COLUMN id SET DEFAULT nextval('booking_change_requests_id_seq'::regclass);


--
-- Name: booking_session_days id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_session_days ALTER COLUMN id SET DEFAULT nextval('booking_session_days_id_seq'::regclass);


--
-- Name: booking_sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_sessions ALTER COLUMN id SET DEFAULT nextval('booking_sessions_id_seq'::regclass);


--
-- Name: bookings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bookings ALTER COLUMN id SET DEFAULT nextval('bookings_id_seq'::regclass);


--
-- Name: bookings_reject_booking_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bookings_reject_booking_reasons ALTER COLUMN id SET DEFAULT nextval('bookings_reject_booking_reasons_id_seq'::regclass);


--
-- Name: credit_cards id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_cards ALTER COLUMN id SET DEFAULT nextval('credit_cards_id_seq'::regclass);


--
-- Name: email_senders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY email_senders ALTER COLUMN id SET DEFAULT nextval('email_senders_id_seq'::regclass);


--
-- Name: favorite_trainers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY favorite_trainers ALTER COLUMN id SET DEFAULT nextval('favorite_trainers_id_seq'::regclass);


--
-- Name: feedbacks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedbacks ALTER COLUMN id SET DEFAULT nextval('feedbacks_id_seq'::regclass);


--
-- Name: feelings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY feelings ALTER COLUMN id SET DEFAULT nextval('feelings_id_seq'::regclass);


--
-- Name: goals id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals ALTER COLUMN id SET DEFAULT nextval('goals_id_seq'::regclass);


--
-- Name: goals_clients id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals_clients ALTER COLUMN id SET DEFAULT nextval('goals_clients_id_seq'::regclass);


--
-- Name: goals_specialities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals_specialities ALTER COLUMN id SET DEFAULT nextval('goals_specialities_id_seq'::regclass);


--
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments ALTER COLUMN id SET DEFAULT nextval('payments_id_seq'::regclass);


--
-- Name: privacy_policy_pages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY privacy_policy_pages ALTER COLUMN id SET DEFAULT nextval('privacy_policy_pages_id_seq'::regclass);


--
-- Name: problem_report_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_report_reasons ALTER COLUMN id SET DEFAULT nextval('problem_report_reasons_id_seq'::regclass);


--
-- Name: problem_reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_reports ALTER COLUMN id SET DEFAULT nextval('problem_reports_id_seq'::regclass);


--
-- Name: reject_booking_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reject_booking_reasons ALTER COLUMN id SET DEFAULT nextval('reject_booking_reasons_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: schedule_histories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY schedule_histories ALTER COLUMN id SET DEFAULT nextval('schedule_histories_id_seq'::regclass);


--
-- Name: schedules id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY schedules ALTER COLUMN id SET DEFAULT nextval('schedules_id_seq'::regclass);


--
-- Name: session_change_requests id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY session_change_requests ALTER COLUMN id SET DEFAULT nextval('session_change_requests_id_seq'::regclass);


--
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- Name: specialities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY specialities ALTER COLUMN id SET DEFAULT nextval('specialities_id_seq'::regclass);


--
-- Name: specialities_trainers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY specialities_trainers ALTER COLUMN id SET DEFAULT nextval('specialities_trainers_id_seq'::regclass);


--
-- Name: system_settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings ALTER COLUMN id SET DEFAULT nextval('system_settings_id_seq'::regclass);


--
-- Name: terms_n_conditions_pages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY terms_n_conditions_pages ALTER COLUMN id SET DEFAULT nextval('terms_n_conditions_pages_id_seq'::regclass);


--
-- Name: trainer_notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainer_notes ALTER COLUMN id SET DEFAULT nextval('trainer_notes_id_seq'::regclass);


--
-- Name: training_places id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY training_places ALTER COLUMN id SET DEFAULT nextval('training_places_id_seq'::regclass);


--
-- Name: training_places_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY training_places_users ALTER COLUMN id SET DEFAULT nextval('training_places_users_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: what_is_pages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY what_is_pages ALTER COLUMN id SET DEFAULT nextval('what_is_pages_id_seq'::regclass);


--
-- Name: admin_notifications admin_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_notifications
    ADD CONSTRAINT admin_notifications_pkey PRIMARY KEY (id);


--
-- Name: announcements announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: attachments attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: bank_accounts bank_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_accounts
    ADD CONSTRAINT bank_accounts_pkey PRIMARY KEY (id);


--
-- Name: block_client_reasons block_client_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY block_client_reasons
    ADD CONSTRAINT block_client_reasons_pkey PRIMARY KEY (id);


--
-- Name: block_trainer_reasons block_trainer_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY block_trainer_reasons
    ADD CONSTRAINT block_trainer_reasons_pkey PRIMARY KEY (id);


--
-- Name: blockings blockings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY blockings
    ADD CONSTRAINT blockings_pkey PRIMARY KEY (id);


--
-- Name: booking_change_requests booking_change_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_change_requests
    ADD CONSTRAINT booking_change_requests_pkey PRIMARY KEY (id);


--
-- Name: booking_session_days booking_session_days_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_session_days
    ADD CONSTRAINT booking_session_days_pkey PRIMARY KEY (id);


--
-- Name: booking_sessions booking_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_sessions
    ADD CONSTRAINT booking_sessions_pkey PRIMARY KEY (id);


--
-- Name: bookings bookings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT bookings_pkey PRIMARY KEY (id);


--
-- Name: bookings_reject_booking_reasons bookings_reject_booking_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bookings_reject_booking_reasons
    ADD CONSTRAINT bookings_reject_booking_reasons_pkey PRIMARY KEY (id);


--
-- Name: credit_cards credit_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_cards
    ADD CONSTRAINT credit_cards_pkey PRIMARY KEY (id);


--
-- Name: email_senders email_senders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY email_senders
    ADD CONSTRAINT email_senders_pkey PRIMARY KEY (id);


--
-- Name: favorite_trainers favorite_trainers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY favorite_trainers
    ADD CONSTRAINT favorite_trainers_pkey PRIMARY KEY (id);


--
-- Name: feedbacks feedbacks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT feedbacks_pkey PRIMARY KEY (id);


--
-- Name: feelings feelings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feelings
    ADD CONSTRAINT feelings_pkey PRIMARY KEY (id);


--
-- Name: goals_clients goals_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals_clients
    ADD CONSTRAINT goals_clients_pkey PRIMARY KEY (id);


--
-- Name: goals goals_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals
    ADD CONSTRAINT goals_pkey PRIMARY KEY (id);


--
-- Name: goals_specialities goals_specialities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goals_specialities
    ADD CONSTRAINT goals_specialities_pkey PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: privacy_policy_pages privacy_policy_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY privacy_policy_pages
    ADD CONSTRAINT privacy_policy_pages_pkey PRIMARY KEY (id);


--
-- Name: problem_report_reasons problem_report_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_report_reasons
    ADD CONSTRAINT problem_report_reasons_pkey PRIMARY KEY (id);


--
-- Name: problem_reports problem_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_reports
    ADD CONSTRAINT problem_reports_pkey PRIMARY KEY (id);


--
-- Name: reject_booking_reasons reject_booking_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reject_booking_reasons
    ADD CONSTRAINT reject_booking_reasons_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schedule_histories schedule_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schedule_histories
    ADD CONSTRAINT schedule_histories_pkey PRIMARY KEY (id);


--
-- Name: schedules schedules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schedules
    ADD CONSTRAINT schedules_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: session_change_requests session_change_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY session_change_requests
    ADD CONSTRAINT session_change_requests_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: specialities specialities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY specialities
    ADD CONSTRAINT specialities_pkey PRIMARY KEY (id);


--
-- Name: specialities_trainers specialities_trainers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY specialities_trainers
    ADD CONSTRAINT specialities_trainers_pkey PRIMARY KEY (id);


--
-- Name: system_settings system_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings
    ADD CONSTRAINT system_settings_pkey PRIMARY KEY (id);


--
-- Name: terms_n_conditions_pages terms_n_conditions_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY terms_n_conditions_pages
    ADD CONSTRAINT terms_n_conditions_pages_pkey PRIMARY KEY (id);


--
-- Name: trainer_notes trainer_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainer_notes
    ADD CONSTRAINT trainer_notes_pkey PRIMARY KEY (id);


--
-- Name: training_places training_places_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY training_places
    ADD CONSTRAINT training_places_pkey PRIMARY KEY (id);


--
-- Name: training_places_users training_places_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY training_places_users
    ADD CONSTRAINT training_places_users_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: what_is_pages what_is_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY what_is_pages
    ADD CONSTRAINT what_is_pages_pkey PRIMARY KEY (id);


--
-- Name: index_admin_notifications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admin_notifications_on_user_id ON admin_notifications USING btree (user_id);


--
-- Name: index_bank_accounts_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bank_accounts_on_user_id ON bank_accounts USING btree (user_id);


--
-- Name: index_blockings_on_from_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_blockings_on_from_user_id ON blockings USING btree (from_user_id);


--
-- Name: index_blockings_on_from_user_id_and_to_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_blockings_on_from_user_id_and_to_user_id ON blockings USING btree (from_user_id, to_user_id);


--
-- Name: index_blockings_on_to_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_blockings_on_to_user_id ON blockings USING btree (to_user_id);


--
-- Name: index_blockings_reasons_on_blocking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_blockings_reasons_on_blocking_id ON blockings_reasons USING btree (blocking_id);


--
-- Name: index_blockings_reasons_on_reason_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_blockings_reasons_on_reason_id ON blockings_reasons USING btree (reason_id);


--
-- Name: index_booking_change_requests_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_change_requests_on_booking_id ON booking_change_requests USING btree (booking_id);


--
-- Name: index_booking_change_requests_on_from_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_change_requests_on_from_user_id ON booking_change_requests USING btree (from_user_id);


--
-- Name: index_booking_change_requests_on_to_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_change_requests_on_to_user_id ON booking_change_requests USING btree (to_user_id);


--
-- Name: index_booking_session_days_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_session_days_on_booking_id ON booking_session_days USING btree (booking_id);


--
-- Name: index_booking_sessions_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_sessions_on_booking_id ON booking_sessions USING btree (booking_id);


--
-- Name: index_booking_sessions_on_feeling_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_booking_sessions_on_feeling_id ON booking_sessions USING btree (feeling_id);


--
-- Name: index_bookings_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bookings_on_client_id ON bookings USING btree (client_id);


--
-- Name: index_bookings_on_trainer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bookings_on_trainer_id ON bookings USING btree (trainer_id);


--
-- Name: index_bookings_reasons_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bookings_reasons_id ON bookings_reject_booking_reasons USING btree (reject_booking_reason_id);


--
-- Name: index_bookings_reject_booking_reasons_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bookings_reject_booking_reasons_on_booking_id ON bookings_reject_booking_reasons USING btree (booking_id);


--
-- Name: index_favorite_trainers_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_favorite_trainers_on_client_id ON favorite_trainers USING btree (client_id);


--
-- Name: index_favorite_trainers_on_trainer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_favorite_trainers_on_trainer_id ON favorite_trainers USING btree (trainer_id);


--
-- Name: index_feedbacks_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_feedbacks_on_user_id ON feedbacks USING btree (user_id);


--
-- Name: index_goals_clients_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goals_clients_on_client_id ON goals_clients USING btree (client_id);


--
-- Name: index_goals_clients_on_goal_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goals_clients_on_goal_id ON goals_clients USING btree (goal_id);


--
-- Name: index_goals_specialities_on_goal_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goals_specialities_on_goal_id ON goals_specialities USING btree (goal_id);


--
-- Name: index_goals_specialities_on_speciality_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goals_specialities_on_speciality_id ON goals_specialities USING btree (speciality_id);


--
-- Name: index_notifications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_user_id ON notifications USING btree (user_id);


--
-- Name: index_payments_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_booking_id ON payments USING btree (booking_id);


--
-- Name: index_payments_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_user_id ON payments USING btree (user_id);


--
-- Name: index_problem_reports_on_problem_report_reason_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_problem_reports_on_problem_report_reason_id ON problem_reports USING btree (problem_report_reason_id);


--
-- Name: index_problem_reports_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_problem_reports_on_user_id ON problem_reports USING btree (user_id);


--
-- Name: index_schedule_histories_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_schedule_histories_on_user_id ON schedule_histories USING btree (user_id);


--
-- Name: index_schedules_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_schedules_on_user_id ON schedules USING btree (user_id);


--
-- Name: index_session_change_requests_on_booking_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_session_change_requests_on_booking_id ON session_change_requests USING btree (booking_id);


--
-- Name: index_session_change_requests_on_booking_session_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_session_change_requests_on_booking_session_id ON session_change_requests USING btree (booking_session_id);


--
-- Name: index_session_change_requests_on_from_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_session_change_requests_on_from_user_id ON session_change_requests USING btree (from_user_id);


--
-- Name: index_session_change_requests_on_to_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_session_change_requests_on_to_user_id ON session_change_requests USING btree (to_user_id);


--
-- Name: index_specialities_trainers_on_speciality_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_specialities_trainers_on_speciality_id ON specialities_trainers USING btree (speciality_id);


--
-- Name: index_specialities_trainers_on_trainer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_specialities_trainers_on_trainer_id ON specialities_trainers USING btree (trainer_id);


--
-- Name: index_trainer_notes_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_trainer_notes_on_client_id ON trainer_notes USING btree (client_id);


--
-- Name: index_trainer_notes_on_trainer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_trainer_notes_on_trainer_id ON trainer_notes USING btree (trainer_id);


--
-- Name: index_training_places_users_on_training_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_training_places_users_on_training_place_id ON training_places_users USING btree (training_place_id);


--
-- Name: index_training_places_users_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_training_places_users_on_user_id ON training_places_users USING btree (user_id);


--
-- Name: problem_reports fk_rails_62db37c5dd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_reports
    ADD CONSTRAINT fk_rails_62db37c5dd FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: booking_sessions fk_rails_77eb99fb61; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_sessions
    ADD CONSTRAINT fk_rails_77eb99fb61 FOREIGN KEY (booking_id) REFERENCES bookings(id);


--
-- Name: bank_accounts fk_rails_92daa8a387; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_accounts
    ADD CONSTRAINT fk_rails_92daa8a387 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: booking_sessions fk_rails_bab93a40c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY booking_sessions
    ADD CONSTRAINT fk_rails_bab93a40c3 FOREIGN KEY (feeling_id) REFERENCES feelings(id);


--
-- Name: feedbacks fk_rails_c57bb6cf28; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT fk_rails_c57bb6cf28 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: problem_reports fk_rails_f0001fe3a6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY problem_reports
    ADD CONSTRAINT fk_rails_f0001fe3a6 FOREIGN KEY (problem_report_reason_id) REFERENCES problem_report_reasons(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20170411111753'),
('20170411111754'),
('20170411111755'),
('20170411114507'),
('20170411121028'),
('20170411125923'),
('20170411130001'),
('20170411130040'),
('20170411130113'),
('20170411130126'),
('20170411133335'),
('20170411133336'),
('20170414113911'),
('20170418123240'),
('20170418123531'),
('20170419075348'),
('20170424123555'),
('20170424133759'),
('20170425122014'),
('20170428092503'),
('20170501083406'),
('20170503122508'),
('20170504120019'),
('20170504120625'),
('20170508084038'),
('20170508121248'),
('20170511120733'),
('20170518122908'),
('20170519133844'),
('20170522083944'),
('20170522084736'),
('20170522105850'),
('20170529081512'),
('20170531082202'),
('20170601120358'),
('20170608081337'),
('20170608082227'),
('20170608085630'),
('20170609090701'),
('20170612100222'),
('20170613112313'),
('20170613113701'),
('20170613141431'),
('20170614103602'),
('20170619081458'),
('20170619131332'),
('20170620130450'),
('20170704131009'),
('20170710123146'),
('20170711084327'),
('20170711093208'),
('20170711121448'),
('20170711181705'),
('20170712062806'),
('20170712093026'),
('20170712102425'),
('20170713073338'),
('20170713134250'),
('20170714085656'),
('20170720084354'),
('20170721073433'),
('20170721081240'),
('20170723122556'),
('20170727094430'),
('20170806153753'),
('20170808093152'),
('20170808093452'),
('20170808171344'),
('20170809083227'),
('20170809112736'),
('20170809131724'),
('20170811144448'),
('20170812162259'),
('20170812181728'),
('20170813105056'),
('20170813170649'),
('20170813200004'),
('20170814160203'),
('20170814160204'),
('20170820115358'),
('20170821072701'),
('20170821115436'),
('20170826144322'),
('20170831153032'),
('20170904141225'),
('20170905082539'),
('20170908092451'),
('20170910132000'),
('20170916133856'),
('20171006150626'),
('20171118142858'),
('20171118144119'),
('20171121110250'),
('20171121110440'),
('20171121130353'),
('20171121131419'),
('20171201105604'),
('20171204165113'),
('20171207152835');


