# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170503122508) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.integer  "entity_id"
    t.string   "entity_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "block_client_reasons", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "block_trainer_reasons", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "credit_cards", force: :cascade do |t|
    t.string "customer_id"
    t.string "email"
    t.string "brand"
    t.string "country"
    t.string "exp_month"
    t.string "exp_year"
    t.string "last4"
  end

  create_table "email_senders", force: :cascade do |t|
    t.string  "address"
    t.string  "port"
    t.string  "domain"
    t.string  "authentication"
    t.string  "user_name"
    t.string  "password"
    t.boolean "enable_starttls_auto"
  end

  create_table "privacy_policy_pages", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reject_booking_reasons", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "token"
    t.integer  "user_id"
    t.string   "push_token"
    t.integer  "device_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "specialities", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "specialities_trainers", force: :cascade do |t|
    t.integer "trainer_id"
    t.integer "speciality_id"
    t.index ["speciality_id"], name: "index_specialities_trainers_on_speciality_id", using: :btree
    t.index ["trainer_id"], name: "index_specialities_trainers_on_trainer_id", using: :btree
  end

  create_table "terms_n_conditions_pages", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "encrypted_password"
    t.string   "salt"
    t.string   "email"
    t.boolean  "confirmed"
    t.string   "confirmation_token"
    t.integer  "role_id"
    t.string   "qb_token"
    t.string   "qb_login"
    t.string   "qb_password"
    t.integer  "qb_user_id"
    t.string   "facebook_id"
    t.boolean  "social",               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "credit_card_id"
    t.string   "reset_password_token"
    t.string   "name"
    t.string   "surname"
    t.integer  "gender"
    t.string   "address"
    t.string   "post_code"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "location_radius"
    t.integer  "registration_step",    default: 0
    t.text     "about"
    t.boolean  "email_confirmed"
  end

  create_table "what_is_pages", force: :cascade do |t|
    t.text "content"
  end

end
