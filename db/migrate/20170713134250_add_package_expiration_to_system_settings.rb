class AddPackageExpirationToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :package_expiration_time, :integer
  end
end
