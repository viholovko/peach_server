class CreateFeedback < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
      t.integer :stars
      t.string :comment
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps
    end
  end
end
