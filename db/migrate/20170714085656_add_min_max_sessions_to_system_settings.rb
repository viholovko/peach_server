class AddMinMaxSessionsToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :min_sessions, :integer
    add_column :system_settings, :max_sessions, :integer
  end
end
