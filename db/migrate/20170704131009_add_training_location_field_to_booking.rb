class AddTrainingLocationFieldToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :training_address, :string
  end
end
