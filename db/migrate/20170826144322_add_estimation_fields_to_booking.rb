class AddEstimationFieldsToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :estimated_sessions_count_per_month, :integer, default: 0
    add_column :bookings, :estimated_first_session_date_in_month , :datetime
    add_column :bookings, :estimated_last_session_date_in_month , :datetime
  end
end
