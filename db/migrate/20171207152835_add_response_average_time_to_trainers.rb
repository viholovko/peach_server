class AddResponseAverageTimeToTrainers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :average_response_time, :integer, default: 30
    add_column :users, :average_response_time_calculated_at, :datetime
  end
end
