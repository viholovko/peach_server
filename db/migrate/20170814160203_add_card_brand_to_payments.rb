class AddCardBrandToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :card_brand, :string
  end
end
