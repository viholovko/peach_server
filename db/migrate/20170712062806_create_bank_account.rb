class CreateBankAccount < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_accounts do |t|
      t.string :name
      t.integer :number
      t.string :bank
      t.integer :sort_code
      t.belongs_to :user, index: true, foreign_key: true
    end
  end
end
