class AddRateAndFeelingToBookingSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :feelings do |t|
      t.string :title
      t.timestamps
    end

    add_column :booking_sessions, :rate, :integer
    add_reference :booking_sessions, :feeling, foreign_key: true
  end
end
