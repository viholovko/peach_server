class AddSessionsPriceToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sessions_price, :decimal, precision: 5, scale: 2
  end
end
