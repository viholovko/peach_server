class AddLastPaymentDateToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :last_payment_date, :date
    add_column :bookings, :notified_about_payment_problems, :boolean, default: false
    add_column :bookings, :notified_about_payment_problems_date, :date
  end
end
