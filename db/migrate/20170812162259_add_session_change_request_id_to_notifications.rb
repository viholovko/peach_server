class AddSessionChangeRequestIdToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :session_change_request_id, :integer
    add_column :session_change_requests, :active, :boolean, default: true
  end
end
