class CreateBookingSessionDays < ActiveRecord::Migration[5.0]
  def change
    create_table :booking_session_days do |t|
      t.references :booking
      t.integer :day
      t.string :from_time
      t.string :to_time
    end
  end
end
