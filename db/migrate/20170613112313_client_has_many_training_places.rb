class ClientHasManyTrainingPlaces < ActiveRecord::Migration[5.0]
  def change
    rename_table :trainers_training_places, :training_places_users
    rename_column :training_places_users, :trainer_id, :user_id
  end
end
