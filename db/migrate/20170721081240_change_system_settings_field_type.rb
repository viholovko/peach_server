class ChangeSystemSettingsFieldType < ActiveRecord::Migration[5.0]
  def up
    change_column :system_settings, :min_radius, :float
    change_column :system_settings, :max_radius, :float
  end

  def down
    change_column :system_settings, :min_radius, :decimal, precision: 5, scale: 2
    change_column :system_settings, :max_radius, :decimal, precision: 5, scale: 2
  end
end
