class CreateBlockClientReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :block_client_reasons do |t|
      t.string :title
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
