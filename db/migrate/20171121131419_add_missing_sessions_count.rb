class AddMissingSessionsCount < ActiveRecord::Migration[5.0]
  def change
    unless column_exists? :payments, :sessions_count
      add_column :payments, :sessions_count, :integer
    end
  end
end
