class AddBookingIdToNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :booking_id, :integer
  end
end
