class CreateBookingChangeRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :booking_change_requests do |t|
      t.references :booking
      t.references :from_user
      t.references :to_user
      t.text :booking_sessions_attributes
      t.timestamps
    end
  end
end
