class CreateRejectBookingReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :reject_booking_reasons do |t|
      t.string :title
      t.timestamps
    end
  end
end
