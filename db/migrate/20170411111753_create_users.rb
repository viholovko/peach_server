class CreateUsers < ActiveRecord::Migration
  def change
      create_table :users do |t|
        t.string :encrypted_password
        t.string :salt
        t.string :email
        t.boolean :confirmed
        t.string :confirmation_token
        t.references :role

        t.string :qb_token
        t.string :qb_login
        t.string :qb_password
        t.integer :qb_user_id

        t.string :facebook_id
        t.boolean :social, default: false

        t.timestamps
      end
  end
end