class AddRegistrationStepToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :registration_step, :integer, default: 0
  end
end
