class AddActionRequiredToNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :action_required, :boolean, default: false
  end
end
