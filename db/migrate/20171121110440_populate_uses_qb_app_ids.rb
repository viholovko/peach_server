class PopulateUsesQbAppIds < ActiveRecord::Migration[5.0]
  def change
    qb_app_id = SystemSettings.instance.qb_application_id
    User.find_each do |user|
      user.update_attribute :qb_app_id, qb_app_id
    end
  end
end
