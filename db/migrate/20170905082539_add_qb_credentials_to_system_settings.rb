class AddQbCredentialsToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :qb_host, :string
    add_column :system_settings, :qb_application_id, :string
    add_column :system_settings, :qb_auth_key, :string
    add_column :system_settings, :qb_auth_secret, :string
  end
end
