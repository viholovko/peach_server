class CreateBlockingsReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :blockings_reasons, id: false do |t|
      t.integer :blocking_id
      t.integer :reason_id
    end
    add_index :blockings_reasons, :blocking_id
    add_index :blockings_reasons, :reason_id

    remove_column :blockings, :block_reason_id, :integer
    add_column :blockings, :comment, :text
  end
end
