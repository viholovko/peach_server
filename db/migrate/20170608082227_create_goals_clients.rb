class CreateGoalsClients < ActiveRecord::Migration[5.0]
  def change
    create_table :goals_clients do |t|
      t.references :goal
      t.references :client
    end
  end
end
