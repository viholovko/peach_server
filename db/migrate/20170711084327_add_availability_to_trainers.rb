class AddAvailabilityToTrainers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :working_hours, :text
  end
end
