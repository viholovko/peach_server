class AddMinMaxRadiusToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :min_radius, :decimal, precision: 5, scale: 2
    add_column :system_settings, :max_radius, :decimal, precision: 5, scale: 2
  end
end
