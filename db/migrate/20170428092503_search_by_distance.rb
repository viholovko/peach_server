class SearchByDistance < ActiveRecord::Migration[5.0]
  def up
    connection.execute(%q{
    create or replace function gc_dist(_lat1 float8, _lon1 float8, _lat2 float8, _lon2 float8) returns float8 as
      $$
      select 2 * 3961 * asin(sqrt((sin(radians(($3 - $1) / 2))) ^ 2 + cos(radians($1)) * cos(radians($3)) * (sin(radians(($4 - $2) / 2))) ^ 2));
      $$ language sql immutable;
    })
  end

  def down
    connection.execute(%q{
      drop function gc_dist;
    })
  end
end
