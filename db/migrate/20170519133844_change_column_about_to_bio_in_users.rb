class ChangeColumnAboutToBioInUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :about, :bio
    add_column :users, :notifications_enabled, :boolean
  end
end
