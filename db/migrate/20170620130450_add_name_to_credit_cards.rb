class AddNameToCreditCards < ActiveRecord::Migration[5.0]
  def change
    add_column :credit_cards, :name, :string
  end
end
