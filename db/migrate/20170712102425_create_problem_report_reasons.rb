class CreateProblemReportReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :problem_report_reasons do |t|
      t.string :title

      t.timestamps
    end
    remove_column :problem_reports, :reported_by, :integer
    add_reference :problem_reports, :problem_report_reason, foreign_key: true
  end
end
