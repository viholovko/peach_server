class AddNextBookingIdToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :next_booking_id, :integer
  end
end
