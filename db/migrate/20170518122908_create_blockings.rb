class CreateBlockings < ActiveRecord::Migration[5.0]
  def change
    create_table :blockings do |t|
      t.integer :from_user_id
      t.integer :to_user_id
      t.integer :block_reason_id

      t.timestamps
    end
    add_index :blockings, :from_user_id
    add_index :blockings, :to_user_id
    add_index :blockings, [:from_user_id, :to_user_id], unique: true
  end
end
