class CreateAdminNotification < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_notifications do |t|
      t.references :user
      t.integer :notify_type
      t.boolean :readed,  default: false
      t.text :data
      t.timestamps
    end
  end
end
