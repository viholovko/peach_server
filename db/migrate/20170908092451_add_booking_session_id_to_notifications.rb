class AddBookingSessionIdToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :booking_session_id, :integer
  end
end
