class AddPaymentMethodLastFourToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :payment_method_last_four, :string
  end
end
