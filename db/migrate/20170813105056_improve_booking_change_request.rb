class ImproveBookingChangeRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :booking_change_requests, :active, :boolean, default: true
  end
end
