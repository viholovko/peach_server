class CreateSchedulesHistory < ActiveRecord::Migration[5.0]
  def change
    create_table :schedule_histories do |t|
      t.references :user
      t.text :value
      t.datetime :created_at
    end
  end
end
