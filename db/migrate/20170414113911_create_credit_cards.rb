class CreateCreditCards < ActiveRecord::Migration[5.0]
  def change
    create_table :credit_cards do |t|
      t.string :customer_id
      t.string :email
      t.string :brand
      t.string :country
      t.string :exp_month
      t.string :exp_year
      t.string :last4
    end
    add_column :users, :credit_card_id, :integer
  end
end
