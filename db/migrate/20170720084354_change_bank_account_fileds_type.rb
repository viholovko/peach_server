class ChangeBankAccountFiledsType < ActiveRecord::Migration[5.0]
  def up
    change_column :bank_accounts, :number, :string
    change_column :bank_accounts, :sort_code, :string
    end

  def down
    change_column :bank_accounts, :number, 'integer USING CAST(number AS integer)'
    change_column :bank_accounts, :sort_code, 'integer USING CAST(sort_code AS integer)'
  end
end
