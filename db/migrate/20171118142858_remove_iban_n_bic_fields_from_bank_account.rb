class RemoveIbanNBicFieldsFromBankAccount < ActiveRecord::Migration[5.0]
  def change
    remove_column :bank_accounts, :bic, :string
    remove_column :bank_accounts, :iban, :string
  end
end
