class AddBookingChangeRequestIdToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :booking_change_request_id, :integer
  end
end
