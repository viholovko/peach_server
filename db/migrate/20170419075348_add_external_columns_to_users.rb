class AddExternalColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :surname, :string
    add_column :users, :gender, :integer
    add_column :users, :address, :string
    add_column :users, :post_code, :string
    add_column :users, :latitude, :float
    add_column :users, :longitude, :float
    add_column :users, :location_radius, :float
  end
end
