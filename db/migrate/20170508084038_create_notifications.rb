class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.references :user
      t.integer :notification_type

      t.timestamps
    end
  end
end
