class CreateProblemReports < ActiveRecord::Migration[5.0]
  def change
    create_table :problem_reports do |t|
      t.integer :reported_by
      t.text :comment

      t.timestamps
    end
    add_reference :problem_reports, :user, foreign_key: true
  end
end
