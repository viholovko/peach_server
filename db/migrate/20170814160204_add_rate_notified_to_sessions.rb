class AddRateNotifiedToSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :booking_sessions, :notified_about_rate, :boolean, default: false
  end
end
