class CreateTableFavoriteTrainers < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_trainers do |t|
      t.references :client
      t.references :trainer
    end
  end
end
