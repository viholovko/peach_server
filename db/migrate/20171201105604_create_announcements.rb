class CreateAnnouncements < ActiveRecord::Migration[5.0]
  def change
    create_table :announcements do |t|
      t.string :title
      t.string :subtitle
      t.text :body
      t.boolean :notify_trainers
      t.boolean :notify_clients
      t.integer :notified_times
      t.timestamps
    end
    add_attachment :announcements, :image
  end
end
