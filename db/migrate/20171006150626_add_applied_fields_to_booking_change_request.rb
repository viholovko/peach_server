class AddAppliedFieldsToBookingChangeRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :booking_change_requests, :applied, :boolean, default: false
    add_column :booking_change_requests, :date, :date
  end
end
