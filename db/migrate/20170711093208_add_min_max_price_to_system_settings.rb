class AddMinMaxPriceToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :min_price, :decimal, precision: 5, scale: 2
    add_column :system_settings, :max_price, :decimal, precision: 5, scale: 2
  end
end
