class AddMissingPaidUp < ActiveRecord::Migration[5.0]
  def change
    unless column_exists? :booking_sessions, :paid_up
      add_column :booking_sessions, :paid_up, :boolean
    end
  end
end
