class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.references :client
      t.references :trainer
      t.references :reject_booking_reason

      t.integer :booking_type
      t.integer :status
      t.float :latitude
      t.float :longitude
      t.float :session_price

      t.timestamps
    end
  end
end
