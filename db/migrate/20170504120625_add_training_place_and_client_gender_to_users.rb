class AddTrainingPlaceAndClientGenderToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :training_place, :integer
    add_column :users, :client_gender, :integer
  end
end
