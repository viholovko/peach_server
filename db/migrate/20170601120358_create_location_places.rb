class CreateLocationPlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :location_places do |t|
      t.string :title

      t.timestamps
    end
  end
end
