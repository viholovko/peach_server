class AddBicIbanFieldsToBankAccounts < ActiveRecord::Migration[5.0]


  def up
    add_column :bank_accounts, :bic, :string
    add_column :bank_accounts, :iban, :string

    change_column :system_settings, :min_price, :float
    change_column :system_settings, :max_price, :float
    change_column :users, :sessions_price, :float
  end

  def down
    change_column :system_settings, :min_price, :decimal, precision: 5, scale: 2
    change_column :system_settings, :max_price, :decimal, precision: 5, scale: 2
    change_column :users, :sessions_price, :decimal, precision: 5, scale: 2
  end
end
