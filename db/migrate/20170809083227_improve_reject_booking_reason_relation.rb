class ImproveRejectBookingReasonRelation < ActiveRecord::Migration[5.0]
  def change
    remove_column :bookings, :reject_booking_reason_id
    create_table :bookings_reject_booking_reasons do |t|
      t.references :booking
      t.references :reject_booking_reason, index: {name: 'index_bookings_reasons_id'}
    end
  end
end
