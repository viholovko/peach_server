class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.string :title
      t.timestamps
    end
    create_table :goals_specialities do |t|
      t.references :goal
      t.references :speciality
    end
  end
end
