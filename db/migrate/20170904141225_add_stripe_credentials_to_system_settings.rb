class AddStripeCredentialsToSystemSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :system_settings, :stripe_secret_key, :string
    add_column :system_settings, :stripe_publishable_key, :string
  end
end
