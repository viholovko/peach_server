class AddQualificationToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :qualification, :string
  end
end
