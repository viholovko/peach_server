class AddQbAppIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :qb_app_id, :string
  end
end
