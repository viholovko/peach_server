class CreateSessionChangeRequest < ActiveRecord::Migration[5.0]
  def change
    create_table :session_change_requests do |t|
      t.references :booking_session
      t.references :booking

      t.timestamp :from_time
      t.timestamp :to_time

      t.timestamps
    end
  end
end
