class AddActiveToCreditCard < ActiveRecord::Migration[5.0]
  def change
    add_column :credit_cards, :active, :boolean, default: false
    remove_column :users, :credit_card_id
    add_column :credit_cards, :user_id, :integer, index: true
  end
end
