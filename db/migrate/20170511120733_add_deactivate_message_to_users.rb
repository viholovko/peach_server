class AddDeactivateMessageToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :deactivate_message, :text
  end
end
