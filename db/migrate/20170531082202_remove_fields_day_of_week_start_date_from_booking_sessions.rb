class RemoveFieldsDayOfWeekStartDateFromBookingSessions < ActiveRecord::Migration[5.0]
  def change
    remove_column :booking_sessions, :start_date, :date
    remove_column :booking_sessions, :day_of_week, :integer
    remove_column :booking_sessions, :from_time, :time
    remove_column :booking_sessions, :finish_time, :time
    add_column :booking_sessions, :from_time, :datetime
    add_column :booking_sessions, :finish_time, :datetime
  end

  def up
    connection.execute(%q{
    CREATE OR REPLACE FUNCTION range_to_interval(tsrange)
  RETURNS interval AS $$ SELECT upper($1) - lower($1) $$ LANGUAGE sql STABLE;

  })
  end

  def down
    connection.execute(%q{
      drop function range_to_interval;
    })
  end


end
