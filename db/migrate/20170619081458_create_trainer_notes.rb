class CreateTrainerNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :trainer_notes do |t|
      t.references :client
      t.references :trainer
      t.string :text

      t.timestamp
    end
  end
end
