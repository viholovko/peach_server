class CreateTableWorkingHours < ActiveRecord::Migration[5.0]
  def change
    create_table :shcedules do |t|
      t.integer :day
      t.time :from_time
      t.time :to_time
      t.references :user
    end
  end
end
