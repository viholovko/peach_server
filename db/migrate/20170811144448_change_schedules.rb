class ChangeSchedules < ActiveRecord::Migration[5.0]
  def change
    remove_column :schedules, :from_time, :time
    add_column :schedules, :from_time, :datetime
    remove_column :schedules, :to_time, :time
    add_column :schedules, :to_time, :datetime
  end
end
