class ImprovePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.references :booking
      t.references :user

      t.integer :session_price
      t.integer :fee
      t.text :stripe_response
      t.date :date
      t.integer :sessions_count
      t.integer :transaction_type

      t.datetime :created_at
    end

    add_column :booking_sessions, :payment_id, :integer, index: true
    add_column :booking_sessions, :paid_up, :boolean, default: false
  end
end
