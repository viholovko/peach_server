class AddBioNQualificationMaxLenghtToSystemSettings < ActiveRecord::Migration[5.0]
  def up
    add_column :system_settings, :trainer_bio_max_lenght, :integer
    add_column :system_settings, :trainer_qualification_max_lenght, :integer
    change_column :users, :qualification, :text
  end

  def down
    remove_column :system_settings, :trainer_bio_max_lenght, :integer
    remove_column :system_settings, :trainer_qualification_max_lenght, :integer
    change_column :users, :qualification, :string
  end
end
