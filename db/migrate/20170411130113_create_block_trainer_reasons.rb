class CreateBlockTrainerReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :block_trainer_reasons do |t|
      t.string :title
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
