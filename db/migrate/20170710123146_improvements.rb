class Improvements < ActiveRecord::Migration[5.0]
  def change
    rename_column :booking_sessions, :finish_time, :to_time
    rename_column :bookings, :training_address, :address
    add_column :bookings, :training_place_id, :integer
  end
end
