class ImproveSessionChangeRequests < ActiveRecord::Migration[5.0]
  def change
    add_reference :session_change_requests, :to_user, index: true
    add_reference :session_change_requests, :from_user, index: true
  end
end
