class CreateWhatIsPages < ActiveRecord::Migration[5.0]
  def change
    create_table :what_is_pages do |t|
      t.text :content
    end
  end
end
