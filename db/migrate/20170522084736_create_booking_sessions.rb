class CreateBookingSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :booking_sessions do |t|
      t.references :booking, foreign_key: true
      t.integer :day_of_week
      t.time :from_time
      t.time :finish_time
      t.date :start_date

      t.timestamps
    end
  end
end
