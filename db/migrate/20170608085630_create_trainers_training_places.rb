class CreateTrainersTrainingPlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :trainers_training_places do |t|
      t.references :trainer
      t.references :training_place
    end
    rename_table :location_places, :training_places
  end
end
