class AddAnnouncementIdToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :announcement_id, :integer
  end
end
