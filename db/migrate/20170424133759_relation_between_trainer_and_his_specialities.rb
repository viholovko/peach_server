class RelationBetweenTrainerAndHisSpecialities < ActiveRecord::Migration[5.0]
  def change
    create_table :specialities_trainers do |t|
      t.references :trainer
      t.references :speciality
    end
  end
end
