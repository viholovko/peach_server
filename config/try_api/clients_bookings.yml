title: 'Bookings'
methods:
  -
    title: 'List'
    path: '/client/bookings'
    method: 'get'
    headers:
      -
        name: 'Session-Token'
    parameters:
      -
        name: 'page'
        type: 'integer'
        description: '1 by default'
      -
        name: 'per_page'
        type: 'integer'
        description: '10 by default'
      -
        name: 'status'
        type: 'string'
        description: 'pending | approved | rejected | canceled_by_client | canceled_by_trainer | finished'
      -
        name: 'booking_type'
        type: 'string'
        description: 'renewable | once | free'
      -
        name: 'sort_column'
        type: 'string'
        description: 'created_at | '
      -
        name: 'sort_type'
        type: 'string'
        description: 'asc|desc'
    example_responses:
      -
        code: 200
        response: >
          {
             "id":11,
             "type":"renewable",
             "status":"pending",
             "first_name":"user28",
             "last_name":"Dou",
             "avatar":null,
             "online":false,
             "days_per_week":1,
             "price":493.8,
             "latitude":55.55,
             "longitude":65.4321,
             "distance":null,
             "address":"test",
             "booking_session_days":[
                {
                   "day":3,
                   "from_time":1502452800.0,
                   "to_time":1502460000.0
                }
             ],
             "reject_reasons":[
                {
                    "id":3,
                    "title":"test"
                }
             ],
             "next_session_date":1502841600,
             "next_session_from_time":"12:00",
             "next_session_to_time":"14:00",
             "sessions_count":17,
             "sessions_in_this_week_count":1,
             "sessions_in_this_month_count":4,
             "first_session_date":1502280000.0,
             "last_session_date":1511956800.0,
             "first_session_in_this_week_date":1502280000.0,
             "last_session_in_this_week_date":1502280000.0,
             "first_session_in_this_month_date":1502280000.0,
             "last_session_in_this_month_date":1504094400.0,
             "once_session_id":44,
             "once_session_from_time":1502280000.0,
             "once_session_to_time":1502287200.0,
             "change_request_id":null,
             "requested_from_time":null,
             "requested_to_time":null
          }
        type: 'json'
      -
        code: 401
        response: var:access_denied_error
        type: 'json'
      -
        code: 422
        response: var:not_trainer_error
        type: 'json'
  -
    title: 'Show'
    path: '/client/bookings/:id'
    method: 'get'
    headers:
      -
        name: 'Session-Token'
    parameters:
      -
        name: 'session_change_request_id'
        type: number
      -
        name: 'booking_change_request_id'
        type: number
    example_responses:
      -
        code: 200
        response: >
          {
             "id":10,
             "type":"renewable",
             "status":"pending",
             "first_name":"user26",
             "last_name":"Dou",
             "avatar":null,
             "online":false,
             "days_per_week":1,
             "price":493.8,
             "latitude":55.55,
             "longitude":65.4321,
             "distance":null,
             "address":"test",
             "booking_session_days":[
                {
                   "day":3,
                   "from_time":1502280000.0,
                   "to_time":1502287200.0
                }
             ],
             "reject_reasons":[
                {
                  "id":4,
                  "title":"test"
                }
             ],
             "next_session_date":1502236800,
             "next_session_from_time":"12:00",
             "next_session_to_time":"14:00",
             "sessions_count":17,
             "sessions_in_this_week_count":1,
             "sessions_in_this_month_count":4,
             "first_session_date":1502280000.0,
             "last_session_date":1511956800.0,
             "first_session_in_this_week_date":1502280000.0,
             "last_session_in_this_week_date":1502280000.0,
             "first_session_in_this_month_date":1502280000.0,
             "last_session_in_this_month_date":1504094400.0,
             "once_session_id":41,
             "once_session_from_time":1502280000.0,
             "once_session_to_time":1502287200.0,
             "change_request_id":null,
             "requested_from_time":null,
             "requested_to_time":null
          }
        type: 'json'
      -
        code: 401
        response: var:access_denied_error
        type: 'json'
      -
        code: 422
        response: var:not_client_error
        type: 'json'
  -
    title: 'Approve'
    path: '/client/bookings/:id/approve'
    method: 'post'
    headers:
          -
            name: 'Session-Token'

    example_responses:
      -
        code: 200
        response: >
          {
            "message": "Booking approved!"
          }
        type: 'json'
      -
        code: 422
        response: >
          {
            "errors": [
              "Booking already approved!"
            ]
          }
        type: 'json'
      -
        code: 422
        response: >
         {
           "errors": [
             "Booking not found!"
           ]
         }
        type: 'json'
  -
    title: 'Reject'
    path: '/client/bookings/:id/reject'
    method: 'post'
    headers:
          -
            name: 'Session-Token'
    parameters:
      -
        name: 'reject_booking_reason_ids'
        type: 'array'
        parameters:
          -
            type: integer

    example_responses:
      -
        code: 200
        response: >
          {
            "message": "Booking rejected!"
          }
        type: 'json'
      -
        code: 422
        response: >
          {
            "errors": [
              "Reject booking reason can't be blank"
            ]
          }
        type: 'json'
      -
        code: 422
        response: >
          {
            "errors": [
              "Booking already rejected!"
            ]
          }
        type: 'json'
      -
        code: 422
        response: >
         {
           "errors": [
             "Booking not found!"
           ]
         }
        type: 'json'
  -
    title: 'Cancel'
    path: '/client/bookings/:id/cancel'
    method: 'post'
    headers:
          -
            name: 'Session-Token'
    parameters:
      -
        name: 'reject_booking_reason_ids'
        type: 'array'
        parameters:
          -
            type: integer

    example_responses:
      -
        code: 200
        response: >
          {
            "message": "Booking cancelled!"
          }
        type: 'json'
      -
        code: 422
        response: >
          {
            "errors": [
              "Reject booking reason can't be blank"
            ]
          }
        type: 'json'
      -
        code: 422
        response: >
          {
            "errors": [
              "Booking already canceled!"
            ]
          }
        type: 'json'
      -
        code: 422
        response: >
         {
           "errors": [
             "Booking not found!"
           ]
         }
        type: 'json'