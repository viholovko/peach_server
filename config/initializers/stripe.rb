begin
  settings = SystemSettings.instance

  Rails.configuration.stripe = {
      :publishable_key => settings.stripe_publishable_key.to_s,
      :secret_key      => settings.stripe_secret_key.to_s
  }

  Stripe.api_key = Rails.configuration.stripe[:secret_key]
rescue
end