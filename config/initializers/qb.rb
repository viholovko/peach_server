begin
  settings = SystemSettings.instance

  QuickBlox.configure do |config|
    config.host = settings.qb_host || "https://api.quickblox.com"
    config.application_id = settings.qb_application_id.to_s
    config.auth_key = settings.qb_auth_key.to_s
    config.auth_secret = settings.qb_auth_secret.to_s
  end
rescue
end