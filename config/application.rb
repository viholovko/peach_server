require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
require 'csv'
module PeachServer
  class Application < Rails::Application
      config.assets.paths << Rails.root.join("node_modules")
      config.autoload_paths << Rails.root.join('app/workers')

      config.i18n.enforce_available_locales = false
      config.i18n.available_locales = [:en]
      config.i18n.default_locale = :en
      config.autoload_paths += %W( #{config.root}/app/streamers )
      config.autoload_paths += %W( #{config.root}/app/workers )
      config.active_record.schema_format = :sql
      config.active_record.time_zone_aware_types = [:datetime]
      config.time_zone = 'UTC'
      config.active_record.default_timezone = :utc

      config.assets.precompile += %w( static.css static.js )
  end
end
