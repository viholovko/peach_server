set :environment, 'development'
set :output, 'log/cron.log'

every 10.minutes do
  runner "RenewSessionsWorker.perform_async"
end

every 10.minutes do
  runner "ChargeClientsWorker.perform_async"
end

every 1.minute do
  runner "RateSessionNotificationWorker.perform_async()"
end

every 10.minutes do
  runner "ApplyChangeRequestsWorker.perform_async()"
end

every 30.minutes do
  runner "AverageResponseTimeWorker.perform_async()"
end