require 'sidekiq/web'

Rails.application.routes.draw do
  resources :country_flags, only: [:index]
  root to: 'pages#index'

  mount TryApi::Engine => '/developers'
  mount Sidekiq::Web, at: '/sidekiq'

  scope '(:locale)' do
    resources :attachments, only: [] do
      collection do
        post '/:entity_type', to: 'attachments#create'
      end
    end

    namespace :api do
      namespace :v1 do

        post :login, to: 'sessions#create'

        scope '(:user_type)' do
          post :signup, to: 'users#create'
          post :'login/facebook', to: 'sessions#facebook'
          delete :logout, to: 'sessions#destroy'
        end

        namespace :trainer do
          resources :steps, only: [:create]
          resources :bookings, only: [:create, :show, :index, :update] do
            member do
              post :cancel
            end
          end
          resources :clients, only: [:index, :show] do
            member do
              resource :blockings, only: [:create]
              resources :note, only: [:create, :index]
              get :sessions,    to: 'clients#sessions'
            end
          end
          resources :sessions, only: [:index]
          resources :bank_accounts, only: [:create, :index]
          resources :earnings, only: [:index]
          resources :schedule, only: [:index]
        end

        namespace :client do
          resources :trainers, only: [:index, :show] do
            member do
              resource :blockings, only: [:create]
            end
          end
          resources :my_trainers, only: [:index]
          resources :top_trainers, only: [:index]

          resources :bookings, only: [:index, :show] do
            member do
              post :approve
              post :reject
              post :cancel
            end
          end

          resources :booking_sessions, only: [:index] do
            member do
              post :rate
            end
          end
          resources :favorite_trainers, only: [:index, :destroy] do
            member do
              post :create, to: 'favorite_trainers#create' # TODO WTF ???
            end
          end

          resources :schedule, only: [:index]
          resources :billings, only: [:index]
        end

        get :profile, to: 'users#profile'

        resources :problem_reports, only: [:create]

        resources :pages, only: [] do
          collection do
            get :what_is
            get :terms_and_conditions
            get :privacy_policy
          end
        end

        resources :pushes, only: [:create]
        resources :users, only: [] do
          collection do
            get :online_status
            get :qb_users
            put :update
            delete :destroy
          end
        end
        resources :credit_cards, only: [:create, :update, :index, :destroy]

        resources :email_verification, only: [:create] do
          collection do
            post :resend
          end
        end
        resources :password_resets, only: [:create] do
          collection do
            put :update
          end
        end
        resources :specialities,          only: [:index]
        resources :goals,                 only: [:index]
        resources :block_client_reasons,  only: [:index]
        resources :block_trainer_reasons, only: [:index]
        resources :notifications,         only: [:index] do
          member do
            post :mark_as_read
          end
          collection do
            get :unread_count
          end
        end
        resources :content,               only: [:index]
        resources :training_places,       only: [:index]
        resources :feelings,              only: [:index]
        resources :problem_report_reasons,only: [:index]
        resources :feedbacks,             only: [:create]

        resources :change_requests, only: [:index, :create, :show] do
          member do
            post :accept
            post :reject
          end
        end

        resources :announcements, only: [:index, :show]
      end
    end

    namespace :admin do
      resources :sessions,                only: [:create] do
        collection do
          delete :destroy
          get :check
        end
      end

      resources :what_is_page,            only: [:create, :index]
      resources :terms_n_conditions_page, only: [:create, :index]
      resources :privacy_policy_page,     only: [:create, :index]

      resources :email_sender,            only: [] do
        collection do
          get :show
          put :update
        end
      end

      resources :system_settings, only: [] do
        collection do
          get :show
          put :update
        end
      end

      resources :specialities,            only: [:index, :create, :show, :destroy, :update, :new]
      resources :goals,                   only: [:index, :create, :show, :destroy, :update, :new]
      resources :block_client_reasons,    only: [:index, :create, :show, :destroy, :update]
      resources :block_trainer_reasons,   only: [:index, :create, :show, :destroy, :update]
      resources :reject_booking_reasons,  only: [:index, :create, :show, :destroy, :update]
      resources :training_places,         only: [:index, :create, :show, :destroy, :update]
      resources :feelings,                only: [:index, :create, :show, :destroy, :update]
      resources :problem_report_reasons,  only: [:index, :create, :show, :destroy, :update]
      resources :admins,                  only: [:index, :create, :show, :destroy]
      resources :clients,                 only: [:index, :show, :update, :destroy] do
        member do
          get :blocked_trainers
          put '/unblock_trainer', :action => 'unblock_trainer', :as => 'unblock_trainer'
          get :blocked_by_trainers
          get :favorite_trainers
          get :booking_sessions
        end
      end
      resources :trainers,                only: [:index, :show, :update, :destroy, :create, :new] do
        member do
          put :confirm, to: 'trainers#confirm'
          get :blocked_clients
          get :clients
          get :blocked_by_clients
          get :bookings
        end
      end
      resources :deactivated_clients,     only: [:index, :destroy] do
        member do
          post :reactivate
        end
      end
      resources :deactivated_trainers,    only: [:index, :destroy] do
        member do
          post :reactivate
        end
      end
      resources :users,                   only: [:destroy]
      resources :problem_reports,         only: [:index, :destroy]
      resources :admin_notifications,     only: [:index] do
        member do
          put :mark_as_read
        end
      end
      resources :feedbacks,              only: [:index] do
        collection do
          get :avg
        end
      end

      resources :announcements,          only: [:index, :create, :update, :destroy, :show] do
        member do
          post :resend
        end
      end
    end

    resources :password_resets, only: [:create, :update, :show, :edit]

    resources :pages, only:[] do
      get :check_session
    end

    get '/app', to: 'pages#app'
    get '/password_restored', to: 'pages#password_restored'

    resources :users, only: [] do
      collection do
        get :confirm_email
      end
    end
  end
end
