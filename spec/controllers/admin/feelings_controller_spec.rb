require 'rails_helper'
RSpec.describe Admin::FeelingsController, type: :controller do
  render_views

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: { format: :json }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: { format: :json }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user
      15.times { create :feeling }

      get :index, params: { format: :json }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['feelings'].count).to eq(15)

      get :index, params: { format: :json, sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['feelings'][4]['title']).to eq('Feeling 5')
    end
  end

  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: { title: 'Olympic' }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: { feeling: { title: 'Olympic' } }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { feeling: { title: ' ' } }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should render successful message if title is present' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { feeling: { title: 'Olympic' } }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Session rating has been successfully added.')
      expect(Feeling.last.title).to eq('Olympic')
    end
  end

  describe '#show' do
    it 'should render unauthorized' do
      feeling = create :feeling

      get :show, params: { id: feeling.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      feeling = create :feeling

      get :show, params: { format: :json, id: feeling.id }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show feeling' do
      user = create :user, :admin
      sign_in user: user
      feeling = create :feeling

      get :show, params: { format: :json, id: feeling.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['feeling']['title']).to eq(feeling.title)
      expect(response_body['feeling']['id']).to eq(feeling.id)
    end
  end

  describe '#destroy' do
    it 'should render unauthorized' do
      feeling = create :feeling

      delete :destroy, params: { id: feeling.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      feeling = create :feeling

      delete :destroy, params: { id: feeling.id }
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user
      feeling = create :feeling

      delete :destroy, params: { id: feeling.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq("Session rating has been successfully removed.")
    end
  end

  describe '#update' do
    it 'should render unauthorized' do
      feeling = create :feeling

      put :update, params: {
        id: feeling.id,
        feeling: { title: 'Olympic' }
      }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      feeling = create :feeling

      put :update, params: {
        id: feeling.id,
        feeling: { title: 'Olympic' }
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user
      feeling = create :feeling

      put :update, params: {
        id: feeling.id,
        feeling: { title: ' ' }
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should successful update feeling title' do
      user = create :user, :admin
      sign_in user: user
      feeling = create :feeling

      put :update, params: {
        id: feeling.id,
        feeling: { title: 'Olympic' }
      }
      feeling.reload
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq("Session rating has been successfully updated.")
      expect(feeling.title).to eq('Olympic')
    end
  end
end
