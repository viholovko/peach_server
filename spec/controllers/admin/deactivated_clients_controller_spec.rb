require 'rails_helper'
RSpec.describe Admin::DeactivatedClientsController, type: :controller do

  render_views

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user

      15.times do |i|
        user_destroyed = create :user, :client
        user_destroyed.destroy
        #delete admin_user_path, params: {id: deleted_user.id, deactivate_message: "Reason"}
      end

      expect(User.only_deleted.count).to eq(15)
      expect(User.count).to eq(1) # one registred user

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body["total"]).to eq(15)
      expect(response_body["deactivated_clients"].count).to eq(10)

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["deactivated_clients"].count).to eq(15)

      get :index, params: {:format => :json, per_page: 10, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["deactivated_clients"].count).to eq(5)

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["deactivated_clients"].count).to eq(10)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user
      15.times do |i|
        user_destroyed = create :user, :client
        user_destroyed.destroy
      end

      get :index, params: {:format => :csv}
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body)
      expect(response_body).to_not eq(nil)
    end


  end
  describe '#destroy' do
    it 'should successfully deleted client from database' do
      user = create :user, :admin
      sign_in user: user

      user_destroyed = create :user, :client
      user_destroyed.destroy

      expect(User.only_deleted.count).to eq(1)
      delete :destroy, params: {id: user_destroyed.id}

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Can not remove last user.'])

      expect(User.only_deleted.count).to eq(1)
      expect(User.count).to eq(1)
      expect(User.with_deleted.count).to eq(2)


      create :user, :client
      user_destroyed = create :user, :client
      user_destroyed.destroy

      expect(User.only_deleted.count).to eq(2)
      delete :destroy, params: {id: user_destroyed.id}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq('Client has been successfully removed.')

      expect(User.only_deleted.count).to eq(1)
      expect(User.count).to eq(2)

    end
  end
  describe '#reactivate' do
    it 'should render unauthorized' do
      post :reactivate, params: {id: 1}
      expect(response.status).to be 401
    end

    it 'should successful reactivate account' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      client.destroy
      expect(User.only_deleted.count).to eq(1)
      post :reactivate, params: {id: client.id}
      expect(User.only_deleted.count).to eq(0)
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq('Client has been successfully reactivated.')
    end

    it 'should render error if user with this email already exists' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client, email: 'user1@gmail.com'
      client.destroy
      expect(User.only_deleted.count).to eq(1)
      create :user, :client, email: 'user1@gmail.com'
      post :reactivate, params: {id: client.id}
      expect(User.only_deleted.count).to eq(1)
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(['User with this email address is already exists.'])
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :reactivate, params: {id: 1}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end
  end
end