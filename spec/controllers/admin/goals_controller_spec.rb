require 'rails_helper'
include SessionsHelper

RSpec.describe Admin::GoalsController, type: :controller do

  render_views

  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: {
          goal: {title: 'Goal'}
      }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: {
          goal: {title: 'Goal'}
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should render error if title and specialities is not filled' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: {
          goal: {title: ''}
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]["title"]).to eq(["can't be blank"])
      expect(response_body["validation_errors"]["specialities"]).to eq(["should have at least one speciality"])
    end
    it 'should render successful message if title is present' do
      user = create :user, :admin
      sign_in user: user

      specialities = []
      3.times {specialities << create(:speciality)}
      post :create, params: {
          goal: {
              title: 'Goal',
              speciality_ids: specialities.map(&:id)
          }
      }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Goal has been successfully added.")
    end


  end

  describe '#new' do
    it 'should render unauthorized' do
      get :new
      expect(response.status).to be 401
    end

    it 'should successful create new and redirect' do
      user = create :user, :admin
      sign_in user: user

      get :new, format: 'json'
      expect(response.status).to be 200
      expect(assigns(:goal)).to be_a_new(Goal)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      get :new
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

  describe '#destroy' do
    it 'should render unauthorized' do
      delete :destroy, params: {id: 2 }
      expect(response.status).to be 401
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user

      goal = create :goal

      delete :destroy, params: {id: goal.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Goal has been successfully removed.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      goal = create :goal

      delete :destroy, params: {id: goal.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

  describe '#update' do
    it 'should render unauthorized' do
      goal = create :goal, title: "Goal"

      put :update, params: {id: goal.id, goal: { title: "New goal" } }
      expect(response.status).to be 401
      expect(Goal.find(goal.id).title).to eq("Goal")
    end

    it 'should successful update' do
      user = create :user, :admin
      sign_in user: user

      goal = create :goal, title: "Goal"

      put :update, params: {id: goal.id, goal: { title: "New goal" } }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(Goal.find(goal.id).title).to eq("New goal")
      expect(response_body["message"]).to eq("Goal has been successfully updated.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      goal = create :goal, title: "Goal"

      put :update, params: {id: goal.id, goal: { title: "New goal" } }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(Goal.find(goal.id).title).to eq("Goal")
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      goal = create :goal, title: "Goal"

      put :update, params: {id: goal.id, goal: { title: "" } }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]).to eq("title" => ["can't be blank"])
      expect(Goal.find(goal.id).title).to eq("Goal")
    end

  end

  describe '#show' do
    it 'should render unauthorized' do
      goal = create :goal

      get :show, params: {id: goal.id}
      expect(response.status).to be 401
    end

    it 'should successfully show' do
      user = create :user, :admin
      sign_in user: user

      specialities = []
      3.times { |i|
        specialities.push(create :speciality, title: "Speciality #{i}")
      }
      spec_ids = specialities.map{|i| i.id}
      goal = create :goal, title: "Goal", speciality_ids: spec_ids
      get :show, params: {id: goal.id, :format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["goal"]["title"]).to eq("Goal")
      expect(response_body["goal"]["id"]).to eq(goal.id)
      expect(response_body["goal"]["created_at"]).to eq(goal.created_at.try(:strftime, "%d/%m/%Y"))
      3.times { |i|
        expect(response_body["goal"]["specialities"][i]["id"]).to eq(specialities[i].id)
        expect(response_body["goal"]["specialities"][i]["title"]).to eq(specialities[i].title)
      }
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      goal = create :goal, title: "Goal"

      get :show, params: {id: goal.id, :format => :json}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end
  end


  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user

      15.times do |i|
        create :goal, title: "My title #{i}"
      end

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["goals"].count).to eq(10)
      iterator = 14
      10.times do |i|
        expect(response_body["goals"][i]["title"]).to eq("My title #{iterator}")
        iterator =  iterator - 1
      end

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["goals"].count).to eq(15)
      expect(response_body["goals"][14]["title"]).to eq("My title 0")

      get :index, params: {:format => :json, per_page: 10, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["goals"].count).to eq(5)
      expect(response_body["goals"][0]["title"]).to eq("My title 4")

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["goals"].count).to eq(10)
      expect(response_body["goals"][0]["title"]).to eq("My title 0")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

end