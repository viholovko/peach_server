require 'rails_helper'
include SessionsHelper

RSpec.describe Admin::TrainersController, type: :controller do

  render_views

  describe '#destroy' do
    it 'should render unauthorized' do
      delete :destroy, params: {id: 2 }
      expect(response.status).to be 401
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user

      trainer = create :user, :trainer

      delete :destroy, params: {id: trainer.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Trainer has been successfully removed.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      trainer = create :user, :trainer

      delete :destroy, params: {id: trainer.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user

      15.times do |i|
        create :user, :trainer, first_name: "John #{i}", last_name: "Carmack"
      end

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["trainers"].count).to eq(10) # for default param. 'per_page'
      10.times do |i|
        expect(response_body["trainers"][i]["full_name"]).to eq("John #{14-i} Carmack")
      end

      get :index, params: {:format => :json, per_page: 5}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["trainers"].count).to eq(5)
      5.times do |i|
        expect(response_body["trainers"][i]["full_name"]).to eq("John #{14-i} Carmack")
      end

      get :index, params: {:format => :json, per_page: 5, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["trainers"].count).to eq(5)
      5.times do |i|
        expect(response_body["trainers"][i]["full_name"]).to eq("John #{9-i} Carmack")
      end

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["trainers"].count).to eq(10)

      10.times do |i|
        expect(response_body["trainers"][i]["full_name"]).to eq("John #{i} Carmack")
      end
    end
    it 'should export CSV successfully' do
      user = create :user, :admin
      sign_in user: user

      get :index, params: {:format => :csv}

      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to eq "text/csv"
      expect(response.body.empty?).not_to eq(true)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      get :index, params: {:format => :json, per_page: 5}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should successful show list with 0 and then 1 user' do
      user = create :user, :admin
      sign_in user: user

      deleted_user = create :user, :trainer
      delete :destroy, params: {id: deleted_user.id}

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(0)

      get :index, params: {:format => :json, :show_all => true}
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(1)

      end

  end

  describe '#show' do
    it 'should render unauthorized' do
      trainer = create :user, :trainer
      get :index, params: {id: trainer.id}
      expect(response.status).to be 401
    end

    it 'should successful show trainer' do
      user = create :user, :admin
      sign_in user: user
      trainer = create :user, :trainer

      get :show, params: {:format => :json, id: trainer.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["trainer"]["full_name"]).to eq(trainer.full_name)
      expect(response_body["trainer"]["id"]).to eq(trainer.id)

    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :show, params: {:format => :json, id: user.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end
  end

  describe '#update' do
    it 'should render unauthorized' do
      trainer = create :user, :trainer
      put :update, params: {id: trainer.id}
      expect(response.status).to be 401
    end

    it 'should successful update trainer' do
      user = create :user, :admin
      sign_in user: user
      trainer = create :user, :trainer, confirmed: false

      put :update, params: {:format => :json, id: trainer.id, trainer: {first_name: "Walter", confirmed: true}}
      trainer.reload
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Trainer has been successfully updated.")
      expect(trainer.first_name).to eq("Walter")
      expect(trainer.confirmed).to eq(true)
    end

    it 'should render validation error if password confirmation is wrong' do
      user = create :user, :admin
      sign_in user: user
      trainer = create :user, :trainer

      put :update, params: {:format => :json, id: trainer.id, trainer: {password: "newpassword", password_confirmation: "another"}}
      trainer.reload
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]["validation_errors"]["password_confirmation"]).to eq(["doesn't match Password"])
    end

    it 'should change password' do
      user = create :user, :admin
      sign_in user: user
      trainer = create :user, :trainer
      old_password = trainer.encrypted_password

      put :update, params: {:format => :json, id: trainer.id, trainer: {password: "newpassword", password_confirmation: "newpassword"}}
      trainer.reload
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Trainer has been successfully updated.")
      expect(old_password).not_to eq(trainer.encrypted_password)
    end

  end

  describe '#blocked clients' do
    it 'should render unauthorized' do
      get :blocked_clients, params: {:format => :json, page: 1, id:1}
      expect(response.status).to be 401
    end

    it 'should successful show blocked clients' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_client_reason, title: "Reason 1"
      trainer.blockings.create({:to_user_id => client.id, :comment => "Weak", block_client_reason_ids: [reason.id]})
      
      get :blocked_clients, params: {:format => :json, id: trainer.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["users"][0]["full_name"]).to eq(client.full_name)
      expect(response_body["users"][0]["block_reasons"]).to eq(reason.title)

    end

    it 'should successful show list blocked by clients' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_trainer_reason, title: "Reason 2"
      client.blockings.create({:to_user_id => trainer.id, :comment => "Weak", block_trainer_reason_ids: [reason.id]})

      get :blocked_by_clients, params: {:format => :json, id: trainer.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["users"][0]["full_name"]).to eq(client.full_name)
      expect(response_body["users"][0]["block_reasons"]).to eq(reason.title)

    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :blocked_clients, params: {:format => :json, id: user.id}
      expect(response.status).to be 401
      get :blocked_by_clients, params: {:format => :json, id: user.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should export blocked clients CSV successfully' do
      user = create :user, :admin
      sign_in user: user

      get :blocked_clients, params: {:format => :csv, id: create(:user, :trainer)}

      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to eq "text/csv"
      expect(response.body.empty?).not_to eq(true)
    end
    it 'should export blocked by clients CSV successfully' do
      user = create :user, :admin
      sign_in user: user

      get :blocked_by_clients, params: {:format => :csv, id: create(:user, :trainer)}

      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to eq "text/csv"
      expect(response.body.empty?).not_to eq(true)
    end
  end

  describe '#bookings' do
    it 'should show bookings list' do
      user = create :user, :admin
      sign_in user: user

      goal1 = create :goal
      goal2 = create :goal
      client1= create :user, :client, goals:[goal1, goal2]
      client2= create :user, :client, goals:[goal1, goal2]


      trainer = create :user, :trainer

      day = Date.tomorrow + 2.days

      booking_sessions_attributes = [
                                      {from_time: Date.tomorrow.beginning_of_day + 7.hour, to_time: Date.tomorrow.beginning_of_day + 8.hour},
                                      {from_time: Date.tomorrow.beginning_of_day + 1.day + 7.hour, to_time: Date.tomorrow.beginning_of_day + 1.day + 8.hour},
                                      {from_time: Date.tomorrow.beginning_of_day + 2.day + 7.hour, to_time: Date.tomorrow.beginning_of_day + 2.day + 8.hour}
                                  ]

      book1 = create :booking,trainer_id: trainer.id, client_id: client1.id
      book2 = create :booking,trainer_id: trainer.id, client_id: client2.id, booking_sessions_attributes: booking_sessions_attributes


      get :bookings,params: {:format => :json, id: trainer.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body['total'].to_i).to be(6)
    end

    it 'should show bookings sorted by id' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        admin = create :user, :admin

        sign_in user: admin

        client1 = create :user, :client
        client2 = create :user, :client
        trainer = create :user, :trainer

        day = Date.tomorrow + 2.days

        booking_sessions_attributes= [
            {from_time: Date.tomorrow.beginning_of_day + 7.hour, to_time: Date.tomorrow.beginning_of_day + 8.hour},
            {from_time: Date.tomorrow.beginning_of_day + 1.day + 7.hour, to_time: Date.tomorrow.beginning_of_day + 1.day + 8.hour}
        ]
        booking1 = create :booking, trainer_id: trainer.id, client_id: client1.id
        booking2 = create :booking, trainer_id: trainer.id, client_id: client2.id, booking_sessions_attributes: booking_sessions_attributes


        get :bookings, params: {id: trainer.id, sort_column: 'booking_id', format: 'json'}

        expect(response.status).to be 200

        response_body = JSON.parse(response.body)

        expect(response_body['total'].to_i).to be 5
      end
    end

    it 'should export booking sessions CSV successfully' do
      user = create :user, :admin
      sign_in user: user

      get :bookings, params: {:format => :csv, id: create(:user, :trainer)}

      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to eq "text/csv"
      expect(response.body.empty?).not_to eq(true)
    end
  end

  describe '#confirm' do
    it 'should render unauthorized' do
      put :confirm, params: {id: 2 }
      expect(response.status).to be 401
    end

    it 'should successful confirm' do
      user = create :user, :admin
      sign_in user: user

      trainer = create :user, :trainer

      put :confirm, params: {id: trainer.id }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Trainer has been approved.")
    end
    it 'should render error if tried to confirm client' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client

      put :confirm, params: {id: client.id }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Can't confirm!"])
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      trainer = create :user, :trainer

      put :confirm, params: {id: trainer.id }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

end
