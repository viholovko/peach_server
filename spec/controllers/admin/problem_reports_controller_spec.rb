require 'rails_helper'
RSpec.describe Admin::ProblemReportsController, type: :controller do

  render_views

  describe '#index' do

    it 'should render unauthorized' do
      get :index, params: { page: 1 }
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason
      reason2 = create :problem_report_reason
      15.times do |i|
        create :problem_report, problem_report_reason_id: [reason.id, reason2.id][i % 2], user_id: user.id
      end

      get :index, params: { format: :json }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["problem_reports"].count).to eq(10)

      get :index, params: { format: :json, per_page: 20 }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["problem_reports"].count).to eq(15)
      expect(response_body["problem_reports"][14]["comment"]).to eq("Comment 1")

      get :index, params: { format: :json, per_page: 10, page: 2 }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["problem_reports"].count).to eq(5)
      expect(response_body["problem_reports"][0]["comment"]).to eq("Comment 5")

      get :index, params: { format: :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc" }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["problem_reports"].count).to eq(10)
      expect(response_body["problem_reports"][0]["comment"]).to eq("Comment 1")

      get :index, params: { format: :json, per_page: 10, page: 1, sort_column: "name", sort_type: "asc" }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["problem_reports"].count).to eq(10)
      expect(response_body["problem_reports"][0]["first_name"]).to eq(user.first_name)

      get :index, params: { format: :json, user_id: user.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)

      get :index, params: { format: :json, problem_report_reason_id: reason.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(8)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: { format: :json, per_page: 20 }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason
      15.times do
        create :problem_report, problem_report_reason_id: reason.id, user_id: user.id
      end

      get :index, params: { format: :csv }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body)
      expect(response_body).to_not eq(nil)
    end

  end

  describe '#destroy' do

    it 'should render unauthorized' do
      delete :destroy, params: {id: 2}
      expect(response.status).to be 401
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user

      reason = create :problem_report_reason
      report = create :problem_report, problem_report_reason_id: reason.id, user_id: user.id

      delete :destroy, params: { id: report.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Problem report has been successfully removed.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      reason = create :problem_report_reason
      report = create :problem_report, problem_report_reason_id: reason.id, user_id: user.id

      delete :destroy, params: { id: report.id }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])

    end

  end

end
