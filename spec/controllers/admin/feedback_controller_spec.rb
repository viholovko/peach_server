require 'rails_helper'
RSpec.describe Admin::FeedbacksController, type: :controller do

  render_views

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      14.times do |i|
        create :feedback
      end
      create :feedback,stars:1
      user = create :user, :admin
      sign_in user: user

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["feedbacks"].count).to eq(10)

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["feedbacks"].count).to eq(15)

      get :index, params: {:format => :json, per_page: 10, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["feedbacks"].count).to eq(5)

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "stars", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["feedbacks"].count).to eq(10)
      expect(response_body["feedbacks"][0]["stars"]).to be 1
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

  it '#avg' do
    15.times do |i|
      create :feedback,stars:5
    end
    user = create :user, :admin
    sign_in user: user

    get :avg, params: {:format => :json}
    expect(response.status).to be 200
    response_body = JSON.parse(response.body)
    expect(response_body["average"]).to be_within(0.1).of(5.0)
  end
end
