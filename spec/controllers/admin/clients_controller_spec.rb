require 'rails_helper'
RSpec.describe Admin::ClientsController, type: :controller do

  render_views

  describe '#index' do


    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      15.times do |i|
        create :user, :client, first_name: "John #{i}"
      end
      user = create :user, :admin
      sign_in user: user

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["clients"].count).to eq(10)

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["clients"].count).to eq(15)
      expect(response_body["clients"][14]["full_name"]).to eq("John 0 Dou")

      get :index, params: {:format => :json, per_page: 10, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["clients"].count).to eq(5)
      expect(response_body["clients"][0]["full_name"]).to eq("John 4 Dou")

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["clients"].count).to eq(10)
      expect(response_body["clients"][0]["full_name"]).to eq("John 0 Dou")

      get :index, params: { format: :json, name: 'John Dou' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user
      15.times do |i|
        create :user, :client, first_name: "John #{i}"
      end

      get :index, params: { format: :csv }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body, headers: true)
      expect(response_body['id'].count).to eq(15)
    end

    it 'should successful show list with 0 and then 1 user' do
      user = create :user, :admin
      sign_in user: user

      deleted_user = create :user, :client
      delete :destroy, params: {id: deleted_user.id}

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(0)

      get :index, params: {:format => :json, :show_all => true}
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(1)

    end

  end

  describe '#show' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show client' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client

      get :show, params: {:format => :json, id: client.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["client"]["full_name"]).to eq(client.full_name)
      expect(response_body["client"]["id"]).to eq(client.id)

    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :show, params: {:format => :json, id: user.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end
  end

  describe '#update' do
    it 'should render unauthorized' do
      client = create :user, :client
      put :update, params: {id: client.id}
      expect(response.status).to be 401
    end

    it 'should successful update client' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client

      put :update, params: {:format => :json, id: client.id, client: {first_name: "Walter"}}
      client.reload
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Client has been successfully updated.")
      expect(client.first_name).to eq("Walter")
    end

    it 'should render validation error if password confirmation is wrong' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client

      put :update, params: {:format => :json, id: client.id, client: {password: "newpassword", password_confirmation: "another"}}
      client.reload
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]["validation_errors"]["password_confirmation"]).to eq(["doesn't match Password"])
    end

    it 'should change password' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client
      old_password = client.encrypted_password

      put :update, params: {:format => :json, id: client.id, client: {password: "newpassword", password_confirmation: "newpassword"}}
      client.reload
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Client has been successfully updated.")
      expect(old_password).not_to eq(client.encrypted_password)
    end

  end

  describe '#blocked_trainers' do
    it 'should render unauthorized' do
      get :blocked_trainers, params: {:format => :json, page: 1, id:1}
      expect(response.status).to be 401
    end

    it 'should successful show blocked trainers' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_trainer_reason, title: "Reason 1"
      client.blockings.create({ to_user_id: trainer.id, comment: 'Unprofessional', block_trainer_reason_ids: [reason.id]})

      get :blocked_trainers, params: { format: :json, id: client.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["users"][0]["full_name"]).to eq(trainer.full_name)
      expect(response_body["users"][0]["block_reasons"]).to eq(reason.title)
      expect(response_body["users"].count).to eq(1)
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :blocked_trainers, params: { format: :json, id: user.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_trainer_reason
      client.blockings.create({ to_user_id: trainer.id, comment: 'Unprofessional', block_trainer_reason_ids: [reason.id] })

      get :blocked_trainers, params: { format: :csv, id: client.id }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body, headers: true)
      expect(response_body['id'].count).to eq(1)
    end
  end

  describe '#blocked_by_trainers' do
    it 'should render unauthorized' do
      get :blocked_by_trainers, params: { format: :json, page: 1, id:1}
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      get :blocked_by_trainers, params: { format: :json, id: user.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should successful show list blocked by trainers' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_client_reason, title: "Reason 2"
      trainer.blockings.create({ to_user_id: client.id, comment: "Weak", block_client_reason_ids: [reason.id]})

      get :blocked_by_trainers, params: { format: :json, id: client.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["users"][0]["full_name"]).to eq(trainer.full_name)
      expect(response_body["users"][0]["block_reasons"]).to eq(reason.title)

    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user
      client = create :user, :client
      trainer = create :user, :trainer
      reason = create :block_client_reason
      trainer.blockings.create({ to_user_id: client.id, comment: "Weak", block_client_reason_ids: [reason.id]})

      get :blocked_by_trainers, params: { format: :csv, id: client.id }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body, headers: true)
      expect(response_body['Id'].count).to eq(1)
    end
  end

  describe '#favorite trainers' do

    it 'should successful show clients favorite trainers' do
      user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      create :user, :trainer
      trainer2 = create :user, :trainer
      trainer3 = create :user, :trainer
      client.as_client.update_attributes favorite_trainer_ids: [trainer2.id, trainer3.id]

      get :favorite_trainers, params: { format: :json, id: client.id, sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["users"].map{|i| i["id"]}.join(" ")).to include(trainer3.id.to_s, trainer2.id.to_s)
      expect(response_body["total"]).to eq(2)
      expect(response_body['users'].first['id']).to eq(trainer2.id)

      get :favorite_trainers, params: { format: :json, id: client.id, name: trainer3.first_name }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(1)
      expect(response_body['users'].first['id']).to eq(trainer3.id)
    end

    it 'should load CSV if user is admin' do
       user = create :user, :admin
      sign_in user: user

      client = create :user, :client
      create :user, :trainer
      trainer2 = create :user, :trainer
      trainer3 = create :user, :trainer
      client.as_client.update_attributes favorite_trainer_ids: [trainer2.id, trainer3.id]

      get :favorite_trainers, params: { format: :csv, id: client.id }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body, headers: true)
      expect(response_body['id'].count).to eq(2)
    end
  end

  describe '#booking_sessions' do
    it 'should successful show clients booking sessions' do
      user = create :user, :admin
      sign_in user: user

      client   = create :user, :client
      client2  = create :user, :client
      trainer  = create :user, :trainer
      trainer2 = create :user, :trainer
      booking  = create :booking, client_id: client.id, trainer_id: trainer.id
      booking2 = create :booking, client_id: client2.id, trainer_id: trainer2.id

      get :booking_sessions, params: { format: :json, id: client.id }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body['booking_sessions'].map{|i| i['trainer_id']}.uniq).to eq([trainer.id])
      expect(response_body['total']).to eq(3)
    end

    it 'should filter clients booking sessions' do
      user = create :user, :admin
      sign_in user: user

      client   = create :user, :client
      client2  = create :user, :client
      trainer  = create :user, :trainer
      trainer2 = create :user, :trainer
      booking  = create :booking, client_id: client.id, trainer_id: trainer.id
      booking2 = create :booking, client_id: client2.id, trainer_id: trainer2.id
      booking_session = create :booking_session, booking_id: booking.id, feeling: (create :feeling, title: "Feeling")

      get :booking_sessions, params: { format: :json, id: client.id, per_page: 10, page: 1, sort_column: 'name', sort_type: 'asc' }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body['booking_sessions'].first['trainer_id']).to eq(trainer.id)
      expect(response_body['total']).to eq(4)

      get :booking_sessions, params: { format: :json, id: client.id, per_page: 10, page: 1, sort_column: 'status', sort_type: 'asc' }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body['booking_sessions'].first['status']).to eq('pending')
      expect(response_body['total']).to eq(4)

      get :booking_sessions, params: { format: :json, id: client.id, per_page: 10, page: 1, sort_column: 'feeling', sort_type: 'asc' }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body['booking_sessions'].first['feeling']).to eq("Feeling")

      get :booking_sessions, params: { format: :json, id: client.id, per_page: 10, page: 1, sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body['booking_sessions'].last['id']).to eq(booking_session.id)
      expect(response_body['total']).to eq(4)
    end

    it 'should load CSV if user is admin' do
      user = create :user, :admin
      sign_in user: user

      client   = create :user, :client
      client2  = create :user, :client
      trainer  = create :user, :trainer
      trainer2 = create :user, :trainer
      booking  = create :booking, client_id: client.id, trainer_id: trainer.id
      booking2 = create :booking, client_id: client2.id, trainer_id: trainer2.id

      get :booking_sessions, params: { format: :csv, id: client.id }
      expect(response.status).to be 200
      expect(response.headers["Content-Type"]).to include "text/csv"
      response_body = CSV.parse(response.body, headers: true)
      expect(response_body['Id'].count).to eq(3)
    end

  end
end
