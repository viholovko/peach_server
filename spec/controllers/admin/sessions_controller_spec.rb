require 'rails_helper'
include SessionsHelper

RSpec.describe Admin::SessionsController, type: :controller do

  describe '#destroy' do
    it 'should log out user' do
      user = create :user, :admin
      session = sign_in user: user

      delete :destroy, params: {session_token: session.token}

      expect(response.status).to be 200
      expect(user.sessions.count).to eq(0)
    end

    it 'should return error if there is no user' do
      delete :destroy

      expect(response.status).to be 401
    end
  end

  describe '#create' do
    it 'should log in user' do
      user = create :user, :admin, password: 'secret', password_confirmation: 'secret'

      post :create, params: {email: user.email, password: 'secret'}

      response_body = JSON.parse response.body
      expect(response.status).to be 200
      expect(response_body["session_token"]).to_not be_nil
      expect(user.sessions.count).to eq(1)
    end

    it 'should return error if wrong password submitted' do
      user = create :user, :admin, password: 'secret', password_confirmation: 'secret'

      post :create, params: {email: user.email, password: 'wrong'}

      expect(response.status).to be 422
      response_body = JSON.parse response.body
      expect(response_body["errors"]).to eq(["Wrong email/password combination."])
      expect(user.sessions.count).to eq(0)
    end

    it 'should return error if user does not exist' do
      post :create, params: {email: 'user@example.com', password: 'secret'}

      expect(response.status).to be 422
      response_body = JSON.parse response.body
      expect(response_body["errors"]).to eq(["Wrong email/password combination."])
      expect(Session.count).to eq(0)
    end

    it 'should return error if user is not admin' do
      user = create :user, :trainer, password: 'secret', password_confirmation: 'secret'

      post :create, params: {email: user.email, password: 'wrong'}

      expect(response.status).to be 422
      response_body = JSON.parse response.body
      expect(response_body["errors"]).to eq(["Wrong email/password combination."])
      expect(user.sessions.count).to eq(0)
    end
  end

  describe '#check' do
    it 'should return current user' do
      user = create :user, :admin
      session = sign_in user: user

      get :check

      response_body = JSON.parse response.body
      expect(response_body['current_user']['email']).to eq(user.email)
      expect(response_body['current_user']['role']).to eq(user.role.name)
      expect(response.status).to be 200
      expect(user.sessions.count).to eq(1)
    end

    it 'should return error if there is no user' do
      get :check

      expect(response.status).to be 401
    end
  end
end