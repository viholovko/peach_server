require 'rails_helper'
RSpec.describe Admin::ProblemReportReasonsController, type: :controller do
  render_views

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: { format: :json }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: { format: :json }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user
      15.times { create :problem_report_reason }

      get :index, params: { format: :json }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['problem_report_reasons'].count).to eq(15)

      get :index, params: { format: :json, sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['problem_report_reasons'][4]['title']).to eq('Reason 5')
    end
  end

  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: { title: 'Other' }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: { problem_report_reason: { title: 'Other' } }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { problem_report_reason: { title: ' ' } }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should render successful message if title is present' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { problem_report_reason: { title: 'Other' } }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Problem report reason has been successfully added.')
      expect(ProblemReportReason.last.title).to eq('Other')
    end
  end

  describe '#show' do
    it 'should render unauthorized' do
      reason = create :problem_report_reason

      get :show, params: { id: reason.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      reason = create :problem_report_reason

      get :show, params: { format: :json, id: reason.id }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show feeling' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason

      get :show, params: { format: :json, id: reason.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['problem_report_reason']['title']).to eq(reason.title)
      expect(response_body['problem_report_reason']['id']).to eq(reason.id)
    end
  end

  describe '#destroy' do
    it 'should render unauthorized' do
      reason = create :problem_report_reason

      delete :destroy, params: { id: reason.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      reason = create :problem_report_reason

      delete :destroy, params: { id: reason.id }
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason

      delete :destroy, params: { id: reason.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Problem report reason has been successfully removed.')
    end
  end

  describe '#update' do
    it 'should render unauthorized' do
      reason = create :problem_report_reason

      put :update, params: {
        id: reason.id,
        problem_report_reason: { title: 'Other' }
      }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      reason = create :problem_report_reason

      put :update, params: {
        id: reason.id,
        problem_report_reason: { title: 'Other' }
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason

      put :update, params: {
        id: reason.id,
        problem_report_reason: { title: ' ' }
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should successful update feeling title' do
      user = create :user, :admin
      sign_in user: user
      reason = create :problem_report_reason

      put :update, params: {
        id: reason.id,
        problem_report_reason: { title: 'Other' }
      }
      reason.reload
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Problem report reason has been successfully updated.')
      expect(reason.title).to eq('Other')
    end
  end
end
