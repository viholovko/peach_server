require 'rails_helper'
include SessionsHelper

RSpec.describe Admin::BlockClientReasonsController, type: :controller do

  render_views

  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: {
          title: 'Reason 1'
      }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: {
          title: 'Reason1'
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: {
          block_client_reasons: {title: ' '}
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]).to eq("title" => ["can't be blank"])
    end
    it 'should render successful message if title is present' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: {
          block_client_reasons: {title: 'My reason'}
      }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Block client reason has been successfully added.")
      expect(BlockClientReason.last.title).to eq('My reason')
    end


  end

  describe '#destroy' do
    it 'should render unauthorized' do
      delete :destroy, params: {id: 2 }
      expect(response.status).to be 401
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user

      reason = create :block_client_reason

      delete :destroy, params: {id: reason.id}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Block client reason has been successfully removed.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      reason = create :block_client_reason, title: "My title"

      delete :destroy, params: {id: reason.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

  describe '#update' do
    it 'should render unauthorized' do
      reason = create :block_client_reason, title: "My title"

      put :update, params: {id: reason.id, block_client_reasons: { title: "New title" } }
      expect(response.status).to be 401
      expect(BlockClientReason.last.title).to eq("My title")
    end

    it 'should successful update' do
      user = create :user, :admin
      sign_in user: user

      reason = create :block_client_reason, title: "My title"

      put :update, params: {id: reason.id, block_client_reasons: { title: "New title" } }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(BlockClientReason.last.title).to eq("New title")
      expect(response_body["message"]).to eq("Block client reason has been successfully updated.")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      reason = create :block_client_reason, title: "My title"

      put :update, params: {id: reason.id, block_client_reasons: { title: "New title" } }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(BlockClientReason.last.title).to eq("My title")
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      reason = create :block_client_reason, title: "My title"


      put :update, params: {id: reason.id, block_client_reasons: { title: " " } }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]).to eq("title" => ["can't be blank"])
      expect(BlockClientReason.last.title).to eq("My title")
    end

  end

  describe '#show' do
    it 'should render unauthorized' do
      reason = create :block_client_reason

      get :show, params: {id: reason.id}
      expect(response.status).to be 401
    end

    it 'should successful show' do
      user = create :user, :admin
      sign_in user: user

      reason = create :block_client_reason, title: "My title"
      get :show, params: {id: reason.id, :format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["block_client_reason"]["title"]).to eq("My title")
      expect(response_body["block_client_reason"]["id"]).to eq(reason.id)
      expect(response_body["block_client_reason"]["created_at"]).to eq(reason.created_at.try(:strftime, "%d/%m/%Y"))
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      reason = create :block_client_reason, title: "My title"

      get :show, params: {id: reason.id, :format => :json}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end


  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: {page: 1}
      expect(response.status).to be 401
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user

      15.times do |i|
        create :block_client_reason, title: "My title #{i}"
      end

      get :index, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["block_client_reasons"].count).to eq(10)
      iterator = 14
      10.times do |i|
        expect(response_body["block_client_reasons"][i]["title"]).to eq("My title #{iterator}")
        iterator =  iterator - 1
      end

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["block_client_reasons"].count).to eq(15)
      expect(response_body["block_client_reasons"][14]["title"]).to eq("My title 0")

      get :index, params: {:format => :json, per_page: 10, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["block_client_reasons"].count).to eq(5)
      expect(response_body["block_client_reasons"][0]["title"]).to eq("My title 4")

      get :index, params: {:format => :json, per_page: 10, page: 1, sort_column: "id", sort_type: "asc"}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["total"]).to eq(15)
      expect(response_body["block_client_reasons"].count).to eq(10)
      expect(response_body["block_client_reasons"][0]["title"]).to eq("My title 0")
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: {:format => :json, per_page: 20}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

  end

end