require 'rails_helper'

RSpec.describe Admin::SystemSettingsController, type: :controller do

  render_views

  describe '#show' do
    it 'should render unauthorized' do
      get :show
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :show
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should show system fee equal to 0' do
      user = create :user, :admin
      sign_in user: user
      SystemSettings.instance
      expect(SystemSettings.all.count).to eq(1)
      get :show, params: {:format => :json}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["system_settings"]["fee"]).to eq(0)
      expect(SystemSettings.all.count).to eq(1)
      expect(SystemSettings.first_or_create.fee).to eq(0)
    end

  end

  describe '#update' do
    it 'should render unauthorized' do

      put :update, params: {system_settings: { fee: 13 } }
      expect(response.status).to be 401
      expect(SystemSettings.all.count).to eq(0)
    end

    it 'should successful update' do
      user = create :user, :admin
      sign_in user: user

      put :update, params: {system_settings: { fee: 13 }}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("System settings updated.")
      expect(SystemSettings.all.count).to eq(1)
      expect(SystemSettings.first_or_create.fee).to eq(13)

    end

    it 'should render error if user is not a admin' do
      user = create :user, :client
      sign_in user: user

      put :update, params: {system_settings: { fee: 13 }}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access denied !"])
    end

    it 'should render error if fee is less then 0 or higher then 100' do
      user = create :user, :admin
      sign_in user: user

      put :update, params: {system_settings: { fee: -5 }}

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]).to eq("fee" => ["must be greater than or equal to 0"])

      put :update, params: {system_settings: { fee: 101 }}

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["validation_errors"]).to eq("fee" => ["must be less than or equal to 100"])
    end
  end

end