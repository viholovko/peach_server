require 'rails_helper'
RSpec.describe Admin::TrainingPlacesController, type: :controller do
  render_views

  describe '#index' do
    it 'should render unauthorized' do
      get :index, params: { format: :json }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      get :index, params: { format: :json }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show list' do
      user = create :user, :admin
      sign_in user: user
      15.times do
        create :training_place
      end

      get :index, params: { format: :json }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['training_places'].count).to eq(15)

      get :index, params: { format: :json, sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['training_places'][4]['title']).to eq('Some place 5')
    end
  end

  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: { title: 'Gym' }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: { training_place: { title: 'Gym' } }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { training_place: { title: ' ' } }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should render successful message if title is present' do
      user = create :user, :admin
      sign_in user: user

      post :create, params: { training_place: { title: 'Gym' } }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Location place has been successfully added.')
      expect(TrainingPlace.last.title).to eq('Gym')
    end
  end

  describe '#show' do
    it 'should render unauthorized' do
      place = create :training_place

      get :show, params: { id: place.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      place = create :training_place

      get :show, params: { format: :json, id: place.id }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful show location place' do
      user = create :user, :admin
      sign_in user: user
      place = create :training_place

      get :show, params: { format: :json, id: place.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['training_place']['title']).to eq(place.title)
      expect(response_body['training_place']['id']).to eq(place.id)
    end
  end

  describe '#destroy' do
    it 'should render unauthorized' do
      place = create :training_place

      delete :destroy, params: { id: place.id }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      place = create :training_place

      delete :destroy, params: { id: place.id }
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should successful delete' do
      user = create :user, :admin
      sign_in user: user
      place = create :training_place

      delete :destroy, params: { id: place.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Location place has been successfully removed.')
    end
  end

  describe '#update' do
    it 'should render unauthorized' do
      place = create :training_place

      put :update, params: {
        id: place.id,
        training_place: { title: 'Stadium' }
      }
      expect(response.status).to be 401
    end

    it 'should render error if user is not a admin' do
      user = create :user, :trainer
      sign_in user: user
      place = create :training_place

      put :update, params: {
        id: place.id,
        training_place: { title: 'Stadium' }
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(['Access denied !'])
    end

    it 'should render error if title is not filled' do
      user = create :user, :admin
      sign_in user: user
      place = create :training_place

      put :update, params: {
        id: place.id,
        training_place: { title: ' ' }
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['validation_errors']).to eq('title' => ["can't be blank"])
    end

    it 'should successful update location place title' do
      user = create :user, :admin
      sign_in user: user
      place = create :training_place

      put :update, params: {
        id: place.id,
        training_place: { title: 'Stadium' }
      }
      place.reload
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['message']).to eq('Location place has been successfully updated.')
      expect(place.title).to eq('Stadium')
    end
  end
end
