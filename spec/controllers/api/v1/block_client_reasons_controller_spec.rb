require 'rails_helper'

RSpec.describe Api::V1::BlockClientReasonsController, type: :controller do
  render_views
  describe "#index" do

      it "return acces denied if user is not logged" do

        get :index
        expect(response.status).to be 401
        response_body = JSON.parse(response.body)
        expect(response_body["errors"]).to eq(["Access Denied !"])
      end

      it "return list if user is logged" do
        user = create :user, :trainer
        sign_in user: user

        reasons = []
        4.times { reasons << create(:block_client_reason) }

        get :index
        expect(response.status).to be 200
        response_body = JSON.parse(response.body)
        expect(response_body.count).to eq(4)

        response_body.each_with_index do |reason, index|
          expect(reason['title']).to eq(reasons[index].title)
        end
      end

  end
end
