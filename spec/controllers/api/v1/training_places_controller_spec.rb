require 'rails_helper'

RSpec.describe Api::V1::TrainingPlacesController, type: :controller do
  render_views

  describe '#index' do
    it "should return list of location places" do
      places = []
      4.times { places << create(:training_place) }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(4)

      response_body.each_with_index do |place, index|
        expect(place['title']).to eq(places[index].title)
      end
    end
  end
end
