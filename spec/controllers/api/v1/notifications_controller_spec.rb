require 'rails_helper'

RSpec.describe Api::V1::NotificationsController, type: :controller do
  render_views
  describe "#index" do
    it "return access denied if user is not logged" do
      get :index
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access Denied !"])
    end

    it "notification booking was created" do
      trainer = create :user, :trainer
      client = create :user, :client
      booking = create :booking, trainer_id: trainer.id, client_id: client.id

      sign_in user: client

      get :index
      expect(response.status).to be 200
    end

    it 'should correctly display approved bookings' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_type: :renewable,
                         status: :pending,
                         booking_sessions_attributes: [{
                                                           from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                           to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                       }]
        booking.approve(Time.now)

        get :index

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['total']).to eq(1)

        expect(response_body['notifications'][0][''])

        expect(response_body['notifications'][0]['booking_id']).to                       eq(booking.id)
        expect(response_body['notifications'][0]['notification_type']).to                eq("client_approved_renewable_booking")
        expect(response_body['notifications'][0]['title']).to                            eq("Your subscription was accepted!")
        expect(response_body['notifications'][0]['description']).to                      eq("Schedule is updated.")
        expect(response_body['notifications'][0]['from_user_id']).to                     eq(client.id)
        expect(response_body['notifications'][0]['avatar']).to_not                       be(nil)
        expect(response_body['notifications'][0]['first_name']).to                       eq(client.first_name)
        expect(response_body['notifications'][0]['last_name']).to                        eq(client.last_name)
        expect(response_body['notifications'][0]['sessions_count']).to                   eq(17)
        expect(response_body['notifications'][0]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['notifications'][0]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['notifications'][0]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['notifications'][0]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_i)
        expect(response_body['notifications'][0]['last_session_in_this_week_date']).to   eq(booking.booking_sessions.where("from_time < ?", Time.now.end_of_week).order(created_at: :desc).limit(1).first.from_time.to_f)
        expect(response_body['notifications'][0]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_i)
        expect(response_body['notifications'][0]['last_session_in_this_month_date']).to  eq(booking.booking_sessions.where("from_time < ?", Time.now.end_of_month).order(created_at: :desc).limit(1).first.from_time.to_f)
        expect(response_body['notifications'][0]['action_required']).to                  eq(false)
        expect(response_body['notifications'][0]['session_change_request_id']).to        eq(nil)
        expect(response_body['notifications'][0]['read']).to                             eq(false)
      end
    end
  end

  describe "#mark_as_read" do
    it "return access denied if user is not logged" do
      post :mark_as_read, params: { id: 5 }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access Denied !"])
    end

    it "should mark notification as read" do
      trainer = create :user, :trainer
      client = create :user, :client
      booking = create :booking, trainer_id: trainer.id, client_id: client.id

      sign_in user: client
      booking.approve(Time.now)

      notification = Notification.last
      post :mark_as_read, params: { id: notification.id }
      notification.reload
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Marked as read!")
      expect(notification.read).to eq(true)
    end

    it "should show error notification not found" do
      trainer = create :user, :trainer
      client = create :user, :client
      booking = create :booking, trainer_id: trainer.id, client_id: client.id

      sign_in user: client
      booking.approve(Time.now)

      notification = Notification.last
      post :mark_as_read, params: { id: notification.id + 1 }
      expect(response.status).to eq(404)
    end
  end

  describe "#unread_count" do
    it "return access denied if user is not logged" do
      get :unread_count
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access Denied !"])
    end

    it "should respond with number of unread notifications" do
      trainer = create :user, :trainer
      client = create :user, :client
      booking = create :booking, trainer_id: trainer.id, client_id: client.id

      sign_in user: client
      booking.approve(Time.now)

      get :unread_count
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body["unread_count"]).to eq(1)
    end
  end
end
