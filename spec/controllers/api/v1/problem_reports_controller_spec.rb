require 'rails_helper'

RSpec.describe Api::V1::ProblemReportsController, type: :controller do

  render_views

  describe "#create" do
    context "should create a new problem report" do
      it "with valid attributes" do
        user = create :user, :trainer
        sign_in user: user
        reason = create :problem_report_reason

        post :create, params: {
          problem_report_reason_id: reason.id,
          comment: "Lorem ispum"
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Problem report has been added.")
        expect(ProblemReport.last.comment).to eq("Lorem ispum")
      end

      it "with invalid attributes" do
        user = create :user, :trainer
        sign_in user: user
        reason = create :problem_report_reason

        post :create, params: {
          problem_report_reason_id: reason.id + 1,
          comment: "Lorem ispum"
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response_body["errors"]).to eq(["Problem report reason not found!"])
        expect(response.status).to be 422
      end

      it "should render unauthorized" do
        reason = create :problem_report_reason

        post :create, params: {
          problem_report_reason_id: reason.id,
          comment: "test"
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response_body["session_token"]).to be_nil
        expect(response_body["errors"]).to eq(["Access Denied !"])
        expect(response.status).to be 401
      end
    end
  end

end
