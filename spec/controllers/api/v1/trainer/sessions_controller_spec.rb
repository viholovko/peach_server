require 'rails_helper'
RSpec.describe Api::V1::Trainer::SessionsController, type: :controller do
  render_views
  describe '#index' do
    let(:response_body){JSON.parse(response.body)}

    it 'upcoming' do
      Timecop.travel Date.today.beginning_of_month do
        goal1 = create :goal
        speciality1 = create :speciality
        feeling = create :feeling

        client = create :user, :client, goals: [goal1], gender: :female, latitude: 20, longitude: 20
        trainer = create :user, :trainer, specialities: [speciality1], latitude: 20, longitude: 21
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_sessions_attributes: [
                             { from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours, to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours}
                         ]

        booking.approve Date.today.beginning_of_month
        booking.booking_sessions.first.update_attributes feeling_id: feeling.id, rate: 5


        sign_in user: trainer

        get :index, params: { select: 'upcoming' }

        expect(response.status).to be 200

        expect(response_body['total']).to be 1
        expect(response_body['booking_sessions'].length).to be 1

        expect(response_body['booking_sessions'][0]['id']).to_not be(nil)
        expect(response_body['booking_sessions'][0]['booking_id']).to be(booking.id)
        expect(response_body['booking_sessions'][0]['client_id']).to be(client.id)
        expect(response_body['booking_sessions'][0]['first_name']).to eq(client.first_name)
        expect(response_body['booking_sessions'][0]['last_name']).to eq(client.last_name)
        expect(response_body['booking_sessions'][0]['gender']).to eq('female')
        expect(response_body['booking_sessions'][0]['avatar']).to include('avatars/medium/factory_image.png')
        expect(response_body['booking_sessions'][0]['completed']).to eq(false)
        expect(response_body['booking_sessions'][0]['specialities'].length).to eq(1)

        expect(response_body['booking_sessions'][0]['specialities'][0]['id']).to eq(speciality1.id)
        expect(response_body['booking_sessions'][0]['specialities'][0]['title']).to eq(speciality1.title)

        expect(response_body['booking_sessions'][0]['rate']).to eq(5)
        expect(response_body['booking_sessions'][0]['feeling_id']).to eq(feeling.id)
        expect(response_body['booking_sessions'][0]['feeling_title']).to eq(feeling.title)
        expect(response_body['booking_sessions'][0]['latitude']).to be(booking.latitude)
        expect(response_body['booking_sessions'][0]['longitude']).to be(booking.longitude)
        expect(response_body['booking_sessions'][0]['location_radius']).to eq(client.location_radius)
        expect(response_body['booking_sessions'][0]['session_price']).to eq(123.45)
        expect(response_body['booking_sessions'][0]['distance'].to_i).to eq(64)
        expect(response_body['booking_sessions'][0]['address']).to eq(booking.address)
        expect(response_body['booking_sessions'][0]['from_time']).to eq(booking.booking_sessions.first.from_time.to_f)
        expect(response_body['booking_sessions'][0]['to_time']).to eq(booking.booking_sessions.first.to_time.to_f)
        expect(response_body['booking_sessions'][0]['booking_type']).to eq('once')
      end
    end

    it 'completed' do
      Timecop.travel Date.today.beginning_of_month do
        goal1 = create :goal
        speciality1 = create :speciality
        feeling = create :feeling

        client = create :user, :client, goals: [goal1], gender: :female, latitude: 20, longitude: 20
        trainer = create :user, :trainer, specialities: [speciality1], latitude: 20, longitude: 21
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_sessions_attributes: [
                             { from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours, to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours}
                         ]

        booking.approve Date.today.beginning_of_month
        booking.booking_sessions.first.update_attributes feeling_id: feeling.id, rate: 5

        Timecop.travel Time.now + 2.days do
          sign_in user: trainer

          get :index, params: { select: 'completed' }

          expect(response.status).to be 200

          expect(response_body['total']).to be 1
          expect(response_body['booking_sessions'].length).to be 1

          expect(response_body['booking_sessions'][0]['id']).to_not be(nil)
          expect(response_body['booking_sessions'][0]['booking_id']).to be(booking.id)
          expect(response_body['booking_sessions'][0]['client_id']).to be(client.id)
          expect(response_body['booking_sessions'][0]['first_name']).to eq(client.first_name)
          expect(response_body['booking_sessions'][0]['last_name']).to eq(client.last_name)
          expect(response_body['booking_sessions'][0]['gender']).to eq('female')
          expect(response_body['booking_sessions'][0]['avatar']).to include('avatars/medium/factory_image.png')
          expect(response_body['booking_sessions'][0]['completed']).to eq(true)
          expect(response_body['booking_sessions'][0]['specialities'].length).to eq(1)

          expect(response_body['booking_sessions'][0]['specialities'][0]['id']).to eq(speciality1.id)
          expect(response_body['booking_sessions'][0]['specialities'][0]['title']).to eq(speciality1.title)

          expect(response_body['booking_sessions'][0]['rate']).to eq(5)
          expect(response_body['booking_sessions'][0]['feeling_id']).to eq(feeling.id)
          expect(response_body['booking_sessions'][0]['feeling_title']).to eq(feeling.title)
          expect(response_body['booking_sessions'][0]['latitude']).to be(booking.latitude)
          expect(response_body['booking_sessions'][0]['longitude']).to be(booking.longitude)
          expect(response_body['booking_sessions'][0]['location_radius']).to eq(client.location_radius)
          expect(response_body['booking_sessions'][0]['session_price']).to eq(123.45)
          expect(response_body['booking_sessions'][0]['distance'].to_i).to eq(64)
          expect(response_body['booking_sessions'][0]['address']).to eq(booking.address)
          expect(response_body['booking_sessions'][0]['from_time']).to eq(booking.booking_sessions.first.from_time.to_f)
          expect(response_body['booking_sessions'][0]['to_time']).to eq(booking.booking_sessions.first.to_time.to_f)
          expect(response_body['booking_sessions'][0]['booking_type']).to eq('once')
        end
      end
    end
  end
end