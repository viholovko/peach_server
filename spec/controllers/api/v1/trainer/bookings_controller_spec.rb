require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::Trainer::BookingsController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }

  describe '#create booking' do
    it 'should render unauthorized if there is no session' do
      user = create :user, :trainer
      post :create, params: {}
      expect(response.status).to be 401
    end

    it 'should allow only trainers' do
      user = create :user, :client
      sign_in user: user
      post :create, params: {}
      expect(response.status).to be 422
    end

    it 'should create once booking' do
      trainer = create :user, :trainer
      sign_in user: trainer
      client = create :user, :client

      expect_any_instance_of(User).to receive(:notify).with(
          "Check out your training package!", {
            target_id: anything,
            from_user: trainer.id,
            title: 'Your training package is here!',
            description: 'Check out the new schedule.'
          }
      )

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i
              }
          ]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      # puts response.body

      booking = Booking.last

      expect(booking.trainer_id).to eq(trainer.id)
      expect(booking.client_id).to eq(client.id)
      expect(booking.once?).to eq(true)
      expect(booking.booking_sessions.count).to eq(1)

      expect(booking.booking_sessions[0].from_time).to eq(Date.tomorrow.beginning_of_day + 9.hours)
      expect(booking.booking_sessions[0].to_time).to eq(Date.tomorrow.beginning_of_day + 10.hours)

      expect(Notification.last.notification_type).to eq("trainer_created_once_booking")
    end

    it 'should create renewable booking' do
      trainer = create :user, :trainer
      sign_in user: trainer
      client = create :user, :client

      expect_any_instance_of(User).to receive(:notify).with(
        "Check out your training package!",
        {
          target_id: anything,
          from_user: trainer.id,
          title: 'Your training package is here!',
          description: 'Check out the new schedule.'
        }
      )

      post :create, params: {
          client_id: client.id,
          booking_type: "renewable",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).utc.to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours).utc.to_i
              },
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours + 2.days).utc.to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours + 2.days).utc.to_i
              }
          ]
      }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      # puts response.body

      booking = Booking.last

      expect(booking.trainer_id).to eq(trainer.id)
      expect(booking.client_id).to eq(client.id)
      expect(booking.renewable?).to eq(true)

      expect(Notification.last.notification_type).to eq("trainer_created_renewable_booking")

      expect(booking.booking_session_days.count).to eq(2)
      expect(booking.booking_session_days[0].from_time).to eq("09:00")
      expect(booking.booking_session_days[0].to_time).to eq("10:00")
      expect(booking.booking_session_days[0].day).to eq(Date.tomorrow.wday)

      expect(booking.booking_session_days[1].from_time).to eq("09:00")
      expect(booking.booking_session_days[1].to_time).to eq("10:00")
      expect(booking.booking_session_days[1].day).to eq((Date.tomorrow + 2.days).wday)

      from = Date.tomorrow.beginning_of_day.to_date
      to = (Date.today + 3.months).end_of_month
      i = 0
      booking.booking_session_days.each do |session_day|
        from_hour, from_minute = session_day[:from_time].split(':').map(&:to_i)
        to_hour, to_minute = session_day[:to_time].split(':').map(&:to_i)

        (from..to).to_a.select{|day| day.wday == session_day[:day]}.each do |day|
          expect(booking.booking_sessions[i].from_time).to eq(day.in_time_zone('UTC').change(hour: from_hour, minute: from_minute))
          expect(booking.booking_sessions[i].to_time).to eq( day.in_time_zone('UTC').change(hour: to_hour, minute: to_minute))
          i += 1
        end
      end

      expect(booking.booking_sessions.count).to eq(i)

      expect(booking.booking_sessions[0].from_time).to eq(Date.tomorrow.beginning_of_day + 9.hours)
      expect(booking.booking_sessions[0].to_time).to eq(Date.tomorrow.beginning_of_day + 10.hours)
    end

    it 'should create renewable booking (special case)' do
      Timecop.travel(Date.parse("2017-10-25 00:00:00").beginning_of_day) do

        trainer = create :user, :trainer
        sign_in user: trainer
        client = create :user, :client

        post :create, params: {
            client_id: client.id,
            booking_type: "renewable",
            session_price: 123.45,
            latitude: 55.55,
            longitude: 65.4321,
            address: 'test',
            training_place_id: 2,
            booking_sessions_attributes: [
                {
                    from_time: (DateTime.parse("2017-11-02 09:00:00")).utc.to_i,
                    to_time: (DateTime.parse("2017-11-02 10:00:00")).utc.to_i
                },
                {
                    from_time: (DateTime.parse("2017-11-03 09:00:00")).utc.to_i,
                    to_time: (DateTime.parse("2017-11-03 10:00:00")).utc.to_i
                }
            ]
        }

        response_body = JSON.parse(response.body)
        expect(response.status).to be 200

        booking = Booking.find(response_body['id'])

        expect(booking.booking_session_days.count).to eq(2)

        expect(booking.booking_session_days[0].from_time).to eq("09:00")
        expect(booking.booking_session_days[0].to_time).to eq("10:00")
        expect(booking.booking_session_days[0].day).to eq(4)

        expect(booking.booking_session_days[1].from_time).to eq("09:00")
        expect(booking.booking_session_days[1].to_time).to eq("10:00")
        expect(booking.booking_session_days[1].day).to eq(5)

        sessions = booking.booking_sessions.sort_by(&:from_time)

        expect(sessions[0].from_time).to eq(DateTime.parse("2017-11-02 09:00:00"))
        expect(sessions[1].from_time).to eq(DateTime.parse("2017-11-03 09:00:00"))
        expect(sessions[2].from_time).to eq(DateTime.parse("2017-11-09 09:00:00"))
        expect(sessions[3].from_time).to eq(DateTime.parse("2017-11-10 09:00:00"))
        expect(sessions[4].from_time).to eq(DateTime.parse("2017-11-16 09:00:00"))
        expect(sessions[5].from_time).to eq(DateTime.parse("2017-11-17 09:00:00"))
        expect(sessions[6].from_time).to eq(DateTime.parse("2017-11-23 09:00:00"))
        expect(sessions[7].from_time).to eq(DateTime.parse("2017-11-24 09:00:00"))
        expect(sessions[8].from_time).to eq(DateTime.parse("2017-11-30 09:00:00"))

        get :show, params: {id: booking.id}
        response_body = JSON.parse(response.body)

        expect(Time.at(response_body['last_session_date']).utc.to_datetime).to eq(DateTime.parse("2017-11-30 09:00:00"))
      end
    end

    it 'should create renewable booking with today boking' do
      Timecop.travel(DateTime.parse("2017-08-09 08:55:00 UTC")) do
        trainer = create :user, :trainer
        sign_in user: trainer
        client = create :user, :client

        post :create, params: {
            client_id: client.id,
            booking_type: "renewable",
            session_price: 123.45,
            address: 'test',
            training_place_id: 2,
            booking_sessions_attributes: [
                {
                    from_time: (Time.now.utc.beginning_of_day + 9.hours).to_i,
                    to_time: (Time.now.utc.beginning_of_day + 10.hours).to_i
                }
            ]
        }

        booking = Booking.last

        expect(booking.booking_sessions[0].from_time).to eq(DateTime.parse("2017-08-09 09:00:00 UTC"))
        expect(booking.booking_sessions[0].to_time).to eq(DateTime.parse("2017-08-09 10:00:00 UTC"))
      end
    end

    it 'should create free booking' do
      trainer = create :user, :trainer
      sign_in user: trainer
      client = create :user, :client

      expect_any_instance_of(User).to receive(:notify).with(
          "Check out your training package!",
          {
            target_id: anything,
            from_user: trainer.id,
            title: 'Your training package is here!',
            description: 'Check out the new schedule.'
          }
      )

      post :create, params: {
          client_id: client.id,
          booking_type: "free",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i
              }
          ]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      # puts response.body

      booking = Booking.last

      expect(booking.trainer_id).to eq(trainer.id)
      expect(booking.client_id).to eq(client.id)
      expect(booking.free?).to eq(true)
      expect(booking.booking_sessions.count).to eq(1)

      expect(booking.booking_sessions[0].from_time).to eq(Date.tomorrow.beginning_of_day + 9.hours)
      expect(booking.booking_sessions[0].to_time).to eq(Date.tomorrow.beginning_of_day + 10.hours)

      expect(Notification.last.notification_type).to eq("trainer_created_free_booking")
    end

    it 'should create consultation booking' do
      trainer = create :user, :trainer
      sign_in user: trainer
      client = create :user, :client

      expect_any_instance_of(User).to receive(:notify).with(
          "Check out your training package!",
          {
            target_id: anything,
            from_user: trainer.id,
            title: 'Your training package is here!',
            description: 'Check out the new schedule.'
          }
      )

      post :create, params: {
          client_id: client.id,
          booking_type: "consultation",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i
              }
          ]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200

      booking = Booking.last

      expect(booking.trainer_id).to eq(trainer.id)
      expect(booking.client_id).to eq(client.id)
      expect(booking.consultation?).to eq(true)
      expect(booking.booking_sessions.count).to eq(1)

      expect(booking.booking_sessions[0].from_time).to eq(Date.tomorrow.beginning_of_day + 9.hours)
      expect(booking.booking_sessions[0].to_time).to eq(Date.tomorrow.beginning_of_day + 10.hours)

      expect(Notification.last.notification_type).to eq("trainer_created_consultation_booking")
    end

    it 'should render error if sessions overlaps' do
      user = create :user, :trainer
      sign_in user: user
      client = create :user, :client

      day = Date.tomorrow

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [
            {from_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i, to_time: (Date.tomorrow + 11.hours).to_i},
            {from_time: (Date.tomorrow.beginning_of_day + 10.hours + 30.minutes).to_i, to_time: (Date.tomorrow + 11.hours + 30.minutes).to_i},
          ]
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Booking session should not overlap!"])
    end

    it 'should render validation error if there is another session booked' do
      trainer = create :user, :trainer
      client = create :user, :client
      credit_card = create :credit_card, user: client

      booking = create :booking,
             client_id: client.id,
             trainer_id: trainer.id,
             booking_sessions_attributes: [{
               from_time: (Date.tomorrow.beginning_of_day + 5.hours).utc,
               to_time: (Date.tomorrow.beginning_of_day + 6.hours).utc
             }]

      booking.approve(Time.now)

      sign_in user: trainer
      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [{
            from_time: (Date.tomorrow.beginning_of_day + 5.hours).utc.to_i,
            to_time: (Date.tomorrow.beginning_of_day + 6.hours).utc.to_i
          }]
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["trainer are busy!", "client are busy!"])
    end

    it 'should not create booking if client is busy' do
      trainer = create :user, :trainer
      trainer2 = create :user, :trainer
      client = create :user, :client
      card = create :credit_card, user: client, active: true

      sign_in user: trainer

      booking2 = create :booking,
                        client_id: client.id,
                        trainer_id: trainer2.id,
                        booking_sessions_attributes: [{
                                                          from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                          to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                      }],
                        booking_type: 'once',
                        status: 'approved'

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [{
                                            from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                                            to_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i
                                        }]
      }

      response_body = JSON.parse(response.body)
      # puts response.body
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["client are busy!"])
    end

    it 'should not create booking if trainer is busy' do
      trainer = create :user, :trainer
      client = create :user, :client
      client2 = create :user, :client
      card = create :credit_card, user: client, active: true

      sign_in user: trainer

      booking2 = create :booking,
                        client_id: client2.id,
                        trainer_id: trainer.id,
                        booking_sessions_attributes: [{
                                                          from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                          to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                      }],
                        booking_type: 'once',
                        status: 'approved'

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [{
                                            from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                                            to_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i
                                        }]
      }

      response_body = JSON.parse(response.body)
      # puts response.body
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["trainer are busy!"])
    end

    it 'should allow only min-max bookings per week' do
      trainer = create :user, :trainer
      client = create :user, :client

      sign_in user: trainer

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: []
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Booking sessions should be at least one"])

      SystemSettings.instance.update_attributes min_sessions: 2, max_sessions: 3

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [{
            from_time: (Date.tomorrow.beginning_of_day + 5.hours).to_i,
            to_time: (Date.tomorrow.beginning_of_day + 6.hours).to_i
          }]
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Booking sessions less than 2 per week is not allowed"])

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          booking_sessions_attributes: [
            { from_time: (Date.tomorrow.beginning_of_day + 5.hours).to_i, to_time: (Date.tomorrow.beginning_of_day + 6.hours).to_i },
            { from_time: (Date.tomorrow.beginning_of_day + 6.hours).to_i, to_time: (Date.tomorrow.beginning_of_day + 7.hours).to_i },
            { from_time: (Date.tomorrow.beginning_of_day + 7.hours).to_i, to_time: (Date.tomorrow.beginning_of_day + 8.hours).to_i },
            { from_time: (Date.tomorrow.beginning_of_day + 8.hours).to_i, to_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i },
          ]
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Booking sessions more than 3 per week is not allowed"])
    end

    it 'should show validation errors' do
      user = create :user, :trainer
      sign_in user: user

      post :create, params: {
          booking_type: "wrong"
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to include("Client can't be blank")
      expect(response_body["errors"]).to include("Booking type can't be blank")
      expect(response_body["errors"]).to include("'wrong' is not a valid booking type")

    end

    it 'should allow booking duration only 1 hour' do
      trainer = create :user, :trainer
      sign_in user: trainer
      client = create :user, :client

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 9.hours + 29.minutes).to_i
              }
          ]
      }

      expect(response.status).to be 422
      expect(response_body['errors']).to eq(["Booking sessions duration of training should be equal to 1 hour!"])

      post :create, params: {
          client_id: client.id,
          booking_type: "once",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i,
                  to_time: (Date.tomorrow.beginning_of_day + 15.hour + 1.minutes).to_i
              }
          ]
      }

      expect(response.status).to be 422
      expect(response_body['errors']).to eq(["Booking sessions duration of training should be equal to 1 hour!"])

      post :create, params: {
          client_id: client.id,
          booking_type: "renewable",
          session_price: 123.45,
          latitude: 55.55,
          longitude: 65.4321,
          address: 'test',
          training_place_id: 2,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours).to_i.to_s,
                  to_time: (Date.tomorrow.beginning_of_day + 9.hours + 1.hour).to_i.to_s
              }
          ]
      }

      expect(response.status).to be 200
    end
  end

  describe '#cancel booking' do
    it 'should render unauthorized if there is no session' do
      post :cancel, params: {
          id: 2,
          reject_reason_id: 235
      }
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access Denied !"])
    end

    it 'should cancel booking' do
      user = create :user, :trainer
      client = create :user, :client
      booking = create :booking, client_id: client.id, trainer_id: user.id, status: 'approved'
      sign_in user: user
      reason = create :reject_booking_reason
      post :cancel, params: {
          id: booking.id,
          reject_booking_reason_ids: [reason.id]
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Booking canceled!")
      booking.reload
      expect(booking.canceled_by_trainer?).to eq(true)
      expect(booking.reject_booking_reasons.map(&:id)).to eq([reason.id])
    end

    it 'should show error reject reason can not be blank ' do
      user = create :user, :trainer
      client = create :user, :client
      booking = create :booking, client_id: client.id, trainer_id: user.id
      sign_in user: user
      post :cancel, params: {
          id: booking.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Reject booking reasons should be at least one"])
    end

    it 'should show error booking not found ' do
      trainer = create :user, :trainer
      client = create :user, :client

      sign_in user: trainer
      reason = create :reject_booking_reason
      post :cancel, params: {
          id: 969,
          reject_booking_reason_id: reason.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
      expect(response_body["errors"]).to eq(["Record not found."])
    end

    it 'should show booking already canceled' do
      user = create :user, :trainer
      client = create :user, :client
      reason = create :reject_booking_reason

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: user.id,
                       status: :canceled_by_trainer,
                       reject_booking_reason_ids: [reason.id]

      sign_in user: user

      post :cancel, params: {
          id: booking.id,
          reject_booking_reason_id: reason.id
      }
      response_body = JSON.parse(response.body)
      # expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Booking already canceled!"])
    end
  end

  describe '#show' do
    it 'should allow only trainers' do
      user = create :user, :client
      sign_in user: user
      get :show, params: {id: 0}
      expect(response.status).to be 422
    end

    it 'should render unauthorized if there is no session' do
      get :show, params: {id: 34}
      expect(response.status).to be 401
    end

    it 'should render not found if there is no booking with given id' do
      trainer = create :user, :trainer
      sign_in user: trainer

      get :show, params: { id: 100 }
      expect(response.status).to be 404
    end

    it 'should display pending once booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        get :show, params: {id: pending_once_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(pending_once_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_once_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(123.45)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(1)
        expect(response_body['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('once')
      end
    end

    it 'should display approved once booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        approved_once_booking = create :booking,
                                client_id: client.id,
                                trainer_id: trainer.id,
                                booking_type: :once,
                                status: :pending,
                                booking_sessions_attributes: [{
                                                                  from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                  to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                              }]
        approved_once_booking.approve(Time.now)

        get :show, params: {id: approved_once_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(approved_once_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_once_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(123.45)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(1)
        expect(response_body['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('once')
      end
    end

    it 'should display pending renewable booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                         }]

        get :show, params: {id: pending_renewable_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(pending_renewable_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_renewable_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved renewable booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        get :show, params: {id: approved_renewable_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(approved_renewable_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_renewable_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display pending next month booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                          }]

        get :show, params: {id: pending_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(pending_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved next month booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer


        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                 from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                 to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        get :show, params: {id: approved_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should successfully display rejected booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer
        reject_reason = create(:reject_booking_reason)

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :rejected,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                           }],
                                             reject_booking_reasons: [reject_reason]

        get :show, params: {id: approved_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['client_id']).to                        eq(client.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(client.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(client.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([{"id"=>reject_reason.id, "title"=>"Some reason"}])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('rejected')
        expect(response_body['type']).to                             eq('renewable')
      end
    end
  end

  describe '#index' do
    it 'should allow only trainers' do
      user = create :user, :client
      sign_in user: user
      get :index
      expect(response.status).to be 422
    end

    it 'should render unauthorized if there is no session' do
      get :index
      expect(response.status).to be 401
    end

    it 'should display approved bookings' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                                                             from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours),
                                                                             to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours)
                                                                         }]

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours)
                                                                          }]

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 15.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        canceled_booking = create :booking,
                                  client_id: client.id,
                                  trainer_id: trainer.id,
                                  status: :canceled_by_client,
                                  reject_booking_reason_ids: [create(:reject_booking_reason).id]

        free_booking = create :booking,
                              client_id: client.id,
                              trainer_id: trainer.id,
                              status: :approved,
                              booking_type: :free,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 16.hours),
                                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 17.hours)
                                                            }]

        get :index, params: {status: :approved, sort_column: :created_at, sort_type: :asc}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['total']).to eq(4)

        expect(response_body['bookings'][0]['id']).to                               eq(approved_once_booking.id)
        expect(response_body['bookings'][0]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][0]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][0]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][0]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][0]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][0]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][0]['address']).to                          eq('test')
        expect(response_body['bookings'][0]['booking_type']).to                     eq('once')
        expect(response_body['bookings'][0]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][0]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['bookings'][0]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['bookings'][0]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)

        expect(response_body['bookings'][1]['id']).to                               eq(approved_renewable_booking.id)
        expect(response_body['bookings'][1]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][1]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][1]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][1]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][1]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][1]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][1]['address']).to                          eq('test')
        expect(response_body['bookings'][1]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][1]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][1]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][1]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][1]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_date']).to                eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)
        expect(response_body['bookings'][1]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)

        expect(response_body['bookings'][2]['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['bookings'][2]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][2]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][2]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][2]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][2]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][2]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][2]['address']).to                          eq('test')
        expect(response_body['bookings'][2]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][2]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][2]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][2]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][2]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours).to_f)
        expect(response_body['bookings'][2]['last_session_date']).to                eq(DateTime.parse("2017-09-30 14:00:00 UTC").to_f)
        expect(response_body['bookings'][2]['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['bookings'][2]['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['bookings'][2]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours).to_f)
        expect(response_body['bookings'][2]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 14:00:00 UTC").to_f)

        expect(response_body['bookings'][3]['id']).to                               eq(free_booking.id)
        expect(response_body['bookings'][3]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][3]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][3]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][3]['session_price']).to                    eq(0)
        expect(response_body['bookings'][3]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][3]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][3]['address']).to                          eq('test')
        expect(response_body['bookings'][3]['booking_type']).to                     eq('free')
        expect(response_body['bookings'][3]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][3]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][3]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][3]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
      end
    end

    it 'should display pending bookings' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: trainer

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                                                             from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours),
                                                                             to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours)
                                                                         }]

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours)
                                                                          }]

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 15.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        canceled_booking = create :booking,
                                  client_id: client.id,
                                  trainer_id: trainer.id,
                                  status: :canceled_by_client,
                                  reject_booking_reason_ids: [create(:reject_booking_reason).id]

        free_booking = create :booking,
                              client_id: client.id,
                              trainer_id: trainer.id,
                              status: :approved,
                              booking_type: :free,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 16.hours),
                                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 17.hours)
                                                            }]

        get :index, params: {status: :pending, sort_column: :created_at, sort_type: :asc}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['total']).to eq(3)

        expect(response_body['bookings'][0]['id']).to                               eq(pending_once_booking.id)
        expect(response_body['bookings'][0]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][0]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][0]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][0]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][0]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][0]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][0]['address']).to                          eq('test')
        expect(response_body['bookings'][0]['booking_type']).to                     eq('once')
        expect(response_body['bookings'][0]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][0]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['bookings'][0]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['bookings'][0]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)

        expect(response_body['bookings'][1]['id']).to                               eq(pending_renewable_booking.id)
        expect(response_body['bookings'][1]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][1]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][1]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][1]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][1]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][1]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][1]['address']).to                          eq('test')
        expect(response_body['bookings'][1]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][1]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][1]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][1]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][1]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['last_session_date']).to                eq(DateTime.parse("2017-08-30 11:00:00 UTC").to_f)
        expect(response_body['bookings'][1]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_i)
        expect(response_body['bookings'][1]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 11:00:00 UTC").to_f)

        expect(response_body['bookings'][2]['id']).to                               eq(pending_next_month_booking.id)
        expect(response_body['bookings'][2]['client_id']).to                        eq(client.id)
        expect(response_body['bookings'][2]['first_name']).to                       eq(client.first_name)
        expect(response_body['bookings'][2]['last_name']).to                        eq(client.last_name)
        expect(response_body['bookings'][2]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][2]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][2]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][2]['address']).to                          eq('test')
        expect(response_body['bookings'][2]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][2]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][2]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][2]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][2]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours).to_f)
        expect(response_body['bookings'][2]['last_session_date']).to                eq(DateTime.parse("2017-09-30 13:00:00 UTC").to_f)
        expect(response_body['bookings'][2]['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['bookings'][2]['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['bookings'][2]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours).to_i)
        expect(response_body['bookings'][2]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 13:00:00 UTC").to_f)
      end
    end
  end

  describe '#update' do
    it 'should create change request successfully' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ]

      sign_in user: trainer

      expect_any_instance_of(User).to receive(:notify).with(
          "Booking change requested.", anything
      )

      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i
              }
          ]
      }

      expect(response.status).to be 200
      change_request = booking.booking_change_requests.first

      expect(change_request.booking_sessions_attributes[0][:from_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 9.hours)
      expect(change_request.booking_sessions_attributes[0][:to_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 10.hours)
    end

    it 'should correctly validate number of days per week' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               },
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 10.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 11.hours)
                               },
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 11.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 12.hours)
                               },
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                               }
                           ]

      sign_in user: trainer

      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i
              },
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 11.hours).to_i
              },
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 11.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 12.hours).to_i
              },
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 13.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 14.hours).to_i
              }
          ]
      }

      expect(response.status).to be 200
    end

    it 'should create change request successfully even if there is past sessions' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ],
                       booking_type: 'renewable',
                       status: 'approved'

      Timecop.travel Time.now + 5.days do

        sign_in user: trainer

        put :update, params: {
            id: booking.id,
            booking_sessions_attributes: [
                {
                    from_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i,
                    to_time: (Time.now.utc.beginning_of_day + 1.day + 11.hours).to_i
                }
            ]
        }

        expect(response.status).to be 200
      end
    end

    it 'should allow to change only trainer\'s own booking' do
      trainer = create :user, :trainer
      trainer2 = create :user, :trainer
      client = create :user, :client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer2.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ]

      sign_in user: trainer
      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i
              }
          ]
      }

      expect(response.status).to be 404
      response_body = JSON.parse(response.body)
      expect(response_body['errors'][0]).to eq('Record not found.')
    end

    it 'should fail with validation errors' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                           to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                               }
                           ]

      sign_in user: trainer

      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours).to_i
              }
          ]
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"errors" => ["Booking sessions time can't be blank or past", "Booking sessions time do not fit with trainer working schedule"]})
    end

    it 'should not create change request if time is not included in trainer\'s working hours' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                           to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                               }
                           ]

      sign_in user: trainer

      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours).to_i
              }
          ]
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"errors" => ["Booking sessions time do not fit with trainer working schedule"]})
    end

    it 'should not create change request if there is another session at the same time' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Time.now.utc.beginning_of_day + 1.day),
                                                                           to_time: (Time.now.utc.end_of_day + 1.day)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Time.now.utc.beginning_of_day + 1.day + 12.hours),
                                                         to_time: (Time.now.utc.beginning_of_day + 1.day + 13.hours)
                                                     }],
                       status: 'approved'

      booking2 = create :booking,
                        client_id: client.id,
                        trainer_id: trainer.id,
                        booking_sessions_attributes: [{
                                                          from_time: (Time.now.utc.beginning_of_day + 1.day + 15.hours),
                                                          to_time: (Time.now.utc.beginning_of_day + 1.day + 16.hours)
                                                      }],
                        status: 'approved'

      sign_in user: trainer
      put :update, params: {
          id: booking.id,
          booking_sessions_attributes: [
              {
                  from_time: (Time.now.utc.beginning_of_day + 1.day + 15.hour).to_i,
                  to_time: (Time.now.utc.beginning_of_day + 1.day + 16.hour).to_i
              }
          ]
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)

      expect(response_body).to eq({"errors" => ["From time time do not fit with other trainer sessions!", "From time time do not fit with other client sessions!"]})
    end

    it 'should render unauthorized if there no session' do
      client = create :user, :client
      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 15.hour).to_i,
          to_time: (Date.tomorrow.beginning_of_day + 16.hour).to_i
      }

      expect(response.status).to be 401
    end
  end
end