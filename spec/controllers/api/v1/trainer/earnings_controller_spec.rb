require 'rails_helper'
RSpec.describe Api::V1::Trainer::EarningsController, type: :controller do
  render_views
  describe '#index'do
    let(:response_body){JSON.parse(response.body)}

    it 'completed' do
      Timecop.travel Date.today.beginning_of_month do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'approved',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours,
                                 to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours
                             }
                         ]

        booking.approve Date.today.beginning_of_month

        sign_in user: trainer
        get :index

        expect(response.status).to be 200
        expect(response_body['total']).to eq(1)
        expect(response_body['earnings'][0]['client_id']).to eq(client.id)
        expect("#{ ENV['HOST'] }#{ client.avatar.try(:url, :medium) }").to include(response_body['earnings'][0]['avatar'])
        expect(response_body['earnings'][0]['first_name']).to eq(client.first_name)
        expect(response_body['earnings'][0]['last_name']).to eq(client.last_name)
        expect(response_body['earnings'][0]['booking_id']).to eq(booking.id)
        expect(response_body['earnings'][0]['session_price']).to eq(123)
        expect(response_body['earnings'][0]['sessions_count']).to eq(1)
        expect(response_body['earnings'][0]['payment_method_last_four']).to eq('1111')
      end
    end

    it 'should display total earnings' do
      Timecop.travel Date.today.beginning_of_month do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'approved',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours,
                                 to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours
                             }
                         ],
                         session_price: 123

        booking2 = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'approved',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 2.day + 4.hours,
                                 to_time: Time.now.utc.beginning_of_day + 2.day + 5.hours
                             }
                         ],
                         session_price: 123

        booking.approve Date.today.beginning_of_month
        booking2.approve Date.today.beginning_of_month

        sign_in user: trainer
        get :index

        expect(response.status).to be 200
        expect(response_body['total_earnings']).to eq(
          booking.session_price * booking.booking_sessions.where(paid_up: true).count +
          booking2.session_price * booking2.booking_sessions.where(paid_up: true).count)
      end
    end
  end
end