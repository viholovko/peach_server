require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::Trainer::StepsController, type: :controller do
  render_views

  describe '#create' do
    it 'should render unauthorized if there is no session' do
      post :create, params: {
          registration_step: 1,
          first_name: 'John',
          last_name: 'Dou',
          gender: 'male'
      }

      expect(response.status).to be 401
    end

    it 'should render error if user is not a trainer' do
      user = create :user, :client
      sign_in user: user

      post :create, params: {
          registration_step: 1,
          first_name: 'John',
          last_name: 'Dou',
          gender: 'male'
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["You are not a trainer."])
    end

    it 'should render error if user has already completed his steps' do
      user = create :user, :trainer, registration_step: 3
      sign_in user: user

      post :create, params: {
          registration_step: 1,
          first_name: 'John',
          last_name: 'Dou',
          gender: 'male'
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["You are already completed your steps."])
    end

    it 'should render error if user try to move for more than one step forward' do
      user = create :user, :trainer,
                    registration_step: 1,
                    specialities: [create(:speciality)],
                    address: 'test',
                    post_code: 'test',
                    latitude: 0,
                    longitude: 0,
                    location_radius: 0
      sign_in user: user

      post :create, params: {
          registration_step: 3,
          first_name: 'John',
          last_name: 'Dou',
          gender: 'male'
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Registration step Step not allowed. Your allowed step is 2"])
    end

    it 'should successfully update user to the step 1' do
      user = create :user, :trainer, registration_step: 0, latitude: nil, longitude: nil, location_radius: nil
      sign_in user: user

      post :create, params: {
          registration_step: 1,
          first_name: 'John',
          last_name: 'Dou',
          gender: 'male'
      }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['user']['first_name']).to eq("John")
      expect(response_body['user']['last_name']).to eq("Dou")
      expect(response_body['user']['gender']).to eq("male")
      expect(response_body['user']['registration_step']).to eq(1)
    end

    it 'should fail with validations when try to move to the step 1' do
      user = create :user, :trainer, registration_step: 0, gender: nil
      sign_in user: user

      post :create, params: {
          registration_step: 1,
          first_name: 'John',
          last_name: 'Dou'
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(["Gender can't be blank", "Gender is not included in the list"])
    end

    it 'should successfully update user to the step 2' do
      user = create :user, :trainer, registration_step: 1, latitude: nil, longitude: nil, location_radius: nil
      speciality1 = create :speciality
      speciality2 = create :speciality

      sign_in user: user

      post :create, params: {
          registration_step: 2,
          speciality_ids: [speciality1.id, speciality2.id]
      }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['user']['specialities']).to eq([{"id"=>speciality1.id, "title"=>speciality1.title}, {"id"=>speciality2.id, "title"=>speciality2.title}])
      expect(response_body['user']['registration_step']).to eq(2)
    end

    it 'should fail with validations when try to move to the step 2' do
      user = create :user, :trainer, registration_step: 1
      sign_in user: user

      post :create, params: {
          registration_step: 2
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(["Specialities Should be at least one."])
    end

    it 'should successfully update user to the step 3' do
      user = create :user, :trainer, registration_step: 2, specialities: [create(:speciality)]

      sign_in user: user

      post :create, params: {
          registration_step: 3,
          address: 'test address',
          post_code: 'test post code',
          latitude: 22,
          longitude: 22,
          location_radius: 10
      }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['user']['registration_step']).to eq(3)
      expect(response_body['user']['address']).to eq('test address')
      expect(response_body['user']['post_code']).to eq('test post code')
      expect(response_body['user']['latitude']).to eq(22)
      expect(response_body['user']['longitude']).to eq(22)
      expect(response_body['user']['location_radius']).to eq(10)
    end

    it 'should fail with validations when try to move to the step 2' do
      user = create :user, :trainer, registration_step: 2, specialities: [create(:speciality)]

      sign_in user: user

      post :create, params: {
          registration_step: 3,
          address: 'test address',
          post_code: 'test post code'
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body['errors']).to eq(["Latitude can't be blank", "Longitude can't be blank"])
    end
  end
end
