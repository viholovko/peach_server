require 'rails_helper'

RSpec.describe Api::V1::Trainer::NoteController, type: :controller do
  render_views
  describe '#create' do
    it 'should render unauthorized' do
      trainer = create :user, :trainer
      client = create :user, :client
      post :create, params: {
          id: client.id,
          text: "This is a note"
      }
      expect(response.status).to be 401
    end
    it 'should render error if user is not a trainer' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: client
      post :create, params: {
          id: client.id,
          text: "This is a note"
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["You are not a trainer."])
    end
    it 'should create note successfully' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer
      post :create, params: {
          id: client.id,
          text: "This is a note"
      }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Trainer note has been successfully updated.")
    end
    it 'should update note successfully' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer
      post :create, params: {
          id: client.id,
          text: "Note 1"
      }
      post :create, params: {
          id: client.id,
          text: "Note 2"
      }
      expect(TrainerNote.count).to eq(1)
      get :index, params: {id: client.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["text"]).to eq("Note 2")
    end
    it 'should render error if text is empty' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer
      post :create, params: {
          id: client.id,
          text: ""
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Text can't be blank"])
    end
    it 'should render error if user not found' do
      trainer = create :user, :trainer
      client_id = User.last().id + 1 # not an existing id
      sign_in user: trainer
      note = "Test note"
      post :create, params: {
          id: client_id,
          text: note
      }
      expect(response.status).to be 404
      response_body = JSON.parse(response.body)
      expect(response_body["errors"][0]).to start_with("Record not found")
    end
  end
  describe "#index" do
    it 'should show note for user' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer
      note = "Test note"
      post :create, params: {
          id: client.id,
          text: note
      }
      get :index, params: {id: client.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["text"]).to eq(note)
    end
    it 'should show empty note for user' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer
      get :index, params: {id: client.id }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["text"]).to eq("")
    end
  end
end