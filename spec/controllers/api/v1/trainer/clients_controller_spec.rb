require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::Trainer::ClientsController, type: :controller do
  render_views

  describe '#index' do
    it 'should render unauthorized if there is no session' do
      create :user, :client
      create :user, :trainer
      get :index
      expect(response.status).to be 401
    end

    it 'should render client list' do
      user = create :user, :trainer

      sign_in user: user

      client1 = create :user, :client, goals: [create(:goal, specialities: [create(:speciality), create(:speciality)]), create(:goal, specialities: [create(:speciality), create(:speciality)])]
      client2 = create :user, :client, goals: [create(:goal, specialities: [create(:speciality), create(:speciality)])]

      now = DateTime.now

      booking_sessions_attributes = []
      booking_sessions_attributes.push({from_time: now + 1.month, to_time: now + 1.month + 1.hour})
      booking_sessions_attributes2 = []
      booking_sessions_attributes2.push({from_time: now + 2.month, to_time: now + 2.month + 1.hour})
      booking_sessions_attributes3 = []
      booking_sessions_attributes3.push({from_time: now + 20.days, to_time: now + 20.days + 1.hour})

      b1 = create :booking, client_id: client1.id, trainer_id: user.id, booking_sessions_attributes: booking_sessions_attributes
      b2 = create :booking, client_id: client1.id, trainer_id: user.id, booking_sessions_attributes: booking_sessions_attributes2
      b3 = create :booking, client_id: client2.id, trainer_id: user.id, booking_sessions_attributes: booking_sessions_attributes3

      b1.approved!
      b2.approved!
      b3.approved!

      get :index

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body['total']).to eq(2)
      expect(response_body["clients"].count).to eq(2)
      expect(response_body['clients'].map{|t| t['first_name']}.sort).to eq([client1, client2].map{|t| t.first_name}.sort)
      expect(response_body['clients'].map{|t| t['address']}).to eq([client1, client2].map(&:address))
      expect(response_body['clients'].map{|t| t['post_code']}).to eq([client1, client2].map(&:post_code))
    end

    it 'should render clients list with goals and specialities' do
      trainer = create :user, :trainer

      sign_in user: trainer

      specialitiy1 = create(:speciality)
      specialitiy2 = create(:speciality)
      specialitiy3 = create(:speciality)
      specialitiy4 = create(:speciality)

      goal1 = create(:goal, specialities: [specialitiy1, specialitiy2])
      goal2 = create(:goal, specialities: [specialitiy3])

      client = create :user, :client, goals: [goal1, goal2]


      b1 = create :booking,
                  client_id: client.id,
                  trainer_id: trainer.id,
                  status: 'approved',
                  booking_sessions_attributes: [{from_time: Time.now + 1.month, to_time: Time.now + 1.month + 1.hour}]

      get :index

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body['total']).to eq(1)
      expect(response_body['clients'][0]['specialities'].map{|i| i['id']}).to eq([specialitiy1.id, specialitiy2.id, specialitiy3.id ])
      expect(response_body['clients'][0]['goals'].map{|i| i['id']}).to eq([goal1.id, goal2.id])
    end
  end

  describe "#show" do
    it 'should render unauthorized if there is no session' do
      user = create :user, :trainer
      client = create :user, :client
      get :show, params: {id: client.id}
      expect(response.status).to be 401
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Access Denied !"])
    end
    it 'should show error if current user not a trainer' do
      client = create :user, :client
      user = create :user, :client
      sign_in user: user
      get :show, params: {id: client.id}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["You are not a trainer."])
    end
    it 'should show  error client not found' do
      client = create :user, :client
      user = create :user, :trainer
      sign_in user: user
      get :show, params: {id: Client.last.id + 1 || 999 }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
      expect(response_body["errors"][0]).to start_with("Record not found")
    end
    it 'should show client details' do
      client = create :user, :client, latitude: 49.248732, longitude: 23.861721
      user = create :user, :trainer, latitude: 49.347495, longitude: 23.508608
      sign_in user: user
      booking_sessions_attributes = []
      booking_sessions_attributes.push({from_time: (Date.tomorrow + 1.hour), to_time: (Date.tomorrow + 2.hours)})
      b = create :booking, client_id: client.id, trainer_id: user.id, booking_sessions_attributes: booking_sessions_attributes
      b.approved!
      get :show, params: {id: client.id}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["client"]["id"]).to eq(client.id)
      expect(response_body["client"]["address"]).to eq(client.address)
      expect(response_body["client"]["post_code"]).to eq(client.post_code)
      expect(response_body["client"]["booking_id"]).to eq(b.id)
      expect(response_body["client"]["next_session_date"]).to eq(booking_sessions_attributes[0][:from_time].to_f)
      expect(response_body["client"]["distance"]).to eq(17.32175680493484)
    end
  end

  describe '#sessions' do
    it 'route'do
      should route(:get,'api/v1/trainer/clients/1/sessions').to('api/v1/trainer/clients#sessions',id: 1)
    end
    it 'should unauthorized' do
      get :sessions,params:{
          id:1
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 401
      expect(response_body['errors']).to eq(["Access Denied !"])
    end
    context do
      before(:each)do
        user = create :user, :trainer
        sign_in user: user
      end
      let(:response_body){JSON.parse(response.body)}

      it 'should not found' do
        get :sessions,params:{
            id:0  #client_id
        }
        expect(response.status).to be 422
        expect(response_body['errors']).to eq(["Bookings not found"])
      end
      it 'list scheduled client sessions' do
        day = Date.tomorrow.beginning_of_day + 3.days
        booking_sessions_attributes = []
        booking_sessions_attributes.push({from_time: (day + 10.hours), to_time: (day + 11.hours)})
        booking_sessions_attributes.push({from_time: (day + 15.hours), to_time: (day + 16.hours)})

        client=create :user,:client
        trainer=create :user,:trainer
        booking1= create :booking,client_id:client.id,trainer_id:trainer.id
        booking2= create :booking,client_id:client.id,trainer_id:trainer.id, booking_sessions_attributes:booking_sessions_attributes

        get :sessions,params:{
            id:client.id
        }


        expect(response.status).to be 200
        expect(response.content_type).to eq('application/json')

        expect(response_body.length).to be 2
        expect(response_body[0]['booking_sessions'].length).to be 3
        expect(response_body[1]['booking_sessions'].length).to be 2

        expect(response_body[0]['booking']).to include_json(
                                                   id:booking1.id,
                                                   status: booking1.status,
                                                   booking_type: booking1.booking_type,
                                                   session_price: booking1.session_price,
                                                   latitude: booking1.latitude,
                                                   longitude: booking1.longitude,
                                                   client_id: booking1.client_id,
                                                   trainer_id: booking1.trainer_id
                                               )
        expect(response_body[1]['booking']).to include_json(
                                                   id:booking2.id,
                                                   status: booking2.status,
                                                   booking_type: booking2.booking_type,
                                                   session_price: booking2.session_price,
                                                   latitude: booking2.latitude,
                                                   longitude: booking2.longitude,
                                                   client_id: booking2.client_id,
                                                   trainer_id: booking2.trainer_id
                                               )


        expect(response_body[0]['booking_sessions']).to include_json([
                                                                         {
                                                            id: booking1.booking_sessions[0].id,
                                                                      },{
                                                            id: booking1.booking_sessions[1].id,
                                                                      }
                                                        ])
        expect(response_body[1]['booking_sessions']).to include_json([
                                                                         {
                                                            id: booking2.booking_sessions[0].id,
                                                                      },{
                                                            id: booking2.booking_sessions[1].id,
                                                                      }
                                                        ])
      end

    end
  end

end
