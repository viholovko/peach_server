require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::Trainer::BlockingsController, type: :controller do
  render_views

  describe '#create' do
    it 'should render unauthorized if there is no session' do
      client = create :user, :client

      post :create, params: {
          id: client.id,
          block_client_reason_ids: []
      }
      expect(response.status).to be 401
    end

    it 'should block trainer' do
      client = create :user, :client
      trainer = create :user, :trainer
      sign_in user: trainer

      reason = create :block_client_reason, title: "Reason 1"
      reason2 = create :block_client_reason, title: "Reason 2"
      reasons = [reason, reason2]
      post :create, params: {
          id: client.id,
          comment: "Comment",
          block_client_reason_ids: reasons.map(&:id)
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Client blocked!")
      blocking = Blocking.last
      expect(blocking.from_user_id).to eq(trainer.id)
      expect(blocking.to_user_id).to eq(client.id)
      expect(blocking.comment).to eq("Comment")
      reasons.each {|i|
        expect(blocking.block_client_reasons).to include(i)
      }

    end

    it 'should not block client twice' do
      client = create :user, :client
      trainer = create :user, :trainer
      sign_in user: trainer

      reason = create :block_client_reason, title: "Reason 1"
      reason2 = create :block_client_reason, title: "Reason 2"
      reasons = [reason.id, reason2.id]

      post :create, params: {
          id: client.id,
          comment: "Comment",
          block_client_reason_ids: reasons
      }
      expect(response.status).to be 200
      post :create, params: {
          id: client.id,
          comment: "Comment",
          block_client_reason_ids: reasons
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["From user already blocked!"])
    end

    it 'should not block if user.role is not trainer' do
      user = create :user, :trainer
      trainer = create :user, :trainer
      sign_in user: trainer

      reason = create :block_client_reason, title: "Reason 1"
      reason2 = create :block_client_reason, title: "Reason 2"
      reasons = [reason.id, reason2.id]

      post :create, params: {
          id: user.id,
          comment: "Comment",
          block_client_reason_ids: reasons
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq("Client not found!")
    end

    it 'should render error if reason is blank' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer

      create :block_client_reason, title: "Reason 1"
      post :create, params: {
          id: client.id,
          comment: "Comment",
          block_client_reason_ids: []
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(["Block client reasons should have at least one reason."])
    end

    it 'should block trainer if comment is blank' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: trainer

      reason = create :block_client_reason, title: "Reason 1"
      reason2 = create :block_client_reason, title: "Reason 2"
      reasons = [reason.id, reason2.id]
      post :create, params: {
          id: client.id,
          comment: "",
          block_client_reason_ids: reasons
      }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Client blocked!")
      blocking = Blocking.last
      expect(blocking.from_user_id).to eq(trainer.id)
      expect(blocking.to_user_id).to eq(client.id)
      expect(blocking.comment).to be_nil
    end
  end

end
