require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::SessionsController, type: :controller do
  render_views
  describe '#destroy' do
    it 'should log out user' do
      user = create :user, :trainer
      sign_in user: user

      delete :destroy

      expect(response.status).to be 200
      expect(user.sessions.count).to eq(0)
    end

    it 'should return error if there is no user' do
      delete :destroy

      expect(response.status).to be 401
    end
  end

  describe '#facebook' do
    it 'should log in client from facebook' do
      post :facebook, params: { access_token: 'test_access_token', user_type: 'client' }

      response_body = JSON.parse(response.body)
      expect(response_body["session_token"]).to_not be_nil
      expect(Session.count).to eq(1)
    end
  end

  describe '#facebook' do
    it 'should log in trainer from facebook' do
      post :facebook, params: { access_token: 'test_access_token', user_type: 'trainer' }

      response_body = JSON.parse(response.body)

      expect(response_body["session_token"]).to be_nil
      expect(Session.count).to eq(0)
    end
  end

  describe '#create' do
    it 'should log in user' do
      user = create :user, :trainer, password: 'secret', password_confirmation: 'secret'

      post :create, params: { email: user.email, password: 'secret', user_type: 'trainer'}

      response_body = JSON.parse response.body
      expect(response.status).to be 200
      expect(response_body["session_token"]).to_not be_nil
      expect(user.sessions.count).to eq(1)
    end

    it 'should return error if trainer is not submitted' do
      user = create :user, :trainer, password: 'secret', password_confirmation: 'secret'
      user.confirmed = false
      user.save

      post :create, params: { email: user.email, password: 'secret', user_type: 'trainer'}

      response_body = JSON.parse response.body
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Your account is not confirmed by administrator!"])
      expect(user.sessions.count).to eq(0)
    end

    it 'should return error if wrong password submitted' do
      user = create :user, :trainer, password: 'secret', password_confirmation: 'secret'

      post :create, params: { email: user.email, password: 'wrong', user_type: 'trainer' }

      expect(response.status).to be 422
      response_body = JSON.parse response.body
      expect(response_body["errors"]).to eq(["Wrong login/password combination."])
      expect(user.sessions.count).to eq(0)
    end

    it 'should return error if user does not exist' do
      post :create, params: {email: 'user@example.com', password: 'secret', user_type: 'trainer'}

      expect(response.status).to be 422
      response_body = JSON.parse response.body
      expect(response_body["errors"]).to eq(["Wrong login/password combination."])
      expect(Session.count).to eq(0)
    end
  end
end
