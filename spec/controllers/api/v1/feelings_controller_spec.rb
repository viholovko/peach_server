require 'rails_helper'

RSpec.describe Api::V1::FeelingsController, type: :controller do
  render_views

  describe '#index' do
    it "should return list of feelings" do
      feelings = []
      4.times { feelings << create(:feeling) }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(4)

      response_body.each_with_index do |feeling, index|
        expect(feeling['title']).to eq(feelings[index].title)
      end
    end
  end
end
