require 'rails_helper'

RSpec.describe Api::V1::ProblemReportReasonsController, type: :controller do
  render_views

  describe '#index' do
    it "should return list of problem report reasons" do
      reasons = []
      4.times { reasons << create(:problem_report_reason) }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(4)

      response_body.each_with_index do |reason, index|
        expect(reason['title']).to eq(reasons[index].title)
      end
    end
  end
end
