require 'rails_helper'

RSpec.describe Api::V1::ChangeRequestsController, type: :controller do
  render_views

  describe '#index' do
    it 'should list' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ]

      session = booking.booking_sessions.first

      change_request = create :session_change_request,
                              booking: booking,
                              booking_session: session,
                              from_time: Date.tomorrow.beginning_of_day + 12.hours,
                              to_time: Date.tomorrow.beginning_of_day + 13.hours,
                              from_user: trainer,
                              to_user: client

      sign_in user: client

      get :index

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({
                                      "change_requests" => [{
                                                                "id"                  => change_request.id,
                                                                "requested_from_time" => change_request.from_time.to_i,
                                                                "requested_to_time"   => change_request.to_time.to_i,
                                                                "current_from_time"   => session.from_time.to_i,
                                                                "current_to_time"     => session.to_time.to_i,
                                                                "trainer_id"          => trainer.id,
                                                                "client_id"           => client.id
                                                            }],
                                      "total" => 1
                                  })
    end

    it 'should render unauthorized if there no session' do
      get :index
      expect(response.status).to be 401
    end
  end

  describe '#create' do
    it 'should create change request successfully' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ]

      session = booking.booking_sessions.first

      sign_in user: client

      expect_any_instance_of(User).to receive(:notify).with(
          "Session change requested.", anything
      )

      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 12.hours).to_i,
          to_time: (Date.tomorrow.beginning_of_day + 13.hours).to_i
      }

      expect(response.status).to be 200
      change_request = session.session_change_requests.first

      expect(change_request.from_time).to eq(Date.tomorrow.beginning_of_day + 12.hours)
      expect(change_request.to_time).to eq(Date.tomorrow.beginning_of_day + 13.hours)
    end

    it 'should allow to change only clien\'s own session' do
      client = create :user, :client
      client2 = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client2.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               }
                           ]

      session = booking.booking_sessions.first

      sign_in user: client
      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 12.hours),
          to_time: (Date.tomorrow.beginning_of_day + 13.hours)
      }

      expect(response.status).to be 404
      response_body = JSON.parse(response.body)
      expect(response_body['errors'][0]).to eq('Record not found.')
    end

    it 'should fail with validation errors' do
      Timecop.travel Time.now.beginning_of_month + 15.days do

        client = create :user, :client
        trainer = create :user, :trainer, working_hours: [{
                                                              "day": Date.tomorrow.wday,
                                                              "ranges": [{
                                                                             from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                             to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                         }]
                                                          }]

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_sessions_attributes:
                             [
                                 {
                                     from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                     to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                 }
                             ]

        session = booking.booking_sessions.first

        sign_in user: client
        post :create, params: {
            booking_session_id: session.id
        }

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)
        expect(response_body).to eq({"errors" => ["From time can't be blank", "To time can't be blank"]})

        post :create, params: {
            booking_session_id: session.id,
            from_time: (6.days.ago.beginning_of_day + 13.hours).to_i,
            to_time: (6.days.ago.beginning_of_day + 12.hours).to_i
        }

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)
        expect(response_body).to eq({"errors" => ["Session can't be move to a date that has passed", "From time has to be lower than finish time"]})

        post :create, params: {
            booking_session_id: session.id,
            from_time: (Date.tomorrow.beginning_of_day + 12.hours).to_i,
            to_time: (Date.tomorrow.beginning_of_day + 12.hours + 15.minutes).to_i
        }

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)
        expect(response_body).to eq({"errors" => ["Duration of training should be equal to 1 hour!"]})
      end
    end

    it 'should not create change request if time is not included in trainer\'s working hours' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                           to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                               }
                           ]

      session = booking.booking_sessions.first

      sign_in user: client
      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 10.hours).to_i,
          to_time: (Date.tomorrow.beginning_of_day + 11.hours).to_i
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"errors" => ["Booking sessions time do not fit with trainer working schedule"]})
    end

    it 'should not create change request if there is another session at the same time' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day),
                                                                           to_time: (Date.tomorrow.end_of_day)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                     }]

      booking2 = create :booking,
                        client_id: client.id,
                        trainer_id: trainer.id,
                        booking_sessions_attributes: [{
                                                          from_time: (Date.tomorrow.beginning_of_day + 15.hours),
                                                          to_time: (Date.tomorrow.beginning_of_day + 16.hours)
                                                      }]

      session = booking.booking_sessions.first

      sign_in user: client
      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 15.hours).to_i,
          to_time: (Date.tomorrow.beginning_of_day + 16.hours).to_i
      }

      expect(response.status).to be 422
      response_body = JSON.parse(response.body)

      expect(response_body).to eq({"errors" => ["From time time do not fit with other trainer sessions!", "From time time do not fit with other client sessions!"]})
    end

    it 'should not allow to move sessions to next month' do
      Timecop.travel Time.now.beginning_of_month + 3.days do
        client = create :user, :client
        trainer = create :user, :trainer # by default user working hours is 1 - 23 - see factory

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_sessions_attributes: [{
                                                           from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                           to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                       }]

        session = booking.booking_sessions.first

        sign_in user: client
        post :create, params: {
            booking_session_id: session.id,
            from_time: (Date.tomorrow.beginning_of_day + 15.hours + 1.month).to_i,
            to_time: (Date.tomorrow.beginning_of_day + 16.hours + 1.month).to_i
        }

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)

        expect(response_body).to eq({"errors" => ["Subscription session can't be moved from one month to the next. Select a date within the current calendar month."]})
      end
    end

    it 'should render unauthorized if there no session' do
      client = create :user, :client
      post :create, params: {
          booking_session_id: session.id,
          from_time: (Date.tomorrow.beginning_of_day + 15.hours).to_i,
          to_time: (Date.tomorrow.beginning_of_day + 16.hours).to_i
      }

      expect(response.status).to be 401
    end
  end

  describe '#reject' do
    it 'should successfully reject request' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      session = booking.booking_sessions.first

      change_request = create :session_change_request,
                              booking: booking,
                              booking_session: session,
                              from_time: Date.tomorrow.beginning_of_day + 12.hours,
                              to_time: Date.tomorrow.beginning_of_day + 13.hours,
                              from_user: client,
                              to_user: trainer

      sign_in user: trainer

      expect_any_instance_of(User).to receive(:notify).with(
          "Session change rejected.", anything
      )

      post :reject, params: {id: change_request.id}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"message" => "Change request rejected."})
    end

    it 'should render unauthorized if there no session' do
      client = create :user, :client
      post :reject, params: {id: 1}
      expect(response.status).to be 401
    end

    it 'should render not found if there no change request with given id' do
      client = create :user, :client
      sign_in user: client
      post :reject, params: {id: 1}
      expect(response.status).to be 404
    end
  end

  describe '#accept' do
    it 'should successfully accept request' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      session = booking.booking_sessions.first

      change_request = create :session_change_request,
                              booking: booking,
                              booking_session: session,
                              from_time: Date.tomorrow.beginning_of_day + 12.hours,
                              to_time: Date.tomorrow.beginning_of_day + 13.hours,
                              from_user: client,
                              to_user: trainer

      sign_in user: trainer

      expect_any_instance_of(User).to receive(:notify).with(
          "Session change approved.", anything
      )

      post :accept, params: {id: change_request.id}

      # expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"message" => "Change request accepted."})
      session.reload

      expect(session.from_time).to eq(change_request.from_time)
      expect(session.to_time).to eq(change_request.to_time)
    end

    it 'should render unauthorized if there no session' do
      client = create :user, :client
      post :accept, params: {id: 1}
      expect(response.status).to be 401
    end

    it 'should render not found if there no change request with given id' do
      client = create :user, :client
      sign_in user: client
      post :accept, params: {id: 1}
      expect(response.status).to be 404
    end

    it 'should not create change request if time is not included in trainer\'s working hours' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day + 1.hour),
                                                                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 8.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 9.hours)
                               }
                           ]

      session = booking.booking_sessions.first

      change_request = create :session_change_request,
                              booking: booking,
                              booking_session: session,
                              from_time: Date.tomorrow.beginning_of_day + 2.hours,
                              to_time: Date.tomorrow.beginning_of_day + 3.hours,
                              from_user: client,
                              to_user: trainer

      trainer.update_attributes working_hours: [{
                                                    "day": (Date.tomorrow + 1.day).wday,
                                                    "ranges": [{
                                                                   from_time: (Date.tomorrow.beginning_of_day + 5.hours),
                                                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                               }]
                                                }]

      sign_in user: trainer

      post :accept, params: { id: change_request.id }

      # expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"errors" => ["From time time do not fit with trainer working schedule"]})
    end

    it 'should not create change request if there is another session at the same time' do
      client = create :user, :client
      trainer = create :user, :trainer, working_hours: [{
                                                            "day": Date.tomorrow.wday,
                                                            "ranges": [{
                                                                           from_time: (Date.tomorrow.beginning_of_day),
                                                                           to_time: (Date.tomorrow.end_of_day)
                                                                       }]
                                                        }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 5.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 6.hours)
                                                     }]

      session = booking.booking_sessions.first

      change_request = create :session_change_request,
                              booking: booking,
                              booking_session: session,
                              from_time: Date.tomorrow.beginning_of_day + 7.hours,
                              to_time: Date.tomorrow.beginning_of_day + 8.hours,
                              from_user: client,
                              to_user: trainer

      booking2 = create :booking,
                        client_id: client.id,
                        trainer_id: trainer.id,
                        booking_sessions_attributes: [{
                                                          from_time: (Date.tomorrow.beginning_of_day + 7.hours),
                                                          to_time: (Date.tomorrow.beginning_of_day + 8.hours)
                                                      }]

      sign_in user: trainer
      post :accept, params: { id: change_request.id }

      # expect(response.status).to be 422
      response_body = JSON.parse(response.body)

      expect(response_body).to eq({"errors" => ["From time time do not fit with other trainer sessions!", "From time time do not fit with other client sessions!"]})
    end
  end
end
