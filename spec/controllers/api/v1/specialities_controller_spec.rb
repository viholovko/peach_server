require 'rails_helper'

RSpec.describe Api::V1::SpecialitiesController, type: :controller do
  render_views

  describe "#index" do
    it "should return list of specialities" do
      specialities = []
      4.times { specialities << create(:speciality) }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(4)

      response_body.each_with_index do |speciality, index|
        expect(speciality['title']).to eq(specialities[index].title)
      end
    end

  end
end