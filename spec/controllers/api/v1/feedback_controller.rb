require 'rails_helper'

RSpec.describe Api::V1::FeedbacksController, type: :controller do
  render_views
  describe "#create" do
    context "authenticate_user"do
      before(:each){@user=create :user, :trainer;sign_in user:@user}
      it "valid attributes" do
        post :create, params:{
            stars:5
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Feedback successfully created!")
        expect(Feedback.last.stars).to be 5
        expect(Feedback.last.user_id).to be @user.id

        post :create, params:{
            stars:1,
            comment:"123456789123456"
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Feedback successfully created!")
        expect(Feedback.last.stars).to be 1
        expect(Feedback.last.comment).to eq("123456789123456")
        expect(Feedback.last.user_id).to be @user.id

      end
      it "invalid attributes" do
        post :create, params:{
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 422
        expect(response_body["errors"]).to eq("stars" => ["is not a number"])
      end
    end
    context "no authenticate user" do
      it "valid attributes" do
        post :create, params:{
            stars:5
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Feedback successfully created!")
        expect(Feedback.last.stars).to be 5

        post :create, params:{
            stars:1,
            comment:"123456789123456"
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Feedback successfully created!")
        expect(Feedback.last.stars).to be 1
        expect(Feedback.last.comment).to eq("123456789123456")
      end
      it "invalid attributes" do
        post :create, params:{
        }
        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 422
        expect(response_body["errors"]).to eq("stars" => ["is not a number"])
      end
    end
    it "should stars<=3 comment presence & minimum: 10" do
      post :create, params:{
          stars:1
      }
      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq("comment" => ["is too short (minimum is 10 characters)", "can't be blank"])

      post :create, params:{
          stars:2,
          comment:"123"
      }
      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq("comment" => ["is too short (minimum is 10 characters)"])
    end
  end
end
