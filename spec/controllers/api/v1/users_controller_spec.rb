require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views
  describe "#create" do
    context "should create a new" do
      it "trainer with valid attributes" do

        # expect_any_instance_of(User).to receive(:notify).with("Hello, John Dou! Your trainer application is pending approval", {})

        post :create, params: {
            first_name: 'John',
            last_name: 'Dou',
            email: 'user@example.com',
            password: 'secret',
            password_confirmation: 'secret',
            user_type: 'trainer',
            address: 'test address',
            post_code: 'test postcode'
        }

        response_body = JSON.parse(response.body).with_indifferent_access

        expect(response_body["session_token"]).to be_nil
        expect(response.status).to be 200
        expect(response_body[:message]).to eq("Registration successful. Wait for administrator confirm Your account!")
        user = User.last
        expect(user.first_name).to eq("John")
        expect(user.last_name).to eq("Dou")
        expect(user.email).to eq("user@example.com")
        expect(Session.count).to eq(0)
      end

      it "client with valid attributes" do

        # expect_any_instance_of(User).to receive(:notify).with("Hello, John Dou!", {})

        post :create, params: {
            first_name: 'John',
            last_name: 'Dou',
            email: 'user@example.com',
            password: 'secret',
            password_confirmation: 'secret',
            user_type: 'client',
            address: 'test address',
            post_code: 'test postcode'
        }

        response_body = JSON.parse(response.body).with_indifferent_access

        expect(response_body["session_token"]).to_not be_nil
        expect(response.status).to be 200
        expect(response_body[:user][:id]).to_not be_nil
        expect(response_body[:user][:email]).to eq("user@example.com")
        expect(response_body[:user][:last_name]).to eq("Dou")
        expect(response_body[:user][:first_name]).to eq("John")
        expect(Session.count).to eq(1)
      end

      it "client's registration step should became '3' immediately after registration" do

        post :create, params: { last_name:'petro',
                                first_name:'petrov',
                                email: 'client@example.com',
                                password: 'secret',
                                password_confirmation: 'secret',
                                user_type: 'client' }

        response_body = JSON.parse(response.body).with_indifferent_access

        expect(response_body["session_token"]).to_not be_nil
        expect(response.status).to be 200
        expect(response_body[:user][:id]).to_not be_nil
        expect(response_body[:user][:email]).to eq("client@example.com")
        expect(response_body[:user][:registration_step]).to eq(3)
        expect(Session.count).to eq(1)
      end

      it "with invalid attributes" do

        post :create, params: {
            email: 'userexample.com',
            password: '1234567',
            password_confirmation: '7654321',
            user_type: 'trainer'
        }

        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response_body["session_token"]).to be_nil
        expect(response_body["errors"]).to eq(["Email is incorrect", "Last name can't be blank", "First name can't be blank"])
        # "Password confirmation doesn't match Password"])
        expect(response.status).to be 422
        expect(Session.count).to eq(0)
      end
    end
  end

  describe "#update" do
    it 'should render unauthorized' do
      put :update, params: {client_gender: 'client_accept_male'}
      expect(response.status).to be 401
    end

    it "should update current user" do
      user = create :user, :trainer
      sign_in user: user
      create :speciality
      speciality1 = create :speciality, title: 'Fitness'
      speciality2 = create :speciality, title: 'Strength'
      place1 = create :training_place
      place2 = create :training_place

      put :update, params: {
          id: user.id,
          client_gender: "client_accept_male",
          training_place_ids: [place1, place2],
          speciality_ids: [speciality1.id, speciality2.id],
          working_hours: [{day: 1, ranges: [{from_time: (Time.now.utc.beginning_of_day + 2.hours).to_f, to_time: (Time.now.utc.beginning_of_day + 4.hours).to_f}]}]
      }

      user.reload
      response_body = JSON.parse(response.body)
      expect(response_body["user"]["specialities"][0]).to include("title" => "Fitness")
      expect(response_body["user"]["specialities"][1]).to include("title" =>  "Strength")
      expect(response_body["user"]["working_hours"][0]["day"]).to eq(0)
      expect(response_body["user"]["working_hours"][0]["ranges"]).to eq([])
      expect(response_body["user"]["working_hours"][1]["day"]).to eq(1)
      expect(response_body["user"]["working_hours"][1]["ranges"]).to eq([{"from_time"=>(Time.now.utc.beginning_of_day + 2.hours).to_f, "to_time"=>(Time.now.utc.beginning_of_day + 4.hours).to_f}])
      expect(response_body["user"]["working_hours"][2]["day"]).to eq(2)
      expect(response_body["user"]["working_hours"][2]["ranges"]).to eq([])
      expect(response_body["user"]["working_hours"][3]["day"]).to eq(3)
      expect(response_body["user"]["working_hours"][3]["ranges"]).to eq([])
      expect(response_body["user"]["working_hours"][4]["day"]).to eq(4)
      expect(response_body["user"]["working_hours"][4]["ranges"]).to eq([])
      expect(response_body["user"]["working_hours"][5]["day"]).to eq(5)
      expect(response_body["user"]["working_hours"][5]["ranges"]).to eq([])
      expect(response_body["user"]["working_hours"][6]["day"]).to eq(6)
      expect(response_body["user"]["working_hours"][6]["ranges"]).to eq([])
      expect(user.client_gender).to eq("client_accept_male")
      expect(user.training_place_ids).to eq([place1.id, place2.id])
    end

    it "should add new day to working hours" do
      Timecop.travel Date.parse("2017-08-09 00:00:00").beginning_of_day do
        client = create :user, :client
        trainer = create :user, :trainer, working_hours: [{
                                                              day: Date.tomorrow.wday,
                                                              ranges: [{
                                                                           from_time: Time.now.utc.beginning_of_day + 1.day + 2.hours,
                                                                           to_time:   Time.now.utc.beginning_of_day + 1.day + 3.hours
                                                                       }]
                                                          }]
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         booking_type: :once,
                         booking_sessions_attributes: [{
                                                           from_time: Time.now.utc.beginning_of_day + 1.day + 2.hours,
                                                           to_time:   Time.now.utc.beginning_of_day + 1.day + 3.hours
                                                       }]
        booking.approve(Time.now)

        sign_in user: trainer

        put :update, params: {
            working_hours: [{
                                day: Date.tomorrow.wday,
                                ranges: [{
                                             from_time: (Time.now.utc.beginning_of_day + 1.day + 2.hours).to_i,
                                             to_time:   (Time.now.utc.beginning_of_day + 1.day + 3.hours).to_i
                                         }]
                            },{
                                day: (Date.tomorrow + 1.day).wday,
                                ranges: [{
                                             from_time: (Time.now.utc.beginning_of_day + 2.day + 2.hours).to_i,
                                             to_time:   (Time.now.utc.beginning_of_day + 2.day + 3.hours).to_i
                                         }]
                            }]
        }

        response_body = JSON.parse(response.body)

        expect(response_body["user"]["working_hours"][0]["day"]).to eq(0)
        expect(response_body["user"]["working_hours"][0]["ranges"]).to eq([])
        expect(response_body["user"]["working_hours"][1]["day"]).to eq(1)
        expect(response_body["user"]["working_hours"][1]["ranges"]).to eq([])
        expect(response_body["user"]["working_hours"][2]["day"]).to eq(2)
        expect(response_body["user"]["working_hours"][2]["ranges"]).to eq([])
        expect(response_body["user"]["working_hours"][3]["day"]).to eq(3)
        expect(response_body["user"]["working_hours"][3]["ranges"]).to eq([])
        expect(response_body["user"]["working_hours"][4]["day"]).to eq(4)
        expect(response_body["user"]["working_hours"][4]["ranges"]).to eq([{"from_time"=>1502244000.0, "to_time"=>1502247600.0}])
        expect(response_body["user"]["working_hours"][5]["day"]).to eq(5)
        expect(response_body["user"]["working_hours"][5]["ranges"]).to eq([{"from_time"=>1502244000.0, "to_time"=>1502247600.0}])
        expect(response_body["user"]["working_hours"][6]["day"]).to eq(6)
        expect(response_body["user"]["working_hours"][6]["ranges"]).to eq([])
      end
    end

    it "should not allow to update trainer's working hours of there is another session scheduled for this time" do
      Timecop.travel Date.parse("2017-08-09 00:00:00").beginning_of_day do
        client = create :user, :client
        trainer = create :user, :trainer, working_hours: [{
                                                              day: Date.tomorrow.wday,
                                                              ranges: [{
                                                                           from_time: Time.now.utc.beginning_of_day + 1.day + 2.hours,
                                                                           to_time:   Time.now.utc.beginning_of_day + 1.day + 3.hours
                                                                       }]
                                                          }]
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         booking_type: :once,
                         booking_sessions_attributes: [{
                                                           from_time: Time.now.utc.beginning_of_day + 1.day + 2.hours,
                                                           to_time:   Time.now.utc.beginning_of_day + 1.day + 3.hours
                                                       }]
        booking.approve(Time.now)

        sign_in user: trainer

        put :update, params: {
            working_hours: [{
                                day: Date.tomorrow.wday,
                                ranges: [{
                                             from_time: (Time.now.utc.beginning_of_day + 5.hours).to_f,
                                             to_time: (Time.now.utc.beginning_of_day + 8.hours).to_f
                                         }]
                            }]
        }

        response_body = JSON.parse(response.body)
        expect(response_body["errors"]).to eq(["Working hours You have sessions for that time. To change working hours, please rearrange your sessions first."])
      end
    end

    it "should show invalid password error on password change" do

      user = create :user, :trainer
      sign_in user: user
      put :update, params: {id: user.id, password: 'Wrong password'}

      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response_body["errors"]).to eq(["Wrong current password."])
      expect(response.status).to be 422
    end

    it "with invalid update parameters" do
      user = create :user, :trainer, training_place:  "mobile"
      sign_in user: user
      put :update, params: {id: user.id, training_place: "wrong_value", client_gender: "Alien"}

      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["wrong_value is not a valid training place", "Alien is not a valid client gender"])
      expect(User.find(user.id).mobile?).to eq(true)
    end
  end

  describe "#profile" do
      it "should show a user" do
        user = create :user, :trainer, working_hours: [{
          "day": 1,
          "ranges": [
            {
                from_time: (Time.new.utc.beginning_of_day + 1.hour),
                to_time: (Time.new.utc.beginning_of_day + 23.hour)},
           ]
        }]

        sign_in user: user
        get :profile

        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response.status).to be 200
        expect(response_body[:user][:id]).to eq(user.id)
        expect(response_body[:user][:email]).to eq(user.email)
        expect(response_body[:user][:working_hours]).to eq([
          { "day" => 0, "ranges" => [] },
          { "day" => 1, "ranges" => [{
                                     "from_time" => (Time.new.utc.beginning_of_day + 1.hour).to_f,
                                     "to_time" => (Time.new.utc.beginning_of_day + 23.hour).to_f
                                 }]},
          { "day" => 2, "ranges" => []},
          { "day" => 3, "ranges" => [] },
          { "day" => 4, "ranges" => [] },
          { "day" => 5, "ranges" => [] },
          { "day" => 6, "ranges" => [] },
        ])

        user.update_attributes working_hours: [{
                                                   "day": 2,
                                                   "ranges": [{
                                                       from_time: (Time.new.utc.beginning_of_day + 1.hour),
                                                       to_time: (Time.new.utc.beginning_of_day + 23.hour)},
                                              ]}]

        get :profile

        response_body = JSON.parse(response.body).with_indifferent_access
        expect(response_body[:user][:working_hours]).to eq([
                                                               { "day" => 0, "ranges" => [] },
                                                               { "day" => 1, "ranges" => [] },
                                                               { "day" => 2, "ranges" => [{
                                                                                              "from_time" => (Time.new.utc.beginning_of_day + 1.hour).to_f,
                                                                                              "to_time" => (Time.new.utc.beginning_of_day + 23.hour).to_f
                                                                                          }]},
                                                               { "day" => 3, "ranges" => [] },
                                                               { "day" => 4, "ranges" => [] },
                                                               { "day" => 5, "ranges" => [] },
                                                               { "day" => 6, "ranges" => [] },
                                                           ])
      end
    end
  describe "#destroy" do
    it 'should render unauthorized' do
      user = create :user, :trainer
      delete :destroy, params: {id: user.id}
      expect(response.status).to be 401
    end

    it "should soft delete current user" do

      user = create :user, :trainer
      sign_in user: user

      delete :destroy, params: {deactivate_message: "Don't want no more trainings anymore"}
      user.reload
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("Account deactivated.")
      expect(user.deactivate_message).to eq("Don't want no more trainings anymore")
      expect(user.deleted_at).not_to be_nil
      expect(user.sessions.count).to eq(0)
    end

    it "should register user with same email of deleted user" do

      user = create :user, :trainer, email: "user@mail.com"
      sign_in user: user
      delete :destroy, params: {deactivate_message: "Don't want no more trainings anymore"}
      create :user, :trainer, email: "user@mail.com"
      user3 = User.new(email: "user@mail.com")
      user3.validate
      expect(User.with_deleted.where(email: "user@mail.com").count).to eq(2)
      expect(user3.errors.messages[:email]).to eq(["User with this email address is already exists."])
    end
  end

  describe "#qb_users" do
    it "should show users by qb_user_ids" do
      user = create :user, :trainer
      sign_in user: user

      client = create :user, :client, qb_user_id: 123456
      trainer = create :user, :trainer, qb_user_id: 654321
      get :qb_users, params: {qb_user_ids: [client.qb_user_id, trainer.qb_user_id]}

      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 200
      expect(response_body[:total]).to eq(2)
      expect(response_body[:users].map{|i| i[:qb_user_id]}).to include(client.qb_user_id, trainer.qb_user_id)
    end
  end

  describe "online_status" do
    it "should show user status online or offline" do
      user = create :user, :trainer, qb_user_id: 123456
      userOffline = create :user, :trainer

      sign_in user: user

      get :online_status, params: {qb_user_id: user.qb_user_id}

      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 200
      expect(response_body[:online]).to eq(true)

      get :online_status, params: {id: user.id}

      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 200
      expect(response_body[:online]).to eq(true)

      get :online_status, params: {id: userOffline.id}

      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 200
      expect(response_body[:online]).to eq(false)

    end

    it "should show error user not found" do
      user = create :user, :trainer
      sign_in user: user

      get :online_status, params: {qb_user_id: 9966444555112242}
      response_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to be 422
      expect(response_body[:errors]).to eq(["User not found!"])

    end
  end

end
