require 'rails_helper'

RSpec.describe Api::V1::GoalsController, type: :controller do
  render_views

  describe "#index" do
    it "should return list of goals" do
      goals = []
      4.times { goals << create(:goal) }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(4)

      response_body.each_with_index do |goal, index|
        expect(goal['title']).to eq(goals[index].title)
      end
    end

  end
end