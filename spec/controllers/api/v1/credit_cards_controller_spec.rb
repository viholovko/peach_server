require 'rails_helper'
include SessionsHelper
require 'stripe'

RSpec.describe Api::V1::CreditCardsController, type: :controller do
  render_views
  describe '#create' do
    it 'should render unauthorized' do
      post :create, params: {
        stripe_token: nil
      }
      expect(response.status).to be 401
    end
    it 'should return error' do
      user = create :user, :client
      sign_in user: user
      post :create, params: {
        stripe_token: 'iii' # bad token
      }
      expect(response.status).to be 422
      response_body = JSON.parse(response.body)
      expect(response_body["errors"]).to eq(['No such token: iii'])
    end
    it 'should create new credit cart' do
      user = create :user, :client
      sign_in user: user
      token = Stripe::Token.create(
          :card => {
              :number => "4242424242424242",
              :exp_month => 4,
              :exp_year => 2018,
              :cvc => "314"
          },
      )
      post :create, params: {
          stripe_token: token["id"]
      }
      expect(user.credit_cards.last.active).to eq(false)
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq('Credit card has been successfully added.')
    end
  end
  describe '#update' do
    it 'should render unauthorized' do
      put :update, params: {
          id: 0
      }
      expect(response.status).to be 401
    end
    it 'should activate credit card and deactivate old' do
      user = create :user, :client
      sign_in user: user
      token = Stripe::Token.create(
          :card => {
              :number => "4242424242424242",
              :exp_month => 4,
              :exp_year => 2018,
              :cvc => "314"
          },
      )
      # create first card
      post :create, params: {
          stripe_token: token["id"]
      }
      card1 = user.credit_cards.last
      expect(card1.active).to eq(false)
      put :update, params: {
          id: card1.id
      }
      expect(card1.reload.active).to eq(true)
      # create second card
      post :create, params: {
          stripe_token: token["id"]
      }
      card2 = user.credit_cards.last
      expect(card2.active).to eq(false)
      put :update, params: {
          id: card2.id
      }
      expect(card2.reload.active).to eq(true)
      expect(card1.reload.active).to eq(false) # first card must be deactivated

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq('Credit card has been successfully activated.')
    end
    describe 'should show error' do

    end
  end

  describe '#index' do
    it 'should render unauthorized' do
      get :index
      expect(response.status).to be 401
    end
    it "should show credit cart list for current user" do
      user = create :user, :client
      sign_in user: user
      token = Stripe::Token.create(
          :card => {
              :number => "4242424242424242",
              :exp_month => 4,
              :exp_year => 2018,
              :cvc => "314"
          },
      )
      post :create, params: {
          stripe_token: token["id"]
      }
      post :create, params: {
          stripe_token: token["id"]
      }

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body.count).to eq(2)
      response_body.each { |c|
        expect(c["exp_month"]).to eq("4")
        expect(c["exp_year"]).to eq("2018")
      }
    end
  end

  describe '#destroy' do
    it 'should render unauthorized' do
      delete :destroy, params: {
          id: 0
      }
      expect(response.status).to be 401
    end
    it "should remove credit card" do
      user = create :user, :client
      card = create :credit_card, user: user

      sign_in user: user

      delete :destroy, params: { id: card.id }

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq('Credit card has been successfully removed.')
    end
  end
end
