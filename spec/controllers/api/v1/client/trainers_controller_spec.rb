require 'rails_helper'

RSpec.describe Api::V1::Client::TrainersController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }

  describe '#index' do
    it 'should correctly filter trainers by goals and training places' do
      goal1 = create :goal
      goal2 = create :goal

      place1 = create :training_place
      place2 = create :training_place

      speciality1 = create :speciality, goals: [goal1, goal2]
      speciality2 = create :speciality, goals: [goal1]

      trainer1 = create :user, :trainer, specialities: [speciality1], training_places: [place1, place2]
      trainer2 = create :user, :trainer, specialities: [speciality2], training_places: [place1]

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1, trainer2].map{|t| t.first_name}.sort)
      expect(response_body['trainers'].map{|t| t['address']}).to eq([trainer1, trainer2].map(&:address))
      expect(response_body['trainers'].map{|t| t['post_code']}).to eq([trainer1, trainer2].map(&:post_code))

      get :index, params: { goal_ids: [goal1.id]}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1, trainer2].map{|t| t.first_name}.sort)

      get :index, params: { goal_ids: [goal2.id]}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(1)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1].map{|t| t.first_name}.sort)

      get :index, params: { goal_ids: [goal1.id, goal2.id]}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1, trainer2].map{|t| t.first_name}.sort)

      get :index, params: { training_place_ids: [place1.id] }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1, trainer2].map{|t| t.first_name}.sort)

      get :index, params: { training_place_ids: [place2.id] }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(1)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1].map{|t| t.first_name}.sort)

      get :index, params: { training_place_ids: [place1.id, place2.id], training_hours: [{day: 1, ranges: [from_time: Time.now.to_f, to_time: Time.now.to_f]}] }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].map{|t| t['first_name']}.sort).to eq([trainer1, trainer2].map{|t| t.first_name}.sort)
    end

    it 'should correctly sort trainers by gender places' do
      create :user, :trainer, gender: 'male'
      create :user, :trainer, gender: 'female'
      create :user, :trainer, gender: 'other'
      create :user, :trainer, gender: 'female'
      create :user, :trainer, gender: 'female'

      get :index, params: {sort_column: 'gender', sort_type: 'desc'}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(5)
      expect(response_body['trainers'][0]['gender']).to eq('other')
      expect(response_body['trainers'][1]['gender']).to eq('female')
      expect(response_body['trainers'][2]['gender']).to eq('female')
      expect(response_body['trainers'][3]['gender']).to eq('female')
      expect(response_body['trainers'][4]['gender']).to eq('male')
    end

    it 'should correctly filter trainers by gender' do
      create :user, :trainer, gender: 'male'
      create :user, :trainer, gender: 'female'
      create :user, :trainer, gender: 'other'
      create :user, :trainer, gender: 'female'
      create :user, :trainer, gender: 'female'

      get :index, params: {gender: 'other'}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)
      expect(response_body['trainers'][0]['gender']).to eq('other')

      get :index, params: {gender: 'male'}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)
      expect(response_body['trainers'][0]['gender']).to eq('male')

      get :index, params: {gender: 'female'}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(3)
      expect(response_body['trainers'][0]['gender']).to eq('female')
      expect(response_body['trainers'][1]['gender']).to eq('female')
      expect(response_body['trainers'][2]['gender']).to eq('female')
    end

    it 'should correctly filter trainers by working hours' do
      create :user, :trainer, working_hours: [
                                                {
                                                    "day": Date.tomorrow.wday,
                                                    "ranges": [
                                                        {from_time: (Date.tomorrow.beginning_of_day + 1.hour), to_time: (Date.tomorrow.beginning_of_day + 9.hour)},
                                                    ]
                                                },
                                                {
                                                    "day": (Date.tomorrow + 1.day).wday,
                                                    "ranges": [
                                                        {from_time: (Date.tomorrow.beginning_of_day + 1.day + 1.hour), to_time: (Date.tomorrow.beginning_of_day + 1.day + 9.hour)},
                                                        {from_time: (Date.tomorrow.beginning_of_day + 1.day + 10.hour), to_time: (Date.tomorrow.beginning_of_day + 1.day + 11.hour)},
                                                    ]
                                                }
                                             ]

      get :index, params: {training_hours: [{day: Date.tomorrow.wday, ranges: [
                             {from_time: (Date.tomorrow.beginning_of_day + 1.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 2.hour).to_i}
                                                                              ]}]}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)

      get :index, params: {training_hours: [{day: Date.tomorrow.wday, ranges: [
                             {from_time: (Date.tomorrow.beginning_of_day + 1.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 2.hour).to_i},
                             {from_time: (Date.tomorrow.beginning_of_day + 2.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 3.hour).to_i}
                                                                              ]}]}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)

      get :index, params: {training_hours: [
          {day: Date.tomorrow.wday,           ranges: [{from_time: (Date.tomorrow.beginning_of_day + 1.hour).to_i,         to_time: (Date.tomorrow.beginning_of_day + 2.hour).to_i}]},
          {day: (Date.tomorrow + 1.day).wday, ranges: [{from_time: (Date.tomorrow.beginning_of_day + 1.day + 2.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 1.day +  3.hour).to_i}]}
      ]}

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)

      get :index, params: {training_hours: [{day: (Date.tomorrow + 1.day).wday, ranges: [
                             {from_time: (Date.tomorrow.beginning_of_day + 1.day + 1.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 1.day + 2.hour).to_i},
                             {from_time: (Date.tomorrow.beginning_of_day + 1.day + 10.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 1.day + 10.hours + 30.minutes).to_i}
                                                                              ]}]}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)

      get :index, params: {training_hours: [{day: (Date.tomorrow + 2.days).wday, ranges: [
                             {from_time: (Date.tomorrow.beginning_of_day + 1.day + 1.hour).to_i, to_time: (Date.tomorrow.beginning_of_day + 1.day + 2.hour).to_i}
                                                                              ]}]}
      expect(response).to redirect_to api_v1_client_top_trainers_path
    end

    it 'should correctly filter trainers by availability' do
      trainer = create :user, :trainer
      client = create :user, :client

      booking = create :booking,
                client_id: client.id,
                trainer_id: trainer.id,
                booking_type: :renewable,
                status: :pending,
                booking_sessions_attributes: [{
                                                  from_time: (Time.now.utc.beginning_of_day + 1.day + 12.hours),
                                                  to_time: (Time.now.utc.beginning_of_day + 1.day + 13.hours)
                                              }]

      booking.approve(Time.now)

      get :index, params: {training_hours: [{day: (Time.now.utc.beginning_of_day + 1.day).wday, ranges: [
                             {from_time: (Time.now.utc.beginning_of_day + 1.day + 1.hour).to_i, to_time: (Time.now.utc.beginning_of_day + 1.day + 2.hour).to_i}
                                                                              ]}]}
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body['total']).to eq(1)

      get :index, params: {training_hours: [{day: (Time.now.utc.beginning_of_day + 1.day).wday, ranges: [
                             {from_time: (Time.now.utc.beginning_of_day + 1.day + 12.hour).to_i, to_time: (Time.now.utc.beginning_of_day + 1.day + 14.hour).to_i}
                                                                              ]}]}
      expect(response).to redirect_to api_v1_client_top_trainers_path
    end

    it 'should correctly filter trainers by schedule' do

    end

    it 'should correctly filter trainers by goals and training places' do
      get :index
      expect(response).to redirect_to(api_v1_client_top_trainers_path)
    end

    it 'should not show blocked trainers' do
      user = create :user, :client
      trainer1 = create :user, :trainer
      trainer2 = create :user, :trainer
      reason = create :block_trainer_reason
      user.blockings.create(to_user_id: trainer1.id, comment: "comment", block_trainer_reason_ids:[reason.id])
      sign_in user: user

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['trainers'][0]['first_name']).to eq(trainer2.first_name)
      expect(response_body['total']).to eq(1)
    end
  end

  describe "#show" do
    it 'should show  error trainer not found' do
      trainer = create :user, :trainer
      user = create :user, :client
      sign_in user: user

      get :show, params: {id: Trainer.last.id + 1 || 999 }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
      expect(response_body["errors"][0]).to start_with("Record not found.")
    end

    it 'should show trainer details' do
      trainer = create :user, :trainer, latitude: 49.248732, longitude: 23.861721
      user = create :user, :client, latitude: 49.347495, longitude: 23.508608
      sign_in user: user

      booking_sessions_attributes = []
      booking_sessions_attributes.push({from_time: (Date.tomorrow + 1.hour), to_time: (Date.tomorrow + 2.hours)})

      b = create :booking, client_id: user.id, trainer_id: trainer.id, booking_sessions_attributes: booking_sessions_attributes
      b.approved!

      get :show, params: {id: trainer.id}

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["trainer"]["id"]).to eq(trainer.id)
      expect(response_body["trainer"]["address"]).to eq(trainer.address)
      expect(response_body["trainer"]["post_code"]).to eq(trainer.post_code)
      expect(response_body["trainer"]["booking_id"]).to eq(b.id)
      expect(response_body["trainer"]["next_session_date"]).to eq(booking_sessions_attributes[0][:from_time].to_f)
      expect(response_body["trainer"]["distance"]).to eq(17.32175680493484)
    end
    it 'should show trainer details for unauthorized client' do
      trainer = create :user, :trainer, latitude: 49.248732, longitude: 23.861721

      get :show, params: {id: trainer.id}

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["trainer"]["id"]).to eq(trainer.id)
      expect(response_body["trainer"]["booking_id"]).to be_nil
      expect(response_body["trainer"]["next_session_date"]).to be_nil
      expect(response_body["trainer"]["next_session_time"]).to be_nil
      expect(response_body["trainer"]["distance"]).to be_nil
    end
  end
end

