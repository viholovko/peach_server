require 'rails_helper'
include SessionsHelper

RSpec.describe Api::V1::Client::BookingSessionsController, type: :controller do
  render_views

  describe '#rate booking session' do
    it 'should render unauthorized if there is no session' do
      client = create :user, :client
      trainer = create :user, :trainer
      feeling = create :feeling

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      post :rate, params: {
          id: booking.booking_sessions.first.id,
          rate: 3,
          feeling_id: feeling.id
      }
      expect(response.status).to be 401
    end

    it 'should rate' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user
      booking = create :booking,
                       client_id: user.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      feeling = create :feeling

      post :rate, params: {
          id: booking.booking_sessions.first.id,
          rate: 3,
          feeling_id: feeling.id
      }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["message"]).to eq("Booking session rated!")
    end

    it 'should not allow to rate session twice' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user
      booking = create :booking,
                       client_id: user.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      feeling = create :feeling

      post :rate, params: {
          id: booking.booking_sessions.first.id,
          rate: 3,
          feeling_id: feeling.id
      }

      post :rate, params: {
          id: booking.booking_sessions.first.id,
          rate: 3,
          feeling_id: feeling.id
      }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Already rated!"])
    end

    it 'should show error' do
      client = create :user, :client
      trainer = create :user, :trainer
      feeling = create :feeling

      sign_in user: client
      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }]

      post :rate, params: {
          id: booking.booking_sessions.first.id,
          rate: 7,
          feeling_id: feeling.id + 1
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Feeling can't be blank","Rate must be less than or equal to 5"])
    end

    it 'should show error booking not found' do
      client = create :user, :client
      sign_in user: client
      feeling = create :feeling

      post :rate, params: {
          id: 555,
          rate: 3,
          feeling_id: feeling.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
    end
  end

  describe '#index' do

    it 'upcoming'do
      Timecop.travel Date.today.beginning_of_month do
        client = create :user, :client, goals:[create(:goal)]
        trainer = create :user, :trainer
        create :credit_card, user: client
        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'approved',
                         booking_sessions_attributes: [
                             {from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours, to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours}
                         ]
        booking.approve Date.today.beginning_of_month

        sign_in user: client

        get :index, params: { select: 'upcoming' }
        response_body = JSON.parse(response.body)

        expect(response.status).to be 200

        expect(response_body['total']).to be 1
        expect(response_body['booking_sessions'].length).to be 1

        expect(response_body['booking_sessions'][0]['id']).to be booking.booking_sessions.first.id
        expect(response_body['booking_sessions'][0]['booking_id']).to be booking.id
        expect(response_body['booking_sessions'][0]['trainer_id']).to be trainer.id
      end
    end

    it 'completed' do
      Timecop.travel Date.today.beginning_of_month do
        client = create :user, :client, goals:[create(:goal)]
        trainer = create :user, :trainer
        create :credit_card, user: client
        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_sessions_attributes: [
                             {from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours, to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours}
                         ]
        booking.approve Date.today.beginning_of_month

        Timecop.travel(Time.now + 12.days) do
          sign_in user: client

          get :index, params: { select: 'completed' }
          response_body = JSON.parse(response.body)

          expect(response.status).to be 200

          expect(response_body['total']).to be 1
          expect(response_body['booking_sessions'].length).to be 1

          expect(response_body['booking_sessions'][0]['id']).to be booking.booking_sessions.first.id
          expect(response_body['booking_sessions'][0]['booking_id']).to be booking.id
          expect(response_body['booking_sessions'][0]['trainer_id']).to be trainer.id
        end
      end
    end
  end
end
