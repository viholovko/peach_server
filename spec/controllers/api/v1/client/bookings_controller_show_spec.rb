require 'rails_helper'
include SessionsHelper
require 'stripe'

require 'net/smtp'

RSpec.describe Api::V1::Client::BookingsController, type: :controller do
  render_views

  describe '#show' do
    it 'should render unauthorized if there is no session' do
      get :show, params: {id: 34}
      expect(response.status).to be 401
    end

    it 'should render not found if there is no booking with given id' do
      client = create :user, :client
      sign_in user: client

      get :show, params: { id: 100 }
      expect(response.status).to be 404
    end

    it 'should allow only clients' do
      user = create :user, :trainer
      sign_in user: user
      get :show, params: {id: 23}
      expect(response.status).to be 422
    end

    it 'should display pending once booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        get :show, params: {id: pending_once_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(pending_once_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_once_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(123.45)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(1)
        expect(response_body['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('once')
      end
    end

    it 'should display approved once booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        get :show, params: {id: approved_once_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(approved_once_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_once_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(123.45)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(1)
        expect(response_body['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('once')
      end
    end

    it 'should display approved once booking after 1 month' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client, latitude: 10, longitude: 10
        trainer = create :user, :trainer
        create :credit_card, user: client

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       latitude: 11,
                                       longitude: 10,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        date = Date.tomorrow.to_time.utc.beginning_of_day

        Timecop.travel Time.now + 1.month do
          sign_in user: client

          get :show, params: {id: approved_once_booking.id}

          expect(response.status).to be 200
          response_body = JSON.parse(response.body)

          expect(response_body['address']).to                          eq('test')
          expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
          expect(response_body['booking_session_days'].count).to       eq(1)
          expect(response_body['booking_session_days'][0]).to          eq({
                                                                              'day'=>3,
                                                                              'from_time'=>1504947600.0,
                                                                              'to_time'=>1504951200.0
                                                                          })
          expect(response_body['change_request_id']).to                eq(nil)
          expect(response_body['trainer_id']).to                       eq(trainer.id)
          expect(response_body['days_per_week']).to                    eq(1)
          expect(response_body['distance']).to                         eq(69.13249167149539)
          expect(response_body['first_name']).to                       eq(trainer.first_name)
          expect(response_body['first_session_date']).to               eq((date + 9.hours).to_f)
          expect(response_body['first_session_in_this_month_date']).to eq(1502269200.0)
          expect(response_body['first_session_in_this_week_date']).to  eq(nil)
          expect(response_body['id']).to                               eq(approved_once_booking.id)
          expect(response_body['last_name']).to                        eq(trainer.last_name)
          expect(response_body['last_session_date']).to                eq((date + 9.hours).to_f)
          expect(response_body['last_session_in_this_month_date']).to  eq((date + 9.hours).to_f)
          expect(response_body['last_session_in_this_week_date']).to   eq(nil)
          expect(response_body['latitude']).to                         eq(11.0)
          expect(response_body['longitude']).to                        eq(10)
          expect(response_body['next_session_date']).to                eq(0)
          expect(response_body['next_session_from_time']).to           eq(nil)
          expect(response_body['next_session_to_time']).to             eq(nil)
          expect(response_body['once_session_from_time']).to           eq((date + 9.hours).to_f)
          expect(response_body['once_session_id']).to                  eq(approved_once_booking.booking_sessions.first.id)
          expect(response_body['once_session_to_time']).to             eq((date + 10.hours).to_f)
          expect(response_body['online']).to                           eq(false)
          expect(response_body['price']).to                            eq(123.45)
          expect(response_body['reject_reasons']).to                   eq([])
          expect(response_body['requested_from_time']).to              eq(nil)
          expect(response_body['requested_to_time']).to                eq(nil)
          expect(response_body['sessions_count']).to                   eq(1)
          expect(response_body['sessions_in_this_month_count']).to     eq(1)
          expect(response_body['sessions_in_this_week_count']).to      eq(0)
          expect(response_body['status']).to                           eq('approved')
          expect(response_body['type']).to                             eq('once')
        end
      end
    end

    it 'should display pending renewable booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                                                             from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                             to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                         }]

        get :show, params: {id: pending_renewable_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(pending_renewable_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_renewable_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved renewable booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        get :show, params: {id: approved_renewable_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>3,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['id']).to                               eq(approved_renewable_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_renewable_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved renewable booking after 1 month' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        Timecop.travel Time.now + 1.month do
          ChargeClientsWorker.new.perform

          sign_in user: client
          get :show, params: {id: approved_renewable_booking.id}

          expect(response.status).to be 200
          response_body = JSON.parse(response.body)

          expect(response_body['address']).to                          eq('test')
          expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
          expect(response_body['booking_session_days'].count).to       eq(1)
          expect(response_body['booking_session_days'][0]).to          eq({
                                                                              'day'=>3,
                                                                              'from_time'=>1504947600.0,
                                                                              'to_time'=>1504951200.0
                                                                          })
          expect(response_body['change_request_id']).to                eq(nil)
          expect(response_body['trainer_id']).to                       eq(trainer.id)
          expect(response_body['days_per_week']).to                    eq(1)
          expect(response_body['distance']).to                         eq(nil)
          expect(response_body['first_name']).to                       eq(trainer.first_name)
          expect(response_body['first_session_date']).to               eq(1502269200.0)
          expect(response_body['first_session_in_this_month_date']).to eq(1502269200.0)
          expect(response_body['first_session_in_this_week_date']).to  eq(nil)
          expect(response_body['id']).to                               eq(approved_renewable_booking.id)
          expect(response_body['last_name']).to                        eq(trainer.last_name)
          expect(response_body['last_session_date']).to                eq(1504083600.0)
          expect(response_body['last_session_in_this_month_date']).to  eq(1504083600.0)
          expect(response_body['last_session_in_this_week_date']).to   eq(nil)
          expect(response_body['latitude']).to                         eq(55.55)
          expect(response_body['longitude']).to                        eq(65.4321)
          expect(response_body['next_session_date']).to                eq(0.0)
          expect(response_body['next_session_from_time']).to           eq(nil)
          expect(response_body['next_session_to_time']).to             eq(nil)
          expect(response_body['once_session_from_time']).to           eq(1502269200.0)
          expect(response_body['once_session_id']).to                  eq(approved_renewable_booking.booking_sessions.first.id)
          expect(response_body['once_session_to_time']).to             eq(1502272800.0)
          expect(response_body['online']).to                           eq(false)
          expect(response_body['price']).to                            eq(493.8)
          expect(response_body['reject_reasons']).to                   eq([])
          expect(response_body['requested_from_time']).to              eq(nil)
          expect(response_body['requested_to_time']).to                eq(nil)
          expect(response_body['sessions_count']).to                   eq(4)
          expect(response_body['sessions_in_this_month_count']).to     eq(4)
          expect(response_body['sessions_in_this_week_count']).to      eq(1)
          expect(response_body['status']).to                           eq('finished')
          expect(response_body['type']).to                             eq('renewable')
        end
      end
    end

    it 'should display pending next month booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                          }]

        get :show, params: {id: pending_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(pending_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(pending_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('pending')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved next month booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client


        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        get :show, params: {id: approved_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('approved')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should display approved next month booking after 1 month' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        Timecop.travel Time.now + 1.month do
          sign_in user: client

          get :show, params: {id: approved_next_month_booking.id}

          expect(response.status).to be 200
          response_body = JSON.parse(response.body)

          expect(response_body['address']).to                          eq('test')
          expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
          expect(response_body['booking_session_days'].count).to       eq(1)
          expect(response_body['booking_session_days'][0]).to          eq({
                                                                              'day'=>6,
                                                                              'from_time'=>1504947600.0,
                                                                              'to_time'=>1504951200.0
                                                                          })
          expect(response_body['change_request_id']).to                eq(nil)
          expect(response_body['trainer_id']).to                       eq(trainer.id)
          expect(response_body['days_per_week']).to                    eq(1)
          expect(response_body['distance']).to                         eq(nil)
          expect(response_body['first_name']).to                       eq(trainer.first_name)
          expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
          expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
          expect(response_body['first_session_in_this_week_date']).to  eq(1504947600.0)
          expect(response_body['id']).to                               eq(approved_next_month_booking.id)
          expect(response_body['last_name']).to                        eq(trainer.last_name)
          expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
          expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
          expect(response_body['last_session_in_this_week_date']).to   eq(1504947600.0)
          expect(response_body['latitude']).to                         eq(55.55)
          expect(response_body['longitude']).to                        eq(65.4321)
          expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day).to_f)
          expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
          expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
          expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
          expect(response_body['once_session_id']).to                  eq(approved_next_month_booking.booking_sessions.first.id)
          expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
          expect(response_body['online']).to                           eq(false)
          expect(response_body['session_price']).to                    eq(123.45)
          expect(response_body['price']).to                            eq(493.8)
          expect(response_body['reject_reasons']).to                   eq([])
          expect(response_body['requested_from_time']).to              eq(nil)
          expect(response_body['requested_to_time']).to                eq(nil)
          expect(response_body['sessions_count']).to                   eq(4)
          expect(response_body['sessions_in_this_month_count']).to     eq(4)
          expect(response_body['sessions_in_this_week_count']).to      eq(1)
          expect(response_body['status']).to                           eq('approved')
          expect(response_body['type']).to                             eq('renewable')
        end
      end
    end

    it 'should successfully display booking change request' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'approved'

      change_request = create :booking_change_request,
                              booking: booking,
                              from_user: trainer,
                              to_user: client,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                            }]

      sign_in user: client

      post :show, params: {id: booking.id, booking_change_request_id: change_request.id}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body['booking_session_days']).to eq([{
                                                               "day"=>Date.tomorrow.wday,
                                                               "from_time"=>(Date.tomorrow.beginning_of_day + 12.hours).to_f,
                                                               "to_time"=>(Date.tomorrow.beginning_of_day + 13.hours).to_f
                                                           }])
    end

    it 'should successfully display rejected booking' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client
        reject_reason = create(:reject_booking_reason)

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :rejected,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours)
                                                                           }],
                                             reject_booking_reasons: [reject_reason]

        get :show, params: {id: approved_next_month_booking.id}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['address']).to                          eq('test')
        expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
        expect(response_body['booking_session_days'].count).to       eq(1)
        expect(response_body['booking_session_days'][0]).to          eq({
                                                                            'day'=>6,
                                                                            'from_time'=>1502269200.0,
                                                                            'to_time'=>1502272800.0
                                                                        })
        expect(response_body['change_request_id']).to                eq(nil)
        expect(response_body['trainer_id']).to                       eq(trainer.id)
        expect(response_body['days_per_week']).to                    eq(1)
        expect(response_body['distance']).to                         eq(nil)
        expect(response_body['first_name']).to                       eq(trainer.first_name)
        expect(response_body['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['last_name']).to                        eq(trainer.last_name)
        expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 09:00:00 UTC").to_f)
        expect(response_body['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['latitude']).to                         eq(55.55)
        expect(response_body['longitude']).to                        eq(65.4321)
        expect(response_body['next_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month).to_f)
        expect(response_body['next_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['next_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['once_session_from_time']).to           eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 9.hours).to_f)
        expect(response_body['once_session_id']).to                  eq(approved_next_month_booking.booking_sessions.first.id)
        expect(response_body['once_session_to_time']).to             eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 10.hours).to_f)
        expect(response_body['online']).to                           eq(false)
        expect(response_body['session_price']).to                    eq(123.45)
        expect(response_body['price']).to                            eq(493.8)
        expect(response_body['reject_reasons']).to                   eq([{"id"=>reject_reason.id, "title"=>"Some reason"}])
        expect(response_body['requested_from_time']).to              eq(nil)
        expect(response_body['requested_to_time']).to                eq(nil)
        expect(response_body['sessions_count']).to                   eq(4)
        expect(response_body['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['status']).to                           eq('rejected')
        expect(response_body['type']).to                             eq('renewable')
      end
    end

    it 'should successfully display renewable booking after payment fails' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        credit_card = create :credit_card, user: client

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_type: :renewable,
                         status: :pending,
                         booking_sessions_attributes: [{
                                                           from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                           to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                          }]
        booking.approve(Time.now)

        Timecop.travel Time.now + 1.month do
          credit_card.destroy

          booking.reload
          expect(booking.notified_about_payment_problems).to eq(false)
          expect(booking.notified_about_payment_problems_date).to be(nil)

          ChargeClientsWorker.new.perform

          booking.reload
          expect(booking.notified_about_payment_problems).to eq(true)
          expect(booking.notified_about_payment_problems_date).to_not be(nil)
          expect(booking.notifications.client_charge_failed.count).to eq(1)
          expect(booking.notifications.client_charge_failed_after_two_days.count).to eq(0)

          Timecop.travel Time.now + 3.days do

            ChargeClientsWorker.new.perform

            booking.reload
            expect(booking.notified_about_payment_problems).to eq(true)
            expect(booking.notified_about_payment_problems_date).to_not be(nil)
            expect(booking.notifications.client_charge_failed.count).to eq(1)
            expect(booking.notifications.client_charge_failed_after_two_days.count).to eq(2)

            sign_in user: client
            get :show, params: {id: booking.id}

            expect(response.status).to be 200
            response_body = JSON.parse(response.body)

            expect(response_body['address']).to                          eq('test')
            expect(response_body['avatar']).to                           eq("#{ ENV['HOST'] }#{ trainer.avatar.try(:url, :medium) }")
            expect(response_body['booking_session_days'].count).to       eq(1)
            expect(response_body['booking_session_days'][0]).to          eq({
                                                                                'day'=>3,
                                                                                'from_time'=>1505206800.0,
                                                                                'to_time'=>1505210400.0
                                                                            })
            expect(response_body['change_request_id']).to                eq(nil)
            expect(response_body['trainer_id']).to                       eq(trainer.id)
            expect(response_body['days_per_week']).to                    eq(1)
            expect(response_body['distance']).to                         eq(nil)
            expect(response_body['first_name']).to                       eq(trainer.first_name)
            expect(response_body['first_session_date']).to               eq(1502269200.0)
            expect(response_body['first_session_in_this_month_date']).to eq(1502269200.0)
            expect(response_body['first_session_in_this_week_date']).to  eq(nil)
            expect(response_body['id']).to                               eq(booking.id)
            expect(response_body['last_name']).to                        eq(trainer.last_name)
            expect(response_body['last_session_date']).to                eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
            expect(response_body['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 09:00:00 UTC").to_f)
            expect(response_body['last_session_in_this_week_date']).to   eq(nil)
            expect(response_body['latitude']).to                         eq(55.55)
            expect(response_body['longitude']).to                        eq(65.4321)
            expect(response_body['next_session_date']).to                eq(0.0)
            expect(response_body['next_session_from_time']).to           eq(nil)
            expect(response_body['next_session_to_time']).to             eq(nil)
            expect(response_body['once_session_from_time']).to           eq(1502269200.0)
            expect(response_body['once_session_id']).to                  eq(booking.booking_sessions.first.id)
            expect(response_body['once_session_to_time']).to             eq(1502272800.0)
            expect(response_body['online']).to                           eq(false)
            expect(response_body['price']).to                            eq(493.8)
            expect(response_body['reject_reasons'].count).to             eq(0)
            expect(response_body['requested_from_time']).to              eq(nil)
            expect(response_body['requested_to_time']).to                eq(nil)
            expect(response_body['sessions_count']).to                   eq(4)
            expect(response_body['sessions_in_this_month_count']).to     eq(4)
            expect(response_body['sessions_in_this_week_count']).to      eq(1)
            expect(response_body['status']).to                           eq('finished')
            expect(response_body['type']).to                             eq('renewable')
          end
        end
      end
    end

    describe 'should successfully display approved booking change request' do
      it 'before apply' do
        Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
          client = create :user, :client
          trainer = create :user, :trainer

          booking = create :booking,
                           client_id: client.id,
                           trainer_id: trainer.id,
                           booking_sessions_attributes: [{
                                                             from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                             to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                         }],
                           booking_type: 'renewable',
                           status: 'approved'

          change_request = create :booking_change_request,
                                  booking: booking,
                                  from_user: trainer,
                                  to_user: client,
                                  booking_sessions_attributes: [{
                                                                    from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                    to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                },
                                                                {
                                                                    from_time: (Date.tomorrow.beginning_of_day + 14.hours),
                                                                    to_time: (Date.tomorrow.beginning_of_day + 15.hours)
                                                                }]

          change_request.accept

          sign_in user: client

          post :show, params: {id: booking.id}

          expect(response.status).to be 200
          response_body = JSON.parse(response.body)

          expect(response_body['booking_session_days'].count).to eq(1)
          expect(response_body['booking_session_days'][0]['day']).to eq(Date.tomorrow.wday)
          expect(Time.at(response_body['booking_session_days'][0]['from_time'].to_i).utc.hour).to eq(9)
          expect(Time.at(response_body['booking_session_days'][0]['from_time'].to_i).utc.min).to eq(0)
          expect(Time.at(response_body['booking_session_days'][0]['to_time'].to_i).utc.hour).to eq(10)
          expect(Time.at(response_body['booking_session_days'][0]['to_time'].to_i).utc.min).to eq(0)
        end
      end

      it 'after apply' do
        Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
          client = create :user, :client
          trainer = create :user, :trainer

          booking = create :booking,
                           client_id: client.id,
                           trainer_id: trainer.id,
                           booking_sessions_attributes: [{
                                                             from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                             to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                         }],
                           booking_type: 'renewable',
                           status: 'approved'

          change_request = create :booking_change_request,
                                  booking: booking,
                                  from_user: trainer,
                                  to_user: client,
                                  booking_sessions_attributes: [{
                                                                    from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                    to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                                },
                                                                {
                                                                    from_time: (Date.tomorrow.beginning_of_day + 14.hours),
                                                                    to_time: (Date.tomorrow.beginning_of_day + 15.hours)
                                                                }]

          change_request.accept


          Timecop.travel Time.now + 1.month do

            ApplyChangeRequestsWorker.new.perform

            sign_in user: client

            post :show, params: {id: booking.id, booking_change_request_id: change_request.id}

            expect(response.status).to be 200
            response_body = JSON.parse(response.body)

            expect(response_body['booking_session_days'].count).to eq(2)
            expect(Time.at(response_body['booking_session_days'][0]['from_time'].to_i).utc.hour).to eq(12)
            expect(Time.at(response_body['booking_session_days'][0]['to_time'].to_i).utc.hour).to eq(13)
            expect(Time.at(response_body['booking_session_days'][1]['from_time'].to_i).utc.hour).to eq(14)
            expect(Time.at(response_body['booking_session_days'][1]['to_time'].to_i).utc.hour).to eq(15)
          end
        end
      end
    end

  end
end