require 'rails_helper'
include SessionsHelper
require 'stripe'

require 'net/smtp'

RSpec.describe Api::V1::Client::BookingsController, type: :controller do
  render_views

  describe '#approve' do

    it 'should render unauthorized if there is no session' do
      post :approve, params: { id: 123 }
      expect(response.status).to be 401
    end

    it 'should approve once booking' do
      trainer = create :user, :trainer
      client = create :user, :client
      card = create :credit_card, user: client, active: true

      sign_in user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                       }],
                       booking_type: 'once'

      post :approve, params: { id: booking.id }

      response_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response_body["message"]).to eq("Booking approved!")
      booking.reload
      expect(booking.approved?).to eq(true)

      booking.booking_sessions.each do |session|
        expect(session.paid_up).to eq(true)
      end
    end

    it 'should approve renewable booking' do
      Timecop.travel Time.now.utc.beginning_of_month + 2.days do
        trainer = create :user, :trainer
        client = create :user, :client
        card = create :credit_card, user: client, active: true

        sign_in user: client

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_sessions_attributes: [{
                                                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                       }],
                         booking_type: 'renewable'

        post :approve, params: { id: booking.id }

        response_body = JSON.parse(response.body)


        expect(response.status).to eq(200)
        expect(response_body["message"]).to eq("Booking approved!")
        booking.reload

        expect(booking.approved?).to eq(true)

        booking.booking_sessions.each do |session|
          if session.from_time < Date.today.end_of_month
            expect(session.paid_up).to eq(true)
          else
            expect(session.paid_up).to eq(false)
          end
        end
      end
    end

    it 'should not approve booking if client is busy' do
      trainer = create :user, :trainer
      trainer2 = create :user, :trainer
      client = create :user, :client
      card = create :credit_card, user: client, active: true

      sign_in user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                       }],
                       booking_type: 'once'

      booking2 = create :booking,
                        client_id: client.id,
                        trainer_id: trainer2.id,
                        booking_sessions_attributes: [{
                            from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                            to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                        }],
                        booking_type: 'once'

      post :approve, params: { id: booking.id }
      post :approve, params: { id: booking2.id }

      response_body = JSON.parse(response.body)

      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["client are busy!"])
      booking.reload
      booking2.reload
      expect(booking.approved?).to eq(true)
      expect(booking2.approved?).to eq(false)
    end

    it 'should not allow to approve approve booking without client\'s active creadit card' do
      trainer = create :user, :trainer
      client = create :user, :client
      sign_in user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                       }]

      post :approve, params: { id: booking.id }

      response_body = JSON.parse(response.body)

      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Please add a credit card to your profile"])
    end

    it 'should show error booking not found' do
      client = create :user, :client
      sign_in user: client

      post :approve, params: { id: 100 }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body["errors"]).to eq(["Record not found."])
    end
  end

  describe '#reject booking' do
    it 'should render unauthorized if there is no session' do
      post :reject, params: {
        id: 32
      }
      expect(response.status).to be 401
    end

    it 'should reject booking' do
      client = create :user, :client
      trainer = create :user, :trainer
      booking = create :booking, client_id: client.id, trainer_id: trainer.id
      sign_in user: client

      reason = create :reject_booking_reason

      post :reject, params: {
          id: booking.id,
          reject_booking_reason_ids: [reason.id]
      }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Booking rejected!")
      booking.reload
      expect(booking.rejected?).to eq(true)
      expect(booking.reject_booking_reasons.map(&:id)).to eq([reason.id])
    end

    it 'should show error reject reason can not be blank' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user
      booking = create :booking, client_id: user.id, trainer_id: trainer.id

      post :reject, params: { id: booking.id }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Reject booking reasons should be at least one"])
    end

    it 'should show error booking not found' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user

      post :reject, params: {
          id: 100,
          reject_booking_reason_id: 1
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
      expect(response_body["errors"]).to eq(["Record not found."])
    end

    it 'should render error if booking is already rejected' do
      user = create :user, :client
      trainer = create :user, :trainer
      reason = create :reject_booking_reason

      sign_in user: user
      booking = create :booking,
                       client_id: user.id,
                       trainer_id: trainer.id,
                       status: :rejected,
                       reject_booking_reason_ids: [reason.id]

      post :reject, params: {
        id: booking.id,
        reject_booking_reason_id: reason.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Booking already rejected!"])

      post :cancel, params: {
        id: booking.id,
        reject_booking_reason_id: reason.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Booking already rejected!"])
    end
  end

  describe '#cancel booking' do
    it 'should render unauthorized if there is no session' do
      post :cancel, params: {
          id: 11,
          reject_reason_id: 235
      }
      expect(response.status).to be 401
    end

    it 'should cancel booking' do
      user = create :user, :client
      trainer = create :user, :trainer
      booking = create :booking, client_id: user.id, trainer_id: trainer.id
      reason = create :reject_booking_reason

      sign_in user: user
      post :cancel, params: {
          id: booking.id,
          reject_booking_reason_ids: [reason.id]
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Booking cancelled!")
      booking.reload
      expect(booking.canceled_by_client?).to eq(true)
      expect(booking.reject_booking_reasons.map(&:id)).to eq([reason.id])
    end

    it 'should show error reject reason can not be blank' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user
      booking = create :booking, client_id: user.id, trainer_id: trainer.id
      post :cancel, params: {
          id: booking.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Reject booking reasons should be at least one"])
    end

    it 'should show error booking not found' do
      user = create :user, :client
      trainer = create :user, :trainer
      sign_in user: user

      post :cancel, params: {
          id: 969,
          reject_booking_reason_id: 12
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 404
      expect(response_body["errors"]).to eq(["Record not found."])
    end

    it 'should render error if booking is already canceled' do
      user = create :user, :client
      trainer = create :user, :trainer
      reason = create :reject_booking_reason
      booking = create :booking,
                       client_id: user.id,
                       trainer_id: trainer.id,
                       status: :canceled_by_client,
                       reject_booking_reason_ids: [reason.id]

      sign_in user: user

      post :cancel, params: {
        id: booking.id,
        reject_booking_reason_id: reason.id
      }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Booking already canceled!"])

      post :reject, params: {
        id: booking.id,
        reject_booking_reason_id: reason.id
      }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body["errors"]).to eq(["Booking already canceled!"])
    end
  end

  describe '#reject booking change request' do

    it 'should successfully reject request' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       status: 'approved'

      change_request = create :booking_change_request,
                              booking: booking,
                              from_user: trainer,
                              to_user: client,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.beginning_of_day + 12.hours),
                                                                to_time: (Date.tomorrow.beginning_of_day + 13.hours)
                                                            }]

      sign_in user: client

      expect_any_instance_of(User).to receive(:notify).with(
          "Booking change request rejected.", anything
      )

      post :reject, params: {id: booking.id}

      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body).to eq({"message" => "Change request rejected."})
      expect(change_request.reload.active).to eq(false)
    end

    it 'should render not found if there no change request with given id' do
      client = create :user, :client
      sign_in user: client
      post :reject, params: {id: 1}
      expect(response.status).to be 404
    end
  end

  describe '#accept booking change request' do
    it 'should successfully accept request' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_sessions_attributes: [{
                                                           from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                           to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                       }],
                         booking_type: 'renewable',
                         status: 'approved'

        expect(booking.booking_session_days[0]['day']).to eq(Date.tomorrow.wday)
        expect(booking.booking_session_days[0]['from_time']).to eq('09:00')
        expect(booking.booking_session_days[0]['to_time']).to eq('10:00')

        booking.booking_sessions.each_with_index do |session, index|
          expect(session.from_time).to eq(Time.now.utc.beginning_of_day + index.weeks + 1.day + 9.hours)
          expect(session.to_time).to eq(Time.now.utc.beginning_of_day + index.weeks + 1.day + 10.hours)
        end

        Timecop.travel Time.now.utc.beginning_of_day + 2.days do
          change_request = create :booking_change_request,
                                  booking: booking,
                                  from_user: trainer,
                                  to_user: client,
                                  booking_sessions_attributes: [{
                                                                    from_time: (Time.now.beginning_of_day + 12.hours),
                                                                    to_time: (Time.now.beginning_of_day + 13.hours)
                                                                }]

          sign_in user: client

          expect_any_instance_of(User).to receive(:notify).with(
              "Booking change request approved.", anything
          )

          post :approve, params: {id: booking.id}

          # expect(response.status).to be 200
          response_body = JSON.parse(response.body)
          expect(response_body).to eq({"message" => "Change request approved."})
        end
      end
    end

    it 'should render not found if there no change request with given id' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'approved'

      sign_in user: client
      post :approve, params: {id: booking.id}
      expect(response.status).to be 404
    end

    it 'should not accept change request if time is not included in trainer\'s working hours' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'approved'

      Timecop.travel Time.now.utc.beginning_of_day + 2.days do
        change_request = create :booking_change_request,
                                booking: booking,
                                from_user: trainer,
                                to_user: client,
                                booking_sessions_attributes: [{
                                                                  from_time: (Time.now.beginning_of_day + 12.hours),
                                                                  to_time: (Time.now.beginning_of_day + 13.hours)
                                                              }]

        trainer.update_attributes working_hours: [{
                                                      "day": Time.now.wday,
                                                      "ranges": [{
                                                                     from_time: (Date.tomorrow.beginning_of_day + 5.hours),
                                                                     to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                                 }]
                                                  }]

        sign_in user: client

        post :approve, params: {id: booking.id}

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)
        expect(response_body).to eq({"errors" => ["Booking sessions time do not fit with trainer working schedule"]})
      end
    end

    it 'should not accept change request if there is another session at the same time' do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'approved'

      Timecop.travel Time.now.utc.beginning_of_day + 2.days do
        change_request = create :booking_change_request,
                                booking: booking,
                                from_user: trainer,
                                to_user: client,
                                booking_sessions_attributes: [{
                                                                  from_time: (Time.now.beginning_of_day + 12.hours),
                                                                  to_time: (Time.now.beginning_of_day + 13.hours)
                                                              }]

        booking2 = create :booking,
                          client_id: client.id,
                          trainer_id: trainer.id,
                          booking_sessions_attributes: [{
                                                            from_time: (Time.now.beginning_of_day + 12.hours),
                                                            to_time: (Time.now.beginning_of_day + 13.hours)
                                                        }],
                          status: 'approved'

        sign_in user: client
        post :approve, params: {id: booking.id}

        expect(response.status).to be 422
        response_body = JSON.parse(response.body)
        expect(response_body).to eq({"errors" => ["From time time do not fit with other trainer sessions!", "From time time do not fit with other client sessions!"]})
      end
    end

    it 'should correctly validate min/max sessions per week' do
      client = create :user, :client
      trainer = create :user, :trainer,  working_hours: [{
                                                             "day": Date.tomorrow.wday,
                                                             "ranges": [
                                                                 {
                                                                     from_time: (Date.tomorrow.beginning_of_day + 1.hour),
                                                                     to_time: (Date.tomorrow.beginning_of_day + 23.hour)
                                                                 }
                                                             ]
                                                         }]

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_type: 'renewable',
                       status: 'approved',
                       booking_sessions_attributes:
                           [
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                               },
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 10.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 11.hours)
                               },
                               {
                                   from_time: (Date.tomorrow.beginning_of_day + 11.hours),
                                   to_time: (Date.tomorrow.beginning_of_day + 12.hours)
                               }
                           ]

      change_request = create :booking_change_request,
          booking_id: booking.id,
          from_user_id: trainer.id,
          to_user_id: client.id,
          active: true,
          booking_sessions_attributes: [
              {
                  from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                  to_time: (Date.tomorrow.beginning_of_day + 10.hours)
              },
              {
                  from_time: (Date.tomorrow.beginning_of_day + 10.hours),
                  to_time: (Date.tomorrow.beginning_of_day + 11.hours)
              },
              {
                  from_time: (Date.tomorrow.beginning_of_day + 11.hours),
                  to_time: (Date.tomorrow.beginning_of_day + 12.hours)
              },
              {
                  from_time: (Date.tomorrow.beginning_of_day + 13.hours),
                  to_time: (Date.tomorrow.beginning_of_day + 14.hours)
              }
          ]

      sign_in user: client
      post :approve, params: {id: booking.id}

      expect(response.status).to be 200


      expect(change_request.booking_sessions_attributes[0][:from_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 9.hours)
      expect(change_request.booking_sessions_attributes[0][:to_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 10.hours)
      expect(change_request.booking_sessions_attributes[1][:from_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 10.hours)
      expect(change_request.booking_sessions_attributes[1][:to_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 11.hours)
      expect(change_request.booking_sessions_attributes[2][:from_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 11.hours)
      expect(change_request.booking_sessions_attributes[2][:to_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 12.hours)
      expect(change_request.booking_sessions_attributes[3][:from_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 13.hours)
      expect(change_request.booking_sessions_attributes[3][:to_time]).to eq(Time.now.utc.beginning_of_day + 1.day + 14.hours)

      sign_in user: client


    end
  end
end