require 'rails_helper'

RSpec.describe Api::V1::Client::TopTrainersController, type: :controller do
  render_views

  describe '#index' do
    it 'should show top trainers' do
      trainer1 = create :user, :trainer

      client1 = create :user, :client
      client2 = create :user, :client

      booking1 = create :booking, trainer_id: trainer1.id, client_id: client1.id
      booking1.booking_sessions.each {|s| s.update_attribute(:rate, 5)}
      booking1.approved!

      day = Date.tomorrow
      bookings = []
      5.times do |i|
        booking_sessions_attributes = [{from_time: day + i.days + 13.hours, to_time: day + i.days + 14.hours}]
        b = create :booking, trainer_id: (create :user, :trainer).id, client_id: client2.id, booking_sessions_attributes: booking_sessions_attributes
        b.booking_sessions.each {|s| s.update_attribute(:rate, [*1..5].sample)}
        b.approved!
        bookings << b
      end
      bookings = bookings.sort_by{|b| -(b.booking_sessions.sum(:rate)) } # desc. sort by sum

      sign_in user: client1

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['trainers'].count).to eq(3)
      response_body['trainers'].each_with_index {|s, i|
        expect(s["id"]).to eq(bookings[i].trainer_id)
      }
      expect(response_body['trainers']).to_not include(client1) # shouldn't include yourself
    end
    it 'should show top trainers for unauthorized user' do
      client = create :user, :client

      day = Date.tomorrow
      bookings = []
      5.times do |i|
        booking_sessions_attributes = [{from_time: day + i.days + 13.hours, to_time: day + i.days + 14.hours}]
        b = create :booking, trainer_id: (create :user, :trainer).id, client_id: client.id, booking_sessions_attributes: booking_sessions_attributes
        b.booking_sessions.each {|s| s.update_attribute(:rate, [*1..5].sample)}
        b.approved!
        bookings << b
      end
      bookings = bookings.sort_by{|b| -(b.booking_sessions.sum(:rate)) } # desc. sort by sum

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['trainers'].count).to eq(3)
      response_body['trainers'].each_with_index {|s, i|
        expect(s["id"]).to eq(bookings[i].trainer_id)
      }
    end
  end
end

