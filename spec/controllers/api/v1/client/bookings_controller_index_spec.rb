require 'rails_helper'
include SessionsHelper
require 'stripe'

require 'net/smtp'

RSpec.describe Api::V1::Client::BookingsController, type: :controller do
  render_views

  describe '#index' do
    it 'should allow only clients' do
      user = create :user, :trainer
      sign_in user: user
      get :index
      expect(response.status).to be 422
    end

    it 'should render unauthorized if there is no session' do
      get :index
      expect(response.status).to be 401
    end

    it 'should display approved bookings' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                                                             from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours),
                                                                             to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours)
                                                                         }]

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours)
                                                                          }]

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 15.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        canceled_booking = create :booking,
                                  client_id: client.id,
                                  trainer_id: trainer.id,
                                  status: :canceled_by_client,
                                  reject_booking_reason_ids: [create(:reject_booking_reason).id]

        free_booking = create :booking,
                              client_id: client.id,
                              trainer_id: trainer.id,
                              status: :approved,
                              booking_type: :free,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 16.hours),
                                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 17.hours)
                                                            }]

        get :index, params: {status: :approved, sort_column: :created_at, sort_type: :asc}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['total']).to eq(4)

        expect(response_body['bookings'][0]['id']).to                               eq(approved_once_booking.id)
        expect(response_body['bookings'][0]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][0]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][0]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][0]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][0]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][0]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][0]['address']).to                          eq('test')
        expect(response_body['bookings'][0]['booking_type']).to                     eq('once')
        expect(response_body['bookings'][0]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][0]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['bookings'][0]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_f)
        expect(response_body['bookings'][0]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 10.hours).to_i)

        expect(response_body['bookings'][1]['id']).to                               eq(approved_renewable_booking.id)
        expect(response_body['bookings'][1]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][1]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][1]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][1]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][1]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][1]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][1]['address']).to                          eq('test')
        expect(response_body['bookings'][1]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][1]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][1]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][1]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][1]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_date']).to                eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)
        expect(response_body['bookings'][1]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 12.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)

        expect(response_body['bookings'][2]['id']).to                               eq(approved_next_month_booking.id)
        expect(response_body['bookings'][2]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][2]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][2]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][2]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][2]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][2]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][2]['address']).to                          eq('test')
        expect(response_body['bookings'][2]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][2]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][2]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][2]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][2]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours).to_f)
        expect(response_body['bookings'][2]['last_session_date']).to                eq(DateTime.parse("2017-09-30 14:00:00 UTC").to_f)
        expect(response_body['bookings'][2]['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['bookings'][2]['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['bookings'][2]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours).to_f)
        expect(response_body['bookings'][2]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 14:00:00 UTC").to_f)

        expect(response_body['bookings'][3]['id']).to                               eq(free_booking.id)
        expect(response_body['bookings'][3]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][3]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][3]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][3]['session_price']).to                    eq(0)
        expect(response_body['bookings'][3]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][3]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][3]['address']).to                          eq('test')
        expect(response_body['bookings'][3]['booking_type']).to                     eq('free')
        expect(response_body['bookings'][3]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][3]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][3]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][3]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
        expect(response_body['bookings'][3]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 16.hours).to_f)
      end
    end

    it 'should display pending bookings' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        sign_in user: client

        pending_once_booking = create :booking,
                                      client_id: client.id,
                                      trainer_id: trainer.id,
                                      booking_type: :once,
                                      status: :pending,
                                      booking_sessions_attributes: [{
                                                                        from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 9.hours),
                                                                        to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours)
                                                                    }]

        approved_once_booking = create :booking,
                                       client_id: client.id,
                                       trainer_id: trainer.id,
                                       booking_type: :once,
                                       status: :pending,
                                       booking_sessions_attributes: [{
                                                                         from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 10.hours),
                                                                         to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours)
                                                                     }]
        approved_once_booking.approve(Time.now)

        pending_renewable_booking = create :booking,
                                           client_id: client.id,
                                           trainer_id: trainer.id,
                                           booking_type: :renewable,
                                           status: :pending,
                                           booking_sessions_attributes: [{
                                                                             from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 11.hours),
                                                                             to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours)
                                                                         }]

        approved_renewable_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                                          }]
        approved_renewable_booking.approve(Time.now)

        pending_next_month_booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours)
                                                                          }]

        approved_next_month_booking = create :booking,
                                             client_id: client.id,
                                             trainer_id: trainer.id,
                                             booking_type: :renewable,
                                             status: :pending,
                                             booking_sessions_attributes: [{
                                                                               from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 14.hours),
                                                                               to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 15.hours)
                                                                           }]
        approved_next_month_booking.approve(Time.now)

        canceled_booking = create :booking,
                                  client_id: client.id,
                                  trainer_id: trainer.id,
                                  status: :canceled_by_client,
                                  reject_booking_reason_ids: [create(:reject_booking_reason).id]

        free_booking = create :booking,
                              client_id: client.id,
                              trainer_id: trainer.id,
                              status: :approved,
                              booking_type: :free,
                              booking_sessions_attributes: [{
                                                                from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 16.hours),
                                                                to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 17.hours)
                                                            }]

        get :index, params: {status: :pending, sort_column: :created_at, sort_type: :asc}

        expect(response.status).to be 200
        response_body = JSON.parse(response.body)

        expect(response_body['total']).to eq(3)

        expect(response_body['bookings'][0]['id']).to                               eq(pending_once_booking.id)
        expect(response_body['bookings'][0]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][0]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][0]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][0]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][0]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][0]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][0]['address']).to                          eq('test')
        expect(response_body['bookings'][0]['booking_type']).to                     eq('once')
        expect(response_body['bookings'][0]['sessions_count']).to                   eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][0]['sessions_in_this_month_count']).to     eq(1)
        expect(response_body['bookings'][0]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['bookings'][0]['last_session_date']).to                eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_f)
        expect(response_body['bookings'][0]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)
        expect(response_body['bookings'][0]['last_session_in_this_month_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 9.hours).to_i)

        expect(response_body['bookings'][1]['id']).to                               eq(pending_renewable_booking.id)
        expect(response_body['bookings'][1]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][1]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][1]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][1]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][1]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][1]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][1]['address']).to                          eq('test')
        expect(response_body['bookings'][1]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][1]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][1]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][1]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][1]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['last_session_date']).to                eq(DateTime.parse("2017-08-30 11:00:00 UTC").to_f)
        expect(response_body['bookings'][1]['first_session_in_this_week_date']).to  eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['last_session_in_this_week_date']).to   eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_f)
        expect(response_body['bookings'][1]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 11.hours).to_i)
        expect(response_body['bookings'][1]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 11:00:00 UTC").to_f)

        expect(response_body['bookings'][2]['id']).to                               eq(pending_next_month_booking.id)
        expect(response_body['bookings'][2]['trainer_id']).to                       eq(trainer.id)
        expect(response_body['bookings'][2]['first_name']).to                       eq(trainer.first_name)
        expect(response_body['bookings'][2]['last_name']).to                        eq(trainer.last_name)
        expect(response_body['bookings'][2]['session_price']).to                    eq(123.45)
        expect(response_body['bookings'][2]['latitude']).to                         eq(55.55)
        expect(response_body['bookings'][2]['longitude']).to                        eq(65.4321)
        expect(response_body['bookings'][2]['address']).to                          eq('test')
        expect(response_body['bookings'][2]['booking_type']).to                     eq('renewable')
        expect(response_body['bookings'][2]['sessions_count']).to                   eq(4)
        expect(response_body['bookings'][2]['sessions_in_this_week_count']).to      eq(1)
        expect(response_body['bookings'][2]['sessions_in_this_month_count']).to     eq(4)
        expect(response_body['bookings'][2]['first_session_date']).to               eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours).to_f)
        expect(response_body['bookings'][2]['last_session_date']).to                eq(DateTime.parse("2017-09-30 13:00:00 UTC").to_f)
        expect(response_body['bookings'][2]['first_session_in_this_week_date']).to  eq(nil)
        expect(response_body['bookings'][2]['last_session_in_this_week_date']).to   eq(nil)
        expect(response_body['bookings'][2]['first_session_in_this_month_date']).to eq((Date.tomorrow.to_time.utc.beginning_of_day + 1.month + 13.hours).to_i)
        expect(response_body['bookings'][2]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-09-30 13:00:00 UTC").to_f)
      end
    end

    it 'should display approved bookings on next month if paymant fails' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        credit_card = create :credit_card, user: client

        booking = create :booking,
                                            client_id: client.id,
                                            trainer_id: trainer.id,
                                            booking_type: :renewable,
                                            status: :pending,
                                            booking_sessions_attributes: [{
                                                                              from_time: (Date.tomorrow.to_time.utc.beginning_of_day + 12.hours),
                                                                              to_time: (Date.tomorrow.to_time.utc.beginning_of_day + 13.hours)
                                                                          }]
        booking.approve(Time.now)

        Timecop.travel Time.now + 1.month do
          credit_card.destroy

          booking.reload
          expect(booking.notified_about_payment_problems).to eq(false)
          expect(booking.notified_about_payment_problems_date).to be(nil)

          ChargeClientsWorker.new.perform

          booking.reload
          expect(booking.notified_about_payment_problems).to eq(true)
          expect(booking.notified_about_payment_problems_date).to_not be(nil)
          expect(booking.notifications.client_charge_failed.count).to eq(1)
          expect(booking.notifications.client_charge_failed_after_two_days.count).to eq(0)

          Timecop.travel Time.now + 3.days do

            ChargeClientsWorker.new.perform

            booking.reload
            expect(booking.notified_about_payment_problems).to eq(true)
            expect(booking.notified_about_payment_problems_date).to_not be(nil)
            expect(booking.notifications.client_charge_failed.count).to eq(1)
            expect(booking.notifications.client_charge_failed_after_two_days.count).to eq(2)


            sign_in user: client
            get :index, params: {sort_column: :created_at, sort_type: :asc }

            response_body = JSON.parse(response.body)

            expect(response_body['total']).to eq(1)

            expect(response_body['bookings'][0]['id']).to                               eq(booking.id)
            expect(response_body['bookings'][0]['trainer_id']).to                       eq(trainer.id)
            expect(response_body['bookings'][0]['first_name']).to                       eq(trainer.first_name)
            expect(response_body['bookings'][0]['last_name']).to                        eq(trainer.last_name)
            expect(response_body['bookings'][0]['session_price']).to                    eq(123.45)
            expect(response_body['bookings'][0]['latitude']).to                         eq(55.55)
            expect(response_body['bookings'][0]['longitude']).to                        eq(65.4321)
            expect(response_body['bookings'][0]['address']).to                          eq('test')
            expect(response_body['bookings'][0]['booking_type']).to                     eq('renewable')
            expect(response_body['bookings'][0]['sessions_count']).to                   eq(4)
            expect(response_body['bookings'][0]['sessions_in_this_week_count']).to      eq(1)
            expect(response_body['bookings'][0]['sessions_in_this_month_count']).to     eq(4)
            expect(response_body['bookings'][0]['first_session_date']).to               eq(DateTime.parse("2017-08-09 12:00:00 UTC").to_f)
            expect(response_body['bookings'][0]['last_session_date']).to                eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)
            expect(response_body['bookings'][0]['first_session_in_this_week_date']).to  eq(nil)
            expect(response_body['bookings'][0]['last_session_in_this_week_date']).to   eq(nil)
            expect(response_body['bookings'][0]['first_session_in_this_month_date']).to eq(DateTime.parse("2017-08-09 12:00:00 UTC").to_f)
            expect(response_body['bookings'][0]['last_session_in_this_month_date']).to  eq(DateTime.parse("2017-08-30 12:00:00 UTC").to_f)
          end
        end
      end
    end

  end
end