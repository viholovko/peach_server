require 'rails_helper'

RSpec.describe Api::V1::Client::ScheduleController, type: :controller do
  render_views

  describe '#index' do
    it 'should list' do
      Timecop.travel Time.now.utc.beginning_of_day do

        client = create :user, :client
        trainer = create :user, :trainer, working_hours: [
            {
                day: Date.today.wday,
                ranges: [
                    {
                        from_time: Date.today.beginning_of_day + 9.hour,
                        to_time: Date.today.beginning_of_day + 20.hour
                    }
                ]
            }
        ]

        booking = create :booking,
                         client_id: client.id,
                         trainer_id: trainer.id,
                         booking_type: 'once',
                         status: 'approved',
                         booking_sessions_attributes:
                             [
                                 {
                                     from_time: Date.today.beginning_of_day + 9.hour,
                                     to_time: Date.today.beginning_of_day + 10.hour
                                 }
                             ]

        Timecop.travel Time.now + 1.day do

          trainer.update_attributes working_hours: [
              {
                  day: Date.today.wday,
                  ranges: [
                      {
                          from_time: Date.today.beginning_of_day + 5.hour,
                          to_time: Date.today.beginning_of_day + 10.hour
                      }
                  ]
              }
          ]

          booking2 = create :booking,
                            client_id: client.id,
                            trainer_id: trainer.id,
                            booking_type: 'once',
                            status: 'approved',
                            booking_sessions_attributes:
                                [
                                    {
                                        from_time: Date.today.beginning_of_day + 9.hour,
                                        to_time: Date.today.beginning_of_day + 10.hour
                                    }
                                ]

          sign_in user: client
          get :index, params: { trainer_id: trainer.id, from: (Time.now - 1.day).to_i, to: (Time.now + 14.days).to_i }

          expect(response.status).to be 200
          response_body = JSON.parse(response.body)

          expect(response_body[0]['day']).to eq((Time.now.beginning_of_day - 1.day).to_f)
          expect(response_body[0]['working_hours']).to eq({
                                                              "day" => (Date.today - 1.day).wday,
                                                              "ranges" => [{
                                                                               "from_time" => (Date.today.beginning_of_day - 1.day + 9.hour).to_f,
                                                                               "to_time" => (Date.today.beginning_of_day - 1.day + 20.hour).to_f
                                                                           }]
                                                          })
          expect(response_body[0]['sessions']).to eq([
                                                         {
                                                             "id" => booking.booking_sessions.first.id,
                                                             "from_time" => (Date.today.beginning_of_day + 9.hour - 1.day).to_f,
                                                             "to_time" => (Date.today.beginning_of_day + 10.hour - 1.day).to_f,
                                                         }
                                                     ])

          expect(response_body[1]['day']).to eq((Time.now.beginning_of_day).to_f)
          expect(response_body[1]['working_hours']).to eq({
                                                              "day" => (Date.today).wday,
                                                              "ranges" => [{
                                                                               "from_time" => (Date.today.beginning_of_day + 5.hour).to_f,
                                                                               "to_time" => (Date.today.beginning_of_day + 10.hour).to_f
                                                                           }]
                                                          })
          expect(response_body[1]['sessions']).to eq([
                                                         {
                                                             "id" => booking2.booking_sessions.first.id,
                                                             "from_time" => (Date.today.beginning_of_day + 9.hour).to_f,
                                                             "to_time" => (Date.today.beginning_of_day + 10.hour).to_f,
                                                         }
                                                     ])
          expect(response_body[2]['day']).to eq((Time.now.beginning_of_day + 1.day).to_f)
          expect(response_body[2]['working_hours']).to eq({
                                                              "day" => (Time.now + 1.day).wday,
                                                              "ranges" => []
                                                          })
          expect(response_body[2]['sessions']).to eq([])

          expect(response_body[3]['day']).to eq((Time.now.beginning_of_day + 2.day).to_f)
          expect(response_body[3]['working_hours']).to eq({"day" => (Time.now + 2.day).wday, "ranges" => []})
          expect(response_body[3]['sessions']).to eq([])

          expect(response_body[4]['day']).to eq((Time.now.beginning_of_day + 3.day).to_f)
          expect(response_body[4]['working_hours']).to eq({"day" => (Time.now + 3.day).wday, "ranges" => []})
          expect(response_body[4]['sessions']).to eq([])

          expect(response_body[5]['day']).to eq((Time.now.beginning_of_day + 4.day).to_f)
          expect(response_body[5]['working_hours']).to eq({"day" => (Time.now + 4.day).wday, "ranges" => []})
          expect(response_body[5]['sessions']).to eq([])

          expect(response_body[6]['day']).to eq((Time.now.beginning_of_day + 5.day).to_f)
          expect(response_body[6]['working_hours']).to eq({"day" => (Time.now + 5.day).wday, "ranges" => []})
          expect(response_body[6]['sessions']).to eq([])

          expect(response_body[7]['day']).to eq((Time.now.beginning_of_day + 6.day).to_f)
          expect(response_body[7]['working_hours']).to eq({"day" => (Time.now + 6.day).wday, "ranges" => []})
          expect(response_body[7]['sessions']).to eq([])

          expect(response_body[8]['day']).to eq((Time.now.beginning_of_day + 7.day).to_f)
          expect(response_body[8]['working_hours']).to eq({"day" => (Time.now + 7.day).wday, "ranges" => [
              {
                  "from_time" => (Date.today.beginning_of_day + 5.hour).to_f,
                  "to_time" => (Date.today.beginning_of_day + 10.hour).to_f
              }
          ]})
          expect(response_body[8]['sessions']).to eq([])

          expect(response_body[9]['day']).to eq((Time.now.beginning_of_day + 8.day).to_f)
          expect(response_body[9]['working_hours']).to eq({"day" => (Time.now + 8.day).wday, "ranges" => []})
          expect(response_body[9]['sessions']).to eq([])

          expect(response_body[10]['day']).to eq((Time.now.beginning_of_day + 9.day).to_f)
          expect(response_body[10]['working_hours']).to eq({"day" => (Time.now + 9.day).wday, "ranges" => []})
          expect(response_body[10]['sessions']).to eq([])

          expect(response_body[11]['day']).to eq((Time.now.beginning_of_day + 10.day).to_f)
          expect(response_body[11]['working_hours']).to eq({"day" => (Time.now + 10.day).wday, "ranges" => []})
          expect(response_body[11]['sessions']).to eq([])

          expect(response_body[12]['day']).to eq((Time.now.beginning_of_day + 11.day).to_f)
          expect(response_body[12]['working_hours']).to eq({"day" => (Time.now + 11.day).wday, "ranges" => []})
          expect(response_body[12]['sessions']).to eq([])

          expect(response_body[13]['day']).to eq((Time.now.beginning_of_day + 12.day).to_f)
          expect(response_body[13]['working_hours']).to eq({"day" => (Time.now + 12.day).wday, "ranges" => []})
          expect(response_body[13]['sessions']).to eq([])

          expect(response_body[14]['day']).to eq((Time.now.beginning_of_day + 13.day).to_f)
          expect(response_body[14]['working_hours']).to eq({"day" => (Time.now + 13.day).wday, "ranges" => []})
          expect(response_body[14]['sessions']).to eq([])

          expect(response_body[15]['day']).to eq((Time.now.beginning_of_day + 14.day).to_f)
          expect(response_body[15]['working_hours']).to eq({"day" => (Time.now + 14.day).wday, "ranges" => [
              {
                  "from_time" => (Date.today.beginning_of_day + 5.hour).to_f,
                  "to_time" => (Date.today.beginning_of_day + 10.hour).to_f
              }
          ]})
          expect(response_body[15]['sessions']).to eq([])
        end
      end
    end


    it 'should render unauthorized if there no session' do
      client = create :user, :client
      get :index
      expect(response.status).to be 401
    end

    it 'should allow only client users' do
      client = create :user, :trainer
      sign_in user: client
      get :index
      expect(response.status).to be 422
    end
  end
end
