require 'rails_helper'
RSpec.describe Api::V1::Client::BillingsController, type: :controller do
  render_views
  describe '#index'do
    let(:response_body){JSON.parse(response.body)}

    it 'completed' do
      Timecop.travel(Date.parse("2017-08-09 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client
        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_type: 'once',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 1.day + 4.hours,
                                 to_time: Time.now.utc.beginning_of_day + 1.day + 5.hours
                             }
                         ]
        booking.approve Date.today.beginning_of_month

        booking2 = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_type: 'renewable',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 1.day + 5.hours,
                                 to_time: Time.now.utc.beginning_of_day + 1.day + 6.hours
                             }
                         ]

        booking2.approve Date.today.beginning_of_month

        expect(client.as_client.payments.count).to eq(2)

        sign_in user: client
        get :index

        expect(response.status).to be 200
        expect(response_body['total']).to eq(3)
        expect(response_body['earnings'][0]['type']).to eq('history')
        expect(response_body['earnings'][0]['date']).to eq(DateTime.parse("2017-08-09 00:00:00 UTC").to_f)
        expect(response_body['earnings'][0]['trainer_id']).to eq(trainer.id)
        expect(response_body['earnings'][0]['avatar']).to_not be(nil)
        expect(response_body['earnings'][0]['first_name']).to eq(trainer.first_name)
        expect(response_body['earnings'][0]['last_name']).to eq(trainer.last_name)
        expect(response_body['earnings'][0]['booking_id']).to eq(booking.id)
        expect(response_body['earnings'][0]['session_price']).to eq(123)
        expect(response_body['earnings'][0]['sessions_count']).to eq(1)
        expect(response_body['earnings'][0]['payment_method_last_four']).to eq('1111')
        expect(response_body['earnings'][0]['card_brand']).to eq('Visa')

        expect(response_body['earnings'][1]['type']).to eq('history')
        expect(response_body['earnings'][1]['date']).to eq(DateTime.parse("2017-08-09 00:00:00 UTC").to_f)
        expect(response_body['earnings'][1]['trainer_id']).to eq(trainer.id)
        expect(response_body['earnings'][1]['avatar']).to_not be(nil)
        expect(response_body['earnings'][1]['first_name']).to eq(trainer.first_name)
        expect(response_body['earnings'][1]['last_name']).to eq(trainer.last_name)
        expect(response_body['earnings'][1]['booking_id']).to eq(booking2.id)
        expect(response_body['earnings'][1]['session_price']).to eq(123)
        expect(response_body['earnings'][1]['sessions_count']).to eq(4)
        expect(response_body['earnings'][1]['payment_method_last_four']).to eq('1111')
        expect(response_body['earnings'][1]['card_brand']).to eq('Visa')

        expect(response_body['earnings'][2]['type']).to eq('upcoming')
        expect(response_body['earnings'][2]['date']).to eq(DateTime.parse("2017-09-01 00:00:00 UTC").to_f)
        expect(response_body['earnings'][2]['trainer_id']).to eq(trainer.id)
        expect(response_body['earnings'][2]['avatar']).to_not be(nil)
        expect(response_body['earnings'][2]['first_name']).to eq(trainer.first_name)
        expect(response_body['earnings'][2]['last_name']).to eq(trainer.last_name)
        expect(response_body['earnings'][2]['booking_id']).to eq(booking2.id)
        expect(response_body['earnings'][2]['session_price']).to eq(123)
        expect(response_body['earnings'][2]['sessions_count']).to eq(4)
        expect(response_body['earnings'][2]['payment_method_last_four']).to eq('1111')
        expect(response_body['earnings'][2]['card_brand']).to eq('Visa')
      end
    end

    it 'should correctly estimate upcoming billing' do
      Timecop.travel(Date.parse("2017-08-15 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_type: 'renewable',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 1.day + 5.hours,
                                 to_time: Time.now.utc.beginning_of_day + 1.day + 6.hours
                             }
                         ]

        booking.approve Date.today.beginning_of_month

        expect(client.as_client.payments.count).to eq(1)

        sign_in user: client
        get :index

        expect(response.status).to be 200
        expect(response_body['total']).to eq(2)

        expect(response_body['earnings'][0]['type']).to eq('history')
        expect(response_body['earnings'][0]['date']).to eq(DateTime.parse("2017-08-15 00:00:00 UTC").to_f)
        expect(response_body['earnings'][0]['trainer_id']).to eq(trainer.id)
        expect(response_body['earnings'][0]['avatar']).to_not be(nil)
        expect(response_body['earnings'][0]['first_name']).to eq(trainer.first_name)
        expect(response_body['earnings'][0]['last_name']).to eq(trainer.last_name)
        expect(response_body['earnings'][0]['booking_id']).to eq(booking.id)
        expect(response_body['earnings'][0]['session_price']).to eq(123)
        expect(response_body['earnings'][0]['sessions_count']).to eq(3)
        expect(response_body['earnings'][0]['payment_method_last_four']).to eq('1111')
        expect(response_body['earnings'][0]['card_brand']).to eq('Visa')

        expect(response_body['earnings'][1]['type']).to eq('upcoming')
        expect(response_body['earnings'][1]['date']).to eq(DateTime.parse("2017-09-01 00:00:00 UTC").to_f)
        expect(response_body['earnings'][1]['trainer_id']).to eq(trainer.id)
        expect(response_body['earnings'][1]['avatar']).to_not be(nil)
        expect(response_body['earnings'][1]['first_name']).to eq(trainer.first_name)
        expect(response_body['earnings'][1]['last_name']).to eq(trainer.last_name)
        expect(response_body['earnings'][1]['booking_id']).to eq(booking.id)
        expect(response_body['earnings'][1]['session_price']).to eq(123)
        expect(response_body['earnings'][1]['sessions_count']).to eq(4)
        expect(response_body['earnings'][1]['payment_method_last_four']).to eq('1111')
        expect(response_body['earnings'][1]['card_brand']).to eq('Visa')
      end
    end

    it 'should create renewable booking payment from next month if there is no available days in this month' do
      Timecop.travel(Date.parse("2017-10-30 00:00:00").beginning_of_day) do
        client = create :user, :client
        trainer = create :user, :trainer
        create :credit_card, user: client

        booking = create :booking,
                         trainer_id: trainer.id,
                         client_id: client.id,
                         status: 'pending',
                         booking_type: 'renewable',
                         booking_sessions_attributes: [
                             {
                                 from_time: Time.now.utc.beginning_of_day + 5.day + 5.hours,
                                 to_time: Time.now.utc.beginning_of_day + 5.day + 6.hours
                             }
                         ]

        booking.approve Date.today.beginning_of_month

        expect(client.as_client.payments.count).to eq(1)

        sign_in user: client
        get :index

        expect(response.status).to be 200
        expect(response_body['total']).to eq(2)

        expect(response_body['earnings'][0]['type']).to eq('history')
        expect(Time.at(response_body['earnings'][0]['date'])).to eq(DateTime.parse("2017-10-30 00:00:00 UTC"))


        expect(response_body['earnings'][1]['type']).to eq('upcoming')
        expect(Time.at(response_body['earnings'][1]['date'])).to eq(DateTime.parse("2017-12-01 00:00:00 UTC"))
      end
    end
  end
end