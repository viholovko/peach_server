require 'rails_helper'

RSpec.describe Api::V1::Client::MyTrainersController, type: :controller do
  render_views
  describe '#index' do
    it 'should render unauthorized if there is no session' do
      create :user, :client
      create :user, :trainer
      get :index
      expect(response.status).to be 401
    end
    it 'should render my_trainers' do
      trainer = create :user, :trainer
      trainer2 = create :user, :trainer
      trainer3 = create :user, :trainer
      other_client = create :user, :client
      user = create :user, :client
      sign_in user: user

      day = Date.tomorrow

      booking_sessions_attributes = []
      booking_sessions_attributes.push({from_time: day + 13.hours, to_time: day + 14.hours})

      booking_sessions_attributes2 = []
      booking_sessions_attributes2.push({from_time: day + 15.hours, to_time: day + 16.hours})

      b1 = create :booking, client_id: user.id, trainer_id: trainer.id
      b2 = create :booking, client_id: user.id, trainer_id: trainer2.id, booking_sessions_attributes: booking_sessions_attributes
      b3 = create :booking, client_id: user.id, trainer_id: trainer3.id, booking_sessions_attributes: booking_sessions_attributes2
      b4 = create :booking, client_id: other_client.id, trainer_id: trainer3.id
      b1.approved!
      b2.approved!
      b4.approved!

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body["trainers"].count).to eq(2)
      expect(response_body["total"]).to eq(2)
      response_body["trainers"].each do |person|
        expect(person["id"]).to be_in([trainer.id, trainer2.id])
      end

      trainer2.destroy
      get :index, params: { name: trainer2.first_name, show_deleted: true }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body["trainers"].count).to eq(1)
      expect(response_body["total"]).to eq(1)
      expect(response_body['trainers'].first['id']).to eq(trainer2.id)

      get :index, params: { sort_column: 'id', sort_type: 'asc' }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body["trainers"].count).to eq(1)
      expect(response_body["total"]).to eq(1)
      expect(response_body['trainers'].first['id']).to eq(trainer.id)

      trainer.update_attributes(latitude: 0, longitude: 0, location_radius: 500)
      get :index, params: { latitude: 0, longitude: 0, distance: 1000 }
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)

      expect(response_body["trainers"].count).to eq(1)
      expect(response_body["total"]).to eq(1)
      expect(response_body['trainers'].first['id']).to eq(trainer.id)
    end
  end
end

