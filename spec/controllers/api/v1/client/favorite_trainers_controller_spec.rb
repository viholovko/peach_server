require 'rails_helper'

RSpec.describe Api::V1::Client::FavoriteTrainersController, type: :controller do
  render_views

  describe '#index' do
    it 'should correctly show favorite trainers' do
      trainer1 = create :user, :trainer
      trainer2 = create :user, :trainer
      client = create :user, :client
      Client.find(client.id).favorite_trainer_ids= [trainer1.id, trainer2.id]

      get :index
      expect(response.status).to be 401

      sign_in user: client

      get :index
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].count).to eq(2)

      get :index, params: { per_page: 1}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].count).to eq(1)

      get :index, params: { per_page: 1, page: 2}
      expect(response.status).to be 200
      response_body = JSON.parse(response.body)
      expect(response_body['total']).to eq(2)
      expect(response_body['trainers'].count).to eq(1)

    end
  end

  describe '#create' do
    it 'should bookmark trainer' do
      trainer = create :user, :trainer
      user = create :user, :client
      sign_in user: user

      post :create, params: {id: trainer.id }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Trainer added to favorites.")
      user.reload
      fav = Client.find(user.id).favorite_trainers
      expect(fav.count).to eq(1)
      expect(fav.first.id).to eq(trainer.id)

    end

    it 'should show  error trainer not found' do
      trainer = create :user, :trainer
      user = create :user, :client
      sign_in user: user

      post :create, params: {id: user.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Trainer not found!"])
    end

    end

  describe '#destroy' do
    it 'should remove bookmark ' do
      trainer1 = create :user, :trainer
      trainer2 = create :user, :trainer
      client = create :user, :client
      Client.find(client.id).favorite_trainer_ids= [trainer1.id, trainer2.id]
      sign_in user: client
      client.reload

      fav = Client.find(client.id).favorite_trainers
      expect(fav.count).to eq(2)

      delete :destroy, params: {id: trainer1.id }
      response_body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(response_body["message"]).to eq("Trainer removed from favorites.")
      fav = Client.find(client.id).favorite_trainers
      expect(fav.count).to eq(1)
      expect(fav.first.id).to eq(trainer2.id)

    end

    it 'should show  error trainer not found' do
      trainer = create :user, :trainer
      user = create :user, :client
      sign_in user: user

      delete :destroy, params: {id: user.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(response_body["errors"]).to eq(["Trainer not found!"])
    end

  end
end

