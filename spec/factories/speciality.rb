FactoryGirl.define do
  factory :speciality do
    sequence(:title) {|n| "speciality#{n}" }
  end
end