FactoryGirl.define do
  factory :booking_change_request, class: BookingChangeRequest do
    booking {FactoryGirl.create :booking}
  end
end