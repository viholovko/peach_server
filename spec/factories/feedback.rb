FactoryGirl.define do
  factory :feedback, class: Feedback do
    stars 4
    sequence(:comment) {|n| "feedback #{n}" }
  end
end
