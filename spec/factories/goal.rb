FactoryGirl.define do
  factory :goal do
    sequence(:title) {|n| "goal#{n}" }
    specialities {[FactoryGirl.create(:speciality)]}
  end
end