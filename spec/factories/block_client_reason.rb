FactoryGirl.define do
  factory :block_client_reason, class: BlockClientReason do
    sequence(:title) {|n| "Block Client Reason ##{n}" }
  end
end