FactoryGirl.define do
  factory :user do
    sequence(:email)      {|n| "user#{n}@example.com" }
    sequence(:first_name) {|n| "user#{n}" }
    password              'secret!'
    password_confirmation 'secret!'
    email_confirmed       true
    gender                'male'
    last_name             'Dou'
    address               '233 River Road, New London'
    post_code             '1234'
    location_radius       1000
    avatar { File.open(Rails.root.join('spec', 'fixtures', 'factory_image.png')) }
    working_hours [{
                       "day": Date.tomorrow.wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 23.hour)},
                       ]
                   },{
                       "day": (Date.tomorrow+1.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 1.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 1.day + 23.hour)},
                       ]
                   },{
                       "day": (Date.tomorrow+2.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 2.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 2.day + 23.hour)}
                       ]
                   },{
                       "day": (Date.tomorrow+3.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 3.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 3.day + 23.hour)}
                       ]
                   },{
                       "day": (Date.tomorrow+4.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 4.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 4.day + 23.hour)}
                       ]
                   },{
                       "day": (Date.tomorrow+5.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 5.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 5.day + 23.hour)}
                       ]
                   },{
                       "day": (Date.tomorrow+6.day).wday,
                       "ranges": [
                           {from_time: (Date.tomorrow.beginning_of_day + 6.day + 1.hour),
                            to_time: (Date.tomorrow.beginning_of_day + 6.day + 23.hour)}
                       ]
                   }]
    trait :admin do
      role { Role.admin }
    end

    trait :trainer do
      role { Role.trainer }
      confirmed        true

    end

    trait :client do
      role { Role.client }
    end
  end
end