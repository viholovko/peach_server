FactoryGirl.define do
  factory :session_change_request, class: SessionChangeRequest do
    booking {FactoryGirl.create :booking}
    booking_session {FactoryGirl.create :booking_session}
    from_time { Time.now }
    to_time { Time.now + 1.hour }
  end
end