FactoryGirl.define do
  factory :attachment do
    file { File.new("#{Rails.root}/spec/fixtures/test.mp3") }
  end
end