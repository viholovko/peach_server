FactoryGirl.define do
  factory :credit_card, class: CreditCard do
    stripe_token { Stripe::Token.create(card: {number: "4242424242424242", exp_month: 4, exp_year: Time.now.year + 1, cvc: "314"})}
    user_id { FactoryGirl.create(:user, :client).id }
    active true
  end
end