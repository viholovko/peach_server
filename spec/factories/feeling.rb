FactoryGirl.define do
  factory :feeling, class: Feeling do
    sequence(:title) {|n| "Feeling #{n}" }
  end
end
