FactoryGirl.define do
  factory :training_place do
    sequence(:title) {|n| "Some place #{n}" }
  end
end
