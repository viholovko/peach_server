FactoryGirl.define do
  factory :booking_session, class: BookingSession do
    time = Time.now.tomorrow
    sequence(:from_time)   {|n| time + n.days}
    sequence(:to_time) {|n| time + n.days + 1.hour}
  end
end