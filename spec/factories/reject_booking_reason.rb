FactoryGirl.define do
  factory :reject_booking_reason, class: RejectBookingReason do
    title 'Some reason'
  end
end