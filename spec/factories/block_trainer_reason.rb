FactoryGirl.define do
  factory :block_trainer_reason, class: BlockTrainerReason do
    sequence(:title) {|n| "Block Client Reason ##{n}" }
  end
end