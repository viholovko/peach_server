FactoryGirl.define do
  factory :problem_report do
    sequence(:comment) { |n| "Comment #{n}" }
  end
end
