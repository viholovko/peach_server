FactoryGirl.define do
  factory :booking, class: Booking do
    status :pending
    booking_type "once"
    session_price 123.45
    latitude 55.55
    longitude 65.4321
    address 'test'
    booking_sessions_attributes [
                                    {from_time: (Date.tomorrow.beginning_of_day + 9.hours), to_time: (Date.tomorrow.beginning_of_day + 10.hours)},
                                    {from_time: (Date.tomorrow.beginning_of_day + 1.day + 9.hours), to_time: (Date.tomorrow.beginning_of_day + 1.day + 10.hours)},
                                    {from_time: (Date.tomorrow.beginning_of_day + 2.days + 9.hours), to_time: (Date.tomorrow.beginning_of_day + 2.days + 10.hours)}
                                ]
    client_id { FactoryGirl.create(:user, :client).id }
    trainer_id { FactoryGirl.create(:user, :trainer).id }
  end
end