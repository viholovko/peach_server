FactoryGirl.define do
  factory :problem_report_reason do
    sequence(:title) { |n| "Reason #{n}" }
  end
end
