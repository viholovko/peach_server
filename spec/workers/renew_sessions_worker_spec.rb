require 'rails_helper'

describe RenewSessionsWorker, type: :model do

  it 'should create new sessions for future months' do
    Timecop.travel(Date.parse("2017-08-13 00:00:00").beginning_of_day) do
      client = create :user, :client
      trainer = create :user, :trainer

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'approved'

      expect(booking.booking_sessions.count).to eq(16)

      booking.booking_sessions.sort_by(&:from_time).each_with_index do |session, index|
        expect(session.from_time).to eq(Time.now.utc.beginning_of_day + index.weeks + 1.day + 9.hours)
        expect(session.to_time).to eq(Time.now.utc.beginning_of_day + index.weeks + 1.day + 10.hours)
      end

      old_time = Time.now.utc.beginning_of_day

      Timecop.travel Time.now.utc.beginning_of_day + 4.weeks do

        RenewSessionsWorker.new.perform

        booking.reload

        expect(booking.booking_sessions.count).to eq(20)

        booking.booking_sessions.sort_by(&:from_time).each_with_index do |session, index|
          expect(session.from_time).to eq(old_time + index.weeks + 1.day + 9.hours)
          expect(session.to_time).to eq(old_time + index.weeks + 1.day + 10.hours)
        end
      end
    end
  end
end
