require 'rails_helper'

describe ChargeClientsWorker, type: :model do

  it 'should create new payment' do
    Timecop.travel(Date.parse("2017-08-13 00:00:00").beginning_of_day) do
      client = create :user, :client
      trainer = create :user, :trainer
      credit_card = create :credit_card, user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'pending'
      booking.approve(Time.now)

      expect(booking.payments.count).to eq(1)

      Timecop.travel Time.now.utc.beginning_of_day + 1.month do

        RenewSessionsWorker.new.perform
        ChargeClientsWorker.new.perform

        booking.reload

        expect(booking.payments.count).to eq(1)
      end
    end
  end

  it 'should create new booking when continue to the next month' do
    Timecop.travel(Date.parse("2017-08-13 00:00:00").beginning_of_day) do
      client = create :user, :client
      trainer = create :user, :trainer
      credit_card = create :credit_card, user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'pending'
      booking.approve(Time.now)

      expect(booking.payments.count).to eq(1)

      Timecop.travel Time.now.utc.beginning_of_day + 1.month do

        RenewSessionsWorker.new.perform
        ChargeClientsWorker.new.perform

        booking.reload
        new_booking = Booking.find(booking.next_booking_id)

        expect(new_booking).to_not be(nil)
        expect(booking.payments.count).to eq(1)
        expect(new_booking.payments.count).to eq(1)

        expect(booking.booking_sessions.count).to eq(3)
        expect(new_booking.booking_sessions.count).to eq(17)
      end
    end
  end

  it 'should throw error if there is no credit cards' do
    Timecop.travel(Date.parse("2017-08-13 00:00:00").beginning_of_day) do
      client = create :user, :client
      trainer = create :user, :trainer
      credit_card = create :credit_card, user: client

      booking = create :booking,
                       client_id: client.id,
                       trainer_id: trainer.id,
                       booking_sessions_attributes: [{
                                                         from_time: (Date.tomorrow.beginning_of_day + 9.hours),
                                                         to_time: (Date.tomorrow.beginning_of_day + 10.hours)
                                                     }],
                       booking_type: 'renewable',
                       status: 'pending'
      booking.approve(Time.now)

      expect(booking.payments.count).to eq(1)

      Timecop.travel Time.now.utc.beginning_of_day + 1.month do
        credit_card.destroy

        # expect_any_instance_of(User).to receive(:notify).with(
        #     "Failed to renew subscription.", {
        #     target_id: booking.id,
        #     from_user: trainer.id,
        #     title: "Failed to renew subscription.",
        #     description: "Subscription payment has failed. Please check your card details. We’ll try charge your card again in 2 days. If payment still fails, your subscription and sessions will be cancelled automatically."
        # })

        RenewSessionsWorker.new.perform
        ChargeClientsWorker.new.perform

        booking.reload
        expect(booking.payments.count).to eq(1)
        expect(booking.notified_about_payment_problems).to eq(true)


        Timecop.travel Time.now.utc.beginning_of_day + 3.days do

          # expect_any_instance_of(User).to receive(:notify).with(
          #     "Subscription payment failed.", {
          #     target_id: booking.id,
          #     from_user: trainer.id,
          #     title: "Subscription payment failed.",
          #     description: "Card was declined. Sessions cancelled."
          # })

          ChargeClientsWorker.new.perform
          booking.reload
          expect(booking.payments.count).to eq(1)
          expect(booking.status).to eq('finished')
        end
      end
    end
  end
end
