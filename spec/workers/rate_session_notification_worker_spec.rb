require 'rails_helper'

describe RateSessionNotificationWorker, type: :model do

  it 'should create new sessions for future months' do
    client = create :user, :client
    trainer = create :user, :trainer

    date = (Time.now.utc.beginning_of_day + 1.day + 10.hours)

    booking = create :booking,
                     client_id: client.id,
                     trainer_id: trainer.id,
                     booking_sessions_attributes: [{
                                                       from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours),
                                                       to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours)
                                                   }],
                     booking_type: 'renewable',
                     status: 'approved'

    booking.booking_sessions.update_all(paid_up: true)

    Timecop.travel Time.now + 1.week do

      expect_any_instance_of(User).to receive(:notify).with(
          "How do you feel? Rate session from #{date.strftime('%b, %d')}.", {
          target_id: booking.booking_sessions.first.id,
          booking_session_id: booking.booking_sessions.first.id,
          from_user: trainer.id,
          to_time: date,
          title: "How do you feel?",
          description: "Rate session from #{date.strftime('%b, %d')}."
        }
      )

      RateSessionNotificationWorker.new.perform


    end
  end

  it 'should not duplicate notifications' do
    client = create :user, :client
    trainer = create :user, :trainer

    date = (Time.now.utc.beginning_of_day + 1.day + 10.hours)

    booking = create :booking,
                     client_id: client.id,
                     trainer_id: trainer.id,
                     booking_sessions_attributes: [{
                                                       from_time: (Time.now.utc.beginning_of_day + 1.day + 9.hours),
                                                       to_time: (Time.now.utc.beginning_of_day + 1.day + 10.hours)
                                                   },
                                                   {
                                                       from_time: (Time.now.utc.beginning_of_day + 1.day + 11.hours),
                                                       to_time: (Time.now.utc.beginning_of_day + 1.day + 12.hours)
                                                   }],
                     booking_type: 'renewable',
                     status: 'approved'

    booking.booking_sessions.update_all(paid_up: true)

    Timecop.travel Time.now + 1.week do

      expect(booking.notifications.rate_session.count).to eq(0)
      RateSessionNotificationWorker.new.perform
      expect(booking.notifications.rate_session.count).to eq(2)
    end
  end
end
