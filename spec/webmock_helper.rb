RSpec.configure do |config|
  config.before(:each) do
    stub_request(:post, "https://api.quickblox.com/session.json")
        .to_return(
            status: 200,
            body: {
                "user":{
                    "id":                26657938,
                    "owner_id":          64395,
                    "full_name":         nil,
                    "email":             nil,
                    "login":             "test@sdsd.com_trainer_1492761248",
                    "phone":             nil,
                    "website":           nil,
                    "created_at":        "2017-04-21T07:54:09Z",
                    "updated_at":        "2017-04-21T07:54:09Z",
                    "last_request_at":   nil,
                    "external_user_id":  nil,
                    "facebook_id":       nil,
                    "twitter_id":        nil,
                    "blob_id":           nil,
                    "custom_data":       nil,
                    "twitter_digits_id": nil,
                    "user_tags":         nil
                }
            }.to_json,
            headers: {}
        )

    stub_request(:post, "https://api.quickblox.com/users.json")
        .to_return(
            status: 201,
            body: {
                "user":{
                    "id":                26657938,
                    "owner_id":          64395,
                    "full_name":         nil,
                    "email":             nil,
                    "login":             "test@sdsd.com_trainer_1492761248",
                    "phone":             nil,
                    "website":           nil,
                    "created_at":        "2017-04-21T07:54:09Z",
                    "updated_at":        "2017-04-21T07:54:09Z",
                    "last_request_at":   nil,
                    "external_user_id":  nil,
                    "facebook_id":       nil,
                    "twitter_id":        nil,
                    "blob_id":           nil,
                    "custom_data":       nil,
                    "twitter_digits_id": nil,
                    "user_tags":         nil
                }
            }.to_json,
            headers: {})

    stub_request(:delete, "https://api.quickblox.com/session.json")
        .to_return(status: 200, body: "", headers: {})

    stub_request(:post, "https://api.quickblox.com/login.json").
        to_return(
            status: 200,
            body:  {
                "user":{
                    "id":                26657938,
                    "owner_id":          64395,
                    "full_name":         nil,
                    "email":             nil,
                    "login":             "test@sdsd.com_trainer_1492761248",
                    "phone":             nil,
                    "website":           nil,
                    "created_at":        "2017-04-21T07:54:09Z",
                    "updated_at":        "2017-04-21T07:54:09Z",
                    "last_request_at":   nil,
                    "external_user_id":  nil,
                    "facebook_id":       nil,
                    "twitter_id":        nil,
                    "blob_id":           nil,
                    "custom_data":       nil,
                    "twitter_digits_id": nil,
                    "user_tags":         nil
                }
            }.to_json,
            headers: {})

    stub_request(:get, "https://graph.facebook.com/v2.8/me?fields=first_name,last_name,email").
        with({headers:{'Authorization'=>'Bearer test_access_token'}}).
        to_return(body: {
            "email": "user@example.com",
            "first_name": "John",
            "last_name": "Doe",
            "name": "John Doe",
            "picture": {
                "data": { "is_silhouette": false }
            },
            "id": "2913478127342931"
        }.to_json)

    stub_request(:get, "https://graph.facebook.com/v2.7/2913478127342931/picture?width=1000").
        to_return(status: 200, body: File.read('spec/fixtures/factory_image.png'))

    stub_request(:post, "https://api.stripe.com/v1/tokens").
      to_return(
        status: 200,
        body: {
          "id": "tok_189gBz2eZvKYlo2Cm2Z3qsT2",
          "object": "token",
          "card": {
            "id": "card_189gBz2eZvKYlo2CBZAUeFKj",
            "object": "card",
            "address_city": nil,
            "address_country": nil,
            "address_line1": nil,
            "address_line1_check": nil,
            "address_line2": nil,
            "address_state": nil,
            "address_zip": nil,
            "address_zip_check": nil,
            "brand": "Visa",
            "country": "US",
            "cvc_check": nil,
            "dynamic_last4": nil,
            "exp_month": 8,
            "exp_year": 2017,
            "fingerprint": "Xt5EWLLDS7FJjR1c",
            "funding": "credit",
            "last4": "4242",
            "metadata": {
            },
            "name": nil,
            "tokenization_method": nil
          },
          "client_ip": nil,
          "created": 1462907211,
          "livemode": false,
          "type": "card",
          "used": false
        }.to_json)
    stub_request(:post, "https://api.stripe.com/v1/customers").
      with(body: hash_including({ # only source without email
        source: "tok_189gBz2eZvKYlo2Cm2Z3qsT2"
      })).
      to_return(
        status: 200,
        body: {
          "id": "cus_AaJZ7G7fGdVwrz",
          "object": "customer",
          "account_balance": 0,
          "created": 1493762011,
          "currency": "usd",
          "default_source": "card_1AF8w42eZvKYlo2CMvrGfQuL",
          "delinquent": false,
          "description": nil,
          "discount": nil,
          "email": "tests+1493762008@showclix.com",
          "livemode": false,
          "metadata": {
          },
          "shipping": nil,
          "sources": {
            "object": "list",
            "data": [
              {
                "id": "card_1AF8w42eZvKYlo2CMvrGfQuL",
                "object": "card",
                "address_city": "Pittsburgh",
                "address_country": "US",
                "address_line1": "414 Grant St",
                "address_line1_check": "pass",
                "address_line2": "",
                "address_state": "PA",
                "address_zip": "15219",
                "address_zip_check": "pass",
                "brand": "Visa",
                "country": "US",
                "customer": "cus_AaJZ7G7fGdVwrz",
                "cvc_check": "pass",
                "dynamic_last4": nil,
                "exp_month": 4,
                "exp_year": 2018,
                "fingerprint": "tiDP36QdYA6X7km8",
                "funding": "unknown",
                "last4": "1111",
                "metadata": {
                },
                "name": "Harold Hamburger",
                "tokenization_method": nil
              }
            ],
            "has_more": false,
            "total_count": 1,
            "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/sources"
          },
          "subscriptions": {
            "object": "list",
            "data": [

            ],
            "has_more": false,
            "total_count": 0,
            "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/subscriptions"
          }
        }.to_json
      )
    stub_request(:post, "https://api.stripe.com/v1/customers").
      with(body: hash_including({
        source: "iii" # checking for bad token
      })).
      to_return(
        status: 422,
        body: {
          "error": {
            "type": "invalid_request_error",
            "message": "No such token: iii",
            "param": "source"
          }
        }.to_json
      )
    stub_request(:get, "https://api.stripe.com/v1/customers/cus_AaJZ7G7fGdVwrz")
        .to_return(
            status: 200,
            body: {
                "id": "cus_AaJZ7G7fGdVwrz",
                "object": "customer",
                "account_balance": 0,
                "created": 1497617688,
                "currency": "usd",
                "default_source": "card_1AF8w42eZvKYlo2CMvrGfQuL",
                "delinquent": false,
                "description": "Demo Applicant - Card 3",
                "discount": nil,
                "email": nil,
                "livemode": false,
                "metadata": {
                },
                "shipping": nil,
                "sources": {
                    "object": "list",
                    "data": [
                        {
                            "id": "card_1AF8w42eZvKYlo2CMvrGfQuL",
                            "object": "card",
                            "address_city": nil,
                            "address_country": nil,
                            "address_line1": nil,
                            "address_line1_check": nil,
                            "address_line2": nil,
                            "address_state": nil,
                            "address_zip": "77777",
                            "address_zip_check": "pass",
                            "brand": "Visa",
                            "country": "US",
                            "customer": "cus_AaJZ7G7fGdVwrz",
                            "cvc_check": "pass",
                            "dynamic_last4": nil,
                            "exp_month": 4,
                            "exp_year": 2018,
                            "fingerprint": "Xt5EWLLDS7FJjR1c",
                            "funding": "credit",
                            "last4": "4242",
                            "metadata": {
                            },
                            "name": nil,
                            "tokenization_method": nil
                        }
                    ],
                    "has_more": false,
                    "total_count": 1,
                    "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/sources"
                },
                "subscriptions": {
                    "object": "list",
                    "data": [

                    ],
                    "has_more": false,
                    "total_count": 0,
                    "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/subscriptions"
                }
            }.to_json
        )
    stub_request(:delete, "https://api.stripe.com/v1/customers/cus_AaJZ7G7fGdVwrz")
        .to_return(status: 200)
    stub_request(:post, "https://api.stripe.com/v1/customers")
      .with(body: hash_including({ source: "tok_visa" }))
      .to_return(
        status: 200,
        body: {
          "id": "cus_AaJZ7G7fGdVwrz",
          "object": "customer",
          "account_balance": 0,
          "created": 1493762011,
          "currency": "gbp",
          "default_source": "card_1AF8w42eZvKYlo2CMvrGfQuL",
          "delinquent": false,
          "description": nil,
          "discount": nil,
          "email": "tests+1493762008@showclix.com",
          "livemode": false,
          "metadata": {
          },
          "shipping": nil,
          "sources": {
            "object": "list",
            "data": [
              {
                "id": "card_1AF8w42eZvKYlo2CMvrGfQuL",
                "object": "card",
                "address_city": "Pittsburgh",
                "address_country": "US",
                "address_line1": "414 Grant St",
                "address_line1_check": "pass",
                "address_line2": "",
                "address_state": "PA",
                "address_zip": "15219",
                "address_zip_check": "pass",
                "brand": "Visa",
                "country": "US",
                "customer": "cus_AaJZ7G7fGdVwrz",
                "cvc_check": "pass",
                "dynamic_last4": nil,
                "exp_month": 4,
                "exp_year": 2018,
                "fingerprint": "tiDP36QdYA6X7km8",
                "funding": "unknown",
                "last4": "1111",
                "metadata": {
                },
                "name": "Harold Hamburger",
                "tokenization_method": nil
              }
            ],
            "has_more": false,
            "total_count": 1,
            "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/sources"
          },
          "subscriptions": {
            "object": "list",
            "data": [

            ],
            "has_more": false,
            "total_count": 0,
            "url": "/v1/customers/cus_AaJZ7G7fGdVwrz/subscriptions"
          }
        }.to_json
      )
    stub_request(:post, "https://api.stripe.com/v1/charges")
      .to_return(
        status: 200,
        body: {
          "id": "ch_1AhwlA2eZvKYlo2CtRMYFDKK",
          "object": "charge",
          "amount": 999,
          "balance_transaction": "txn_19XJJ02eZvKYlo2ClwuJ1rbA",
          "created": 1500626720,
          "customer": "cus_8JExO3EezaGADX",
          "description": "Charge successful!"
        }.to_json
      )
  end
end
