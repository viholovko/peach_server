require 'rails_helper'

describe Goal, type: :model do

  let(:goal) { build(:goal) }

  describe 'associations' do
    it { should have_and_belong_to_many(:specialities) }
  end
  describe 'validations' do
    it { should validate_presence_of(:title) }
    ## test for at_least_one_speciality
    # it 'should have least one Goal' do
    #   goal = build :goal, title: "Test", specialities: []
    #   goal.valid?
    #   goal.errors.full_messages.should include("Goal should have at least one Goal.")
    #   expect(goal.goals).to be_blank
    # end

  end
  describe 'methods' do
    it 'should return query with desc order' do
      params = {}
      query = Goal.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "goals"\."id" DESC/i)
    end
    it 'should return query with asc order' do
      params = {
          sort_column: "id",
          sort_type: "asc"
      }
      query = Goal.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "goals"\."id" ASC/i)
    end
    it 'should return count query' do
      params = {count: true}
      query = Goal.search_query(params)
      expect(query.to_sql).to match(/SELECT COUNT\(\*\) FROM "goals".*/i)
    end
    it 'should return query with pagination params' do
      page = 3
      per_page = 5
      query = Goal.search_query({}).take(per_page).skip((page - 1) * per_page)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY .+ DESC LIMIT 5 OFFSET 10/i)
    end
  end
end