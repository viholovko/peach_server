require 'rails_helper'

describe RejectBookingReason, type: :model do
  let(:reject_booking_reason) { build(:reject_booking_reason) }


  describe 'validations' do
    it { should validate_presence_of(:title) }
  end
  describe 'methods' do
    it 'should return query with desc order' do
      params = {}
      query = RejectBookingReason.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "reject_booking_reasons"\."id" DESC/i)
    end
    it 'should return query with asc order' do
      params = {
          sort_column: "id",
          sort_type: "asc"
      }
      query = RejectBookingReason.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "reject_booking_reasons"\."id" ASC/i)
    end
    it 'should return query with where ' do
      params = {
          title:"title"
      }
      query = RejectBookingReason.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ WHERE "reject_booking_reasons"\."title" ILIKE '%#{params[:title]}%' .+ ORDER BY "reject_booking_reasons"\."id" DESC/i)
    end
    it 'should return count query' do
      params = {count: true}
      query = RejectBookingReason.search_query(params)
      expect(query.to_sql).to match(/SELECT COUNT\(\*\) FROM "reject_booking_reasons".*/i)
    end
    it 'should return query with pagination params' do
      page = 3
      per_page = 5
      query = RejectBookingReason.search_query({}).take(per_page).skip((page - 1) * per_page)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY .+ DESC LIMIT 5 OFFSET 10/i)
    end
  end
  describe 'title' do
    it { should_not allow_value("").for(:title) }
    it { should allow_value("title").for(:title) }
  end

end