require 'rails_helper'

describe Booking, type: :model do

  let(:booking) { build(:booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id) }

  describe 'validations' do
    it { expect(booking).to validate_presence_of(:session_price).on(:update) }
    #it { expect(booking).to validate_presence_of(:trainer_id).on(:update) } trainer_id=null validation :methods; methods- use trainer_id it raise
    it { expect(booking).to validate_presence_of(:client_id).on(:update) }
    it { expect(booking).to validate_presence_of(:booking_type).on(:update) }

    # it { expect(booking).to validate_numericality_of(:session_price).is_greater_than_or_equal_to(0).on(:update) }

    #it { expect(booking).to_not allow_value("", nil).for(:trainer_id).on(:update) }
    it { expect(booking).to_not allow_value("", nil).for(:client_id).on(:update) }
    it { expect(booking).to_not allow_value("", nil).for(:booking_type).on(:update) }

    context 'if booking not canceled' do
      before { allow(booking).to receive(:check_status).and_return(false) }
    end
    context 'if booking canceled' do
      before { allow(booking).to receive(:check_status).and_return(true) }
    end
  end

  describe 'associations' do
    it { should belong_to(:client) }
    it { should belong_to(:trainer) }
    it { should have_many(:booking_sessions) }
  end

  describe 'booking types' do
    it 'should correctly set type' do
      expect(booking).to respond_to(:renewable?)
      expect(booking).to respond_to(:once?)

      booking = create :booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id
      booking.renewable!
      expect(booking.renewable?).to be true
      expect(booking.once?).to_not be true

      booking = create :booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id
      booking.once!
      expect(booking.renewable?).to_not be true
      expect(booking.once?).to be true
    end
  end
  describe 'booking status' do
    it 'should correctly set status' do
      expect(booking).to respond_to(:pending?)
      expect(booking).to respond_to(:approved?)
      expect(booking).to respond_to(:rejected?)
      expect(booking).to respond_to(:canceled_by_client?)
      expect(booking).to respond_to(:canceled_by_trainer?)

      booking = create :booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id, status: :pending
      expect(booking.pending?).to be true
      expect(booking.approved?).to_not be true
      expect(booking.rejected?).to_not be true
      expect(booking.canceled_by_client?).to_not be true
      expect(booking.canceled_by_trainer?).to_not be true

      booking = create :booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id
      booking.approved!
      expect(booking.pending?).to_not be true
      expect(booking.approved?).to be true
      expect(booking.rejected?).to_not be true
      expect(booking.canceled_by_client?).to_not be true
      expect(booking.canceled_by_trainer?).to_not be true

      booking = create :booking,
                       trainer_id: create(:user, :trainer).id,
                       client_id: create(:user, :client).id,
                       reject_booking_reasons: [create(:reject_booking_reason)]
      booking.rejected!
      expect(booking.pending?).to_not be true
      expect(booking.approved?).to_not be true
      expect(booking.rejected?).to be true
      expect(booking.canceled_by_client?).to_not be true
      expect(booking.canceled_by_trainer?).to_not be true

      booking = create :booking,
                       trainer_id: create(:user, :trainer).id,
                       client_id: create(:user, :client).id,
                       reject_booking_reasons: [create(:reject_booking_reason)]
      booking.canceled_by_client!
      expect(booking.pending?).to_not be true
      expect(booking.approved?).to_not be true
      expect(booking.rejected?).to_not be true
      expect(booking.canceled_by_client?).to be true
      expect(booking.canceled_by_trainer?).to_not be true

      booking = create :booking,
                       trainer_id: create(:user, :trainer).id,
                       client_id: create(:user, :client).id,
                       reject_booking_reasons: [create(:reject_booking_reason)]
      booking.canceled_by_trainer!
      expect(booking.pending?).to_not be true
      expect(booking.approved?).to_not be true
      expect(booking.rejected?).to_not be true
      expect(booking.canceled_by_client?).to_not be true
      expect(booking.canceled_by_trainer?).to be true
    end
  end

  describe 'booking price' do
    it 'should calculate price of booking' do
      session_price = 123.45
      booking_sessions_attributes = []

      day = Date.tomorrow
      booking_sessions_attributes.push({from_time: (day + 10.hours), to_time: (day + 11.hours)})
      booking_sessions_attributes.push({from_time: (day + 15.hours), to_time: (day + 16.hours)})

      booking = create :booking, trainer_id: create(:user, :trainer).id, client_id: create(:user, :client).id,
                       booking_sessions_attributes: booking_sessions_attributes, session_price: session_price
      booking.approved!

      expect(booking.booking_price).to eq(booking_sessions_attributes.count * session_price)
    end
  end

  describe 'sessions' do
    it 'session can\'t be in past' do
      booking = Booking.new booking_sessions_attributes: [
                           {from_time: Time.now.utc.beginning_of_day - 1.day + 12.hours, to_time: Time.now.utc.beginning_of_day - 1.day + 13.hours }
                       ]

      booking.valid?
      expect(booking.errors[:booking_sessions]).to eq(["can't be in past"])
    end
  end
end