require 'rails_helper'

describe Speciality, type: :model do

  let(:speciality) { build(:speciality) }

  describe 'associations' do
    it { should have_and_belong_to_many(:goals) }
  end
  describe 'validations' do
    it { should validate_presence_of(:title) }

  end
  describe 'methods' do
    it 'should return query with desc order' do
      params = {}
      query = Speciality.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "specialities"\."id" DESC/i)
    end
    it 'should return query with asc order' do
      params = {
          sort_column: "id",
          sort_type: "asc"
      }
      query = Speciality.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "specialities"\."id" ASC/i)
    end
    it 'should return count query' do
      params = {count: true}
      query = Speciality.search_query(params)
      expect(query.to_sql).to match(/SELECT COUNT\(\*\) FROM "specialities".*/i)
    end
    it 'should return query with pagination params' do
      page = 3
      per_page = 5
      query = Speciality.search_query({}).take(per_page).skip((page - 1) * per_page)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY .+ DESC LIMIT 5 OFFSET 10/i)
    end
  end
end