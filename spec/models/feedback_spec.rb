require 'rails_helper'
describe Feedback, type: :model do

  let(:feedback) { create(:feedback) }
  subject{create(:feedback)}

  describe 'validations' do
    context "stars" do
      #it { expect(feedback).to validate_presence_of(:stars) }
      it { should allow_value(4).for(:stars) }
      it { should_not allow_value(-5).for(:stars)}
      it { should_not allow_value(6).for(:stars)}
    end
    context "comment" do
      context "if validate_comment" do
        before { allow(subject).to receive(:negative_stars?).and_return(true) }
        it { should validate_presence_of(:comment) }
      end
      context "if not validate_comment" do
        before { allow(subject).to receive(:negative_stars?).and_return(false) }
        it { should_not validate_presence_of(:comment) }
      end
    end
  end
  describe 'associations' do
    it { should belong_to(:user) }
  end
  describe 'attribute' do
    it{ should respond_to(:stars) }
    it{ should respond_to(:comment) }
  end
end