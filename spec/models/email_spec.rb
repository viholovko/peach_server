require 'rails_helper'
include SessionsHelper

describe EmailSender, type: :model do

  let(:email_sender) { build(:email_sender) }

  describe 'methods' do
    it 'should update system settings' do
      email_sender = EmailSender.first_or_create(
        address: 'smtp.gmail.com',
        port: 587,
        domain: 'example.com',
        authentication: 'plain',
        user_name: 'username',
        password: '123456',
        enable_starttls_auto: true
      )

      smtp_settings = ActionMailer::Base.smtp_settings
      expect(email_sender.address).to eq(smtp_settings[:address])
      expect(email_sender.port).to eq(smtp_settings[:port])
      expect(email_sender.domain).to eq(smtp_settings[:domain])
      expect(email_sender.authentication).to eq(smtp_settings[:authentication])
      expect(email_sender.domain).to eq(smtp_settings[:domain])
      expect(email_sender.user_name).to eq(smtp_settings[:user_name])
      expect(email_sender.password).to eq(smtp_settings[:password])
      expect(email_sender.enable_starttls_auto).to eq(smtp_settings[:enable_starttls_auto])
    end
  end
end
