require 'rails_helper'

describe BookingSession, type: :model do

  let(:booking_session) { build(:booking_session) }

  describe 'validations' do
    it { should validate_presence_of(:from_time) }
    it { should validate_presence_of(:to_time) }
    it { should validate_presence_of(:feeling).on(:update) }

    it { should_not allow_value("", nil).for(:from_time) }
    it { should_not allow_value("", nil).for(:to_time) }

    it { should validate_numericality_of(:rate).is_greater_than_or_equal_to(1).on(:update) }
    it { should validate_numericality_of(:rate).is_less_than_or_equal_to(5).on(:update) }

    it 'from_time_lower_than_finish from_time greater than finish_time' do
      booking_session = build :booking_session, from_time: Time.now + 5.minutes, to_time: Time.now
      booking_session.valid?
      expect(booking_session.errors.full_messages).to include("Start time has to be lower than finish time")
    end

    it 'from_time_lower_than_finish duration not equal 30 minutes' do
      booking_session = build :booking_session, from_time: Time.now, to_time: Time.now + 5.minutes
      booking_session.valid?
      expect(booking_session.errors.full_messages).to include("Duration of training should be equal to 1 hour!")
    end

    it 'should allow only 1 hour' do
      booking_session = build :booking_session, from_time: Time.now, to_time: Time.now + 1.hour
      expect(booking_session.valid?).to eq(true)
    end
  end

  describe 'associations' do
    it { should belong_to(:booking) }
    it { should belong_to(:feeling) }
  end

end