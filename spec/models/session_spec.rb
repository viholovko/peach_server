require 'rails_helper'
include SessionsHelper

describe Session, type: :model do

  let(:session) { build(:session) }

  describe 'methods' do
    it 'should destroy expired' do
      user = create :user, :trainer
      session = Session.create(user_id: user.id)

      session.update_attributes(updated_at: Time.now - 1.month - 1.day)
      Session.destroy_expired

      expect(Session.count).to eq(0)
    end
  end
end
