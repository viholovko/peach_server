require 'rails_helper'


describe User, type: :model do

  let(:user) { create(:user,:admin) }
  
  describe 'validations' do
    context "email" do
      it { should validate_presence_of(:email).with_message("is incorrect") }
      it { should allow_value("dhh@nonopinionated.com").for(:email) }
      it { should_not allow_value("base@example").for(:email)}
      it "uniqueness" do
        expect(user.errors[:email]).to eq([])
      end
      it "not uniqueness" do
        user2=create :user,:admin
        user2.email=user.email
        user2.valid?
        expect(user2.errors[:email]).to eq(["User with this email address is already exists."])
      end
    end
    context "password" do
      context "if validate_password" do
        before { allow(subject).to receive(:validate_password?).and_return(true) }
        it { should validate_presence_of(:password) }
        it { should validate_presence_of(:password_confirmation) }
        it { should validate_confirmation_of(:password) }
      end
      context "if not validate_password" do
        before { allow(subject).to receive(:validate_password?).and_return(false) }
        it { should_not validate_presence_of(:password) }
        it { should_not validate_presence_of(:password_confirmation) }
        it { should_not validate_confirmation_of(:password) }
      end
      it "confirmation" do
        expect(user.valid?).to be
        user.password_confirmation='invalid'
        expect(user.valid?).not_to be
      end
    end
    context "avatar" do
      it { should have_attached_file(:avatar) }
      it { should_not validate_attachment_presence(:avatar) }
      it { should validate_attachment_content_type(:avatar).allowing('image/png', 'image/gif').rejecting('text/plain', 'text/xml') }
    end
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:first_name) }
    context "enum" do
      it { should define_enum_for(:gender).with([:male,
                                                 :female,
                                                 :other]) }
      #it { should define_enum_for(:training_place).with([:mobile,
      #                                                   :gym_based,
      #                                                   :both]) }
      #it { should define_enum_for(:client_gender).with([:client_accept_male,
      #                                                  :client_accept_female,
      #                                                  :client_accept_both]) }

    end
    it "training place should not_be valid" do
      user.training_place="none"
      user.valid?
      expect(user.errors[:base]).to include("none is not a valid training place")
    end
    it "client gender should not be valid" do
      user.client_gender="none"
      user.valid?
      expect(user.errors[:base]).to include("none is not a valid client gender")
    end
  end
  describe 'associations' do
    it { should belong_to(:role) }
    it { should have_many(:sessions).dependent(:destroy) }
    it { should have_many(:credit_cards).dependent(:destroy) }
    it { should have_many(:blockings).with_foreign_key('from_user_id').dependent(:destroy) }
    it { should have_many(:blocked_users).through(:blockings).source(:to_user) }
    it { should have_many(:reverse_blocked_users).with_foreign_key('to_user').class_name('Blocking') }
    it { should have_many(:blocked_by_users).through(:reverse_blocked_users).source(:from_user) }
    it { should have_many(:notifications).dependent(:destroy) }
    it { should have_many(:problem_reports).dependent(:destroy) }
    it { should have_and_belong_to_many(:goals).join_table('goals_clients').with_foreign_key('client_id') }
    it { should have_and_belong_to_many(:specialities).join_table('specialities_trainers').with_foreign_key('trainer_id') }
    it { should have_and_belong_to_many(:training_places) }
  end
  describe 'scope' do
    it{expect(User.default_scoped.to_sql).to match(/SELECT .+ WHERE "users"\."deleted_at" IS NULL/i)}
    it{expect(User.admins.to_sql).to match(/SELECT .+ WHERE "users"\."deleted_at" IS NULL AND "users"\."role_id" = #{Role.admin.id}/i)}
    it{expect(User.where(deleted_at:DateTime.now()).only_deleted.to_sql).to match(/SELECT .+ WHERE \("users"\."deleted_at" IS NOT NULL\)/i)}
    it{expect(User.with_deleted.to_sql).to match(/SELECT .+ FROM /i)}
  end
  describe 'callbacks' do
    it 'should call encrypt_password before save' do
      user = create :user, :trainer
      expect(user).to receive(:encrypt_password)
      user.save!
      expect(user.salt).to_not be_nil
      expect(user.encrypted_password).to_not be_nil
    end
    it 'should call downcase_email before validation' do
      user = create :user, :trainer, email: 'TheTrainer@gmail.com'
      expect(user).to receive(:downcase_email)
      user.save!
      expect(user.email).to eq("thetrainer@gmail.com")
    end
    describe 'should call send_confirmation_email after creation' do
      include ActiveJob::TestHelper

      after do
        clear_enqueued_jobs
      end

      before do
        clear_enqueued_jobs
      end

      it 'for already confirmed users' do
        user = create :user, :trainer, email: 'TheTrainer@gmail.com', email_confirmed: true
        expect{ user.save! }.to change { ActionMailer::Base.deliveries.count }.by(0)
      end
    end
    it 'should call create_qb_account before each saving' do
      user = create :user, :trainer
      expect(user).to receive(:create_qb_account)
      user.save!

      expect(user.salt).to_not be_nil
      expect(user.qb_login).to_not be_nil
      expect(user.qb_password).to_not be_nil
      expect(user.qb_user_id).to_not be_nil
    end
    it 'should call validate_destroy before destroy' do
      user = create :user, :trainer
      expect(user).to receive(:validate_destroy)
      user.really_destroy!
    end
    it 'should call qb_user_id before destroy' do
      user = create :user, :trainer
      create :user, :trainer

      expect(user).to receive(:destroy_qb_account)
      user.really_destroy!
    end
  end
  describe 'attribute' do
    it{ should respond_to(:id) }
    it{ should respond_to(:email) }
    it{ should respond_to(:role_id) }
    it{ should respond_to(:first_name) }
    it{ should respond_to(:last_name) }
    it{ should respond_to(:latitude) }
    it{ should respond_to(:longitude) }
  end
  describe 'methods' do
    it 'destroy'do
      should respond_to(:destroy)
      user.destroy
      expect(User.with_deleted.find(user.id).deleted_at).to_not be_nil
      expect(user.errors[:base]).to_not include("Can not remove last admin.")

      user=create :user,:trainer
      user.destroy
      expect(User.with_deleted.find(user.id).deleted_at).to_not be_nil
      expect(user.errors[:base]).to_not include("Can not remove last user.")
    end
    it 'really_destroy!'do
      should respond_to(:really_destroy!)

      user.role=Role.trainer
      user.save!
      user.really_destroy!
      expect(User.with_deleted.find_by id:user.id).to_not be_nil
      expect(user.errors[:base]).to include("Can not remove last user.")
      user=create :user,:admin

      user.really_destroy!
      expect(User.with_deleted.find_by id:user.id).to_not be_nil
      expect(user.errors[:base]).to include("Can not remove last admin.")

      create :user,:admin
      user.valid?

      user.really_destroy!
      expect(User.with_deleted.find_by id:user.id).to be_nil
      expect(user.errors[:base]).to_not include("Can not remove last admin.")

      user=create :user,:trainer
      user.really_destroy!
      expect(User.with_deleted.find_by id:user.id).to be_nil
      expect(user.errors[:base]).to_not include("Can not remove last user.")
    end
    it 'training_place=' do
      should respond_to(:training_place=)
      user.training_place='mobile'
      expect(user.instance_variable_get(:@training_place_backup)).to be_nil

      user.training_place='invalid'
      expect(user.instance_variable_get(:@training_place_backup)).to eq("invalid")
    end
    it 'client_gender=' do
      should respond_to(:client_gender=)

      user.client_gender='client_accept_male'
      expect(user.instance_variable_get(:@client_gender_backup)).to be_nil

      user.client_gender='invalid'
      expect(user.instance_variable_get(:@client_gender_backup)).to eq("invalid")
    end
    it 'authenticate'do
      should respond_to(:authenticate)
      user.password='123'
      user.password_confirmation='123'

      user.save!
      expect(user.authenticate(user.password)).to be
      expect(user.encrypted_password).to eq(user.send(:encrypt,user.password))
    end
    it 'has_credit_card'do
      should respond_to(:has_credit_card)
      expect(user.has_credit_card).to_not be
    end
    it 'has_location'do
      should respond_to(:has_location)
      expect(user.has_location).to_not be
      user.latitude=1
      user.longitude=1
      user.location_radius=1
      expect(user.has_location).to be
    end
    it 'full_name'do
      should respond_to(:full_name)
      expect(user.full_name).to eq([user.first_name, user.last_name].compact.join(' '))
    end
    it 'to_json'do
      should respond_to(:to_json)
      expect(user.to_json).to include_json({
                                          id: user.id,
                                          email: user.email,
                                          first_name: user.first_name,
                                          last_name: user.last_name,
                                          gender: user.gender,
                                          role: user.role.name,
                                          avatar: "#{ ENV['HOST'] }#{ user.avatar.try(:url, :medium) }",
                                          qb_session_token: user.qb_token,
                                          qb_login: user.qb_login,
                                          qb_password: user.qb_password,
                                          qb_user_id: user.qb_user_id,
                                          notifications_enabled: user.notifications_enabled,
                                          registration_step: user.registration_step.to_i,
                                          address: user.address,
                                          post_code: user.post_code,
                                          latitude: user.latitude,
                                          longitude: user.longitude,
                                          location_radius: user.location_radius,
                                          training_places: user.training_places.map{|training_place|
                                            {
                                                id: user.training_place.id,
                                                title: user.training_place.title,
                                            }
                                          },
                                          bio: user.bio
                                      })
      user=create :user, :client
      expect(user.to_json).to include_json({
                                              goals: user.goals.map{ |goal|
                                                {
                                                    id: goal.id,
                                                    title: goal.title,
                                                }
                                              },
                                              trainers_gender:  User.genders.key(user.trainers_gender)
                                          })
      user=create :user, :trainer
      expect(user.to_json).to include_json({
                                               specialities: user.specialities.map{|speciality|
                                                 {
                                                     id: speciality.id,
                                                     title: speciality.title,
                                                 }
                                               }
                                           })
    end

    it 'notify'do
      should respond_to(:notify)
      #user.sessions.build device_type:'ios'
      #user.save!
      #expect(APNS).to receive(:send_notification)
      #user.notify
    end
    it 'send_password_reset'do
      should respond_to(:send_password_reset)

      expect{ user.send_password_reset }.to change { ActionMailer::Base.deliveries.count }.by(1)
      expect(User.find(user.id).reset_password_token).to_not be_nil
      expect(ActionMailer::Base.deliveries.last.to).to eq([user.email])

      mail = UserMailer.password_reset(user.id).deliver_now
      expect(mail.subject).to eq('Peach password recovery')
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq([EmailSender.user_name])
    end
    it 'send_email_confirmation_instructions'do
      should respond_to(:send_email_confirmation_instructions)
      user.social = true
      user.save!

      expect{ user.send_email_confirmation_instructions }.to change { ActionMailer::Base.deliveries.count }.by(0)
      expect(User.find(user.id).confirmation_token).to be_nil.or be false

      user.social = false
      user.save!

      expect{ user.send_email_confirmation_instructions }.to change { ActionMailer::Base.deliveries.count }.by(1)
      expect(User.find(user.id).confirmation_token).to_not be_nil
      expect(ActionMailer::Base.deliveries.last.to).to eq([user.email])

      mail = UserMailer.confirmation_instructions(user.id).deliver_now
      expect(mail.subject).to eq("Start your fitness journey with Peach")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq([EmailSender.user_name])
      expect(mail.body.encoded).to match(user.confirmation_token)

    end
    it 'distance_to'do
      should respond_to(:distance_to)
      expect(user.distance_to(user)).to be_nil
      user.latitude=25.2
      user.longitude=20.22
      expect(user.distance_to(user)).to eq(0.0)
    end
    it 'valid_coordinates?'do
      should respond_to(:valid_coordinates?)
      expect(user.valid_coordinates?).to_not be
      user.latitude=25.2
      user.longitude=20.22
      expect(user.valid_coordinates?).to be
    end
    it 'as_client'do
      should respond_to(:as_client)
      expect(user.as_client).to be_nil
      user.role=Role.client
      user.save!
      expect(user.as_client).to_not be_nil
    end
    it 'as_trainer'do
      should respond_to(:as_trainer)
      expect(user.as_client).to be_nil
      user.role=Role.trainer
      user.save!
      expect(user.as_trainer).to_not be_nil
    end
    describe 'block_query' do
      it do
        should respond_to?(:block_query, true)
      end
      it 'blocked_by'do
        params={}
        params[:type] = 'blocked_by'
        query = user.send(:block_query,params).to_sql
        expect(query).to match(/SELECT .+ LEFT OUTER JOIN "blockings" ON "blockings"\."from_user_id" = "users"\."id" LEFT OUTER JOIN "blockings_reasons" ON "blockings"\."id" = "blockings_reasons"\."blocking_id" WHERE "blockings"\."to_user_id" = #{user.id} .+/i)
      end
      it 'blocked'do
        params={}
        params[:type] = 'blocked'
        query = user.send(:block_query,params).to_sql
        expect(query).to match(/SELECT .+ LEFT OUTER JOIN "blockings" ON "blockings"\."to_user_id" = "users"\."id" LEFT OUTER JOIN "blockings_reasons" ON "blockings"\."id" = "blockings_reasons"\."blocking_id" WHERE "blockings"\."from_user_id" = #{user.id} .+/i)
      end
      context 'role'do
        it 'client blocked' do
          params={}
          params[:type] = 'blocked'
          user.role=Role.client
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+array_agg\(block_trainer_reasons\.title\) as block_reasons.+LEFT OUTER JOIN "block_trainer_reasons" ON "block_trainer_reasons"\."id" = "blockings_reasons"\."reason_id".+/i)
        end
        it 'client blocked_by' do
          params={}
          params[:type] = 'blocked_by'
          user.role=Role.client
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+array_agg\(block_client_reasons\.title\) as block_reasons.+LEFT OUTER JOIN "block_client_reasons" ON "block_client_reasons"\."id" = "blockings_reasons"\."reason_id".+/i)
        end
        it 'trainer blocked' do
          params={}
          params[:type] = 'blocked'
          user.role=Role.trainer
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+array_agg\(block_client_reasons\.title\) as block_reasons.+LEFT OUTER JOIN "block_client_reasons" ON "block_client_reasons"\."id" = "blockings_reasons"\."reason_id".+/i)
        end
        it 'trainer blocked_by' do
          params={}
          params[:type] = 'blocked_by'
          user.role=Role.trainer
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+array_agg\(block_trainer_reasons\.title\) as block_reasons.+ LEFT OUTER JOIN "block_trainer_reasons" ON "block_trainer_reasons"\."id" = "blockings_reasons"\."reason_id".+/i)
        end
      end
      context 'sort_column' do
        it 'default' do
          params={}
          params[:type] = 'blocked_by'
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+ORDER BY block_date.*/i)
        end
        it 'block_date' do
          params={
              sort_column:"block_date",
              sort_type:"asc"
          }
          params[:type] = 'blocked_by'
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+ORDER BY "blockings"\."created_at" ASC.*/i)
        end
        it 'collumn' do
          params={
              sort_column:"id",
              sort_type:"asc"
          }
          params[:type] = 'blocked_by'
          query = user.send(:block_query,params).to_sql
          expect(query).to match(/SELECT .+ORDER BY "users"\."id" ASC.*/i)
        end
      end
    end
    it 'blocked_users_with_comment'do
      should respond_to(:blocked_users_with_comment)
      query = user.send(:blocked_users_with_comment).to_sql
      expect(query).to match(/SELECT .+ LEFT OUTER JOIN "blockings" ON "blockings"\."to_user_id" = "users"\."id" LEFT OUTER JOIN "blockings_reasons" ON "blockings"\."id" = "blockings_reasons"\."blocking_id" WHERE "blockings"\."from_user_id" = #{user.id} .+/i)
    end
    it 'blocked_by_users_with_comment'do
      should respond_to(:blocked_by_users_with_comment)
      query = user.send(:blocked_by_users_with_comment).to_sql
      expect(query).to match(/SELECT .+ LEFT OUTER JOIN "blockings" ON "blockings"\."from_user_id" = "users"\."id" LEFT OUTER JOIN "blockings_reasons" ON "blockings"\."id" = "blockings_reasons"\."blocking_id" WHERE "blockings"\."to_user_id" = #{user.id} .+/i)
    end
  end
  describe 'Databse' do
    context 'columns' do
      it{should have_db_column(:id).of_type(:integer).with_options(primary: true)}
      it{should have_db_column(:encrypted_password).of_type(:string)}
      it{should have_db_column(:salt).of_type(:string)}
      it{should have_db_column(:email).of_type(:string)}
      it{should have_db_column(:confirmed).of_type(:boolean)}
      it{should have_db_column(:confirmation_token).of_type(:string)}
      it{should have_db_column(:role_id).of_type(:integer)}
      it{should have_db_column(:qb_token).of_type(:string)}
      it{should have_db_column(:qb_login).of_type(:string)}
      it{should have_db_column(:qb_password).of_type(:string)}
      it{should have_db_column(:qb_user_id).of_type(:integer)}
      it{should have_db_column(:facebook_id).of_type(:string)}
      it{should have_db_column(:social).of_type(:boolean).with_options(deffault: false)}
      it{should have_db_column(:created_at).of_type(:datetime)}
      it{should have_db_column(:updated_at).of_type(:datetime)}
      it{should have_db_column(:avatar_file_name).of_type(:string)}
      it{should have_db_column(:avatar_content_type).of_type(:string)}
      it{should have_db_column(:avatar_file_size).of_type(:integer)}
      it{should have_db_column(:avatar_updated_at).of_type(:datetime)}
      it{should have_db_column(:reset_password_token).of_type(:string)}
      it{should have_db_column(:first_name).of_type(:string)}
      it{should have_db_column(:last_name).of_type(:string)}
      it{should have_db_column(:gender).of_type(:integer)}
      it{should have_db_column(:address).of_type(:string)}
      it{should have_db_column(:post_code).of_type(:string)}
      it{should have_db_column(:latitude).of_type(:float)}
      it{should have_db_column(:longitude).of_type(:float)}
      it{should have_db_column(:location_radius).of_type(:float)}
      it{should have_db_column(:registration_step).of_type(:integer)}
      it{should have_db_column(:bio).of_type(:text)}
      it{should have_db_column(:email_confirmed).of_type(:boolean)}
      it{should have_db_column(:training_place).of_type(:integer)}
      it{should have_db_column(:client_gender).of_type(:integer)}
      it{should have_db_column(:deleted_at).of_type(:datetime)}
      it{should have_db_column(:deactivate_message).of_type(:text)}
      it{should have_db_column(:notifications_enabled).of_type(:boolean)}
      it{should have_db_column(:trainers_gender).of_type(:integer)}
      it{should have_db_column(:qualification).of_type(:text)}
    end
  end
  describe 'roles' do
    it 'should correctly apply to user' do
      expect(user).to respond_to(:admin?)
      expect(user).to respond_to(:trainer?)
      expect(user).to respond_to(:client?)

      user = create :user, :admin
      expect(user.admin?).to be true
      expect(user.trainer?).to_not be true
      expect(user.client?).to_not be true

      user = create :user, :trainer
      expect(user.admin?).to_not be true
      expect(user.client?).to_not be true
      expect(user.trainer?).to be true

      user = create :user, :client
      expect(user.admin?).to_not be true
      expect(user.trainer?).to_not be true
      expect(user.client?).to be true
    end
  end


end