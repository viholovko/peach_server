require 'rails_helper'

describe Notification, type: :model do

  let(:notification) { build(:notification, user: create(:user, :client)) }

  describe 'associations' do
    it { should belong_to(:user) }
  end
end