require 'rails_helper'

describe Admin, type: :model do

  let(:admin) { build(:admin) }
  describe 'methods' do
    it 'should return query with desc order' do
      params = {}
      query = Admin.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "users"\."id" DESC/i)
    end
    it 'should return query with asc order' do
      params = {
          sort_column: "id",
          sort_type: "asc"
      }
      query = Admin.search_query(params)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY "users"\."id" ASC/i)
    end
    it 'should return query with pagination params' do
      page = 3
      per_page = 5
      query = Admin.search_query({}).take(per_page).skip((page - 1) * per_page)
      expect(query.to_sql).to match(/SELECT .+ ORDER BY .+ DESC LIMIT 5 OFFSET 10/i)
    end
    it 'should return query with searching by name' do
      params = {
          name: "Paul"
      }
      query = Admin.search_query(params)
      expect(query.to_sql)
          .to match(/SELECT .+ WHERE .+ \(\"users\"\.\"first_name\" ILIKE \'%Paul%\' OR \"users\"\.\"last_name\" ILIKE \'%Paul%\'\) .+/i)
    end
    it 'should return query with searching by email' do
      params = {
          email: "user@asdasd.com"
      }
      query = Admin.search_query(params)
      expect(query.to_sql)
          .to match(/SELECT .+ WHERE .+ \"users\"\.\"email\" ILIKE \'%user@asdasd.com%\' .+/i)
    end
  end
end