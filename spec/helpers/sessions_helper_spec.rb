require 'rails_helper'

describe SessionsHelper, type: :helper do
  describe 'sign_in' do
    it 'should sign in user' do
      user = create :user, :client
      session = sign_in user: user
      expect(cookies.permanent[:session_token]).to eq(session.token)
      expect(user.sessions.last).to eq session
      expect(Session.find_by_token(cookies.permanent[:session_token])).to eq session
      expect(current_user).to_not be_nil
    end
  end
  describe 'signed_in?' do
    it 'should be signed in user' do
      user = create :user, :client
      sign_in user: user
      expect(signed_in?).to_not eq current_user.nil?
      expect(signed_in?).to be_truthy
    end
  end
  describe 'current_user' do
    it 'current_user must equal to singed is user' do
      user = create :user, :client
      sign_in user: user
      expect(current_user).to eq user
    end
  end
  describe 'current_session' do
    it 'current_session must equal to created user session' do
      user = create :user, :client
      session = sign_in user: user
      expect(current_session).to eq session
    end
  end
  describe 'sign_out' do
    it 'should sign_out user' do
      user = create :user, :client
      sign_in user: user
      sign_out
      expect(current_user).to be_nil
      expect(cookies.permanent[:session_token]).to be_nil
      expect(current_session).to be_destroyed
    end
  end
  describe 'current_user=' do
    it 'should assign new current user' do
      user = create :user, :client
      sign_in user: user
      current_user = user
      expect(current_user).to eq user
    end
  end
end