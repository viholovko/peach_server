class FavoriteTrainersStreamer
  include Enumerable
  require 'csv'
  def initialize(id)
    @id = id
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full Name',
        'Gender',
        'Email',
        'Avatar',
        'Specialities',
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = Client.favorite_trainers(User.with_deleted.find(@id))
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |trainer|
        yield CSV::Row.new([], [
            trainer.id,
            trainer.first_name,
            trainer.gender,
            trainer.email,
            trainer.avatar,
            trainer.specialities.join(', '),
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
