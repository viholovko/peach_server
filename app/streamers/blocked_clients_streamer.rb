class BlockedClientsStreamer
  include Enumerable
  require 'csv'
  def initialize(id)
    @id = id
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full Name',
        'Gender',
        'Email',
        'Block date',
        'Block reasons',
        'Comment'
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = User.with_deleted.find(@id).blocked_users_with_comment
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |client|
        yield CSV::Row.new([], [
            client.id,
            client.full_name,
            client.gender,
            client.email,
            client.block_date.strftime("%d/%m/%Y"),
            client.block_reasons.join(', '),
            client.comment
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
