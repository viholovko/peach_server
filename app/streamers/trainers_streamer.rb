class TrainersStreamer
  include Enumerable
  require 'csv'
  def initialize()
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full Name',
        'Gender',
        'Email',
        'Email Confirmed',
        'Confirmed',
        'QB Token',
        'Social',
        'Avatar',
        'Has Credit Card',
        'Registered At',
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = Trainer.explore_trainers
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |trainer|
        yield CSV::Row.new([], [
            trainer.id,
            trainer.full_name,
            trainer.gender,
            trainer.email,
            trainer.email_confirmed ? true : false,
            trainer.confirmed ? true : false,
            trainer.qb_token,
            trainer.social ? true : false,
            trainer.avatar,
            trainer.has_credit_card ? true : false,
            trainer.created_at.strftime("%d/%m/%Y"),
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
