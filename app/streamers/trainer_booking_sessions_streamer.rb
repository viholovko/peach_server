class TrainerBookingSessionsStreamer
  include Enumerable
  require 'csv'
  def initialize(id)
    @id = id
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full Name',
        'Email',
        'Gender',
        'Status',
        'Type',
        'Price',
        'Sessions Booking',
        'Rate'
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = Trainer.with_deleted.find(@id).my_bookings
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |user|
        yield CSV::Row.new([], [
            user.id,
            user.full_name,
            user.email,
            user.gender,
            Booking.statuses.key(user['status']),
            Booking.booking_types.key(user['booking_type']),
            user['session_price'],
            DateTime.parse(user['booking_sessions'][0]['from_time']).try(:strftime, "%a %d/%m/%Y %H:%M - ") +
                DateTime.parse(user['booking_sessions'][0]['to_time']).try(:strftime, "%H:%M"),
            user['booking_sessions'][0]['rate']
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
