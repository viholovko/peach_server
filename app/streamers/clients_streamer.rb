class ClientsStreamer
  include Enumerable
  require 'csv'
  def initialize()
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full Name',
        'Gender',
        'Email',
        'Email Confirmed',
        'QB Token',
        'Social',
        'Avatar',
        'Has Credit Card',
        'Registered At',
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = Client.search_query
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |client|
        yield CSV::Row.new([], [
            client.id,
            client.full_name,
            client.gender,
            client.email,
            client.email_confirmed ? true : false,
            client.qb_token,
            client.social ? true : false,
            client.avatar,
            client.has_credit_card ? true : false,
            client.created_at.strftime("%d/%m/%Y"),
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end