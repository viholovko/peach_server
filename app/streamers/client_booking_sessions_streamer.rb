class ClientBookingSessionsStreamer
  include Enumerable
  require 'csv'
  def initialize(id)
    @id = id
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Booking ID',
        'Booking type',
        'Session price',
        'Status',
        'From time',
        'Finish time',
        'Trainer',
        'Session rating',
        'Rate',
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = Client.booking_sessions(User.with_deleted.find(@id))
    offset = 0
    limit = 1000
    results = BookingSession.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = BookingSession.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |session|
        yield CSV::Row.new([], [
            session.id,
            session.booking_id,
            Booking.booking_types.key(session['booking_type']),
            session.session_price,
            Booking.statuses.key(session['status']),
            session.from_time.strftime('%a, %m/%d/%Y %H:%M'),
            session.to_time.strftime('%a, %m/%d/%Y %H:%M'),
            Trainer.with_deleted.find_by_id(session['trainer_id']).full_name,
            session.title,
            session.rate,
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
