class DeactivatedClientsStreamer
  include Enumerable
  require 'csv'
  def initialize()
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full name',
        'Email',
        'Reason',
        'Deactivated at'
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    params = {:show_deleted => true}
    query = Client.search_query(params)
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |client|
        yield CSV::Row.new([], [
            client.id,
            client.full_name,
            client.email,
            client.deactivate_message,
            client.deleted_at.strftime("%d/%m/%Y"),
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end