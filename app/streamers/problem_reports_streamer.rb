class ProblemReportsStreamer
  include Enumerable
  require 'csv'
  def initialize()
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Comment',
        'Reason',
        'Created At',
        'User ID',
        'Full Name',
        'Role',
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    query = ProblemReport.search_query
    offset = 0
    limit = 1000
    results = ProblemReport.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = ProblemReport.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |problem_report|
        yield CSV::Row.new([], [
          problem_report.id,
          problem_report.comment,
          problem_report.title,
          problem_report.created_at.try(:strftime, "%d/%m/%Y"),
          problem_report.user_id,
          [problem_report.first_name, problem_report.last_name].compact.join(' '),
          problem_report.name,
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
