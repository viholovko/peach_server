class DeactivatedTrainersStreamer
  include Enumerable
  require 'csv'
  def initialize()
  end
  def each
    yield CSV::Row.new([], [
        'Id',
        'Full name',
        'Email',
        'Reason',
        'Deactivated At'
    ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
    params = {:show_deleted => true}
    query = Trainer.explore_trainers(params)
    offset = 0
    limit = 1000
    results = User.find_by_sql(query.take(limit).skip(offset))
    while results.size > 0
      results = User.find_by_sql(query.take(limit).skip(offset))
      offset += limit
      results.each do |trainer|
        yield CSV::Row.new([], [
            trainer.id,
            trainer.full_name,
            trainer.email,
            trainer.deactivate_message,
            trainer.deleted_at.strftime("%d/%m/%Y"),
        ], true).to_csv(col_sep: ",", row_sep: "\r\n", quote_char: "\"")
      end
    end
  end
end
