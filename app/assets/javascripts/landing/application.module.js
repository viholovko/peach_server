(function () {
    "use strict";
    var PeachServerLandingApp = angular.module('PeachServerLandingApp', [
        'ui.router',
        'templates',
        'ngDialog',
        'validation.email',
        'ui.bootstrap',
        'formInput.images',
        'formInput.image',
        'toaster',
        'AuthHttp'
    ]);

    PeachServerLandingApp.config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
        function ($urlRouterProvider, $stateProvider, $httpProvider) {

            $httpProvider.defaults.headers.common['X-Requested-With'] = 'AngularXMLHttpRequest';

            $urlRouterProvider.otherwise('login');

            $stateProvider
                .state('login',{
                  url: '/login',
                  templateUrl: 'landing/templates/sessions/new.html',
                  controller: 'SessionsController'
                })
                .state('forgot_password',{
                  url: '/forgot_password',
                  templateUrl: 'landing/templates/passwords/new.html',
                  controller: 'PasswordsController'
                })
                .state('restore_password',{
                    url: '/restore_password/:token',
                    templateUrl: 'landing/templates/passwords/restore.html',
                    controller: 'PasswordsController'
                })
    }]);

    PeachServerLandingApp.run(['$http', '$rootScope', function($http, $rootScope){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        $http.defaults.headers.common['X-CSRF-Token'] = csrf_token;
    }]);

}());