(function () {
    'use strict';
    angular.module('PeachServerLandingApp').factory('PasswordsFactory', ['AuthHttp', '$location', function($http, $location){
        return {
            forgot: function(email){
                return $http.post('/password_resets?email=' + email);
            },
            reset: function(password, password_confirmation, token){
                return $http.put('/password_resets/' + token + '?password=' + (password ? password : '') + '&password_confirmation=' + (password_confirmation ? password_confirmation : ''));
            }
        }
    }])
}());
