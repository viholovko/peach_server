(function () {
    'use strict';
    angular.module('PeachServerLandingApp').factory('SessionsFactory', ['AuthHttp', function($http){
        return {
            check: function(){
                return $http.get('/admin/sessions/check');
            },
            login: function(session){
                return $http.post('/admin/sessions', {
                    email: session.email,
                    password: session.password
                })
            }
        }
    }])
}());