(function () {

    "use strict";

    angular.module('PeachServerLandingApp')
        .controller('PasswordsController', ['$scope', '$state', 'ngDialog', '$stateParams', 'SessionsFactory', 'PasswordsFactory',
            function ($scope, $state, ngDialog, $stateParams, sessions, passwords) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;

                $scope.submitForgotPassword = function(){
                    $scope.submitted = true;

                    $scope.formPending = true;
                    passwords.forgot($scope.email)
                        .success(function(data){
                            $scope.formPending = false;
                            $state.go('login');
                        })
                        .error(function(data){
                            $scope.formPending = false;
                        })
                };

                $scope.submitResetPassword = function(){
                    $scope.submitted = true;

                    $scope.formPending = true;
                    passwords.reset($scope.password, $scope.password_confirmation, $stateParams.token)
                        .success(function(data){
                            $scope.formPending = false;
                            window.location = '/password_restored';
                        })
                        .error(function(data){
                            $scope.formPending = false;
                        })
                };
            }])
}());