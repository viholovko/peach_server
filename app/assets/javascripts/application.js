//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require bootstrap-sprockets
//= require angular
//= require angular-rails-templates
//= require angular-animate/angular-animate
//= require angular-ui-bootstrap/dist/ui-bootstrap
//= require angular-ui-bootstrap/dist/ui-bootstrap-tpls
//= require sweetalert/dist/sweetalert.min
//= require angular-sweetalert/SweetAlert
//= require angular-ui-router
//= require ng-dialog/js/ngDialog
//= require ngmap/build/scripts/ng-map.min.js
//= require angular-input-match
//= require angular-email-available
//= require angular-redactor.directive
//= require angular-images.directive
//= require angular-image.directive
//= require angular-file.directive
//= require toastr
//= require underscore
//= require moment/moment
//= require i18n
//= require i18n/translations
//= require redactor/redactor
//= require redactor/plugins/fontsize
//= require redactor/plugins/table
//= require redactor/plugins/fontcolor
//= require redactor/plugins/fontfamily
//= require twbs-pagination.js
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require peity/jquery.peity.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js
//= require scrollspy
//= require spin
//= require ladda
//= require angular-ladda
//= require angular-auth-http.service
//= require bootstrap-ui-datetime-picker/dist/datetime-picker.min.js
// load angular modules
//= require_three directives
//= require ./landing/application.module.js
//= require ./application/application.module.js
//= require_tree ./application/factories
//= require_tree ./application/controllers
//= require_tree ./application/templates
//= require_tree .

$(document).ready(function(){
    $('#side-menu').slimScroll({
        height: '100%'
    })
})