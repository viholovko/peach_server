(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('ProblemReportReasonsController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'ProblemReportReasonsFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, problem_report_reasons, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'problem_report_reasons') {

                    $scope.filters = {};

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveProblemReportReasons();
                        }, 500)
                    }, true);

                    $scope.retrieveProblemReportReasons = function () {
                        problem_report_reasons.all({query: $scope.filters}).success(function (data) {
                            $scope.problem_report_reasons = data.problem_report_reasons;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveProblemReportReasons();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this reason!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                problem_report_reasons.destroy(id).success(function(){
                                    $scope.retrieveProblemReportReasons();
                                    SweetAlert.swal("Deleted!", "Your reason has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your reason is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_problem_report_reason' || $state.current.name == 'edit_problem_report_reason'){
                    $scope.validation_errors = {};
                    $scope.problem_report_reason = {};

                    if($state.current.name == 'edit_problem_report_reason'){
                        problem_report_reasons.show($stateParams.id).success(function(data){
                            $scope.problem_report_reason = data.problem_report_reason;
                        })
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        problem_report_reasons.upsert($scope.problem_report_reason)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('problem_report_reasons')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());
