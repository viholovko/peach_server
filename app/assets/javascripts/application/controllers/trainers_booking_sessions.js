(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('TrainerBookingSessionsController', ['$scope', '$state', '$stateParams', '$timeout', 'TrainersFactory',
            function ($scope, $state, $stateParams, $timeout, trainer) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;

                $scope.resetFilters = function(){
                    $scope.total=0;
                    $scope.filters = {
                        page:0,
                        per_page: 10,
                        sort_column:'from_time',
                        sort_type:'desc'
                    };
                };
                $scope.resetFilters();

                var timer = false;
                $scope.$watch('filters', function () {
                    if (timer) {
                        $timeout.cancel(timer)
                    }
                    timer = $timeout(function () {
                        $scope.bookings_load();
                    }, 500)
                }, true);

                $scope.$watch('filters.date_from', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.date_to', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.name', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.email', function () {
                    $scope.filters.page = 1
                }, true);

                $scope.downloadCSV = function(){
                    trainer.downloadBookingSessionsCSV($stateParams.id);
                };
                $scope.bookings_load = function(){
                    trainer.bookings({ id: $stateParams.id, query: $scope.filters}).success(function (data) {
                        $scope.bookings = data.bookings;
                        $scope.total = data.total;
                        $scope.bookings.forEach(function(item){
                          item.day = moment(item.from_time).format("MMM Do YY");
                          item.from_time = moment(item.from_time).format('h:mm a');
                          item.to_time = moment(item.to_time).format('h:mm a');
                        })
                    })

                };

                $scope.calendar={
                    format: "dd MMM yyyy HH:mm",
                    isOpen1:false,
                    isOpen2:false,
                    open1:function () {
                        $scope.calendar.isOpen1 = true;
                    },
                    open2:function () {
                        $scope.calendar.isOpen2 = true;
                    }
                }

            }])
}());
