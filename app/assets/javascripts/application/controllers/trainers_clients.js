(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('TrainerClientsController', ['$scope', '$state', '$stateParams', '$timeout', 'TrainersFactory',
            function ($scope, $state, $stateParams, $timeout, trainer) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;

                $scope.resetFilters = function(){
                    $scope.total=0;
                    $scope.filters = {
                        page:0,
                        per_page: 10
                    };
                };
                $scope.resetFilters();

                var timer = false;
                $scope.$watch('filters', function () {
                  if (timer) {
                        $timeout.cancel(timer)
                    }
                    timer = $timeout(function () {
                        $scope.blocked_users();
                    }, 500)
                }, true);

                $scope.$watch('filters.first_name', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.email', function () {
                    $scope.filters.page = 1
                }, true);

                $scope.downloadCSV = function(){
                    trainer.downloadClientsCSV($stateParams.id);
                };
                $scope.blocked_users = function(){
                    trainer.clients({ id: $stateParams.id, query: $scope.filters})
                        .success(function(data){
                            $scope.users = data.users;
                            $scope.total = data.total;
                        });
                };
            }])
}());
