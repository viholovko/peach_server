(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('SystemSettingsController', ['$scope', '$state', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'SystemSettingsFactory',
            function ($scope, $state, $stateParams, $timeout, $sce, SweetAlert, system_settings) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'system_pages';

                $scope.system_settings = {};
                $scope.validation_errors = {};

                system_settings.show()
                    .success(function(data){
                            $scope.system_settings = data.system_settings;
                    }
                );

                $scope.submit = function(){
                    $scope.submitted = true;

                    $scope.formPending = true;
                    system_settings.update($scope.system_settings)
                        .success(function(){
                            $scope.formPending = false;
                            $scope.pending = false;
                            $state.reload();
                        })
                        .error(function(data){
                            $scope.validation_errors = data.validation_errors;
                            $scope.formPending = false;
                        })
                };
            }])
}());
