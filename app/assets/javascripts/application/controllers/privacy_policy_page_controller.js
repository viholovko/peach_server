(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('PrivacyPolicyPageController', ['$scope', '$state', 'ngDialog', 'PrivacyPolicyPagesFactory',
            function ($scope, $state, ngDialog, privacy_policy_pages) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$parent.state = 'state_pages';

                $scope.privacy_policy_page = {};

                privacy_policy_pages.show()
                    .success(function(data){
                        $scope.privacy_policy_page = data.page;
                    }
                )

                $scope.submit = function(){

                    $scope.formPending = true;
                    privacy_policy_pages.upsert($scope.privacy_policy_page)
                        .success(function(){
                            $scope.formPending = false;
                        })
                        .error(function(data){
                            $scope.validation_errors = data.errors;
                            $scope.formPending = false;
                        })
                };
            }])

}());