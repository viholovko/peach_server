(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('ClientsBlockedController', ['$scope', '$state','$stateParams', '$timeout', 'ClientsFactory',
            function ($scope, $state, $stateParams, $timeout, users) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;

                $scope.resetFilters = function(){
                    $scope.total=0;
                    $scope.filters = {
                        page:0,
                        per_page: 10
                    };
                };
                $scope.resetFilters();

                var timer = false;
                $scope.$watch('filters', function () {
                    if (timer) {
                        $timeout.cancel(timer)
                    }
                    timer = $timeout(function () {
                        $scope.blocked_users();
                    }, 500)
                }, true);

                $scope.$watch('filters.first_name', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.email', function () {
                    $scope.filters.page = 1
                }, true);

                $scope.blocked_users = function(){
                    users.blocked_trainers({ id: $stateParams.id, query: $scope.filters})
                        .success(function(data){
                            $scope.users = data.users;
                            $scope.total = data.total;
                        });
                };

                $scope.unblock_trainer = function(trainer_id){
                    var options = {id: $stateParams.id, trainer_id: trainer_id};
                    users.unblock_trainer(options)
                        .success(function(data){
                            $scope.blocked_users();
                        });
                }
            }])
}());