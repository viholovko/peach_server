(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('HomeController', ['$scope', '$state', 'ngDialog', 'SessionsFactory', '$timeout', 'toaster', 'AdminNotificationFactory', '$interval',
            function ($scope, $state, ngDialog, session, $timeout, toaster, adminNotifications, $interval) {

            $scope.I18n = I18n;
            $scope.$state = $state;

            $scope.checkSession = function(){
                session.check()
                        .success(function(data){
                            $scope.current_user = data.current_user;
                        })
                        .error(function(){
                            $scope.current_user = false;
                        });
            };
            if($state.current.name != 'login'){
                $scope.checkSession();
            }

            $scope.$state = $state;

            $scope.logout = function(){
                session.logout().success(function(){
                    window.location = '/'
                })
            };

            $scope.notificationClick = function(notification){
                if (notification.readed == false)
                    adminNotifications.mark_as_readed(notification.id);
                if (notification.url != null)
                    window.location.href = notification.url;

            };

            $scope.options = {
                page: 1,
                per_page: 10
            };

            $scope.getNotifications = function() {
                adminNotifications.all({page: $scope.options.page, query: {}}).success(function(data){
                    $scope.notifications = data.notifications;
                    $scope.unread_count = data.unread_count;
                })
            };
            $interval(function(){$scope.getNotifications()}, 30000);

            $timeout(function(){
                if($scope.flash.error.length > 0){
                    toaster.pop('error', "", $scope.flash.error);
                }
                if($scope.flash.message.length > 0){
                    toaster.pop('success', "", $scope.flash.message);
                }
            }, 1000);

            $scope.getNotifications();

            $scope.changeLanguage = function(locale){
                I18n.locale = locale;
            }
        }])
}());