(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('SpecialitiesController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'SpecialitiesFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, specialities, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'specialities') {

                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveSpecialities();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveSpecialities = function () {
                        specialities.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.specialities = data.specialities;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveSpecialities();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this speciality!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                specialities.destroy(id).success(function(){
                                    $scope.retrieveSpecialities();
                                    SweetAlert.swal("Deleted!", "Your speciality has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your speciality is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_speciality' || $state.current.name == 'edit_speciality'){
                    $scope.validation_errors = {};
                    $scope.speciality = {};

                    if($state.current.name == 'edit_speciality'){
                        specialities.show($stateParams.id)
                            .success(function(data){
                                    $scope.speciality = data.speciality;
                                }
                            )
                    }
                    if($state.current.name == 'new_speciality'){
                        specialities.new()
                            .success(function(data){
                                    $scope.speciality = data.speciality;
                                }
                            )
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        specialities.upsert($scope.speciality)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('specialities')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());