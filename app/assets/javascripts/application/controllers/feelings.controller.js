(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('FeelingsController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'FeelingsFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, feelings, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'feelings') {

                    $scope.filters = {};

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveFeelings();
                        }, 500)
                    }, true);

                    $scope.retrieveFeelings = function () {
                        feelings.all({query: $scope.filters}).success(function (data) {
                            $scope.feelings = data.feelings;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveFeelings();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this session rating!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                feelings.destroy(id).success(function(){
                                    $scope.retrieveFeelings();
                                    SweetAlert.swal("Deleted!", "Your session ratings has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your session rating is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_feeling' || $state.current.name == 'edit_feeling'){
                    $scope.validation_errors = {};
                    $scope.feeling = {};

                    if($state.current.name == 'edit_feeling'){
                        feelings.show($stateParams.id).success(function(data){
                            $scope.feeling = data.feeling;
                        })
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        feelings.upsert($scope.feeling)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('feelings')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());