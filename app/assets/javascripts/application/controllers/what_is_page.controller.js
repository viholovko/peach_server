(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('WhatIsPageController', ['$scope', '$state', 'WhatIsPagesFactory',
            function ($scope, $state, what_is_pages) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$parent.state = 'state_pages';

                $scope.what_is_page = {};


                what_is_pages.show()
                    .success(function(data){
                        $scope.what_is_page = data.page;
                    }
                )

                $scope.submitWhatIsPage = function(){

                    $scope.formPending = true;
                    what_is_pages.upsert($scope.what_is_page)
                        .success(function(){
                            $scope.formPending = false;
                        })
                        .error(function(data){
                            $scope.validation_errors = data.errors;
                            $scope.formPending = false;
                        })
                };
            }])
}());