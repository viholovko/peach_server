(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('ClientsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'ClientsFactory','$window',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, users, window) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'clients';

                if($state.current.name == 'clients') {

                    $scope.clients = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            page:0,
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    $scope.$watch('filters.name', function () {
                        $scope.filters.page = 1
                    }, true);
                    $scope.$watch('filters.email', function () {
                        $scope.filters.page = 1
                    }, true);

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveUsers();
                        }, 500)
                    }, true);

                    $scope.downloadCSV = function(){

                        users.downloadCSV();
                    };

                    $scope.retrieveUsers = function () {
                        users.all({query: $scope.filters}).success(function (data) {
                            $scope.users = data.clients;
                            $scope.total = data.total;
                        });
                    };

                }

                if($state.current.name == 'show_client') {
                    var circle = null;
                    $scope.client = {};
                    $scope.defaultLat = 51.513;
                    $scope.defaultLng = -0.1;

                    $scope.$on('mapInitialized', function(evt, evtMap) {
                        map = evtMap;
                        users.show($stateParams.id)
                          .success(function(data){
                            $scope.client = data.client;
                            if ($scope.client.has_location == true){
                              $scope.defaultLat = parseFloat($scope.client.latitude);
                              $scope.defaultLng = parseFloat($scope.client.longitude);
                              circle = new google.maps.Circle({
                                map: map,
                                center: {lat: parseFloat($scope.client.latitude), lng: parseFloat($scope.client.longitude)},
                                radius: $scope.client.location_radius,
                                editable: false,
                                strokeColor: '#FF0099',
                                strokeOpacity: 1,
                                strokeWeight: 2,
                                fillColor: '#009ee0',
                                fillOpacity: 0.2
                              });

                              var bounds = new google.maps.LatLngBounds();

                              bounds.extend(circle.getBounds().getNorthEast());
                              bounds.extend(circle.getBounds().getSouthWest());

                              map.fitBounds(bounds);
                            }
                            window.dispatchEvent(new Event('resize'));
                          });
                    });

                    $scope.onTabSelected = function(index) {
                        var route;
                        switch (index) {
                            case 1:
                                route = 'show_client.blocked_trainers';
                                break;
                            case 2:
                                route = 'show_client.blocked_by_trainers';
                                break;
                            case 3:
                                route = 'show_client.favorite_trainers';
                                break;
                            case 4:
                                route = 'show_client.booking_sessions';
                                break;
                        }
                        $state.go(route)
                    };

                    $scope.downloadCSV = function(tab, id) {
                        users.downloadCSV(tab, id);
                    };

                    $scope.$on("$destroy", function() {
                        map = null;
                        if(circle!= null)
                            circle.setMap(null);
                    });
                }

                if($state.current.name == 'edit_client'){
                    var circle = null;
                    $scope.client = {};
                    $scope.defaultLat = 51.513;
                    $scope.defaultLng = -0.1;

                    $scope.update = function(){
                        $scope.submitted = true;
                        $scope.formPending = true;
                        users.upsert($scope.client)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('clients')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };

                    var map;
                    $scope.$on('mapInitialized', function(evt, evtMap) {
                        map = evtMap;

                        users.show($stateParams.id)
                            .success(function(data){
                                    $scope.client = data.client;

                                if ($scope.client.has_location == true){
                                    $scope.defaultLat = parseFloat($scope.client.latitude);
                                    $scope.defaultLng = parseFloat($scope.client.longitude);
                                    circle = new google.maps.Circle({
                                        map: map,
                                        center: {lat: parseFloat($scope.client.latitude), lng: parseFloat($scope.client.longitude)},
                                        radius: $scope.client.location_radius,
                                        editable: true,
                                        strokeColor: '#FF0099',
                                        strokeOpacity: 1,
                                        strokeWeight: 2,
                                        fillColor: '#009ee0',
                                        fillOpacity: 0.2
                                    });
                                    circle.addListener('click', function () {
                                        circle.setMap(null);
                                    });
                                    circle.addListener('radius_changed', function () {
                                        $scope.$apply(function(){$scope.client.location_radius = circle.getRadius();})
                                    });
                                    circle.addListener('center_changed', function () {
                                        $scope.$apply(function(){
                                            $scope.client.latitude = circle.getCenter().lat();
                                            $scope.client.longitude = circle.getCenter().lng();
                                        })
                                    });
                                }
                                }
                            );
                    });

                    $scope.addMarker = function(event) {
                       if (circle != null)
                           circle.setMap(null);

                       circle = new google.maps.Circle({
                                map: map,
                                center: event.latLng,
                                radius: 400,
                                editable: true,
                                strokeColor: '#FF0099',
                                strokeOpacity: 1,
                                strokeWeight: 2,
                                fillColor: '#009ee0',
                                fillOpacity: 0.2
                            });
                        $scope.client.latitude = circle.getCenter().lat();
                        $scope.client.longitude = circle.getCenter().lng();
                        $scope.client.location_radius = circle.getRadius();

                        circle.addListener('click', function () {
                            circle.setMap(null);
                            });

                        circle.addListener('radius_changed', function () {
                            $scope.$apply(function(){$scope.client.location_radius = circle.getRadius();})
                        });

                        circle.addListener('center_changed', function () {
                            $scope.$apply(function(){
                                $scope.client.latitude = circle.getCenter().lat();
                                $scope.client.longitude = circle.getCenter().lng();
                            })
                        });
                    };

                    $scope.$on("$destroy", function() {
                        map = null;
                        if(circle!= null)
                            circle.setMap(null);
                    });
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this user!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                users.destroy(id).success(function(){
                                    $scope.retrieveUsers();
                                    SweetAlert.swal("Deleted!", "Your user has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your user is safe :)", "error");
                            }
                        }
                    );
                };
            }])
}());