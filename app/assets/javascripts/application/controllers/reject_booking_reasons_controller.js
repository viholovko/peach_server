(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('RejectBookingReasonsController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'RejectBookingReasonsFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, reject_booking_reasons, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'reject_booking_reasons') {

                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveRejectBookingReasons();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveRejectBookingReasons = function () {
                        reject_booking_reasons.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.reject_booking_reasons = data.reject_booking_reasons;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveRejectBookingReasons();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this reject booking reason!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                reject_booking_reasons.destroy(id).success(function(){
                                    $scope.retrieveRejectBookingReasons();
                                    SweetAlert.swal("Deleted!", "Your reason has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your reason is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_reject_booking_reason' || $state.current.name == 'edit_reject_booking_reason'){
                    $scope.validation_errors = {};
                    $scope.reject_booking_reason = {};

                    if($state.current.name == 'edit_reject_booking_reason'){
                        reject_booking_reasons.show($stateParams.id)
                            .success(function(data){
                                    $scope.reject_booking_reason = data.reject_booking_reason;
                                }
                            )
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        reject_booking_reasons.upsert($scope.reject_booking_reason)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('reject_booking_reasons')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());