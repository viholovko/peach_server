(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('TrainersController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce',
          'SweetAlert', 'TrainersFactory', '$window',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, trainer, window) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'trainers';

                if($state.current.name == 'trainers') {

                    $scope.trainers = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            page:0,
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    $scope.$watch('filters.name', function () {
                        $scope.filters.page = 1
                    }, true);
                    $scope.$watch('filters.email', function () {
                        $scope.filters.page = 1
                    }, true);
                    $scope.$watch('filters.show_all', function () {
                        $scope.filters.page = 1
                    }, true);

                    var timer = false;
                    $scope.$watch('filters', function () {
                         if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveTrainers();
                        }, 500)
                    }, true);

                    $scope.downloadCSV = function(){

                        trainer.downloadCSV();
                    };

                    $scope.confirm = function(id){
                        trainer.confirm_account(id)
                            .success(function(){
                            $scope.formPending = false;
                            $scope.retrieveTrainers();
                        });
                    };

                    $scope.retrieveTrainers = function () {
                        trainer.all({query: $scope.filters}).success(function (data) {
                            $scope.trainers = data.trainers;
                            $scope.total = data.total;
                        });
                    };

                }

                if($state.current.name == 'show_trainer') {
                    var circle = null;
                    $scope.trainer = {};
                    $scope.defaultLat = 51.513;
                    $scope.defaultLng = -0.1;

                    $scope.$on('mapInitialized', function(evt, evtMap) {
                        map = evtMap;
                        trainer.show($stateParams.id)
                          .success(function(data){
                            $scope.trainer = data.trainer;
                            if ($scope.trainer.has_location == true){
                              $scope.defaultLat = parseFloat($scope.trainer.latitude);
                              $scope.defaultLng = parseFloat($scope.trainer.longitude);
                              circle = new google.maps.Circle({
                                  map: map,
                                  center: {lat: parseFloat($scope.trainer.latitude), lng: parseFloat($scope.trainer.longitude)},
                                  radius: $scope.trainer.location_radius,
                                  editable: false,
                                  strokeColor: '#FF0099',
                                  strokeOpacity: 1,
                                  strokeWeight: 2,
                                  fillColor: '#009ee0',
                                  fillOpacity: 0.2
                              });

                              var bounds = new google.maps.LatLngBounds();

                              bounds.extend(circle.getBounds().getNorthEast());
                              bounds.extend(circle.getBounds().getSouthWest());

                              map.fitBounds(bounds);
                            }
                            window.dispatchEvent(new Event('resize'));
                          }
                      );
                    });

                    $scope.onTabSelected = function(index) {
                        var route;
                            switch (index) {
                                case 1:
                                    route = 'show_trainer.blocked_clients';
                                    break;
                                case 2:
                                    route = 'show_trainer.blocked_by_clients';
                                    break;
                                case 3:
                                    route = 'show_trainer.booking_sessions';
                                    break;
                                case 4:
                                    route = 'show_trainer.clients';
                                    break;
                            }
                        $state.go(route)
                    };

                    $scope.$on("$destroy", function() {
                        map = null;
                        if(circle!= null)
                            circle.setMap(null);
                    });

                    $scope.confirm = function(id){
                        trainer.confirm_account(id).success(function(){
                            trainer.show(id).success(function (data) {
                                $scope.trainer = data.trainer;
                            });
                        });
                    };
                }

                if($state.current.name == 'edit_trainer' || $state.current.name == 'new_trainer'){
                    $scope.trainer = {};
                    $scope.defaultLat = 51.513;
                    $scope.defaultLng = -0.1;
                    var circle = null;

                    $scope.submit = function(){
                        $scope.submitted = true;
                        $scope.formPending = true;
                        trainer.upsert($scope.trainer)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('trainers')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.errors.validation_errors;
                                $scope.formPending = false;
                            })
                    };

                    var map;

                    var initializeCircle = function(center, radius){
                      circle = new google.maps.Circle({
                        map: map,
                        center: center,
                        radius: radius,
                        editable: true,
                        strokeColor: '#FF0099',
                        strokeOpacity: 1,
                        strokeWeight: 2,
                        fillColor: '#009ee0',
                        fillOpacity: 0.2
                      });
                      circle.addListener('click', function () {
                        circle.setMap(null);
                      });
                      circle.addListener('radius_changed', function () {
                        $scope.$apply(function(){$scope.trainer.location_radius = circle.getRadius();})
                      });
                      circle.addListener('center_changed', function () {
                        $scope.$apply(function(){
                          $scope.trainer.latitude = circle.getCenter().lat();
                          $scope.trainer.longitude = circle.getCenter().lng();
                        })
                      });
                    }

                    $scope.$on('mapInitialized', function(evt, evtMap) {
                        map = evtMap;

                        if($state.current.name == 'edit_trainer'){
                          trainer.show($stateParams.id).success(function(data){
                            $scope.trainer = data.trainer;

                            if ($scope.trainer.has_location == true){
                              $scope.defaultLat = $scope.trainer.latitude;
                              $scope.defaultLng = $scope.trainer.longitude;

                              initializeCircle({lat: $scope.trainer.latitude, lng: $scope.trainer.longitude}, $scope.trainer.location_radius);
                            }
                          });
                        }else{
                          trainer.new().success(function(data){
                            $scope.trainer = data.trainer;
                          });
                        }
                    });

                    $scope.addMarker = function(event) {
                        if (circle != null){
                          circle.setMap(null);
                        }

                        initializeCircle(event.latLng, 400);

                        $scope.trainer.latitude = circle.getCenter().lat();
                        $scope.trainer.longitude = circle.getCenter().lng();
                        $scope.trainer.location_radius = circle.getRadius();
                    };

                    $scope.$watch('trainer.location_radius', function(){
                      $timeout(function() {
                        if($scope.trainer.location_radius && circle){
                          circle.setRadius($scope.trainer.location_radius)
                        }
                      })
                    })
                    $scope.$watchGroup(['trainer.latitude', 'trainer.longitude'], function(){
                      $timeout(function() {
                        if($scope.trainer.latitude && circle){
                          circle.setCenter({lat: $scope.trainer.latitude, lng: $scope.trainer.longitude})
                        }
                      })
                    })

                    $scope.$on("$destroy", function() {
                        map = null;
                        if(circle!= null)
                            circle.setMap(null);
                    });
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this trainer!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                trainer.destroy(id).success(function(){
                                    $scope.retrieveTrainers();
                                    SweetAlert.swal("Deleted!", "Your trainer has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your trainer is safe :)", "error");
                            }
                        }
                    );
                };
            }])
}());
