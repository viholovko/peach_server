(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('DeactivatedTrainersController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'DeactivatedTrainersFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, deactivated_trainers) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'state_deactivated';

                if($state.current.name == 'deactivated_trainers') {

                    $scope.trainers = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveDeactivatedTrainers();
                        }, 500)
                    }, true);

                    $scope.downloadCSV = function(){

                        deactivated_trainers.downloadCSV();
                    };

                    $scope.page = 1;
                    $scope.retrieveDeactivatedTrainers = function () {
                        deactivated_trainers.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.deactivated_trainers = data.deactivated_trainers;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveDeactivatedTrainers();
                }
                $scope.reactivate = function(id){
                    deactivated_trainers.reactivate(id).success(function(){
                        $scope.retrieveDeactivatedTrainers();
                    });
                };
                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this trainer!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false,
                            showLoaderOnConfirm: true},
                        function(isConfirm){
                            if (isConfirm) {
                                deactivated_trainers.destroy(id).success(function(){
                                    $scope.retrieveDeactivatedTrainers();
                                    SweetAlert.swal("Deleted!", "Your trainer has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your trainer is safe :)", "error");
                            }
                        }
                    );
                };
            }])
}());
