(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('ClientsBookingSessionsController', ['$scope', '$state','$stateParams', '$timeout', 'ClientsFactory',
            function ($scope, $state, $stateParams, $timeout, users) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;

                $scope.resetFilters = function(){
                    $scope.total=0;
                    $scope.filters = {
                        page:0,
                        per_page: 10
                    };
                };
                $scope.resetFilters();

                var timer = false;
                $scope.$watch('filters', function () {
                    if (timer) {
                        $timeout.cancel(timer)
                    }
                    timer = $timeout(function () {
                        $scope.booking_sessions();
                    }, 500)
                }, true);


                $scope.$watch('filters.status', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.booking_id', function () {
                    $scope.filters.page = 1
                }, true);

                $scope.$watch('filters.rate', function () {
                    $scope.filters.page = 1
                }, true);
                $scope.$watch('filters.name', function () {
                    $scope.filters.page = 1
                }, true);


                $scope.booking_sessions = function(){
                    users.booking_sessions({ id: $stateParams.id, query: $scope.filters})
                        .success(function(data){
                            $scope.sessions = data.booking_sessions;
                            $scope.total = data.total;
                            $scope.options = data.options;
                        });
                };
            }])
}());