(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('EmailSenderController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'EmailSenderFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, email_sender, flags) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'system_pages';

                $scope.sender = {};
                $scope.validation_errors = {};

                email_sender.show()
                    .success(function(data){
                        $timeout(function(){
                            $scope.sender = data.email_sender;
                        }, 0);
                    }
                );

                $scope.submit = function(){
                    $scope.submitted = true;

                    $scope.pending = true;
                    email_sender.update($scope.sender)
                        .success(function(){
                            $scope.pending = false;
                        })
                        .error(function(data){
                            $scope.validation_errors = data.validation_errors;
                            $scope.pending = false;
                        })
                };

            }])

}());