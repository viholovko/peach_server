(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('FeedbacksController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'FeedbackFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, feedback, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;


                if($state.current.name == 'feedbacks') {

                    $scope.resetFilters = function(){
                        $scope.total=0;
                        $scope.filters = {
                            page:0,
                            per_page: 10,
                            search:{}
                        };
                    };
                    $scope.resetFilters();

                    $scope.$watch('filters.name', function () {
                        $scope.filters.page = 1
                    }, true);

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveFeedbacks();
                        }, 500)
                    }, true);


                    $scope.retrieveFeedbacks = function () {
                        feedback.all({query: $scope.filters}).success(function (data) {
                            $scope.feedbacks = data.feedbacks;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveFeedbacksAvg=function () {
                        feedback.avg().success(function (data) {
                            $scope.feedbeck_avg=data.average;
                            $scope.feedbeck_avg_array=new Array(Math.round(data.average));
                        });
                    };

                    $scope.retrieveFeedbacksAvg();
                }

            }])

}());