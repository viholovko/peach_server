(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('AdminsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'AdminsFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, users) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'system_pages';

                if($state.current.name == 'admins') {

                    $scope.admin = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveUsers();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveUsers = function () {
                        users.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.users = data.admins;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveUsers();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this user!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                users.destroy(id).success(function(){
                                    $scope.retrieveUsers();
                                    SweetAlert.swal("Deleted!", "Your user has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your user is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_admin' || $state.current.name == 'edit_admin'){
                    $scope.admin = {};
                    $scope.validation_errors = {};

                    if($state.current.name == 'edit_admin'){
                        users.show($stateParams.id)
                            .success(function(data){
                                    $scope.admin = data.admin;
                                }
                            )
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        users.upsert($scope.admin)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('admins')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());