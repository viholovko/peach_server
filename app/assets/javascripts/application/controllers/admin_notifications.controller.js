(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('AdminNotificationsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'AdminNotificationFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, adminNotifications) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'system_pages';

                    $scope.notifications = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.getNotifications();
                        }, 500)
                    }, true);

                        $scope.notificationClick = function(notification){
                            if (notification.readed == false)
                                adminNotifications.mark_as_readed(notification.id);
                            if (notification.url != null)
                                window.location.href = notification.url;
                        };

                        $scope.markAsSeen = function(notification){
                            if (notification.readed == false)
                                adminNotifications.mark_as_readed(notification.id).success(function(){
                                    $scope.getNotifications();
                                });

                        };

                        $scope.filters = {
                            per_page: 10
                        };

                        $scope.getNotifications = function() {
                            adminNotifications.all({page: $scope.page, query: $scope.filters}).success(function(data){
                                $scope.notifications = data.notifications;
                                $scope.total = data.total;
                            })
                        };

                    $scope.page = 1;

            }])

}());