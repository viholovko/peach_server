(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('TrainingPlacesController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'TrainingPlacesFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, training_places, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'training_places') {

                    $scope.filters = {};

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveTrainingPlaces();
                        }, 500)
                    }, true);

                    $scope.retrieveTrainingPlaces = function () {
                        training_places.all({query: $scope.filters}).success(function (data) {
                            $scope.training_places = data.training_places;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveTrainingPlaces();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this location place!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                training_places.destroy(id).success(function(){
                                    $scope.retrieveTrainingPlaces();
                                    SweetAlert.swal("Deleted!", "Your location place has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your location place is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_training_place' || $state.current.name == 'edit_training_place'){
                    $scope.validation_errors = {};
                    $scope.training_place = {};

                    if($state.current.name == 'edit_training_place'){
                        training_places.show($stateParams.id).success(function(data){
                            $scope.training_place = data.training_place;
                        })
                    }

                    $scope.submit = function(){
                        $scope.submitted = true;

                        $scope.formPending = true;
                        training_places.upsert($scope.training_place)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('training_places')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());