(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('BlockTrainerReasonsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'BlockTrainerReasonsFactory', 'CountryFlagsFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, block_trainer_reasons, flags) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'block_trainer_reasons'){

                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };
                    $scope.resetFilters();

                    $scope.block_trainer_reason = [];

                    var timer = false;
                    $scope.$watch('filters', function(){
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if(timer){
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer= $timeout(function(){
                            $scope.retrieveBlockTrainerReasons();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveBlockTrainerReasons = function(){
                        block_trainer_reasons.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.block_trainer_reasons = data.block_trainer_reasons;
                            $scope.total = data.total;

                        }).error(function (data) {

                        });
                    };

                    $scope.retrieveBlockTrainerReasons();
                }

                $scope.destroy = function(id){
                    var scope = $scope;
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this block trainer reason!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            allowOutsideClick: true },
                        function(isConfirm){
                            if (isConfirm) {
                                block_trainer_reasons.destroy(id).success(function(){
                                    $scope.retrieveBlockTrainerReasons();
                                });
                            } else {

                            }
                        }
                    );
                };

                if($state.current.name == 'new_block_trainer_reason' || $state.current.name == 'edit_block_trainer_reason'){

                    $scope.block_trainer_reason = {};


                    if($state.current.name == 'edit_block_trainer_reason'){
                        block_trainer_reasons.show($stateParams.id)
                            .success(function(data){
                                $timeout(function(){
                                    $scope.block_trainer_reason = data.block_trainer_reason;
                                }, 0);
                            }
                        )
                    }

                    $scope.submitBlockTrainerReason = function(){
                        $scope.submitted = true;
                        if($scope.BlockTrainerReasonForm.$invalid ){
                            return false;
                        }

                        $scope.formPending = true;
                        block_trainer_reasons.upsert($scope.block_trainer_reason)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('block_trainer_reasons')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.errors;
                                $scope.formPending = false;
                            })
                    };
                }

                if($state.current.name == 'show_block_trainer_reason'){
                    block_trainer_reasons.show($stateParams.id).success(function(data){
                        $scope.block_trainer_reason = data.block_trainer_reason;

                    });
                }
            }])

}());