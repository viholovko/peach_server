(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('ProblemReportsController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'ProblemReportsFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, problem_reports, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'problem_reports') {
                    $scope.options = {};
                    $scope.problem_reports = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveProblemReports();
                        }, 500)
                    }, true);

                    $scope.downloadCSV = function(){

                        problem_reports.downloadCSV();
                    };

                    $scope.page = 1;
                    $scope.retrieveProblemReports = function () {
                        problem_reports.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.problem_reports = data.problem_reports;
                            $scope.options = data.options;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveProblemReports();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this problem report!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                problem_reports.destroy(id).success(function(){
                                    $scope.retrieveProblemReports();
                                    SweetAlert.swal("Deleted!", "Your report has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your report is safe :)", "error");
                            }
                        }
                    );
                };

            }])

}());
