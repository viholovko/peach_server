(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('TermsNConditionsPageController', ['$scope', '$state', 'ngDialog', '$stateParams', 'SweetAlert', 'TermsNConditionsPagesFactory',
            function ($scope, $state, ngDialog, $stateParams, SweetAlert, terms_n_conditions_pages) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'state_pages';

                $scope.terms_n_conditions_page = {};


                terms_n_conditions_pages.show()
                    .success(function(data){
                        $scope.terms_n_conditions_page = data.page;
                    }
                )

                $scope.submit = function(){
                    $scope.formPending = true;
                    terms_n_conditions_pages.upsert($scope.terms_n_conditions_page)
                        .success(function(){
                            $scope.formPending = false;
                        })
                        .error(function(data){
                            $scope.validation_errors = data.errors;
                            $scope.formPending = false;
                        })
                };

            }])

}());