(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('DeactivatedClientsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'DeactivatedClientsFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, deactivated_clients) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'state_deactivated';

                if($state.current.name == 'deactivated_clients') {

                    $scope.clients = [];
                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveDeactivatedClients();
                        }, 500)
                    }, true);

                    $scope.downloadCSV = function(){
                        deactivated_clients.downloadCSV();
                    };

                    $scope.page = 1;
                    $scope.retrieveDeactivatedClients = function () {
                        deactivated_clients.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.deactivated_clients = data.deactivated_clients;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveDeactivatedClients();
                }
                $scope.reactivate = function(id){
                    deactivated_clients.reactivate(id).success(function(){
                        $scope.retrieveDeactivatedClients();
                    });
                };
                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this client!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false,
                            showLoaderOnConfirm: true},
                        function(isConfirm){
                            if (isConfirm) {
                                deactivated_clients.destroy(id).success(function(){
                                    $scope.retrieveDeactivatedClients();
                                    SweetAlert.swal("Deleted!", "Your client has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your client is safe :)", "error");
                            }
                        }
                    );
                };
            }])
}());