(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('GoalsController', ['$scope', '$state', '$stateParams', 'SweetAlert', 'GoalsFactory', '$timeout',
            function ($scope, $state, $stateParams, SweetAlert, goals, $timeout) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'goals') {

                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };

                    $scope.resetFilters();

                    var timer = false;
                    $scope.$watch('filters', function () {
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if (timer) {
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer = $timeout(function () {
                            $scope.retrieveGoals();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveGoals = function () {
                        goals.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.goals = data.goals;
                            $scope.total = data.total;
                        });
                    };

                    $scope.retrieveGoals();
                }

                $scope.destroy = function(id){
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this goal!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {
                                goals.destroy(id).success(function(){
                                    $scope.retrieveGoals();
                                    SweetAlert.swal("Deleted!", "Your goal has been deleted.", "success");
                                });
                            } else {
                                SweetAlert.swal("Cancelled", "Your goal is safe :)", "error");
                            }
                        }
                    );
                };


                if($state.current.name == 'new_goal' || $state.current.name == 'edit_goal'){
                    $scope.validation_errors = {};
                    $scope.goal = {};

                    if($state.current.name == 'edit_goal'){
                        goals.show($stateParams.id)
                            .success(function(data){
                                    $scope.goal = data.goal;
                                }
                            )
                    }

                    if($state.current.name == 'new_goal'){
                        goals.new()
                            .success(function(data){
                                    $scope.goal = data.goal;
                                }
                            )
                    }

                    $scope.submit = function(){
                        $scope.formPending = true;
                        goals.upsert($scope.goal)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('goals')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.validation_errors;
                                $scope.formPending = false;
                            })
                    };
                }

            }])

}());