(function () {

    "use strict";

    angular.module('PeachServerApp').controller(
      'AnnouncementsController',
      [
        '$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'AnnouncementsFactory',
        function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, announcements) {
          $scope.I18n = I18n;
          $scope._ = _;
          $scope.$state = $state;
          $scope.$parent.state = 'announcements';

          if($state.current.name == 'announcements') {
            $scope.announcements = [];

            $scope.resetFilters = function(){
              $scope.filters = {
                per_page: 10
              };
            };

            $scope.resetFilters();

            var timer = false;
            $scope.$watch('filters', function () {
              var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
              $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
              if (timer) {
                $scope.page = 1;
                $timeout.cancel(timer)
              }
              timer = $timeout(function () {
                $scope.retrieveAnnouncements();
              }, 500)
            }, true);

            $scope.page = 1;
            $scope.retrieveAnnouncements = function () {
              announcements.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                $scope.announcements = data.announcements;
                $scope.total = data.total;
              });
            };

            $scope.retrieveAnnouncements();
          }

          $scope.destroy = function(id){
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this announcement!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false },
              function(isConfirm){
                if (isConfirm) {
                  announcements.destroy(id).success(function(){
                    $scope.retrieveAnnouncements();
                    SweetAlert.swal("Deleted!", "Your announcement has been deleted.", "success");
                  });
                } else {
                  SweetAlert.swal("Cancelled", "Your announcement is safe :)", "error");
                }
              }
            );
          };

          $scope.resend = function(id){
            announcements.resend(id)
          };

          if($state.current.name == 'new_announcement' || $state.current.name == 'edit_announcement'){
            $scope.announcement = {};
            $scope.validation_errors = {};

            if($state.current.name == 'edit_announcement'){
              announcements.show($stateParams.id)
                .success(function(data){
                    $scope.announcement = data.announcement;
                  }
                )
            }

            $scope.submit = function(){
              $scope.submitted = true;

              $scope.formPending = true;
              announcements.upsert($scope.announcement)
                .success(function(){
                  $scope.formPending = false;
                  $state.go('announcements')
                })
                .error(function(data){
                  $scope.validation_errors = data.validation_errors;
                  $scope.formPending = false;
                })
            };
          }

        }
      ]
    )
}());