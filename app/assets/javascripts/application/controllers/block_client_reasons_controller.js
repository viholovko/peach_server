(function () {

    "use strict";

    angular.module('PeachServerApp')
        .controller('BlockClientReasonsController', ['$scope', '$state', 'ngDialog', '$stateParams', '$timeout', '$sce', 'SweetAlert', 'BlockClientReasonsFactory', 'CountryFlagsFactory',
            function ($scope, $state, ngDialog, $stateParams, $timeout, $sce, SweetAlert, block_client_reasons, flags) {
                $scope.I18n = I18n;
                $scope._ = _;
                $scope.$state = $state;
                $scope.$parent.state = 'content_pages';

                if($state.current.name == 'block_client_reasons'){

                    $scope.resetFilters = function(){
                        $scope.filters = {
                            per_page: 10
                        };
                    };
                    $scope.resetFilters();

                    $scope.block_client_reason = [];

                    var timer = false;
                    $scope.$watch('filters', function(){
                        var total_pages = Math.ceil($scope.total / $scope.filters.per_page);
                        $scope.filters.page = $scope.filters.page > total_pages ? total_pages : $scope.filters.page;
                        if(timer){
                            $scope.page = 1;
                            $timeout.cancel(timer)
                        }
                        timer= $timeout(function(){
                            $scope.retrieveBlockClientReasons();
                        }, 500)
                    }, true);

                    $scope.page = 1;
                    $scope.retrieveBlockClientReasons = function(){
                        block_client_reasons.all({page: $scope.page, query: $scope.filters}).success(function (data) {
                            $scope.block_client_reasons = data.block_client_reasons;
                            $scope.total = data.total;

                        }).error(function (data) {

                        });
                    };

                    $scope.retrieveBlockClientReasons();
                }

                $scope.destroy = function(id){
                    var scope = $scope;
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this block client reasons!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel plx!",
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            allowOutsideClick: true },
                        function(isConfirm){
                            if (isConfirm) {
                                block_client_reasons.destroy(id).success(function(){
                                    $scope.retrieveBlockClientReasons();
                                });
                            } else {

                            }
                        }
                    );
                };

                if($state.current.name == 'new_block_client_reason' || $state.current.name == 'edit_block_client_reason'){

                    $scope.block_client_reason = {};


                    if($state.current.name == 'edit_block_client_reason'){
                        block_client_reasons.show($stateParams.id)
                            .success(function(data){
                                $timeout(function(){
                                    $scope.block_client_reason = data.block_client_reason;
                                }, 0);
                            }
                        )
                    }

                    $scope.submitBlockClientReason = function(){
                        $scope.submitted = true;
                        if($scope.BlockClientReasonForm.$invalid ){
                            return false;
                        }

                        $scope.formPending = true;
                        block_client_reasons.upsert($scope.block_client_reason)
                            .success(function(){
                                $scope.formPending = false;
                                $state.go('block_client_reasons')
                            })
                            .error(function(data){
                                $scope.validation_errors = data.errors;
                                $scope.formPending = false;
                            })
                    };
                }

                if($state.current.name == 'show_block_client_reason'){
                    block_client_reasons.show($stateParams.id).success(function(data){
                        $scope.block_client_reason = data.block_client_reason;

                    });
                }
            }])

}());