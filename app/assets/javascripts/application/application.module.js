(function () {
    "use strict";
    var PeachServerApp = angular.module('PeachServerApp', [
        'ui.router',
        'templates',
        'ngDialog',
        'validation.match',
        'validation.email',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'redactor',
        'formInput.images',
        'formInput.image',
        'formInput.file',
        'toaster',
        'angular-ladda',
        'AuthHttp',
        'oitozero.ngSweetAlert',
        'pagination',
        'tablesort',
        'ngMap',
    ]);

    PeachServerApp.config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
        function ($urlRouterProvider, $stateProvider, $httpProvider) {

            $httpProvider.defaults.headers.common['X-Requested-With'] = 'AngularXMLHttpRequest';

            $urlRouterProvider.otherwise('trainers');

            $stateProvider
                .state('profile',{
                    url: '/profile',
                    templateUrl: 'application/templates/users/edit.html',
                    controller: 'UsersController'
                })
                .state('terms_n_conditions_page',{
                    url: '/terms_n_conditions_page',
                    templateUrl: 'application/templates/terms_n_conditions_page/form.html',
                    controller: 'TermsNConditionsPageController'
                })
                .state('privacy_policy_page',{
                    url: '/privacy_policy_page',
                    templateUrl: 'application/templates/privacy_policy_page/form.html',
                    controller: 'PrivacyPolicyPageController'
                })
                .state('what_is_page',{
                    url: '/what_is_page',
                    templateUrl: 'application/templates/what_is_page/form.html',
                    controller: 'WhatIsPageController'
                })
                .state('email_sender',{
                    url: '/email_sender',
                    templateUrl: 'application/templates/email_sender/form.html',
                    controller: 'EmailSenderController'
                })
                .state('admins',{
                    url: '/admins',
                    templateUrl: 'application/templates/admins/index.html',
                    controller: 'AdminsController'
                })
                .state('new_admin',{
                    url: '/admin/new',
                    templateUrl: 'application/templates/admins/form.html',
                    controller: 'AdminsController'
                })
                .state('edit_admin',{
                    url: '/admin/:id/edit',
                    templateUrl: 'application/templates/admins/form.html',
                    controller: 'AdminsController'
                })
                .state('system_settings',{
                    url: '/system_settings',
                    templateUrl: 'application/templates/system_settings/form.html',
                    controller: 'SystemSettingsController'
                })
                .state('specialities',{
                    url: '/specialities',
                    templateUrl: 'application/templates/specialities/index.html',
                    controller: 'SpecialitiesController'
                })
                .state('new_speciality',{
                    url: '/speciality/new',
                    templateUrl: 'application/templates/specialities/form.html',
                    controller: 'SpecialitiesController'
                })
                .state('edit_speciality',{
                    url: '/speciality/:id/edit',
                    templateUrl: 'application/templates/specialities/form.html',
                    controller: 'SpecialitiesController'
                })
                .state('goals',{
                    url: '/goals',
                    templateUrl: 'application/templates/goals/index.html',
                    controller: 'GoalsController'
                })
                .state('new_goal',{
                    url: '/goal/new',
                    templateUrl: 'application/templates/goals/form.html',
                    controller: 'GoalsController'
                })
                .state('edit_goal',{
                    url: '/goal/:id/edit',
                    templateUrl: 'application/templates/goals/form.html',
                    controller: 'GoalsController'
                })
                .state('block_client_reasons',{
                    url: '/block_client_reasons',
                    templateUrl: 'application/templates/block_client_reasons/index.html',
                    controller: 'BlockClientReasonsController'
                })
                .state('new_block_client_reason',{
                    url: '/block_client_reasons/new',
                    templateUrl: 'application/templates/block_client_reasons/form.html',
                    controller: 'BlockClientReasonsController'
                })
                .state('edit_block_client_reason',{
                    url: '/block_client_reasons/:id/edit',
                    templateUrl: 'application/templates/block_client_reasons/form.html',
                    controller: 'BlockClientReasonsController'
                })
                .state('block_trainer_reasons',{
                    url: '/block_trainer_reasons',
                    templateUrl: 'application/templates/block_trainer_reasons/index.html',
                    controller: 'BlockTrainerReasonsController'
                })
                .state('new_block_trainer_reason',{
                    url: '/block_trainer_reasons/new',
                    templateUrl: 'application/templates/block_trainer_reasons/form.html',
                    controller: 'BlockTrainerReasonsController'
                })
                .state('edit_block_trainer_reason',{
                    url: '/block_trainer_reasons/:id/edit',
                    templateUrl: 'application/templates/block_trainer_reasons/form.html',
                    controller: 'BlockTrainerReasonsController'
                })
                .state('reject_booking_reasons',{
                    url: '/reject_booking_reasons',
                    templateUrl: 'application/templates/reject_booking_reasons/index.html',
                    controller: 'RejectBookingReasonsController'
                })
                .state('new_reject_booking_reason',{
                    url: '/reject_booking_reason/new',
                    templateUrl: 'application/templates/reject_booking_reasons/form.html',
                    controller: 'RejectBookingReasonsController'
                })
                .state('edit_reject_booking_reason',{
                    url: '/reject_booking_reason/:id/edit',
                    templateUrl: 'application/templates/reject_booking_reasons/form.html',
                    controller: 'RejectBookingReasonsController'
                })
                .state('clients',{
                    url: '/clients',
                    templateUrl: 'application/templates/clients/index.html',
                    controller: 'ClientsController'
                })
                .state('problem_reports',{
                    url: '/problem_reports',
                    templateUrl: 'application/templates/problem_reports/index.html',
                    controller: 'ProblemReportsController'
                })
                .state('edit_client',{
                    url: '/clients/:id/edit',
                    templateUrl: 'application/templates/clients/form.html',
                    controller: 'ClientsController'
                })
                .state('show_client',{
                    url: '/clients/:id/show',
                    templateUrl: 'application/templates/clients/show.html',
                    controller: 'ClientsController'
                })
                .state('show_client.blocked_trainers',{
                    templateUrl: 'application/templates/clients/blocked_trainers.html',
                    controller: 'ClientsBlockedController'
                })
                .state('show_client.blocked_by_trainers',{
                    templateUrl: 'application/templates/clients/blocked_by_trainers.html',
                    controller: 'ClientsBlockedByController'
                })
                .state('show_client.favorite_trainers',{
                    templateUrl: 'application/templates/clients/favorite_trainers.html',
                    controller: 'ClientsFavoriteTrainersController'
                })
                .state('show_client.booking_sessions',{
                    templateUrl: 'application/templates/clients/booking_sessions.html',
                    controller: 'ClientsBookingSessionsController'
                })
                .state('edit_trainer',{
                    url: '/trainers/:id/edit',
                    templateUrl: 'application/templates/trainers/form.html',
                    controller: 'TrainersController'
                })
                .state('new_trainer',{
                    url: '/trainers/new',
                    templateUrl: 'application/templates/trainers/form.html',
                    controller: 'TrainersController'
                })
                .state('trainers',{
                    url: '/trainers',
                    templateUrl: 'application/templates/trainers/index.html',
                    controller: 'TrainersController'
                })
                .state('show_trainer',{
                    url: '/trainers/:id/show',
                    templateUrl: 'application/templates/trainers/show.html',
                    controller: 'TrainersController'
                })
                .state('show_trainer.blocked_clients',{
                    templateUrl: 'application/templates/trainers/blocked_clients.html',
                    controller: 'TrainerBlockedClientsController'
                })
                .state('show_trainer.blocked_by_clients',{
                    templateUrl: 'application/templates/trainers/blocked_by_clients.html',
                    controller: 'TrainerBlockedByClientsController'
                })
                .state('show_trainer.booking_sessions',{
                    templateUrl: 'application/templates/trainers/booking_sessions.html',
                    controller: 'TrainerBookingSessionsController'
                })
                .state('show_trainer.clients',{
                    templateUrl: 'application/templates/trainers/clients.html',
                    controller: 'TrainerClientsController'
                })
                .state('deactivated_clients',{
                    url: '/deactivated_clients',
                    templateUrl: 'application/templates/deactivated_clients/index.html',
                    controller: 'DeactivatedClientsController'
                })
                .state('deactivated_trainers',{
                    url: '/deactivated_trainers',
                    templateUrl: 'application/templates/deactivated_trainers/index.html',
                    controller: 'DeactivatedTrainersController'
                })
                .state('training_places',{
                    url: '/training_places',
                    templateUrl: 'application/templates/training_places/index.html',
                    controller: 'TrainingPlacesController'
                })
                .state('new_training_place',{
                    url: '/training_place/new',
                    templateUrl: 'application/templates/training_places/form.html',
                    controller: 'TrainingPlacesController'
                })
                .state('edit_training_place',{
                    url: '/training_place/:id/edit',
                    templateUrl: 'application/templates/training_places/form.html',
                    controller: 'TrainingPlacesController'
                })
                .state('feelings',{
                    url: '/session_ratings',
                    templateUrl: 'application/templates/feelings/index.html',
                    controller: 'FeelingsController'
                })
                .state('new_feeling',{
                    url: '/session_ratings/new',
                    templateUrl: 'application/templates/feelings/form.html',
                    controller: 'FeelingsController'
                })
                .state('edit_feeling',{
                    url: '/session_ratings/:id/edit',
                    templateUrl: 'application/templates/feelings/form.html',
                    controller: 'FeelingsController'
                })
                .state('problem_report_reasons',{
                    url: '/problem_report_reasons',
                    templateUrl: 'application/templates/problem_report_reasons/index.html',
                    controller: 'ProblemReportReasonsController'
                })
                .state('new_problem_report_reason',{
                    url: '/problem_report_reason/new',
                    templateUrl: 'application/templates/problem_report_reasons/form.html',
                    controller: 'ProblemReportReasonsController'
                })
                .state('edit_problem_report_reason',{
                    url: '/problem_report_reason/:id/edit',
                    templateUrl: 'application/templates/problem_report_reasons/form.html',
                    controller: 'ProblemReportReasonsController'
                })
                .state('feedbacks',{
                    url: '/feedbacks',
                    templateUrl: 'application/templates/feedbacks/index.html',
                    controller: 'FeedbacksController'
                })
                .state('admin_notifications',{
                    url: '/admin_notifications',
                    templateUrl: 'application/templates/admin_notifications/index.html',
                    controller: 'AdminNotificationsController'
                })
                .state('announcements',{
                    url: '/announcements',
                    templateUrl: 'application/templates/announcements/index.html',
                    controller: 'AnnouncementsController'
                })
                .state('edit_announcement',{
                    url: '/announcements/:id/edit',
                    templateUrl: 'application/templates/announcements/form.html',
                    controller: 'AnnouncementsController'
                })
                .state('new_announcement',{
                    url: '/announcements/new',
                    templateUrl: 'application/templates/announcements/form.html',
                    controller: 'AnnouncementsController'
                });
    }]);

    PeachServerApp.run(['$http', '$rootScope', function($http, $rootScope){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        $http.defaults.headers.common['X-CSRF-Token'] = csrf_token;
    }]);

}());
