(function () {
    'use strict';
    angular.module('PeachServerApp').factory('FeedbackFactory', ['AuthHttp', function($http){
        return {

            all: function(options){
                var url = '/admin/feedbacks.json?';
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },
            avg: function () {
                return $http.get('/admin/feedbacks/avg.json?');
            }
        }
    }])
}());