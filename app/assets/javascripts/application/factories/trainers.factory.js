(function () {
    'use strict';
    angular.module('PeachServerApp').factory('TrainersFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
                var url = '/admin/trainers.json?';
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            downloadCSV: function(){

                var url = '/admin/trainers.csv';
                $http.download(url, {query:{}});
            },
            downloadBlockedClientsCSV: function(id){
                var url = '/admin/trainers/' + id + '/blocked_clients.csv';
                $http.download(url, {query:{}});
            },
            downloadBlockedByClientsCSV: function(id){
                var url = '/admin/trainers/' + id + '/blocked_by_clients.csv';
                $http.download(url, {query:{}});
            },
            downloadBookingSessionsCSV: function(id){
                var url = '/admin/trainers/' + id + '/bookings.csv';
                $http.download(url, {query:{}});
            },
            confirm_account: function(id){
                return $http.put('/admin/trainers/'+ id +'/confirm', {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },
            show: function(id){
                return $http.get('/admin/trainers/' + id + '.json');
            },
            new: function(){
                return $http.get('/admin/trainers/new.json');
            },

            blocked_clients: function(options){
                var url = '/admin/trainers/' + options.id + '/blocked_clients.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            clients: function(options){
                var url = '/admin/trainers/' + options.id + '/clients.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            blocked_by_clients: function(options){
                var url = '/admin/trainers/' + options.id + '/blocked_by_clients.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            upsert: function(trainer){
                var fd = new FormData();
                fd.append('trainer[first_name]', trainer.first_name || '');
                fd.append('trainer[last_name]', trainer.last_name || '');
                fd.append('trainer[email]', trainer.email || '');
                fd.append('trainer[gender]', trainer.gender);
                fd.append('trainer[address]', trainer.address || '');
                fd.append('trainer[post_code]', trainer.post_code || '');
                fd.append('trainer[longitude]', trainer.longitude || '');
                fd.append('trainer[latitude]', trainer.latitude || '');
                fd.append('trainer[confirmed]', trainer.confirmed || false);
                fd.append('trainer[email_confirmed]', trainer.email_confirmed || false);
                fd.append('trainer[location_radius]', trainer.location_radius || '' );
                fd.append('trainer[bio]', trainer.bio || '' );
                fd.append('trainer[qualification]', trainer.qualification || '' );
                if(trainer.avatar.file){
                    fd.append('trainer[avatar]', trainer.avatar.file);
                }
                if(trainer.password){
                    fd.append('trainer[password]', trainer.password);
                    fd.append('trainer[password_confirmation]', trainer.password_confirmation);
                }
                if(trainer.sessions_price){
                  fd.append('trainer[sessions_price]', trainer.sessions_price );
                }

                var selected_specialities = trainer.specialities.filter(function(speciality){ return speciality.selected})

                if(selected_specialities.length > 0){
                  selected_specialities.forEach(function(speciality){
                    fd.append('trainer[speciality_ids][]', speciality.id)
                  })
                }else{
                  fd.append('trainer[speciality_ids]', '[]')
                }

                if(trainer.id){
                    return $http.put('/admin/trainers/' + trainer.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                  return $http.post('/admin/trainers', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                  });
                };
            },

            destroy: function (id) {
                return $http.delete('/admin/trainers/' + id)
            },

            bookings: function (options) {
                var url = '/admin/trainers/'+options.id+'/bookings.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            }
        }
    }])
}());
