(function () {
    'use strict';
    angular.module('PeachServerApp').factory('SessionsFactory', ['AuthHttp', function($http){
        return {
            check: function(){
                return $http.get('/admin/sessions/check');
            },
            logout: function(){
                return $http.delete('/admin/sessions')
            }
        }
    }])
}());