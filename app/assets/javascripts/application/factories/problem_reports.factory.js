(function () {
    'use strict';
    angular.module('PeachServerApp').factory('ProblemReportsFactory', ['AuthHttp', function($http){
        return {

            all: function(options){
                var url = '/admin/problem_reports.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            downloadCSV: function(){

                var url = '/admin/problem_reports.csv';
                $http.download(url, {query:{}});
            },

            destroy: function(id){
                return $http.delete('/admin/problem_reports/' + id)
            }
        }
    }])
}());