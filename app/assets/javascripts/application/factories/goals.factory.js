(function () {
    'use strict';
    angular.module('PeachServerApp').factory('GoalsFactory', ['AuthHttp', function($http){
        return {

            upsert: function(goal){
                var fd = new FormData();

                fd.append('goal[title]', goal.title || '' );
                fd.append('goal[speciality_ids][]', ''); // send empty array if there is no specialities selected
                if(goal.specialities){
                    goal.specialities.forEach(function(speciality){
                        if(speciality.selected)
                        fd.append('goal[speciality_ids][]', speciality.id)
                    })
                }

                if(goal.id){
                    return $http.put('/admin/goals/' + goal.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/goals', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/goals.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/goals/' + id + '.json');
            },

            new: function(){
                return $http.get('/admin/goals/new.json');
            },

            destroy: function(id){
                return $http.delete('/admin/goals/' + id)
            }
        }
    }])
}());