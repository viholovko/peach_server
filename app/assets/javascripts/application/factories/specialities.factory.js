(function () {
    'use strict';
    angular.module('PeachServerApp').factory('SpecialitiesFactory', ['AuthHttp', function($http){
        return {

            upsert: function(speciality){
                var fd = new FormData();

                fd.append('speciality[title]', speciality.title || '' );
                if(speciality.goals){
                    speciality.goals.forEach(function(goal){
                        if(goal.selected)
                            fd.append('speciality[goal_ids][]', goal.id)
                    })
                }

                if(speciality.id){
                    return $http.put('/admin/specialities/' + speciality.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/specialities', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/specialities.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/specialities/' + id + '.json');
            },

            new: function(){
                return $http.get('/admin/specialities/new.json');
            },

            destroy: function(id){
                return $http.delete('/admin/specialities/' + id)
            }
        }
    }])
}());