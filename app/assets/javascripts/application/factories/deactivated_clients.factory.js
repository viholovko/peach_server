(function () {
    'use strict';
    angular.module('PeachServerApp').factory('DeactivatedClientsFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
                var url = '/admin/deactivated_clients.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            downloadCSV: function(){

                var url = '/admin/deactivated_clients.csv';
                $http.download(url, {query:{}});
            },

            reactivate: function (id) {
                return $http.post('/admin/deactivated_clients/' + id + '/reactivate')
            },

            destroy: function (id) {
                return $http.delete('/admin/deactivated_clients/' + id)
            }
        }
    }])
}());