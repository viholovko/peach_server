(function () {
    'use strict';
    angular.module('PeachServerApp').factory('ClientsFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
                var url = '/admin/clients.json?';
               
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            downloadCSV: function(tab = 0, id) {
                switch (tab) {
                    case 1:
                        var url = `/admin/clients/${id}/blocked_trainers.csv`;
                        break;
                    case 2:
                        var url = `/admin/clients/${id}/blocked_by_trainers.csv`;
                        break;
                    case 3:
                        var url = `/admin/clients/${id}/favorite_trainers.csv`;
                        break;
                    case 4:
                        var url = `/admin/clients/${id}/booking_sessions.csv`;
                        break;
                    default:
                        var url = '/admin/clients.csv';
                };
                $http.download(url, {query:{}});
            },
            show: function(id){
                return $http.get('/admin/clients/' + id + '.json');
            },

            blocked_trainers: function(options){
                var url = '/admin/clients/' + options.id + '/blocked_trainers.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);

            },
            unblock_trainer: function(options){
                var fd = new FormData();
                fd.append('trainer_id', options.trainer_id || '');

                return $http.put('/admin/clients/' + options.id + '/unblock_trainer', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },

            blocked_by_trainers: function(options){
                var url = '/admin/clients/' + options.id + '/blocked_by_trainers.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            favorite_trainers: function(options){
                var url = '/admin/clients/' + options.id + '/favorite_trainers.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            booking_sessions: function(options){
                var url = '/admin/clients/' + options.id + '/booking_sessions.json?';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            destroy: function (id) {
                return $http.delete('/admin/users/' + id)
            },


            upsert: function(client){
                var fd = new FormData();
                fd.append('client[first_name]', client.first_name || '');
                fd.append('client[last_name]', client.last_name || '');
                fd.append('client[email]', client.email || '');
                fd.append('client[gender]', client.gender);
                fd.append('client[address]', client.address || '');
                fd.append('client[post_code]', client.post_code || '');
                fd.append('client[longitude]', client.longitude || '');
                fd.append('client[latitude]', client.latitude || '');
                fd.append('client[location_radius]', client.location_radius || '' );
                fd.append('client[email_confirmed]', client.email_confirmed || false);
                if(client.avatar.file){
                    fd.append('client[avatar]', client.avatar.file);
                }
                if(client.password){
                    fd.append('client[password]', client.password);
                    fd.append('client[password_confirmation]', client.password_confirmation);
                }

                if(client.id){
                    return $http.put('/admin/clients/' + client.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                };
            }
        }
    }])
}());