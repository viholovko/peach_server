(function () {
    'use strict';
    angular.module('PeachServerApp').factory('DeactivatedTrainersFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
                var url = '/admin/deactivated_trainers.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            downloadCSV: function(){

                var url = '/admin/deactivated_trainers.csv';
                $http.download(url, {query:{}});
            },

            reactivate: function (id) {
                return $http.post('/admin/deactivated_trainers/' + id + '/reactivate')
            },

            destroy: function (id) {
                return $http.delete('/admin/deactivated_trainers/' + id)
            }
        }
    }])
}());
