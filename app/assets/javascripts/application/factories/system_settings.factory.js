(function () {
    'use strict';
    angular.module('PeachServerApp').factory('SystemSettingsFactory', ['AuthHttp', function($http){
        return {

            update: function(settings){
                var fd = new FormData();
                fd.append('system_settings[fee]',                               settings.fee || 0);
                fd.append('system_settings[min_price]',                         settings.min_price || 1);
                fd.append('system_settings[max_price]',                         settings.max_price || 2.0);
                fd.append('system_settings[package_expiration_time]',           settings.package_expiration_time || 24);
                fd.append('system_settings[min_sessions]',                      settings.min_sessions || 1);
                fd.append('system_settings[max_sessions]',                      settings.max_sessions || 4);
                fd.append('system_settings[min_radius]',                        settings.min_radius || 0);
                fd.append('system_settings[max_radius]',                        settings.max_radius || 1);

                if(settings.ios_push_certificate && settings.ios_push_certificate.file){
                    fd.append('system_settings[ios_push_certificate]', settings.ios_push_certificate.file);
                }
                fd.append('system_settings[ios_push_apns_host]',                settings.ios_push_apns_host || '');
                fd.append('system_settings[ios_push_environment_sandbox]',      settings.ios_push_environment_sandbox || false);
                fd.append('system_settings[ios_push_password]',                 settings.ios_push_password || '');

                fd.append('system_settings[stripe_publishable_key]',            settings.stripe_publishable_key || '');
                fd.append('system_settings[stripe_secret_key]',                 settings.stripe_secret_key || '');

                fd.append('system_settings[qb_host]',                           settings.qb_host || '');
                fd.append('system_settings[qb_application_id]',                 settings.qb_application_id || '');
                fd.append('system_settings[qb_auth_key]',                       settings.qb_auth_key || '');
                fd.append('system_settings[qb_auth_secret]',                    settings.qb_auth_secret || '');

                fd.append('system_settings[trainer_qualification_max_lenght]',  settings.trainer_qualification_max_lenght || '');
                fd.append('system_settings[trainer_bio_max_lenght]',            settings.trainer_bio_max_lenght || '');

                return $http.put('/admin/system_settings', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },

            show: function(){
                return $http.get('/admin/system_settings');
            }
        }
    }])
}());