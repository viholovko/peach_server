(function () {
    'use strict';
    angular.module('PeachServerApp').factory('AnnouncementsFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
              var url = '/admin/announcements.json?';
              if(options.page)
                  url = url + 'page=' + options.page + '&';

              _.each(Object.keys(options.query), function(key){
                  if(options.query[key])
                      url = url + key + '=' + options.query[key] + '&';
              });

              return $http.get(url);
            },
            destroy: function (id) {
              return $http.delete('/admin/announcements/' + id)
            },
            show: function (id) {
              return $http.get('/admin/announcements/' + id)
            },
            resend: function (id) {
              return $http.post('/admin/announcements/' + id + '/resend')
            },
            upsert: function (options) {
              console.log(options)
              var fd = new FormData();
              fd.append('announcement[title]', options.title || '');
              fd.append('announcement[subtitle]', options.subtitle || '');
              fd.append('announcement[body]', options.body || '');
              fd.append('announcement[notify_trainers]', !!options.notify_trainers);
              fd.append('announcement[notify_clients]', !!options.notify_clients);
              if(options.image && options.image.file){
                fd.append('announcement[image]', options.image.file)
              }
              if(options.id) {
                return $http.put('/admin/announcements/' + options.id, fd)
              }else{
                return $http.post('/admin/announcements', fd)
              }
            }
        }
    }])
}());