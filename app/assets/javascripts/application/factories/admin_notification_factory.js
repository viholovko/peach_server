(function () {
    'use strict';
    angular.module('PeachServerApp').factory('AdminNotificationFactory', ['AuthHttp', function($http){
        return {
            all: function(options){
                var url = '/admin/admin_notifications.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },
            mark_as_readed: function (id) {
                return $http.put('/admin/admin_notifications/'+ id +'/mark_as_read')
            }
        }
    }])
}());