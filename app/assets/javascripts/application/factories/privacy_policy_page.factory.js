(function () {
    'use strict';
    angular.module('PeachServerApp').factory('PrivacyPolicyPagesFactory', ['AuthHttp', function($http){
        return {

            upsert: function(privacy_policy_page){
                var fd = new FormData();

                if(privacy_policy_page.content){
                    fd.append('privacy_policy_page[content]', privacy_policy_page.content );
                }

                return $http.post('/admin/privacy_policy_page', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },

            show: function(id){
                return $http.get('/admin/privacy_policy_page.json');
            }
        }
    }])
}());