(function () {
    'use strict';
    angular.module('PeachServerApp').factory('RejectBookingReasonsFactory', ['AuthHttp', function($http){
        return {

            upsert: function(reason){
                var fd = new FormData();

                fd.append('reject_booking_reason[title]', reason.title || '' );
                if(reason.id){
                    return $http.put('/admin/reject_booking_reasons/' + reason.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/reject_booking_reasons', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/reject_booking_reasons.json?';
                if(options.page)
                    url = url + 'page=' + options.page + '&';

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/reject_booking_reasons/' + id + '.json');
            },

            destroy: function(id){
                return $http.delete('/admin/reject_booking_reasons/' + id)
            }
        }
    }])
}());