(function () {
    'use strict';
    angular.module('PeachServerApp').factory('ProblemReportReasonsFactory', ['AuthHttp', function($http){
        return {

            upsert: function(reason){
                var fd = new FormData();

                fd.append('problem_report_reason[title]', reason.title || '' );
                if(reason.id){
                    return $http.put('/admin/problem_report_reasons/' + reason.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/problem_report_reasons', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/problem_report_reasons.json?';
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/problem_report_reasons/' + id + '.json');
            },

            destroy: function(id){
                return $http.delete('/admin/problem_report_reasons/' + id)
            }
        }
    }])
}());
