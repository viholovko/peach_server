(function () {
    'use strict';
    angular.module('PeachServerApp').factory('FeelingsFactory', ['AuthHttp', function($http){
        return {

            upsert: function(feeling){
                var fd = new FormData();

                fd.append('feeling[title]', feeling.title || '' );
                if(feeling.id){
                    return $http.put('/admin/feelings/' + feeling.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/feelings', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/feelings.json?';
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/feelings/' + id + '.json');
            },

            destroy: function(id){
                return $http.delete('/admin/feelings/' + id)
            }
        }
    }])
}());