(function () {
    'use strict';
    angular.module('PeachServerApp').factory('WhatIsPagesFactory', ['AuthHttp', function($http){
        return {

            upsert: function(what_is_page){
                var fd = new FormData();

                fd.append('what_is_page[content]', what_is_page.content || '');

                return $http.post('/admin/what_is_page', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },

            show: function(id){
                return $http.get('/admin/what_is_page.json');
            }
        }
    }])
}());