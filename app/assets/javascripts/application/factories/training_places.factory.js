(function () {
    'use strict';
    angular.module('PeachServerApp').factory('TrainingPlacesFactory', ['AuthHttp', function($http){
        return {

            upsert: function(place){
                var fd = new FormData();

                fd.append('training_place[title]', place.title || '' );
                if(place.id){
                    return $http.put('/admin/training_places/' + place.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/training_places', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/training_places.json?';
                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/training_places/' + id + '.json');
            },

            destroy: function(id){
                return $http.delete('/admin/training_places/' + id)
            }
        }
    }])
}());