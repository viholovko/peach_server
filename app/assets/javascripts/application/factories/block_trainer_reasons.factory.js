(function () {
    'use strict';
    angular.module('PeachServerApp').factory('BlockTrainerReasonsFactory', ['AuthHttp', function($http){
        return {

            upsert: function(block_trainer_reasons){
                var fd = new FormData();


                    fd.append('block_trainer_reasons[title]', block_trainer_reasons.title || '');


                if(block_trainer_reasons.id){
                    return $http.put('/admin/block_trainer_reasons/' + block_trainer_reasons.id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }else{
                    return $http.post('/admin/block_trainer_reasons', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    });
                }
            },

            all: function(options){
                var url = '/admin/block_trainer_reasons.json?';
                if(options.page)

                _.each(Object.keys(options.query), function(key){
                    if(options.query[key])
                        url = url + key + '=' + options.query[key] + '&';
                });

                return $http.get(url);
            },

            show: function(id){
                return $http.get('/admin/block_trainer_reasons/' + id + '.json');
            },

            destroy: function(id){
                return $http.delete('/admin/block_trainer_reasons/' + id)
            }
        }
    }])
}());