class UserMailer < ApplicationMailer

  def confirmation_instructions(user_id)
    @user = User.find(user_id)
    @subject = @user.trainer? ? 'You\'re now a Peach trainer' : 'Start your fitness journey with Peach'
    mail(to: @user.email, from: EmailSender.user_name, subject: @subject)
  end

  def password_reset(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, from: EmailSender.user_name, subject: 'Peach password recovery')
  end

  def account_approved(user_id)
    @user = User.with_deleted.find(user_id)
    @subject = @user.trainer? ? 'You\'re now a Peach trainer' : 'Start your fitness journey with Peach'
    mail(to: @user.email, from: EmailSender.user_name, subject: @subject)
  end

  def notify_admin_about_new_user(user_id)
    @user = User.find(user_id)
    admins = User.where(role_id: Role.admin)
    mail(to: admins.pluck(:email), from: EmailSender.user_name, subject: 'New user signed up.')
  end

  def notify_admin_about_blocking(blocking_id)
    blocking = Blocking.find(blocking_id)
    @client = blocking.from_user
    @trainer = blocking.to_user
    admins = User.where(role_id: Role.admin)
    mail(to: admins.pluck(:email), from: EmailSender.user_name, subject: 'Client blocked a trainer.')
  end

end
