class AttachmentsController < ApplicationController

  load_and_authorize_resource :attachment
  skip_before_action :verify_authenticity_token

  def create
    params.permit!
    @attachment = Attachment.create attacment_params

    render json: { url: @attachment.file.url, thumb: @attachment.file.url, title: 'Image', id: @attachment.id }
  end

  private

  def attacment_params
    params.permit(:file, :entity_type)
  end
end