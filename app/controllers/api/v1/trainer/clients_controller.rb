class Api::V1::Trainer::ClientsController < Api::V1::Trainer::BaseController

  load_and_authorize_resource :client, parent: false, only: [:show]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = ::Trainer.my_clients(current_user, params)
    count_query = ::Trainer.my_clients(current_user, params.merge({count: true}))

    @clients = ::User.find_by_sql(query.take(per_page).skip((page - 1) * per_page ).to_sql)
    @count = ::User.find_by_sql(count_query.to_sql).first[:count]
  end

  def show
    render json: {client: @client.details_json(current_user)}
  end

  def sessions
    @bookings=Booking.where(client_id: params[:id])
    render json: {errors: ["Bookings not found"] }, status: 422 if @bookings.empty?
  end

end