class Api::V1::Trainer::BaseController < Api::V1::BaseController

  before_action :only_trainers

  private

  def only_trainers
    render json: {errors: ["You are not a trainer."]}, status: :unprocessable_entity and return unless current_user.trainer?
  end
end