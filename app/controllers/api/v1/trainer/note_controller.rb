class Api::V1::Trainer::NoteController < Api::V1::Trainer::BaseController

  def create
    client = ::Client.find(params[:id])
    @note = TrainerNote.where(trainer_id: current_user.id, client_id: client.id).first_or_initialize

    if @note.update_attributes note_attributes
      render json: {message: 'Trainer note has been successfully updated.'}
    else
      render json: {errors: @note.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def index
    @note = current_user.as_trainer.trainer_notes.find_by(client_id: params[:id])
    render json: {text: @note&.text || ''}
  end

  private

  def note_attributes
    params.permit :text
  end
end