class Api::V1::Trainer::ScheduleController < Api::V1::Trainer::BaseController

  def index
    @trainer = current_user.as_trainer
    @from = params[:from].present? ? Time.at(params[:from].to_i) : Time.now.beginning_of_day
    @to = params[:to].present? ? Time.at(params[:to].to_i) : Time.now.end_of_day

    schedule = Booking.schedule_for trainer: @trainer,
                                    from: @from,
                                    to: @to,
                                    options: {detailed: true}

    render json: schedule
  end
end