class Api::V1::Trainer::EarningsController < Api::V1::Trainer::BaseController

  def index
    query = Payment.query(current_user, params.merge({upcoming: false}))
    count_query = Payment.query current_user, params.merge({count: true, upcoming: false})

    @payments = Payment.find_by_sql(query)
    @total = Payment.find_by_sql(count_query).first['count']
    @total_earnings = current_user.as_trainer.total_earnings
  end
end