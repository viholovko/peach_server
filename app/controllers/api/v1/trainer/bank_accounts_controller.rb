class Api::V1::Trainer::BankAccountsController < Api::V1::BaseController

  def index
    render json: {bank_account: current_user.bank_account}
  end

  def create
    bank_account = BankAccount.where(user_id: current_user.id).first_or_initialize

    if bank_account.update_attributes  account_params
      render json: {message: 'Bank Account successfully created.'}
    else
      render json: {errors:bank_account.errors.full_messages}, status: :unprocessable_entity
    end
  end

  private

  def account_params
    params.permit :number, :sort_code, :name, :bank
  end
end