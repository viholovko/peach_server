class Api::V1::Trainer::BookingsController < Api::V1::Trainer::BaseController

  def create
    @booking = current_user.as_trainer.bookings.build booking_params
    if @booking.save
      render json: {message: 'Booking successfully created!', id: @booking.id}
    else
      render json: { errors: @booking.errors.full_messages }, status: 422
    end
  end

  def update
    @booking = current_user.as_trainer.bookings.find params[:id]

    @change_request = @booking.booking_change_requests.build update_params.merge({from_user: current_user, to_user: @booking.client})

    if @change_request.save
      render json: { message: 'Change request has been successfully created, please, wait for approve!' }
    else
      render json: { errors: @change_request.errors.full_messages }, status: 422
    end
  end

  def cancel
    @user = current_user.as_trainer
    @booking = @user.bookings.find(params[:id])

    if @booking.update_attributes cancel_params
      render json: {message: 'Booking canceled!'}
    else
      render json: {errors: @booking.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def show
    @booking = current_user.as_trainer.bookings.find(params[:id])
    @next_session = BookingSession.where("booking_id =  ? AND from_time > ?", @booking.id, Time.now).order(from_time: :asc).first
  end

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Booking.query(current_user, params)
    count_query = Booking.query(current_user, params.merge({count: true}))

    @bookings = Booking.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Booking.find_by_sql(count_query.to_sql).first['count']
  end

  private

  def booking_params
    allowed_params = params.permit :booking_type, :client_id, :latitude, :longitude, :training_place_id, :address, :session_price,
                                   booking_sessions_attributes: [:from_time, :to_time]

    allowed_params[:booking_sessions_attributes].to_a.each do |attr|
      attr[:from_time] = Time.at(attr[:from_time].to_i).utc
      attr[:to_time] = Time.at(attr[:to_time].to_i).utc
    end

    if params[:booking_type] == 'renewable'
      allowed_params[:booking_sessions_attributes].to_a.each do |attr|
        if attr[:from_time] < Time.now.utc
          attr[:from_time] = attr[:from_time] + 1.week
          attr[:to_time] = attr[:to_time] + 1.week
        end
      end
    end

    allowed_params[:status] = :pending

    allowed_params
  end

  def update_params
    allowed_params = params.permit booking_sessions_attributes: [:from_time, :to_time]

    allowed_params[:booking_sessions_attributes].to_a.each do |attr|
      attr[:from_time] = Time.at(attr[:from_time].to_i).utc
      attr[:to_time] = Time.at(attr[:to_time].to_i).utc
    end

    if @booking.renewable?
      allowed_params[:booking_sessions_attributes].to_a.each do |attr|
        if attr[:from_time] < Time.now.utc
          attr[:from_time] = attr[:from_time] + 1.week
          attr[:to_time] = attr[:to_time] + 1.week
        end
      end
    end

    allowed_params
  end

  def cancel_params
    allowed_params = params.permit reject_booking_reason_ids: []
    allowed_params[:status] = 'canceled_by_trainer'
    allowed_params
  end

end