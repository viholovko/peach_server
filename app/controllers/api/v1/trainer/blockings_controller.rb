class Api::V1::Trainer::BlockingsController < Api::V1::Trainer::BaseController

  def create
    client = ::Client.find_by_id(params[:id])
    return render json: { errors: 'Client not found!' }, status: 422 if client.nil?
    blocking = current_user.blockings.new blocking_params.merge({to_user_id: client.id})
    blocking.validate_client_reason = true
    if blocking.save
      render json: {message: 'Client blocked!'}
    else
      render json: { errors: blocking.errors.full_messages }, status: 422
    end
  end

  private

  def blocking_params
    params.permit(:comment, block_client_reason_ids: [])
  end

end