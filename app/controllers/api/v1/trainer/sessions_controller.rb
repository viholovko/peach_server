class Api::V1::Trainer::SessionsController < Api::V1::Trainer::BaseController

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = current_user.my_sessions params
    query_count = current_user.my_sessions params.merge({count:true})

    @sessions = BookingSession.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = BookingSession.find_by_sql(query_count.to_sql).count
  end
end