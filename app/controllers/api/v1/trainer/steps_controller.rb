class Api::V1::Trainer::StepsController < Api::V1::Trainer::BaseController

  def create
    render json: {errors: ["You are already completed your steps."]}, status: :unprocessable_entity and return if current_user.registration_step == 3

    @trainer = ::Trainer.find current_user.id

    if @trainer.update_attributes step_params
      render json: {user: @trainer.to_json}
    else
      render json: {errors: @trainer.errors.full_messages}, status: :unprocessable_entity
    end
  end

  private

  def step_params
    params.permit(:registration_step, :first_name, :last_name, :gender, :address, :post_code, :latitude, :longitude, :location_radius, :avatar, speciality_ids: [])
  end
end