class Api::V1::SpecialitiesController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @specialities = Speciality.all
  end
end