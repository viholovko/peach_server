class Api::V1::PagesController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def what_is
    render json: {
        content: WhatIsPage.content
    }
  end

  def terms_and_conditions
    render json: {
        content: TermsNConditionsPage.content
    }
  end

  def privacy_policy
    render json: {
        content: PrivacyPolicyPage.content
    }
  end
end