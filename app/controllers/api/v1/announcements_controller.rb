class Api::V1::AnnouncementsController < Api::V1::BaseController

  load_and_authorize_resource :announcement

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Announcement.search_query(current_user, params)
    query_count = Announcement.search_query(current_user, params.merge({count: true}))

    @announcements = Announcement.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @total = Announcement.find_by_sql(query_count.to_sql).first[:count]
  end

  def show

  end
end