class Api::V1::UsersController < Api::V1::BaseController

  skip_before_action :authenticate_user, only: [:create]

  def create
    @user = User.new user_params
    if @user.save
      if @user.trainer? && !@user.confirmed?
        render json: { message: 'Registration successful. Wait for administrator confirm Your account!' }
      else
        sign_in user: @user, device_type: params[:device_type], push_token: params[:push_token]
        @user.update_attribute :time_zone, params[:time_zone] if params[:time_zone].present?
        render json: {
            session_token: current_session.token,
            user: @user.to_json,
            message: 'Registration successful.'
        }
      end
    else
      render json: { errors: @user.errors.full_messages }, status: 422
    end
  end

  def profile
    render json: { user: current_user.to_json }
  end

  def qb_users
    @qb_users = User.where(qb_user_id: params[:qb_user_ids]).order(:qb_user_id)
  end

  def update
    @user = current_user

    if params[:password].present? && !@user.authenticate(params[:current_password])
      render json: { errors: ['Wrong current password.'] }, status: :unprocessable_entity and return
    end

    if @user.update_attributes user_params
      render json: {user: @user.to_json }
    else
      render json: {errors: @user.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def destroy
    @user = current_user
    if @user
      if @user.update_attributes user_params
        current_user.destroy
        current_session.destroy
        render json: {message: 'Account deactivated.'}
      end
    end

  end

  def online_status
    if params[:id]
      @user = User.find_by_id(params[:id])
    elsif params[:qb_user_id]
      @user = User.find_by_qb_user_id(params[:qb_user_id])
    end
    if @user
      render json: {online: @user.online}
    else
      render json: { errors: ["User not found!"] }, status: 422
    end

  end

  private

  def user_params
    allowed_params = params.permit :email, :password, :avatar, :first_name, :last_name, :gender, :latitude, :longitude, :qualification,
                                   :location_radius, :bio, :notifications_enabled, :training_place, :client_gender, :post_code, :address,
                                   :deactivate_message, :trainers_gender, :sessions_price, speciality_ids: [], goal_ids: [], training_place_ids: [],
                                   working_hours: [:day, ranges: [:from_time, :to_time]]

    allowed_params[:working_hours].to_a.each do |attr|
      attr[:ranges].to_a.each do |range|
        range[:from_time] = Time.at(range[:from_time].to_i).utc
        range[:to_time] = Time.at(range[:to_time].to_i).utc
      end
    end

    allowed_params[:trainers_gender] = User.genders[allowed_params[:trainers_gender]]
    allowed_params[:role_id] = Role.send(params[:user_type]).id if ['client', 'trainer'].include?(params[:user_type])
    allowed_params[:password_confirmation] = allowed_params[:password]

    allowed_params[:registration_step] = 3 if allowed_params[:role_id] == Role.client.id

    allowed_params
  end
end