class Api::V1::FeedbacksController < Api::V1::BaseController
  skip_before_action :authenticate_user

  def create
    if current_user
      feedback= current_user.feedbacks.build feedback_params
    else
      feedback= Feedback.new feedback_params
    end
    if feedback.save
      render json: { message: 'Feedback successfully created!' }
    else
      render json: {errors:feedback.errors}, status: :unprocessable_entity
    end
  end

  private

  def feedback_params
    params.permit :stars, :comment
  end
end