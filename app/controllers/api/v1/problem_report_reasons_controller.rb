class Api::V1::ProblemReportReasonsController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @problem_report_reasons = ProblemReportReason.all
  end
end
