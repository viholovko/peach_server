class Api::V1::ChangeRequestsController < Api::V1::BaseController

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = SessionChangeRequest.query(current_user, params)
    count_query = SessionChangeRequest.query(current_user, params.merge({count: true}))

    @requests = SessionChangeRequest.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = SessionChangeRequest.find_by_sql(count_query.to_sql).count
  end

  def create
    @user = current_user.client? ? current_user.as_client : current_user.as_trainer

    @booking_session = BookingSession.find(params[:booking_session_id])
    @booking = @user.bookings.find @booking_session.booking_id

    @change_request = @booking_session.session_change_requests.build create_params

    if @change_request.save
      render json: { message: 'Change request has been successfully created, please, wait for approve!' }
    else
      render json: { errors: @change_request.errors.full_messages }, status: 422
    end
  end

  def reject
    @change_request = SessionChangeRequest.where(to_user: current_user).find(params[:id])
    @change_request.reject
    render json: {message: 'Change request rejected.'}
  end

  def accept
    @change_request = SessionChangeRequest.where(to_user: current_user).find(params[:id])
    if @change_request.accept
      render json: {message: 'Change request accepted.'}
    else
      render json: {errors: @change_request.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def show

  end

  private

  def create_params
    allowed_params = params.permit :from_time, :to_time
    {
        from_user: current_user,
        to_user: @booking.send(current_user.client? ? 'trainer' : 'client'),
        booking: @booking,
        booking_session: @booking_session,
        from_time: allowed_params[:from_time] ? Time.at(allowed_params[:from_time].to_i ) : nil,
        to_time: allowed_params[:to_time] ? Time.at(allowed_params[:to_time].to_i ) : nil
    }
  end
end