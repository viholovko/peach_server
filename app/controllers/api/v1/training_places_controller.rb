class Api::V1::TrainingPlacesController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @training_places = TrainingPlace.all
  end
end
