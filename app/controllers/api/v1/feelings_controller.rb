class Api::V1::FeelingsController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @feelings = Feeling.all
  end
end
