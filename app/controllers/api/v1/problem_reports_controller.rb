class Api::V1::ProblemReportsController < Api::V1::BaseController

  def create
    @problem_report = ProblemReport.new problem_report_params
    @problem_report_reason = ProblemReportReason.find_by_id(params[:problem_report_reason_id])
    if @problem_report_reason
      if @problem_report.save
        render json: { message: 'Problem report has been added.' }
      else
        render json: { errors: @problem_report.errors.full_messages }, status: 422
      end
    else
      render json: { errors: ["Problem report reason not found!"] }, status: 422
    end
  end

  private

  def problem_report_params
    allowed_params = params.permit :comment, :problem_report_reason_id
    allowed_params[:user_id] = current_user.id
    allowed_params
  end
end
