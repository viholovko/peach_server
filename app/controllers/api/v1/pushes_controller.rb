class Api::V1::PushesController < Api::V1::BaseController

  skip_before_action :authenticate_user, only: [:create]

  def create
    if %w(ios android).include?(params[:device_type]) && params[:push_token].present?
      if params[:device_type] == 'ios'
        tokens = [ params[:push_token] ]
        password = SystemSettings.ios_push_password
        notification = RubyPushNotifications::APNS::APNSNotification.new tokens, { aps: { alert: params[:message], sound: 'true', badge: 1 } }

        raise 'Push certificate missing.' unless SystemSettings.ios_push_certificate.present?

        pusher = RubyPushNotifications::APNS::APNSPusher.new(
            File.read(SystemSettings.ios_push_certificate.path),
            true, # SystemSettings.ios_push_environment_sandbox,
            password,
            { host: 'gateway.sandbox.push.apple.com' } #SystemSettings.ios_push_apns_host }
        )
        pusher.push [notification]

        pusher = RubyPushNotifications::APNS::APNSPusher.new(
            File.read(SystemSettings.ios_push_certificate.path),
            false, # SystemSettings.ios_push_environment_sandbox,
            password,
            { host: 'gateway.push.apple.com' } # SystemSettings.ios_push_apns_host }
        )

        pusher.push [notification]
      elsif params[:device_type] == 'android'

      else
        render json: {errors: ['Wrong device type.']}, status: :unprocessable_entity and return
      end

      render json: {
          message: 'Notification sent.'
      }
    elsif current_user
      current_user.notify params[:message], message: params[:message]

      render json: {
          message: 'Notification sent.'
      }
    end
  end
end