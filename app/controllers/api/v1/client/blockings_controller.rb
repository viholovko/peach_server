class Api::V1::Client::BlockingsController < Api::V1::Client::BaseController

  def create
    trainer = ::Trainer.find_by_id(params[:id])
    return render json: { errors: 'Trainer not found!' }, status: 422 if trainer.nil?
    blocking = current_user.blockings.new blocking_params.merge({to_user_id: trainer.id})
    blocking.validate_trainer_reason = true
    if blocking.save
      render json: {message: 'Trainer blocked!'}
    else
      render json: { errors: blocking.errors.full_messages }, status: 422
    end
  end

  private

  def blocking_params
    params.permit(:comment, block_trainer_reason_ids: [])
  end

end