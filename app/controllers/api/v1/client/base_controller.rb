class Api::V1::Client::BaseController < Api::V1::BaseController

  before_action :only_clients

  private

  def only_clients
    render json: {errors: ["You are not a client."]}, status: :unprocessable_entity and return unless current_user.client?
  end
end