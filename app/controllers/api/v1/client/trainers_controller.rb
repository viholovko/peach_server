class Api::V1::Client::TrainersController < Api::V1::Client::BaseController

  skip_before_action :authenticate_user, :only => [:index, :show]
  skip_before_action :only_clients, :only => [:index, :show]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = ::Trainer.explore_trainers(query_params.merge({confirmed: true, email_confirmed: true}), current_user)
    count_query = ::Trainer.explore_trainers(query_params.merge({confirmed: true, email_confirmed: true, count: true}), current_user)

    @total = Trainer.find_by_sql(count_query.to_sql).count
    @trainers = Trainer.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)

    redirect_to api_v1_client_top_trainers_path if @total == 0
  end

  def show
    @trainer = ::Trainer.find(params[:id])
    render json: {trainer: @trainer.details_json(current_user)}
  end

  private

  def query_params
    allowed_params = params.permit(:page, :per_page, :gender, :latitude, :longitude, :distance, :sort_type, :sort_column,
                                   :name, :sessions_price, training_hours: [:day, ranges: [:from_time, :to_time]],
                                   goal_ids: [], training_place_ids: [])
    allowed_params[:training_hours] = allowed_params[:training_hours].to_a.map{|i| {day: i[:day], ranges: i[:ranges].to_a.map{|r|
      {
        from_time: Time.at(r[:from_time].to_i).utc,
        to_time: Time.at(r[:to_time].to_i).utc
      }
    }}}

    allowed_params
  end
end
