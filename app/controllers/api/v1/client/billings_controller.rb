class Api::V1::Client::BillingsController < Api::V1::Client::BaseController

  def index
    query = Payment.query(current_user, params.merge({history: true}))
    count_query = Payment.query current_user, params.merge({count: true, history: true})

    @payments = Payment.find_by_sql(query)
    @total = Payment.find_by_sql(count_query).first['count']
  end
end