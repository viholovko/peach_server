class Api::V1::Client::BookingSessionsController < Api::V1::Client::BaseController

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = current_user.my_sessions params
    query_count = current_user.my_sessions params.merge({count:true})
    @sessions = BookingSession.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = BookingSession.find_by_sql(query_count.to_sql).count
  end

  def rate
    @booking_session = current_user.as_client.booking_sessions.find(params[:id])

    render json: {errors: ['Already rated!'] }, status: 422 and return if @booking_session.rate != nil

    if @booking_session.update_attributes rate_params
      render json: { message: 'Booking session rated!' }
    else
      render json: { errors: @booking_session.errors.full_messages }, status: 422
    end
  end

  private

  def rate_params
    params.permit :feeling_id, :rate
  end
end