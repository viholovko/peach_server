class Api::V1::Client::FavoriteTrainersController < Api::V1::Client::BaseController

  def index

    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = ::Client.favorite_trainers(current_user, params)
    count_query = ::Client.favorite_trainers(current_user, params.merge({count: true}))

    @trainers = ::User.find_by_sql(query.take(per_page).skip((page - 1) * per_page ).to_sql)
    @total = ::User.find_by_sql(count_query.to_sql).first[:count]

  end

 def create

   return render json: {errors: ["Trainer not found!"] }, status: 422 if ::Trainer.find_by_id(params[:id]).nil?

   @client = ::Client.find(current_user.id)

   if @client.update_attributes favorite_trainer_ids: @client.favorite_trainer_ids.push(params[:id].to_i)
      render json: {message: "Trainer added to favorites." }
    end
   end

 def destroy

   return render json: {errors: ["Trainer not found!"] }, status: 422 if ::Trainer.with_deleted.find_by_id(params[:id]).nil?

   @client = ::Client.find(current_user.id)

   if @client.update_attributes favorite_trainer_ids: @client.favorite_trainer_ids - [params[:id].to_i]
     render json: {message: "Trainer removed from favorites." }
   end
 end

end
