class Api::V1::Client::MyTrainersController < Api::V1::Client::BaseController

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1
    query = ::Client.my_trainers(current_user, params.merge({confirmed: true, email_confirmed: true}))
    count_query = ::Client.my_trainers(current_user, params.merge({confirmed: true, email_confirmed: true, count: true}))
    @trainers = ::User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @total = ::User.find_by_sql(count_query.to_sql).count
  end

end
