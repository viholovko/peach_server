class Api::V1::Client::BookingsController < Api::V1::Client::BaseController

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Booking.query(current_user, params)
    count_query = Booking.query(current_user, params.merge({count: true}))

    @bookings = Booking.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Booking.find_by_sql(count_query.to_sql).first['count']
  end

  def approve
    @booking = current_user.as_client.bookings.find_by(id: params[:id])

    if @booking && !@booking.approved?
      if @booking.approve @booking.booking_sessions.order(from_time: :asc).first&.from_time
        render json: {message: 'Booking approved!'}
      else
        render json: {errors: @booking.errors.full_messages}, status: :unprocessable_entity
      end
    else
      @change_request = BookingChangeRequest.find_by(to_user: current_user, booking_id: params[:id], active: true)
      raise ActiveRecord::RecordNotFound unless @change_request
      if @change_request.accept
        render json: {message: 'Change request approved.'}
      else
        render json: {errors: @change_request.errors.full_messages}, status: :unprocessable_entity
      end
    end
  end

  def reject
    @booking = current_user.as_client.bookings.find_by(id: params[:id])

    if @booking && !@booking.approved?
      if @booking.update_attributes reject_params
        render json: {message: 'Booking rejected!'}
      else
        render json: {errors: @booking.errors.full_messages }, status: 422
      end
    else
      @change_request = BookingChangeRequest.where(to_user: current_user, booking_id: params[:id], active: true).first
      raise ActiveRecord::RecordNotFound unless @change_request
      @change_request.reject
      render json: {message: 'Change request rejected.'}
    end
  end

  def cancel
    @booking = current_user.as_client.bookings.find(params[:id])

    if @booking.update_attributes cancel_params
      render json: {message: 'Booking cancelled!'}
     else
      render json: {errors: @booking.errors.full_messages }, status: 422
    end
  end

  def show
    @booking = current_user.as_client.bookings.find(params[:id])
    @next_session = BookingSession.where("booking_id =  ? AND from_time > ?", @booking.id, Time.now).order(from_time: :asc).first
  end

  private

  def query_params
    params.permit :pending
  end

  def reject_params
    allowed_params = params.permit reject_booking_reason_ids: []
    allowed_params[:status] = :rejected
    allowed_params
  end

  def cancel_params
    allowed_params = params.permit reject_booking_reason_ids: []
    allowed_params[:status] = :canceled_by_client
    allowed_params
  end

end