class Api::V1::Client::TopTrainersController < Api::V1::Client::BaseController

  skip_before_action :authenticate_user, :only => [:index]
  skip_before_action :only_clients, :only => [:index]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 3 if per_page < 1

    skip= (page - 1) * per_page
    query = ::Trainer.top_trainers(current_user)
    query_total = ::Trainer.top_trainers(current_user,  params.merge({count: true}))
    @trainers = ::User.find_by_sql((query.take(per_page).skip(skip)).to_sql)
    @total = ::User.find_by_sql((query_total).to_sql).count
  end

end
