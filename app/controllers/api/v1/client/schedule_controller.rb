class Api::V1::Client::ScheduleController < Api::V1::Client::BaseController

  def index
    @trainer = User.find(params[:trainer_id]).as_trainer
    @from = params[:from].present? ? Time.at(params[:from].to_i) : Time.now.beginning_of_day
    @to = params[:to].present? ? Time.at(params[:to].to_i) : Time.now.end_of_day

    schedule = Booking.schedule_for trainer: @trainer,
                                    from: @from,
                                    to: @to

    render json: schedule
  end
end