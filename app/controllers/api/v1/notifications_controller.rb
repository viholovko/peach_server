class Api::V1::NotificationsController < Api::V1::BaseController

  def index
    @page = params[:page].to_i
    @page = 1 if @page < 1
    @per_page = params[:per_page].to_i
    @per_page = 10 if @per_page < 1

    query = Notification.search_query(current_user, {parameters: params})
    count_query = query.clone.project('COUNT(*)')
    @notifications = Notification.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
    @total = Notification.find_by_sql(count_query.to_sql).count
  end

  def mark_as_read
    @notification = current_user.notifications.find params[:id]

    if @notification.update_attribute :read, true
      render json: { message: 'Marked as read!' }
    else
      render json: { errors: @notification.errors.full_messages }, status: 422
    end
  end

  def unread_count
    @unread_count = ::User.find_by_id(current_user.id)&.notifications&.where(read: false)&.count
  end

end