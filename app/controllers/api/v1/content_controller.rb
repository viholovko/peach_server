class Api::V1::ContentController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @goals = Goal.all
    @specialities = Speciality.all
    @block_client_reasons = BlockClientReason.all
    @block_trainer_reasons = BlockTrainerReason.all
    @training_places = TrainingPlace.all
    @feelings = Feeling.all
    @problem_report_reasons = ProblemReportReason.all
    @reject_booking_reasons = RejectBookingReason.all
    @system_settings = SystemSettings.instance
  end
end
