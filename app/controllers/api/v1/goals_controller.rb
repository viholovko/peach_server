class Api::V1::GoalsController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def index
    @goals = Goal.all
  end
end