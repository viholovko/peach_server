class Api::V1::CreditCardsController < Api::V1::BaseController

  load_and_authorize_resource :through => :current_user

  def create
    @credit_card = current_user.credit_cards.build(create_params)
    if @credit_card.save
      render json: {message: 'Credit card has been successfully added.'}
    else
      render json: {errors: @credit_card.errors.full_messages}, status: :unprocessable_entity
    end
  end
  def update
    old_card = current_user.credit_cards.where(active: true).first
    if @credit_card.activate
      old_card.try(:deactivate)
      render json: {message: 'Credit card has been successfully activated.'}
    end
  end

  def index
  end

  def destroy
    @credit_card.destroy
    render json: {message: 'Credit card has been successfully removed.'}
  end

  private

  def create_params
    params.permit :stripe_token
  end
end