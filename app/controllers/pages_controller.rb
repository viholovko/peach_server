class PagesController < ApplicationController

  skip_before_action :authenticate_user

  def index
      render layout: 'landing'
  end

  def app

  end

  def password_restored
    render layout: 'static'
  end
end