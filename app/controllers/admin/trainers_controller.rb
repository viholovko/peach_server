class Admin::TrainersController < Admin::BaseController

  # load_and_authorize_resource :trainer, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = Trainer.explore_trainers(params)
        count_query = Trainer.explore_trainers(params.merge({count: true}))

        @trainers = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.find_by_sql(count_query.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=trainers-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = TrainersStreamer.new()
      end
    end
  end

  def confirm
    @trainer = ::Trainer.with_deleted.find_by_id(params[:id])
    if @trainer && @trainer.update_attributes(confirmed: true)
      render json: {message: 'Trainer has been approved.'}
    else
      render json: {errors:  ["Can't confirm!"]}, status: :unprocessable_entity
    end
  end

  def show
    @trainer = User.with_deleted.find(params[:id])
  end

  def new

  end

  def blocked_clients
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = User.with_deleted.find(params[:id]).blocked_users_with_comment params
        @blocked_users = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.with_deleted.find(params[:id]).blocked_users.all.count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=blocked_clients-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = BlockedClientsStreamer.new(params[:id])
      end
    end
  end

  def blocked_by_clients
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = User.with_deleted.find(params[:id]).blocked_by_users_with_comment params
        @blocked_by_users = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.with_deleted.find(params[:id]).blocked_by_users.all.count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=blocked_by_clients-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = BlockedByClientsStreamer.new(params[:id])
      end
    end
  end

  def clients
    respond_to do |format|
      format.json do
        page = params[:page].to_i
        page = 1 if page < 1
        per_page = params[:per_page].to_i
        per_page = 10 if per_page < 1


        query = User.with_deleted.find(params[:id]).as_trainer.my_clients2(params)
        count_query = User.with_deleted.find(params[:id]).as_trainer.my_clients2(params.merge({count: true}))

        @clients = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page ).to_sql)
        @total = User.find_by_sql(count_query.to_sql).first.try(:[], 'count') || 0
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=blocked_by_clients-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = BlockedByClientsStreamer.new(params[:id])
      end
    end
  end

  def update
    @trainer = Trainer.with_deleted.find(params[:id])
    if @trainer.update_attributes trainer_params
      render json: {message: 'Trainer has been successfully updated.'}
    else
      render json: {errors: { validation_errors: @trainer.errors}}, status: :unprocessable_entity
    end
  end

  def create
    @trainer = Trainer.new trainer_params
    if @trainer.save
      render json: {message: 'Trainer has been successfully created.'}
    else
      render json: {errors: { validation_errors: @trainer.errors}}, status: :unprocessable_entity
    end
  end

  def destroy
    @trainer = User.with_deleted.find(params[:id])
    @trainer.deactivate_message = "Deactivated by admin"
    @trainer.destroy
    render json: {message: 'Trainer has been successfully removed.'}
  end

  def bookings
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = Trainer.with_deleted.find(params[:id]).my_bookings(params)
        query_count = Trainer.with_deleted.find(params[:id]).my_bookings(params.merge({count: true}))
        @sessions = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @count = User.find_by_sql(query_count.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=booking_sessions-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = TrainerBookingSessionsStreamer.new(params[:id])
      end
    end
  end

  private
  def trainer_params

  allowed_params = params.require(:trainer).permit :email, :email_confirmed, :password, :confirmed, :password_confirmation,
                                                   :avatar, :first_name, :last_name, :address, :bio, :deactivate_message,
                                                   :gender, :post_code, :latitude, :longitude, :location_radius, :sessions_price,
                                                   :qualification, speciality_ids: []

  allowed_params[:gender] = nil unless User.genders.include? allowed_params[:gender]

  allowed_params
  end

end
