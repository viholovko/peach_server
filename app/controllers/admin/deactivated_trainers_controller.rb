class Admin::DeactivatedTrainersController < Admin::BaseController

  #load_and_authorize_resource :trainer, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        params[:show_deleted] = true
        query = Trainer.explore_trainers(params)
        count_query = query.clone.project('COUNT(*)')

        @trainers = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.find_by_sql(count_query.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=deactivated_trainers-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = DeactivatedTrainersStreamer.new()
      end
    end
  end

  def reactivate
    @trainer = User.only_deleted.find_by_id(params[:id])
    if @trainer.update_attributes(deleted_at: nil, deactivate_message: "")
      render json: {message: 'Trainer has been successfully reactivated.'}
    else
      render json: {errors: @trainer.errors[:email]}, status: :unprocessable_entity
    end
  end

  def destroy
    user = User.with_deleted.find(params[:id])
    user.really_destroy!
    if user.errors.empty?
      render json: {message: 'Trainer has been successfully removed.'}
    else
      render json: {errors: user.errors.full_messages},status: :unprocessable_entity
    end
  end

end
