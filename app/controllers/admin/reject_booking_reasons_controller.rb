class Admin::RejectBookingReasonsController < Admin::BaseController

  load_and_authorize_resource :reject_booking_reason, parent: false

  def index
    @page = params[:page].to_i
    @page = 1 if @page < 1
    @per_page = params[:per_page].to_i
    @per_page = 10 if @per_page < 1

    query = RejectBookingReason.search_query(params)
    count_query = RejectBookingReason.search_query(params.merge({count: true}))

    @reject_booking_reasons = RejectBookingReason.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
    @total = RejectBookingReason.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def create
    @reject_booking_reason = RejectBookingReason.new reject_booking_reason_params

    if @reject_booking_reason.save
      render json: {message: 'Reject booking reason has been successfully added.'}
    else
      render json: {validation_errors: @reject_booking_reason.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @reject_booking_reason.update_attributes reject_booking_reason_params
      render json: {message: 'Reject booking reason has been successfully updated.'}
    else
      render json: {validation_errors: @reject_booking_reason.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @reject_booking_reason.destroy
    render json: {message: 'Reject booking reason has been successfully removed.'}
  end

  private

  def reject_booking_reason_params
    params.require(:reject_booking_reason).permit(:title)
  end

end
