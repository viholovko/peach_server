class Admin::PrivacyPolicyPageController < Admin::BaseController

  def create
    @page = PrivacyPolicyPage.first_or_create

    if @page.update_attributes page_params
      render json: { message: I18n.t('privacy_policy_page.messages.success_upsert') }
    else
      render json: { errors: @page.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def index
    @page = PrivacyPolicyPage.first_or_create
  end

  private 

  def page_params
    params.require(:privacy_policy_page).permit(:content)
  end

end