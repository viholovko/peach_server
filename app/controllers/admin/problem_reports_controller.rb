class Admin::ProblemReportsController < Admin::BaseController

  load_and_authorize_resource :problem_report, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1
        query = ProblemReport.search_query(params)
        count_query = ProblemReport.search_query(params.merge({count: true}))

        @problem_reports = ProblemReport.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        hash = {}
        ProblemReportReason.all.each {|i| hash[i.title] = i.id}
        @options = hash
        @total = ProblemReport.find_by_sql(count_query.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=problem_reports-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = ProblemReportsStreamer.new()
      end
    end
  end

  def destroy
    @problem_report.destroy
    render json: {message: 'Problem report has been successfully removed.'}
  end

end
