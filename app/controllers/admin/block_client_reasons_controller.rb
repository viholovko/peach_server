class Admin::BlockClientReasonsController < Admin::BaseController

  load_and_authorize_resource :block_client_reason, parent: false

  def index
    @page = params[:page].to_i
    @page = 1 if @page < 1
    @per_page = params[:per_page].to_i
    @per_page = 10 if @per_page < 1

    query = BlockClientReason.search_query(params)
    count_query = BlockClientReason.search_query(params.merge({count: true}))

    @block_client_reasons = BlockClientReason.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
    @total = BlockClientReason.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def create
    @block_client_reason = BlockClientReason.new reason_params

    if @block_client_reason.save
      render json: {message: 'Block client reason has been successfully added.'}
    else
      render json: {validation_errors: @block_client_reason.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @block_client_reason.update_attributes reason_params
      render json: {message: 'Block client reason has been successfully updated.'}
    else
      render json: {validation_errors: @block_client_reason.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @block_client_reason.destroy
    render json: {message: 'Block client reason has been successfully removed.'}
  end

  private

  def reason_params
    params.require(:block_client_reasons).permit(:title)
  end

end