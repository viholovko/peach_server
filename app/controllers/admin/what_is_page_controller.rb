class Admin::WhatIsPageController < Admin::BaseController

  def create
    @page = WhatIsPage.first_or_create

    if @page.update_attributes page_params
      render json: { message: I18n.t('what_is_page.messages.success_upsert') }
    else
      render json: { errors: @page.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def index
    @page = WhatIsPage.first_or_create
  end

  private 

  def page_params
    params.require(:what_is_page).permit(:content)
  end

end