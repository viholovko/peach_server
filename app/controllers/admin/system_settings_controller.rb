class Admin::SystemSettingsController < Admin::BaseController

  def update
    @system_settings = SystemSettings.instance

    if @system_settings.update_attributes settings_params
      render json: { message: 'System settings updated.' }
    else
      render json: { validation_errors: @system_settings.errors }, status: :unprocessable_entity
    end
  end

  def show
    @system_settings = SystemSettings.instance
  end

  private

  def settings_params
    params.require(:system_settings).permit :fee,:min_radius,:max_radius, :min_price, :max_price, :min_sessions, :max_sessions,
                                            :package_expiration_time,
                                            :ios_push_environment_sandbox, :ios_push_certificate, :ios_push_apns_host, :ios_push_password,
                                            :stripe_publishable_key, :stripe_secret_key,
                                            :qb_host, :qb_application_id, :qb_auth_key, :qb_auth_secret,
                                            :trainer_qualification_max_lenght, :trainer_bio_max_lenght
  end
end
