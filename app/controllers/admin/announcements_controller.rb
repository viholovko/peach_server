class Admin::AnnouncementsController < Admin::BaseController

  load_and_authorize_resource :announcement, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = Announcement.search_query(current_user, params)
        count_query = Announcement.search_query(current_user, params.merge({count: true}))

        @announcements = Announcement.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = Announcement.find_by_sql(count_query.to_sql).count
      end
    end
  end

  def show

  end

  def create
    @announcement = Announcement.new announcement_params

    if @announcement.save
      render json: {message: 'Announcement has been successfully created'}
    else
      render json: {validation_errors: @announcement.errors}, status: 422
    end
  end

  def update
    @announcement = Announcement.find params[:id]

    if @announcement.update_attributes announcement_params
      render json: {message: 'Announcement has been successfully updated'}
    else
      render json: {validation_errors: @announcement.errors}, status: 422
    end
  end

  def destroy
    @announcement.destroy
    render json: {message: 'Announcement has been successfully removed.'}
  end

  def resend
    @announcement.start_worker
    render json: {message: 'Announcement has been successfully resended.'}
  end

  private

  def announcement_params
    params.require(:announcement).permit :title, :subtitle, :image, :body, :notify_trainers, :notify_clients
  end

end