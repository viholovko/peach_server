class Admin::SessionsController < Admin::BaseController

  skip_before_action :authenticate_user, only: [:create, :check]
  skip_before_action :only_admins, only: [:create, :check, :destroy]

  def destroy
    sign_out
    head :ok
  end

  def create
    @user = User.find_by_email params[:email]

    if @user && @user.admin? && @user.authenticate(params[:password])
      sign_in user: @user
      render json: { session_token: current_session.token}
    else
      render json: { errors: ['Wrong email/password combination.'] }, status: :unprocessable_entity
    end
  end

  def check
    if current_user
      render json: { current_user: {email: current_user.email, role: current_user.role.name }}
    else
      head :unauthorized
    end
  end
end