class Admin::FeedbacksController < Admin::BaseController
  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query=Feedback.search params
    query_count=Feedback.search params.merge({count: true})

    @feedbacks = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @total=User.find_by_sql(query_count.to_sql).count
  end

  def avg
    render json: {average:Feedback.average(:stars).try(:round,3).try(:to_f)}
  end
end
