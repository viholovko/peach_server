class Admin::GoalsController < Admin::BaseController

  load_and_authorize_resource :goal, parent: false

  def index
    @page = params[:page].to_i
    @page = 1 if @page < 1
    @per_page = params[:per_page].to_i
    @per_page = 10 if @per_page < 1

    query = Goal.search_query(params)
    count_query = Goal.search_query(params.merge({count: true}))

    @goals = Goal.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
    @total = Goal.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def new
    @goal = Goal.new
    render action: 'show'
  end

  def create
    @goal = Goal.new goal_params

    if @goal.save
      render json: {message: 'Goal has been successfully added.'}
    else
      render json: {validation_errors: @goal.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @goal.update_attributes goal_params
      render json: {message: 'Goal has been successfully updated.'}
    else
      render json: {validation_errors: @goal.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @goal.destroy
    render json: {message: 'Goal has been successfully removed.'}
  end

  private

  def goal_params
    params.require(:goal).permit(:title, speciality_ids: [])
  end

end