class Admin::DeactivatedClientsController < Admin::BaseController

  #load_and_authorize_resource :client, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        params[:show_deleted] = true
        query = Client.search_query(params)
        count_query = query.clone.project('COUNT(*)')

        @clients = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.find_by_sql(count_query.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=deactivated_clients-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = DeactivatedClientsStreamer.new()
      end
    end
  end

  def reactivate
    @client = User.only_deleted.find_by_id(params[:id])
    if @client.update_attributes(deleted_at: nil, deactivate_message: "")
      render json: {message: 'Client has been successfully reactivated.'}
    else
      render json: {errors: @client.errors[:email]}, status: :unprocessable_entity
    end
  end

  def destroy
    user = User.with_deleted.find(params[:id])
    user.really_destroy!
    if user.errors.empty?
      render json: {message: 'Client has been successfully removed.'}
    else
      render json: {errors: user.errors.full_messages},status: :unprocessable_entity
    end
  end

end
