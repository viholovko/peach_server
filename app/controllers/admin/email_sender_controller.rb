class Admin::EmailSenderController < Admin::BaseController

  def update
    @email_sender = EmailSender.first_or_create
    if @email_sender.update_attributes sender_params
      render json: { message: "Settings updated." }
    else
      render json: { validation_errors: @email_sender.errors }, status: :unprocessable_entity
    end
  end

  def show
    @email_sender = EmailSender.first_or_create
  end

  private 

  def sender_params
    params.require(:email_sender).permit :address, :port, :domain, :authentication, :user_name, :password, :enable_starttls_auto
  end

end