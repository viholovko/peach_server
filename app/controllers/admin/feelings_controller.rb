class Admin::FeelingsController < Admin::BaseController

  load_and_authorize_resource :feeling, parent: false

  def index
    query = Feeling.search_query(params)
    count_query = Feeling.search_query(params.merge({count: true}))

    @feelings = Feeling.find_by_sql(query.to_sql)
    @total = Feeling.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def create
    @feeling = Feeling.new feeling_params

    if @feeling.save
      render json: {message: 'Session rating has been successfully added.'}
    else
      render json: {validation_errors: @feeling.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @feeling.update_attributes feeling_params
      render json: {message: 'Session rating has been successfully updated.'}
    else
      render json: {validation_errors: @feeling.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @feeling.destroy
    render json: {message: 'Session rating has been successfully removed.'}
  end

  private

  def feeling_params
    params.require(:feeling).permit(:title)
  end

end
