class Admin::SpecialitiesController < Admin::BaseController

  load_and_authorize_resource :speciality, parent: false

  def index
    @page = params[:page].to_i
    @page = 1 if @page < 1
    @per_page = params[:per_page].to_i
    @per_page = 10 if @per_page < 1

    query = Speciality.search_query(params)
    count_query = Speciality.search_query(params.merge({count: true}))

    @specialities = Speciality.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
    @total = Speciality.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def new
    @speciality = Speciality.new
    render action: 'show'
  end

  def create
    @speciality = Speciality.new speciality_params

    if @speciality.save
      render json: {message: 'Speciality has been successfully added.'}
    else
      render json: {validation_errors: @speciality.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @speciality.update_attributes speciality_params
      render json: {message: 'Speciality has been successfully updated.'}
    else
      render json: {validation_errors: @speciality.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @speciality.destroy
    render json: {message: 'Speciality has been successfully removed.'}
  end

  private

  def speciality_params
    params.require(:speciality).permit(:title)
  end

end