class Admin::AdminsController < Admin::BaseController

  load_and_authorize_resource :user, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = Admin.search_query(params)
        count_query = query.clone.project('COUNT(*)')

        @admins = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.find_by_sql(count_query.to_sql).count
      end
    end
  end

  def show

  end

  def create
    @user = User.find_by_id(params[:id])
    @user = User.new role: Role.admin unless @user

    if @user.update_attributes admin_params
      render json: {message: 'Admin successfully updated'}
    else
      render json: {validation_errors: @user.errors}, status: 422
    end
  end

  def destroy
    admin = Admin.with_deleted.find(params[:id])

    if admin.id == current_user.id
      render json: {errors: ["You can not remove your current profile!"]}, status: 422
    else
      if admin.really_destroy!
        render json: {message: 'Admin has been successfully removed.'}
      else
        render json: {errors: admin.errors.full_messages}, status: 422
      end
    end


  end

  private

  def admin_params
    params.permit(:first_name, :email, :password, :password_confirmation, :last_name)
  end

end