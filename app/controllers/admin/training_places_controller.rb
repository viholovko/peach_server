class Admin::TrainingPlacesController < Admin::BaseController

  load_and_authorize_resource :training_place, parent: false

  def index
    query = TrainingPlace.search_query(params)
    count_query = TrainingPlace.search_query(params.merge({count: true}))

    @training_places = TrainingPlace.find_by_sql(query.to_sql)
    @total = TrainingPlace.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def create
    @training_place = TrainingPlace.new training_place_params

    if @training_place.save
      render json: {message: 'Location place has been successfully added.'}
    else
      render json: {validation_errors: @training_place.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @training_place.update_attributes training_place_params
      render json: {message: 'Location place has been successfully updated.'}
    else
      render json: {validation_errors: @training_place.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @training_place.destroy
    render json: {message: 'Location place has been successfully removed.'}
  end

  private

  def training_place_params
    params.require(:training_place).permit(:title)
  end

end
