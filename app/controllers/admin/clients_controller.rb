class Admin::ClientsController < Admin::BaseController

  # load_and_authorize_resource :client, parent: false

  def index
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = Client.search_query(params)
        count_query = query.clone.project('COUNT(*)')

        @clients = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.find_by_sql(count_query.to_sql).count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=clients-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = ClientsStreamer.new()
      end
    end
  end

  def show
    @client = User.with_deleted.find(params[:id])
  end

  def blocked_trainers
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = User.with_deleted.find(params[:id]).blocked_users_with_comment params
        @blocked_users = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.with_deleted.find(params[:id]).blocked_users.all.count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=blocked_trainers-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = BlockedTrainersStreamer.new(params[:id])
      end
    end
  end

  def unblock_trainer
    blocking = Blocking.where("from_user_id = ? AND to_user_id = ?", params[:id], params[:trainer_id]).first
    render json: { errors: ['Blocking not found'] }, status: :unprocessable_entity and return if blocking.nil?
    if blocking.delete
      render json: { message: "Trainer unblocked!"}
    else
      render json: { errors: blocking.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def blocked_by_trainers
    respond_to do |format|
      format.json do
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1

        query = User.with_deleted.find(params[:id]).blocked_by_users_with_comment params
        @blocked_by_users = User.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = User.with_deleted.find(params[:id]).blocked_by_users.all.count
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=blocked_by_trainers-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = BlockedByTrainersStreamer.new(params[:id])
      end
    end
  end

  def favorite_trainers
    respond_to do |format|
      format.json do
        page = params[:page].to_i
        page = 1 if page < 1
        per_page = params[:per_page].to_i
        per_page = 10 if per_page < 1

        query = ::Client.favorite_trainers(User.with_deleted.find(params[:id]), params)
        count_query = ::Client.favorite_trainers(User.with_deleted.find(params[:id]), params.merge({count: true}))

        @trainers = ::User.find_by_sql(query.take(per_page).skip((page - 1) * per_page ).to_sql)
        @total = ::User.find_by_sql(count_query.to_sql).first[:count]
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=favorite_trainers-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = FavoriteTrainersStreamer.new(params[:id])
      end
    end
  end

  def booking_sessions
    respond_to do |format|
      format.json do
        page = params[:page].to_i
        page = 1 if page < 1
        per_page = params[:per_page].to_i
        per_page = 10 if per_page < 1

        query = ::Client.booking_sessions(User.with_deleted.find(params[:id]), params)
        count_query = query.clone.project('COUNT(*)')

        @booking_sessions = BookingSession.find_by_sql(query.take(per_page).skip((page - 1) * per_page ).to_sql)
        @total = ProblemReport.find_by_sql(count_query.to_sql).count
        @options = Booking.statuses
      end
      format.csv do
        headers["Content-Type"]        = "text/csv"
        headers["Content-disposition"] = "attachment; filename=booking_sessions-#{Time.now.strftime('%d-%m-%Y')}.csv"
        headers['Last-Modified']       = Time.now.ctime.to_s
        self.response_body = ClientBookingSessionsStreamer.new(params[:id])
      end
    end
  end

  def update
    @client = User.with_deleted.find(params[:id])
    if @client.update_attributes client_params
      render json: {message: 'Client has been successfully updated.'}
    else
      render json: {validation_errors: @client.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @client = User.find(params[:id])
    @client.deactivate_message = "Deactivated by admin"
    @client.destroy
    render json: {message: 'Client has been successfully removed.'}
  end

  private

  def client_params
    allowed_params = params.require(:client).permit(
      :email, :email_confirmed, :password, :password_confirmation, :avatar,
      :first_name, :last_name, :address, :bio, :deactivate_message, :gender,
      :post_code, :latitude, :longitude, :location_radius
    )
    allowed_params
  end
end
