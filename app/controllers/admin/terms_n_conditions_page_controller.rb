class Admin::TermsNConditionsPageController < Admin::BaseController

  def create
    @page = TermsNConditionsPage.first_or_create

    if @page.update_attributes page_params
      render json: { message: I18n.t('terms_n_conditions_page.messages.success_upsert') }
    else
      render json: { errors: @terms_n_conditions_page.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def index
    @page = TermsNConditionsPage.first_or_create
  end

  private 

  def page_params
    params.require(:terms_n_conditions_page).permit :content
  end

end