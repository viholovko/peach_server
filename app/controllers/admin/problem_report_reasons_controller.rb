class Admin::ProblemReportReasonsController < Admin::BaseController

  load_and_authorize_resource :problem_report_reason, parent: false

  def index
    query = ProblemReportReason.search_query(params)
    count_query = ProblemReportReason.search_query(params.merge({count: true}))

    @problem_report_reasons = ProblemReportReason.find_by_sql(query.to_sql)
    @total = ProblemReportReason.find_by_sql(count_query.to_sql).count
  end

  def show

  end

  def create
    @problem_report_reason = ProblemReportReason.new problem_report_reason_params

    if @problem_report_reason.save
      render json: {message: 'Problem report reason has been successfully added.'}
    else
      render json: {validation_errors: @problem_report_reason.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @problem_report_reason.update_attributes problem_report_reason_params
      render json: {message: 'Problem report reason has been successfully updated.'}
    else
      render json: {validation_errors: @problem_report_reason.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @problem_report_reason.destroy
    render json: {message: 'Problem report reason has been successfully removed.'}
  end

  private

  def problem_report_reason_params
    params.require(:problem_report_reason).permit(:title)
  end

end
