class Admin::AdminNotificationsController < Admin::BaseController

  load_and_authorize_resource :admin_notification, parent: false

  def index
        @page = params[:page].to_i
        @page = 1 if @page < 1
        @per_page = params[:per_page].to_i
        @per_page = 10 if @per_page < 1
        query = AdminNotification.search_query(params)
        count_query = AdminNotification.search_query(params.merge({count: true}))

        @notifications = AdminNotification.find_by_sql(query.take(@per_page).skip((@page - 1) * @per_page).to_sql)
        @total = AdminNotification.find_by_sql(count_query.to_sql).count
  end

  def mark_as_read
    if @admin_notification.update_attribute(:readed, true)
      render json: {
          ok: "Ok"
      }
    end
  end

end
