class Admin::UsersController < Admin::BaseController

  load_and_authorize_resource :user, parent: false

  def destroy
    if @user.id == current_user.id
      render json: {errors: ['Can not remove yourself.']}, status: 422 and return
    end

    if @user.destroy
      render json: {message: 'User successfully removed.' }
    else
      render json: {errors: @user.errors.full_messages }
    end
  end

end