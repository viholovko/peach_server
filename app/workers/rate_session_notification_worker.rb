class RateSessionNotificationWorker
  include Sidekiq::Worker
  sidekiq_options queue: "rate_notification"
  sidekiq_options retry: false

  def perform()
    continue = true

    loop do
      BookingSession.transaction do
        session = BookingSession.joins(:booking)
                      .where("to_time < '#{ Time.now.strftime("%Y-%m-%d %H:%M:00") }'")
                      .where(bookings: {status: 'approved'})
                      .where(notified_about_rate: false)
                      .where(paid_up: true)
                      .order(from_time: :asc)
                      .first

        continue = !!session
        break unless continue

        session.update_attribute :notified_about_rate, true

        Notification.create(
            user: session.booking.client,
            notification_type: :rate_session,
            data: {target_id: session.id, from_user: session.booking.trainer_id, to_time: session.to_time, booking_session_id: session.id},
            booking_id: session.booking_id,
            action_required: true,
            booking_session_id: session.id

        )
      end

      break unless continue
    end
  end
end