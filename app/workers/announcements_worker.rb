class AnnouncementsWorker
  include Sidekiq::Worker
  sidekiq_options queue: "announcements"
  sidekiq_options retry: false

  def perform(id)
    Announcement.find(id).notify
  end
end
