class ApplyChangeRequestsWorker
  include Sidekiq::Worker
  sidekiq_options queue: "apply_change_requests"
  sidekiq_options retry: false

  def perform()
    continue = true

    loop do
      BookingChangeRequest.transaction do
        request = BookingChangeRequest.where("date > ? AND applied = ?", Date.today, false).first

        continue = !!request
        break unless continue

        request.apply
      end

      break unless continue
    end
  end

end

ApplyChangeRequestsWorker