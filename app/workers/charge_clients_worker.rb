class ChargeClientsWorker
  include Sidekiq::Worker
  sidekiq_options queue: "charge_clients"
  sidekiq_options retry: false

  def perform()
    continue = true

    loop do
      Booking.transaction do
        booking = Booking.approved.renewable.where("last_payment_date < ? AND notified_about_payment_problems = ?", Date.today.beginning_of_month, false).first

        continue = !!booking
        break unless continue

        if approve_booking booking, Date.today

        else
          Notification.create(
              user: booking.client,
              notification_type: :client_charge_failed,
              data: {
                  target_id: booking.id,
                  from_user: booking.trainer_id
              },
              booking_id: booking.id
          )

          booking.reload.assign_attributes notified_about_payment_problems: true, notified_about_payment_problems_date: Date.today
          booking.save validate: false
        end
      end

      break unless continue
    end

    continue = true

    loop do
      Booking.transaction do
        booking = Booking.approved.renewable.where("notified_about_payment_problems = ? AND notified_about_payment_problems_date < ?", true, Date.today - 2.days).first

        continue = !!booking
        break unless continue

        if approve_booking booking, Date.today
          booking.assign_attributes notified_about_payment_problems: false, notified_about_payment_problems_date: nil
          booking.save validate: false
        else

          cancel_booking(booking)

          Notification.create(
              user: booking.client,
              notification_type: :client_charge_failed_after_two_days,
              data: {
                  target_id: booking.id,
                  from_user: booking.trainer_id
              },
              booking_id: booking.id
          )

          Notification.create(
              user: booking.trainer,
              notification_type: :client_charge_failed_after_two_days,
              data: {
                  target_id: booking.id,
                  from_user: booking.client_id
              },
              booking_id: booking.id
          )

          AdminNotification.create(user: booking.client, notify_type: "charge_failed")
        end
      end

      break unless continue
    end
  end

  private

  def approve_booking(booking, month)
    return true if booking.free? || booking.consultation?

    payment = Payment.new date: month, transaction_type: 'charge_client', booking_id: booking.id
    if payment.save
      booking.update_attribute :status, 'finished'

      new_booking = Booking.new  client_id: booking.client_id,
                                 trainer_id: booking.trainer_id,
                                 booking_type: booking.booking_type,
                                 status: 'approved',
                                 latitude: booking.latitude,
                                 longitude: booking.longitude,
                                 session_price: booking.session_price,
                                 address: booking.address,
                                 training_place_id: booking.training_place_id,
                                 estimated_sessions_count_per_month: booking.estimated_sessions_count_per_month,
                                 estimated_first_session_date_in_month: booking.estimated_first_session_date_in_month,
                                 estimated_last_session_date_in_month: booking.estimated_last_session_date_in_month,
                                 last_payment_date: booking.last_payment_date,
                                 notified_about_payment_problems: booking.notified_about_payment_problems,
                                 notified_about_payment_problems_date: booking.notified_about_payment_problems_date,
                                 booking_session_days_attributes: booking.booking_session_days.map{|i|{
                                   day: i.day,
                                   from_time: i.from_time,
                                   to_time: i.to_time
                                 }},
                                 skip_estimation: true,
                                 skip_notification_about_create: true

      new_booking.save validate: false

      booking.booking_sessions.where("from_time > ?", Time.now.beginning_of_month.utc).update_all booking_id: new_booking.id
      booking.session_change_requests.update_all booking_id: new_booking.id
      booking.booking_change_requests.update_all booking_id: new_booking.id
      payment.update_attribute :booking_id, new_booking.id
      booking.update_attribute :next_booking_id, new_booking.id
      true
    else
      false
    end
  end

  def cancel_booking(booking)
    booking.reload

    booking.assign_attributes status: :finished
    booking.save validate: false

    payment = booking.payments.last
    booking.booking_sessions.where(payment_id: nil).destroy_all
  end
end