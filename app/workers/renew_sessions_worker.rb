class RenewSessionsWorker
  include Sidekiq::Worker
  sidekiq_options queue: "renew"
  sidekiq_options retry: false

  def perform()
    Booking.approved.renewable.find_each do |booking|
      last_session = booking.booking_sessions.last

      booking.booking_sessions_attributes = Booking.future_sessions_attributes(last_session.from_time.beginning_of_day + 1.day, booking.booking_session_days.map{ |d|
        {
            from_time: d[:from_time],
            to_time: d[:to_time],
            day: d[:day],
        }
      })

      if booking.save

      else
        #TODO booking cannot continue, because trainer changed his working hours
      end
    end
  end
end
