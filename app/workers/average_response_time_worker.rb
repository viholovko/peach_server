class AverageResponseTimeWorker
  include Sidekiq::Worker
  sidekiq_options queue: "average_response_time"
  sidekiq_options retry: false

  def perform()
    Trainer.find_each do |trainer|

      trainer.qb_login = "trainer@mail.com_trainer_1512051959"
      trainer.qb_password = "94e59774b1122ffd2c6a254c26322b790ebfd9ca"
      trainer.qb_user_id = 38082967

      dialogs = QuickBlox::Dialogs.list trainer.get_qb_session

      items = []

      dialogs.each do |dialog|
        puts "============================="
        previous_message_from_client = false
        previous_message = nil

        dialog.messages.each do |message|
          if message.sender_id != trainer.qb_user_id
            previous_message_from_client = true
            previous_message = message
          end

          if message.sender_id == trainer.qb_user_id
            if previous_message_from_client
              puts "===#{ Time.at(previous_message.date_sent) }====#{ previous_message.sender_id == trainer.qb_user_id ? 'trainer' : 'client'}==#{ previous_message.message}"
              puts "===#{ Time.at(message.date_sent) }====#{ message.sender_id == trainer.qb_user_id ? 'trainer' : 'client'}==#{ message.message}"

              items << message.date_sent - previous_message.date_sent
            else

            end

            previous_message_from_client = false
          end
        end
      end

      trainer.assign_attributes average_response_time: items.sum / items.count,
                                average_response_time_calculated_at: Time.now

      trainer.save validate: false
    end
  end
end
