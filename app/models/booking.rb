class Booking < ApplicationRecord

  self.skip_time_zone_conversion_for_attributes = [:estimated_first_session_date_in_month, :estimated_last_session_date_in_month]

  belongs_to :client
  belongs_to :trainer
  has_many :booking_sessions, dependent: :destroy
  has_many :booking_session_days, dependent: :destroy
  has_and_belongs_to_many :reject_booking_reasons
  belongs_to :training_place
  has_many :payments, dependent: :destroy
  has_many :session_change_requests, through: :booking_sessions, dependent: :destroy
  has_many :booking_change_requests, dependent: :destroy
  has_many :notifications, dependent: :destroy

  accepts_nested_attributes_for :booking_sessions, allow_destroy: true
  accepts_nested_attributes_for :booking_session_days, allow_destroy: true
  accepts_nested_attributes_for :payments

  validate :at_least_one_reject_booking_reason, if: :check_status
  validates :session_price,
            presence: true,
            numericality: {
                greater_than_or_equal_to: SystemSettings.instance.min_price,
                less_than_or_equal_to: SystemSettings.instance.max_price
            },
            unless: -> { self.free? || self.consultation? }
  validates :trainer_id, :presence =>true, :allow_blank => false
  validate  :trainer_should_be_a_trainer
  validates :client_id, :presence =>true, :allow_blank => false
  validate  :client_should_be_a_client
  validate  :booking_sessions_should_not_overlap
  validates :booking_type, :presence =>true, :allow_blank => false
  validates :address, :presence =>true
  validate  :booking_type_should_be_valid
  validate  :check_rejection
  validate  :check_cancellation
  validate  :check_trainer_availability, :on => [:create, :update]
  validate  :check_sessions_count
  validate  :inclusion_in_trainer_working_hours
  validate :session_days_should_be_in_future

  after_update :notify_update
  after_create :notify_create
  before_create :estimate
  before_validation :set_free
  before_validation :check_trainer_availability, if: -> {self.status_changed?(from: 'pending', to: 'approved')}
  after_save :set_free_as_paid, if: -> {(self.free? || self.consultation?) && self.status_changed?(from: 'pending', to: 'approved')}

  attr_accessor :subscription_data
  attr_accessor :skip_notification_about_create
  attr_accessor :skip_estimation

  enum booking_type: {
      renewable: 0,
      once: 1,
      free: 2,
      consultation: 3
  }

  enum status: {
      pending: 0,
      approved: 1,
      rejected: 2,
      canceled_by_client: 3,
      canceled_by_trainer: 4,
      payment_failed: 5,
      finished: 6
  }

  def booking_price
    case booking_type
      when 'once'
        self.booking_sessions.count * self.session_price
      when 'renewable'
        if pending? || rejected?
          estimated_sessions_count_per_month * self.session_price
        else
          payments.order(date: :desc).first&.booking_sessions&.count.to_i * self.session_price
        end
      when 'free'
        0
      when 'consultation'
        0
    end
  end

  def booking_type=(value)
    super value
    @booking_type_backup = nil
  rescue
    @booking_type_backup = value
    super nil
  end

  def booking_sessions_attributes=(attrs)
    attrs = [] unless attrs.is_a?(Array)

    if new_record?
      days_attributes = []

      attrs.each do |day|
        days_attributes << {
            from_time: day[:from_time].strftime("%H:%M"),
            to_time: day[:to_time].strftime("%H:%M"),
            day: day[:from_time].wday,
        }
      end

      self.booking_session_days_attributes = days_attributes

      if renewable?
        super(Booking.future_sessions_attributes(attrs.sort_by{|i| i[:from_time] }.first[:from_time], days_attributes))
        return
      end
    end

    super(attrs)
  end

  def self.query(user, options)
    users = User.arel_table
    opponents = User.arel_table.alias('opponents')
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table
    payments = Payment.arel_table

    q = bookings
            .join(users).on(bookings[user.client? ? :client_id : :trainer_id].eq(users[:id]))
            .join(opponents).on(bookings[user.client? ? :trainer_id : :client_id].eq(opponents[:id]))

    if options[:count]
      q.project("COUNT(*)")
    else
      if options[:sort_column] == 'created_at' && %w(asc desc).include?(options[:sort_type])
        q.order(bookings[:created_at].send(options[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.order(bookings[:id].desc)
      end

      first_session_in_this_month_query = booking_sessions
                                              .project(booking_sessions[:from_time])
                                              .join(payments).on(payments[:id].eq(booking_sessions[:payment_id]))
                                              .where(payments[:booking_id].eq(bookings[:id]))
                                              .order(payments[:date].desc, booking_sessions[:from_time].asc)
                                              .take(1)

      last_session_in_this_month_query = booking_sessions
                                              .project(booking_sessions[:from_time])
                                              .join(payments).on(payments[:id].eq(booking_sessions[:payment_id]))
                                              .where(payments[:booking_id].eq(bookings[:id]))
                                              .order(payments[:date].desc, booking_sessions[:from_time].desc)
                                              .take(1)

      q.project(
          opponents[:id].as('opponent_id'),
          opponents[:first_name].as('opponent_first_name'),
          opponents[:last_name].as('opponent_last_name'),
          opponents[:avatar_file_name].as('opponent_avatar_file_name'),
          opponents[:avatar_content_type].as('opponent_avatar_content_type'),
          opponents[:avatar_file_size].as('opponent_avatar_file_size'),
          opponents[:last_name].as('opponent_last_name'),
          "(SELECT COUNT(*)
              FROM booking_session_days
              WHERE
                booking_session_days.booking_id = bookings.id
              ) as session_days_count",

          "(#{ booking_sessions
                   .project("COUNT(*)")
                   .where(booking_sessions[:booking_id].eq(bookings[:id]))
                   .to_sql
          }) as sessions_count",

          "(#{ booking_sessions
                   .project("COUNT(*)")
                   .where(booking_sessions[:booking_id].eq(bookings[:id]))
                   .where(booking_sessions[:paid_up].eq(true)
                      .or(bookings[:booking_type].in([Booking.booking_types[:free], Booking.booking_types[:consultation]])))
                   .to_sql
          }) as paid_sessions_count",

          "(SELECT COUNT(*)
              FROM booking_session_days
              WHERE
                booking_session_days.booking_id = bookings.id
              ) as sessions_in_this_week_count",

          "(#{ payments
                   .project(payments[:sessions_count])
                   .where(payments[:booking_id].eq(bookings[:id]))
                   .order(payments[:date].desc)
                   .take(1)
                   .to_sql
          }) as paid_sessions_in_this_month_count",

          "(#{ booking_sessions
                   .project(booking_sessions[:from_time])
                   .where(booking_sessions[:booking_id].eq(bookings[:id]))
                   .where(booking_sessions[:paid_up].eq(true)
                      .or(bookings[:booking_type].in([Booking.booking_types[:free], Booking.booking_types[:consultation]]))
                      .or(bookings[:status].eq(Booking.statuses[:pending])))
                   .order(booking_sessions[:from_time].asc)
                   .take(1)
                   .to_sql
          }) as first_session_date",

          "(#{ booking_sessions
                   .project(booking_sessions[:from_time])
                   .where(booking_sessions[:booking_id].eq(bookings[:id]))
                   .where(booking_sessions[:paid_up].eq(true)
                      .or(bookings[:booking_type].in([Booking.booking_types[:free], Booking.booking_types[:consultation]]))
                      .or(bookings[:status].eq(Booking.statuses[:pending])))
                   .order(booking_sessions[:from_time].desc)
                   .take(1)
                   .to_sql
          }) as last_session_date",

          "(SELECT from_time
              FROM booking_sessions
              WHERE
                booking_sessions.booking_id = bookings.id
                AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
              ORDER BY from_time ASC
              LIMIT 1
              ) as first_session_in_this_week_date",
          "(SELECT from_time
              FROM booking_sessions
              WHERE
                booking_sessions.booking_id = bookings.id
                AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
              ORDER BY from_time DESC
              LIMIT 1
              ) as last_session_in_this_week_date",

          "(#{ first_session_in_this_month_query.to_sql }) as first_session_in_this_month_date",
          "(#{ last_session_in_this_month_query.to_sql }) as last_session_in_this_month_date",

          "(SELECT COUNT(*)
              FROM booking_sessions
              WHERE
                booking_sessions.booking_id = bookings.id
                AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
              ) as sessions_in_this_week_count",

          bookings[:booking_type],
          bookings[:status],
          bookings[:session_price],
          bookings[:latitude],
          bookings[:longitude],
          bookings[:address],
          bookings[:estimated_sessions_count_per_month],
          bookings[:estimated_first_session_date_in_month],
          bookings[:estimated_last_session_date_in_month],
          bookings[:id],
          "(SELECT updated_at FROM sessions WHERE sessions.user_id = opponents.id ORDER BY updated_at DESC LIMIT 1) as online",
          "(gc_dist(#{ user.latitude.to_f }, #{ user.longitude.to_f }, bookings.latitude, bookings.longitude)) AS distance",
      )
    end

    if options[:status].present? && options[:status] == 'approved'
      q.where(bookings[:status].in([Booking.statuses[:approved], Booking.statuses[:finished]]))
    else
      q.where(bookings[:status].eq(Booking.statuses[options[:status].to_sym])) if options[:status].present?
    end

    q.where(bookings[:booking_type].eq(Booking.booking_types[options[:booking_type].to_sym])) if options[:booking_type].present?
    q.where(users[:id].eq(user.id))

    q
  end

  def self.schedule_for(trainer:, from:, to:, options: {})
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table
    clients = User.arel_table.alias(:clients)
    trainers = User.arel_table.alias(:trainers)

    q = booking_sessions
        .join(bookings).on(booking_sessions[:booking_id].eq(bookings[:id]))
        .join(clients).on(bookings[:client_id].eq(clients[:id]))
        .join(trainers).on(bookings[:trainer_id].eq(trainers[:id]))
        .where(trainers[:id].eq(trainer.id))
        .where(booking_sessions[:from_time].gteq(from.beginning_of_day))
        .where(booking_sessions[:from_time].lteq(to.end_of_day))
        .where(bookings[:status].eq(Booking.statuses[:approved]))
        .project(
            booking_sessions[:id],
            clients[:id].as('client_id'),
            clients[:avatar_file_name],
            clients[:avatar_content_type],
            clients[:avatar_file_size],
            "(SELECT updated_at FROM sessions WHERE sessions.user_id = clients.id ORDER BY sessions.updated_at DESC LIMIT 1) as online",
            clients[:first_name],
            clients[:last_name],
            bookings[:address],
            bookings[:latitude],
            bookings[:longitude],
            "(gc_dist(#{ trainer.latitude.to_f }, #{ trainer.longitude.to_f }, bookings.latitude, bookings.longitude)) AS distance",
            booking_sessions[:from_time],
            booking_sessions[:to_time]
        )

    booking_sessions = BookingSession.find_by_sql(q.to_sql)

    result = []

    (from.to_date..to.to_date).each do |day| # TODO move to view ?
      working_days_history = trainer.schedule_histories.where("created_at <= ?", day.end_of_day).order(created_at: :desc).first&.value || trainer.schedules.map(&:to_json)
      working_days_history = working_days_history.select{|i| i[:day] == day.wday }
      working_days_history = [] if day < trainer.created_at.beginning_of_day

      result << {
          day: day.to_time.to_f,
          working_hours: {
              day: day.wday,
              ranges: working_days_history.map{|i|
                {
                    from_time: i[:from_time].to_f,
                    to_time: i[:to_time].to_f
                }
              }
          },
          sessions: booking_sessions.select{|i| i.from_time > day.beginning_of_day && i.to_time < day.end_of_day }.map{|i|
            fake_client = User.new(
                id: i['client_id'],
                avatar_file_name: i['avatar_file_name'],
                avatar_content_type: i['avatar_content_type'],
                avatar_file_size: i['avatar_file_size'],
            )

            session = {
                id: i.id,
                from_time: i.from_time.to_f,
                to_time: i.to_time.to_f,
            }

            if options[:detailed]
              session.merge!(
                  {
                      client_id: i['client_id'],
                      avatar: (fake_client.avatar.present? ? "#{ ENV['HOST'] }#{ fake_client.avatar.try(:url, :medium)}" : nil),
                      online: (i['online'].present? && i['online'] > Time.now - 15.minutes),
                      first_name: i['first_name'],
                      last_name: i['last_name'],
                      address: i['address'],
                      latitude: i['latitude'],
                      longitude: i['longitude'],
                      distance: i['distance'],
                  }
              )
            end

            session
          }
      }
    end

    result
  end

  def self.future_sessions_attributes(from, booking_session_days)
    return if from > Date.today + 3.months

    from = from.beginning_of_day.to_date
    to = (Date.today + 3.months).end_of_month
    result = []

    booking_session_days.each do |session_day|
      from_hour, from_minute = session_day[:from_time].split(':').map(&:to_i)
      to_hour, to_minute = session_day[:to_time].split(':').map(&:to_i)

      (from..to).to_a.select{|day| day.wday == session_day[:day]}.each do |day|
        from_time = day.in_time_zone('UTC').change(hour: from_hour, min: from_minute)

        result << {
            from_time: from_time,
            to_time: day.in_time_zone('UTC').change(hour: to_hour, min: to_minute)
        }
      end
    end

    result
  end

  def approve(month)
    update_attributes status: :approved,
                      payments_attributes: (free? || consultation?) ? [] : [{ date: month, transaction_type: 'charge_client', booking_id: self.id }]
  end

  def paid_sessions_in_this_month_count
    payments.order(date: :desc).first&.booking_sessions&.count
  end

  def first_session_in_this_month_date
    approved? ?
        payments.order(date: :desc).first&.booking_sessions&.sort_by(&:from_time).try(:first).try(:from_time).try(:to_f)
    :
        booking_sessions.sort_by(&:from_time).first&.from_time
  end

  def last_session_in_this_month_date
    if renewable?
      if pending? || rejected?
        estimated_last_session_date_in_month
      else
        payments.order(date: :desc).first&.booking_sessions&.sort_by(&:from_time).try(:last).try(:from_time)
      end
    else
      booking_sessions.sort_by(&:from_time).last&.from_time
    end
  end

  def first_session_date
    case booking_type
      when 'renewable'
        first_session_in_this_month_date
      when 'once', 'free', 'consultation'
        booking_sessions.sort_by(&:from_time).first&.from_time
    end
  end

  def last_session_date
    if renewable?
      if pending? || rejected?
        estimated_last_session_date_in_month
      else
        last_session_in_this_month_date
      end
    else
      booking_sessions.sort_by(&:from_time).last&.from_time
    end
  end

  private

  def check_rejection
    if self.status_changed?
      if self.status_was == 'rejected'
        self.errors.add(:booking, "already rejected!")
        false
      end
    else
      if self.rejected?
        self.errors.add(:booking, "already rejected!")
        false
      end
    end
  end

  def check_cancellation
    if self.status_changed?
      if self.status_was == 'canceled_by_trainer' || self.status_was == 'canceled_by_client'
        self.errors.add(:booking, "already canceled!")
        false
      end
    else
      if self.canceled_by_trainer? || self.canceled_by_client?
        self.errors.add(:booking, "already canceled!")
        false
      end
    end
  end

  def notify_create
    return if skip_notification_about_create
    Notification.create user: self.client, notification_type: "trainer_created_#{ booking_type }_booking", data: {target_id: self.id, from_user: self.trainer_id}, booking_id: self.id, action_required: true
  end

  def notify_update
    if self.status_changed?
      if self.approved?
        Notification.create(user: self.trainer, notification_type: "client_approved_#{ booking_type }_booking", data: {from_user: self.client_id}, booking_id: self.id)
        Notification.where(user_id: self.client, notification_type: "trainer_created_#{ booking_type }_booking", booking_id: self.id).update_all(action_required: false)
      end

      if self.rejected?
        Notification.create(user: self.trainer, notification_type: "client_rejected_#{ booking_type }_booking", data: {from_user: self.client_id}, booking_id: self.id)
        Notification.where(user_id: self.client, notification_type: "trainer_created_#{ booking_type }_booking", booking_id: self.id).update_all(action_required: false)
      end

      if self.canceled_by_trainer?
        Notification.create(user: self.client, notification_type: "trainer_canceled_#{ booking_type }_booking", data: {from_user: self.trainer_id}, booking_id: self.id)
      end

      if self.canceled_by_client?
        Notification.create(user: self.trainer, notification_type: "client_canceled_#{ booking_type }_booking", data: {from_user: self.client_id}, booking_id: self.id)
      end
    end
  end

  def check_status
    rejected? || canceled_by_client? || canceled_by_trainer?
  end

  def booking_type_should_be_valid
    if @booking_type_backup
      self.booking_type ||= @booking_type_backup
      error_message = "'#{@booking_type_backup}' is not a valid booking type"
      errors.add(:base, error_message)
    end
  end

  def trainer_should_be_a_trainer
    trainer = Trainer.find_by_id(self.trainer_id)
    if !trainer
      self.errors.add(:trainer_id, "not found")
      false
    end
  end

  def client_should_be_a_client
    client = Client.find_by_id(self.client_id)
    if !client
      self.errors.add(:client_id, "not found")
      false
    end
  end

  def booking_sessions_should_not_overlap
    return unless new_record?
    if booking_sessions.sort_by(&:from_time).each_cons(2).any?{ |x,y| x.to_time > y.from_time }
      self.errors.add(:booking_session, 'should not overlap!')
      false
    end
  end

  def check_trainer_availability
    return if canceled_by_client? || canceled_by_trainer? || rejected?
    
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    query = booking_sessions
                .project(bookings[:client_id], bookings[:trainer_id])
                .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                .where(bookings[:trainer_id].eq(self.trainer&.id).or(bookings[:client_id].eq(self.client&.id)))
                .where(bookings[:id].not_eq(self.id))
                .where(bookings[:status].eq(Booking.statuses[:approved]))
                .take(1)

    conditions = []

    self.booking_sessions.each do |sessions|
      conditions << booking_sessions[:from_time].gteq(sessions.from_time).and(booking_sessions[:from_time].lt(sessions.to_time))
      conditions << booking_sessions[:to_time].gt(sessions.from_time).and(booking_sessions[:to_time].lteq(sessions.to_time))
    end
    if conditions.any?
      conditions = conditions.map{|i| "(#{ i.to_sql })" }.join(" OR ")
      query.where(Arel::Nodes::SqlLiteral.new("(#{ conditions })"))
    end

    result = BookingSession.find_by_sql(query.to_sql)

    if result.count > 0
      self.errors.add(:base, "trainer are busy!") if result.first['trainer_id'] == trainer.id
      self.errors.add(:base, "client are busy!") if result.first['client_id'] == client.id
    end
  end

  def check_sessions_count
    system = SystemSettings.instance

    if booking_sessions.select{|s| s.id.nil? }.group_by{|t| t.from_time.strftime('%W')}.map{|k, v| v.size > system.max_sessions }.include?(true)
      self.errors.add :booking_sessions, "more than #{system.max_sessions} per week is not allowed"
    end

    if booking_sessions.select{|s| s.id.nil? }.group_by{|t| t.from_time.strftime('%W')}.map{|k, v| v.size < system.min_sessions }.include?(true)
      self.errors.add :booking_sessions, "less than #{system.min_sessions} per week is not allowed"
    end

    self.errors.add :booking_sessions, 'should be at least one' if booking_sessions.to_a.count == 0
  end

  def inclusion_in_trainer_working_hours
    return unless trainer
    return if rejected? || canceled_by_client? || canceled_by_trainer?

    error = 'time do not fit with trainer working schedule'

    booking_sessions.each do |session|
      working_day = trainer.schedules.where(day: session.from_time.wday)

      self.errors.add :booking_sessions, error and next if working_day.blank?

      range = Time.new.change(hour: session.from_time.hour, min: session.from_time.min)..Time.new.change(hour: session.to_time.hour, min: session.to_time.min)

      session_fit = working_day
                        .map{|i| Time.new.change(hour: i.from_time.hour, min: i.from_time.min)..Time.new.change(hour: i.to_time.hour, min: i.to_time.min) }
                        .map{|working_range| working_range.include?(range)}
                        .include?(true)

      self.errors.add :booking_sessions, error unless session_fit
    end
  end

  def set_free
    self.session_price = 0 if self.free? || self.consultation?
  end

  def at_least_one_reject_booking_reason
    self.errors.add :reject_booking_reasons, 'should be at least one' if reject_booking_reasons.blank?
  end

  def session_days_should_be_in_future
    if new_record?
      first_session =  self.booking_sessions.sort_by{|i| i.from_time }.first&.from_time
      self.errors.add :booking_sessions, "can't be in past" if first_session && first_session < Time.now.utc
    end
  end

  def set_free_as_paid
    self.booking_sessions.update_all paid_up: true
  end

  def estimate
    return if skip_estimation

    first_session_time = booking_sessions.to_a.sort_by{|i| i[:from_time]}.first[:from_time]
    self.estimated_sessions_count_per_month = renewable? ? booking_sessions.to_a.count{|i| i[:from_time] <= first_session_time.end_of_month} : booking_sessions.to_a.count
    self.estimated_first_session_date_in_month = booking_sessions.to_a.first[:from_time]

    self.estimated_last_session_date_in_month = booking_sessions.select{|i| i.from_time <= first_session_time.end_of_month }.map(&:from_time).sort.last
  end
end