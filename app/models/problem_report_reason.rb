class ProblemReportReason < ActiveRecord::Base

  has_many :problem_reports

  validates :title, presence: true

  def self.search_query(params)
    reasons = ProblemReportReason.arel_table

    query = reasons.group(reasons[:id])

    if params[:count]
      query.project('COUNT(*)')
    else
      if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
        query = query.order(reasons[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(reasons[:id].desc)
      end
      query.project(Arel.star)
    end

    query
  end

end
