class Trainer < User

  has_many :bookings
  has_many :booking_sessions, through: :bookings
  has_many :trainer_notes
  has_many :payments, through: :bookings
  has_many :session_change_requests, through: :bookings

  validates :first_name,      presence:true
  validates :last_name,       presence:true
  validates :gender,          presence:true, inclusion: { in: User.genders.keys },  if: -> { registration_step >= 1 }
  validate  :at_least_one_speciality,         if: -> { registration_step >= 2 }
  validates :latitude,        presence: true, if: -> { registration_step >= 3 }
  validates :longitude,       presence: true, if: -> { registration_step >= 3 }
  validates :location_radius, presence: true, if: -> { registration_step >= 3 }
  validate  :step_by_step

  after_update :notify_if_confirmed

  after_save :set_registration_step_if_needed

  default_scope -> { where(role_id: Role.trainer.id) }

  def self.explore_trainers(params = {}, current_user = nil)

    trainers                 = self.arel_table
    specialities_trainers    = Arel::Table.new :specialities_trainers
    goals_specialities       = Arel::Table.new :goals_specialities
    goals                    = Goal.arel_table
    specialities             = Speciality.arel_table
    training_places          = TrainingPlace.arel_table
    training_places_users    = Arel::Table.new :training_places_users
    bank_accounts            = BankAccount.arel_table
    schedules                = Schedule.arel_table
    bookings                 = Booking.arel_table
    booking_sessions         = BookingSession.arel_table

    searchingByName = params[:name].present?
    names = params[:name].split if searchingByName
    blocked = Blocking.where(from_user_id: current_user.id).pluck(:to_user_id) if current_user

    query = trainers
                .group(
                    trainers[:id],
                    bank_accounts[:name],
                    bank_accounts[:number],
                    bank_accounts[:bank],
                    bank_accounts[:sort_code])
                .join(specialities_trainers,    Arel::Nodes::OuterJoin).on(specialities_trainers[:trainer_id]           .eq(trainers[:id]))
                .join(specialities,             Arel::Nodes::OuterJoin).on(specialities_trainers[:speciality_id]        .eq(specialities[:id]))
                .join(goals_specialities,       Arel::Nodes::OuterJoin).on(goals_specialities[:speciality_id]           .eq(specialities[:id]))
                .join(goals,                    Arel::Nodes::OuterJoin).on(goals_specialities[:goal_id]                 .eq(goals[:id]))
                .join(training_places_users,    Arel::Nodes::OuterJoin).on(training_places_users[:user_id]              .eq(trainers[:id]))
                .join(training_places,          Arel::Nodes::OuterJoin).on(training_places_users[:training_place_id]    .eq(training_places[:id]))
                .join(bank_accounts,            Arel::Nodes::OuterJoin).on(bank_accounts[:user_id]                      .eq(trainers[:id]))

    query.where(trainers[:role_id].eq(Role.trainer.id))
    query.where(trainers[:id].not_in(blocked)) if current_user
    query.where(trainers[:gender].eq(User.genders[params[:gender].to_sym])) if User.genders.include?(params[:gender])
    query.where(trainers[:confirmed].eq(params[:confirmed]))                if params[:confirmed].present?
    query.where(trainers[:email_confirmed].eq(params[:email_confirmed]))    if params[:email_confirmed].present?
    query.where(goals[:id].in(params[:goal_ids]))                           if params[:goal_ids].is_a?(Array)
    query.where(training_places[:id].in(params[:training_place_ids]))       if params[:training_place_ids].is_a?(Array)

    query.where(trainers[:email].matches("%#{ params[:email] }%"))          if params[:email].present?

    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      query.order("relevancy DESC") if params[:count].nil?
    end

    if params[:show_deleted].present? then
      query.where(trainers[:deleted_at].not_eq(nil))
    else
      query.where(trainers[:deleted_at].eq(nil)) unless params[:show_all].present?
    end

    if params[:latitude].present? && params[:longitude].present?
      if params[:distance].present?
        # to_f should prevent sql injections
        system = SystemSettings.first
        distance = [params[:distance].to_f, system.min_radius.to_f, system.max_radius.to_f].sort[1]
        query.where(Arel::Nodes::SqlLiteral.new("(gc_dist(#{ params[:latitude].to_f }, #{ params[:longitude].to_f }, users.latitude, users.longitude))").lteq(distance))
      else
        # client should be included in radius of all trainers
        query.where(Arel::Nodes::SqlLiteral.new("(gc_dist(#{ params[:latitude].to_f }, #{ params[:longitude].to_f }, users.latitude, users.longitude)) <= users.location_radius"))
      end
    end

    query.where(trainers[:sessions_price].lteq(params[:sessions_price])) if params[:sessions_price].present?

    if params[:training_hours].is_a? Array
      training_hours_subquery = schedules
                                    .project("COUNT(*)")
                                    .where(schedules[:user_id].eq(trainers[:id]))

      availability_subquery = booking_sessions
                                    .project("COUNT(*)")
                                    .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                                    .where(bookings[:trainer_id].eq(trainers[:id]))

      training_hours_conditions = []
      availability_conditions = []

      params[:training_hours].each do |training_day|
        training_day[:ranges].each do |training_range|
          training_hours_conditions.push(
              "( schedules.day = #{ training_day[:day] } AND schedules.from_time::time <= '#{ training_range[:from_time].strftime("%H:%M:00") }' AND schedules.to_time::time >= '#{ training_range[:to_time].strftime("%H:%M:00") }')"
          )

          (Date.today..(Date.today + 1.month)).each do |day|
            if day.wday == training_day[:day].to_i
              from_time = Time.now.utc.change(year: day.year, month: day.month, day: day.day, hour: training_range[:from_time].hour, min: training_range[:from_time].min, sec: 0)
              to_time =   Time.now.utc.change(year: day.year, month: day.month, day: day.day, hour: training_range[:to_time].hour,   min: training_range[:to_time].min,   sec: 0)

              availability_conditions.push("((booking_sessions.from_time <= '#{ from_time.strftime("%Y-%m-%d %H:%M:00") }' AND booking_sessions.to_time >= '#{ from_time.strftime("%Y-%m-%d %H:%M:00") }') OR (booking_sessions.from_time <= '#{ to_time.strftime("%Y-%m-%d %H:%M:00") }' AND booking_sessions.to_time >= '#{ to_time.strftime("%Y-%m-%d %H:%M:00") }') )")
            end
          end
        end
      end

      if training_hours_conditions.any?
        training_hours_subquery.where(Arel::Nodes::SqlLiteral.new(training_hours_conditions.join(' OR ')))
        query.where(Arel::Nodes::SqlLiteral.new("(#{ training_hours_subquery.to_sql }) > 0"))
      end

      if availability_conditions.any?
        availability_subquery.where(Arel::Nodes::SqlLiteral.new("( #{ availability_conditions.join(' OR ') })"))
        query.where(Arel::Nodes::SqlLiteral.new("(#{ availability_subquery.to_sql }) = 0"))
      end
    end

    if params[:count]
      fields = ["COUNT(*)"]
    else
      fields = [
          trainers[Arel.star],
          "(gc_dist(#{ params[:latitude].to_f }, #{ params[:longitude].to_f }, users.latitude, users.longitude)) as distance",
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT specialities.id, specialities.title ) r )) FILTER (WHERE specialities.id IS NOT NULL), '[]') AS specialities",
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT training_places.id, training_places.title ) r )) FILTER (WHERE training_places.id IS NOT NULL), '[]') AS training_places",
          bank_accounts[:name].as('bank_account_name'),
          bank_accounts[:number].as('bank_account_number'),
          bank_accounts[:bank].as('bank_account_bank'),
          bank_accounts[:sort_code].as('bank_account_sort_code'),
      ]
      fields.push("ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy") if searchingByName
      if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
        query = query.order(trainers[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(trainers[:id].desc) if !searchingByName
      end
    end
    query.project(*fields)

    query
  end

  def self.top_trainers(current_user=nil, params={})
    trainers                 = self.arel_table
    bookings                 = Booking.arel_table
    booking_sessions         = BookingSession.arel_table
    specialities_trainers    = Arel::Table.new :specialities_trainers
    specialities             = Speciality.arel_table
    training_places          = TrainingPlace.arel_table
    training_places_users    = Arel::Table.new :training_places_users

    query = trainers
                .join(bookings,                 Arel::Nodes::OuterJoin).on(bookings[:trainer_id]                        .eq(trainers[:id]))

    query.where(bookings[:status].eq(Booking.statuses["approved"]))
    query.where(bookings[:client_id].not_eq(current_user.id)) if current_user
    query.where(trainers[:role_id].eq(Role.trainer.id))
    query.where(trainers[:deleted_at].eq(nil))

    if params[:count]
      query.project("DISTINCT(users.*)")

    else
      query.join(booking_sessions,         Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id]                .eq(bookings[:id]))
           .join(specialities_trainers,    Arel::Nodes::OuterJoin).on(specialities_trainers[:trainer_id]           .eq(trainers[:id]))
           .join(specialities,             Arel::Nodes::OuterJoin).on(specialities_trainers[:speciality_id]        .eq(specialities[:id]))
           .join(training_places_users,    Arel::Nodes::OuterJoin).on(training_places_users[:user_id]              .eq(trainers[:id]))
           .join(training_places,          Arel::Nodes::OuterJoin).on(training_places_users[:training_place_id]    .eq(training_places[:id]))

      query.project(
        trainers[Arel.star],
        "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT specialities.id, specialities.title ) r )) FILTER (WHERE specialities.id IS NOT NULL), '[]') AS specialities",
        "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT training_places.id, training_places.title ) r )) FILTER (WHERE training_places.id IS NOT NULL), '[]') AS training_places",
    )
    query.order(booking_sessions[:rate].sum.desc)
         .group(trainers[:id])
    end

    query
  end

  def my_clients2(options)
    clients = User.arel_table
    bookings = Booking.arel_table

    query = clients
                .join(bookings).on(bookings[:client_id].eq(clients[:id]))
                .where(bookings[:trainer_id].eq(self.id))
                .where(bookings[:status].eq(Booking.statuses["approved"]))
                .group(clients[:id])

    if options[:count]
      query.project("COUNT(*)")
    else

      if options[:sort_column].present? && %w[asc desc].include?(options[:sort_type])
        query = query.order(clients[options[:sort_column]].send(options[:sort_type] == 'asc' ? :asc : :desc))
      end

      query.project("users.*")
    end

    query.where(clients[:first_name].matches("%#{ options[:first_name]}%").or(clients[:last_name].matches("%#{ options[:first_name]}%"))) if options[:first_name].present?
    query.where(clients[:email].matches("%#{ options[:email]}%")) if options[:email].present?

    query
  end

  def self.my_clients(current_user, options = {})
    users = User.arel_table
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table
    goals_specialities = Arel::Table.new :goals_specialities
    goals_users = Arel::Table.new :goals_clients
    goals = Goal.arel_table
    specialities = Speciality.arel_table

    query = users
                .join(bookings, Arel::Nodes::OuterJoin).on(bookings[:client_id].eq(users[:id]).and(bookings[:id].eq(
        bookings.join(booking_sessions, Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id].eq(bookings[:id]))
            .where(bookings[:client_id].eq(users[:id]))
            .where(bookings[:trainer_id].eq(current_user.id))
            .where(bookings[:status].eq(Booking.statuses["approved"]))
            .where(booking_sessions[:to_time].gt(Time.now))
            .group(bookings[:id], booking_sessions[:id])
            .order(booking_sessions[:from_time])
            .take(1)
            .project(bookings[:id])
    )))

    query.where(bookings[:status].eq(Booking.statuses["approved"]))
    query.where(bookings[:trainer_id].eq(current_user.id))

    if options[:count]
      query.project("COUNT(*)")
    else

      query.join(booking_sessions,   Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id]     .eq(bookings[:id]))
           .join(goals_users,        Arel::Nodes::OuterJoin).on(goals_users[:client_id]           .eq(users[:id]))
           .join(goals,              Arel::Nodes::OuterJoin).on(goals[:id]                        .eq(goals_users[:goal_id]))
           .join(goals_specialities, Arel::Nodes::OuterJoin).on(goals_specialities[:goal_id]      .eq(goals[:id]))
           .join(specialities,       Arel::Nodes::OuterJoin).on(goals_specialities[:speciality_id].eq(specialities[:id]))

      select_fields = [
          bookings[:id].as("booking_id"),
          bookings[:client_id],
          'MIN(booking_sessions.from_time) as next_session',
          users[Arel.star],
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT specialities.id, specialities.title ) r )) FILTER (WHERE specialities.id IS NOT NULL), '[]') AS specialities",
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT goals.id, goals.title ) r )) FILTER (WHERE goals.id IS NOT NULL), '[]') AS goals"
      ]

      if current_user && current_user.latitude && current_user.longitude
        select_fields << "(gc_dist(#{ current_user.latitude }, #{ current_user.longitude }, users.latitude, users.longitude)) as distance"
      end

      if options[:sort_column].present? && %w[asc desc].include?(options[:sort_type])
        query = query.order(users[options[:sort_column]].send(options[:sort_type] == 'asc' ? :asc : :desc))
      end

      query.project(*select_fields)
      query.order("next_session")  unless options[:sort_column].present?
      query.group(users[:id], bookings[:id], booking_sessions[:booking_id])
    end

    query
  end

  def next_booking_session(client)
    return nil unless client

    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    query = bookings.join(booking_sessions, Arel::Nodes::OuterJoin).on(bookings[:id].eq(booking_sessions[:booking_id]))
    query.where(bookings[:trainer_id]          .eq(self.id))
    query.where(bookings[:client_id]           .eq(client&.id))
    query.where(bookings[:status]              .eq(Booking.statuses["approved"]))
    query.where(booking_sessions[:from_time]   .gteq(Time.now))
    query.order(booking_sessions[:from_time])
    query.take(1)
    query.project(
        bookings[:id],
        booking_sessions[:id].as('booking_session_id'),
        booking_sessions[:from_time],
        booking_sessions[:to_time]
    )
    Booking.find_by_sql(query.to_sql).first
  end

  def details_json(current_user)
    next_booking = next_booking_session(current_user)

    bank_account = current_user&.bank_account
    to_json.except(:qb_login, :qb_password, :registration_step).merge(
        {
            distance: distance_to(current_user),
            booking_id: next_booking.try(:id),
            booking_session_id: next_booking.try(:booking_session_id),
            next_session_date: next_booking.try(:from_time).try(:to_i),
            next_session_from_time: next_booking.try(:from_time).try(:to_i),
            next_session_to_time: next_booking.try(:to_time).try(:to_i),
            blocked: current_user.try(:id).in?(self.blocked_by_user_ids),

            name_account: bank_account&.name,
            name_bank: bank_account&.bank,
            number_account: bank_account&.number,
            sort_code_account: bank_account&.sort_code
        }
    )
  end

  def my_bookings(options = {})
    users = User.arel_table
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    query = booking_sessions
                .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                .join(users, Arel::Nodes::OuterJoin).on(bookings[:client_id].eq(users[:id]))

    query.where(bookings[:trainer_id].eq(self.id))
    query.where(users[:first_name].matches("%#{ options[:name] }%").or(users[:last_name].matches("%#{ options[:name] }%"))) if options[:name].present?
    query.where(users[:email].matches("%#{ options[:email] }%"))                                                            if options[:email].present?
    query.where(booking_sessions[:from_time].gteq(DateTime.parse(options[:date_from]).beginning_of_day))                    if options[:date_from].present?
    query.where(booking_sessions[:to_time].lteq(DateTime.parse(options[:date_to]).end_of_day))                              if options[:date_from].present?

    query.group(
        users[:id],
        bookings[:id],
        booking_sessions[:id],
    )

    if options[:sort_column].blank?
      options[:sort_column]="from_time"
      options[:sort_type]="desc"
    end

    query.order("#{options[:sort_column.to_sym]} #{options[:sort_type] == 'asc' ? :asc : :desc}")

    if options[:count].present?
      query.project("COUNT(*)")
    else
      query.project(
          booking_sessions[:id],
          booking_sessions[:rate],
          booking_sessions[:from_time],
          booking_sessions[:to_time],
          bookings[:status],
          bookings[:booking_type],
          users[:id].as('user_id'),
          users[:first_name],
          users[:last_name],
          users[:avatar_file_size],
          users[:avatar_file_name],
          users[:avatar_content_type],
          users[:email],
          users[:gender],
          users[:sessions_price]
      )
    end

    query
  end

  def total_earnings
    payments = Payment.arel_table
    bookings = Booking.arel_table

    q = payments
            .project("SUM(payments.session_price * payments.sessions_count)")
            .join(bookings).on(bookings[:id].eq(payments[:booking_id]))
            .where(bookings[:trainer_id].eq(self.id))
    Payment.find_by_sql(q).first['sum']
  end

  private

  def at_least_one_speciality
    self.errors.add :specialities, 'Should be at least one.' if specialities.count == 0
  end

  def step_by_step
    if registration_step.present? && (registration_step.to_i - registration_step_was.to_i) > 1
      self.errors.add :registration_step, "Step not allowed. Your allowed step is #{ registration_step_was + 1 }"
    end
  end

  def notify_if_confirmed
    if self.confirmed_changed? && self.confirmed == true
      UserMailer.account_approved(self.id).deliver_later
    end
  end

  def set_registration_step_if_needed
    if registration_step < 1 && gender.present?
      self.update_attribute :registration_step, 1
    end
    if registration_step < 2 && specialities.to_a.any?
      self.update_attribute :registration_step, 2
    end
    if registration_step < 3 && (latitude.present? || longitude.present? || location_radius.present?)
      self.update_attribute :registration_step, 3
    end
  end
end
