class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :booking_session
  belongs_to :announcement

  after_create :notify
  before_save :add_info

  validate :duplicated_notifications

  serialize :data, Hash

  def duplicated_notifications
    if rate_session? && Notification.where(booking_session_id: booking_session_id, notification_type: 'rate_session').count > 0
      self.errors.add :base, 'duplication'
    end

    if client_charge_failed? && Notification.where(user_id: user_id, booking_id: booking_id, notification_type: 'client_charge_failed').count > 0
      self.errors.add :base, 'duplication'
    end

    if client_charge_failed_after_two_days? && Notification.where(user_id: user_id, booking_id: booking_id, notification_type: 'client_charge_failed_after_two_days').count > 0
      self.errors.add :base, 'duplication'
    end
  end

  enum notification_type: {
    trainer_approved: 2,

    trainer_created_renewable_booking: 3,
    trainer_created_once_booking: 4,
    trainer_created_free_booking: 5,
    trainer_created_consultation_booking: 48,

    client_canceled_renewable_booking: 6,
    client_canceled_once_booking: 7,
    client_canceled_free_booking: 8,
    client_canceled_consultation_booking: 49,

    trainer_canceled_renewable_booking: 9,
    trainer_canceled_once_booking: 10,
    trainer_canceled_free_booking: 11,
    trainer_canceled_consultation_booking: 50,

    client_approved_once_booking: 12,
    client_approved_renewable_booking: 13,
    client_approved_free_booking: 14,
    client_approved_consultation_booking: 51,

    client_rejected_once_booking: 15,
    client_rejected_renewable_booking: 16,
    client_rejected_free_booking: 17,
    client_rejected_consultation_booking: 52,

    rate_session: 18,

    client_charge_failed: 19,

    client_created_change_request_for_renewable_booking: 20,
    client_created_change_request_for_once_booking: 21,
    client_created_change_request_for_free_booking: 22,
    client_created_change_request_for_consultation_booking: 53,
    trainer_created_change_request_for_renewable_booking: 23,
    trainer_created_change_request_for_once_booking: 24,
    trainer_created_change_request_for_free_booking: 25,
    trainer_created_change_request_for_consultation_booking: 54,

    client_approved_change_request_for_renewable_booking: 26,
    client_approved_change_request_for_once_booking: 27,
    client_approved_change_request_for_free_booking: 28,
    client_approved_change_request_for_consultation_booking: 55,
    trainer_approved_change_request_for_renewable_booking: 29,
    trainer_approved_change_request_for_once_booking: 30,
    trainer_approved_change_request_for_free_booking: 31,
    trainer_approved_change_request_for_consultation_booking: 56,

    client_rejected_change_request_for_renewable_booking: 32,
    client_rejected_change_request_for_once_booking: 33,
    client_rejected_change_request_for_free_booking: 34,
    client_rejected_change_request_for_consultation_booking: 57,
    trainer_rejected_change_request_for_renewable_booking: 35,
    trainer_rejected_change_request_for_once_booking: 36,
    trainer_rejected_change_request_for_free_booking: 37,
    trainer_rejected_change_request_for_consultation_booking: 58,

    trainer_created_booking_change_request_for_renewable_booking: 38,
    trainer_created_booking_change_request_for_once_booking: 39,
    trainer_created_booking_change_request_for_free_booking: 40,
    trainer_created_booking_change_request_for_consultation_booking: 59,
    client_approved_booking_change_request_for_renewable_booking: 41,
    client_approved_booking_change_request_for_once_booking: 42,
    client_approved_booking_change_request_for_free_booking: 43,
    client_approved_booking_change_request_for_consultation_booking: 60,
    client_rejected_booking_change_request_for_renewable_booking: 44,
    client_rejected_booking_change_request_for_once_booking: 45,
    client_rejected_booking_change_request_for_free_booking: 46,
    client_rejected_booking_change_request_for_consultation_booking: 61,

    client_charge_failed_after_two_days: 47,

    announcement: 62,
  }

  def self.search_query(current_user, params={})
    notifications = Notification.arel_table
    bookings = Booking.arel_table
    announcements = Announcement.arel_table
    booking_sessions = BookingSession.arel_table
    booking_sessions_2 = BookingSession.arel_table.alias('boooking_sessions_2')
    users = User.arel_table
    payments = Payment.arel_table

    first_session_in_this_month_query = booking_sessions
                                            .project(booking_sessions[:from_time])
                                            .join(payments).on(payments[:id].eq(booking_sessions[:payment_id]))
                                            .where(payments[:booking_id].eq(bookings[:id]))
                                            .order(payments[:date].asc, booking_sessions[:from_time].asc)
                                            .where(payments[:date].gteq(Time.now.beginning_of_month))
                                            .take(1)

    last_session_in_this_month_query = booking_sessions
                                            .project(booking_sessions[:from_time])
                                            .join(payments).on(payments[:id].eq(booking_sessions[:payment_id]))
                                            .where(payments[:booking_id].eq(bookings[:id]))
                                            .order(payments[:date].asc, booking_sessions[:from_time].desc)
                                            .where(payments[:date].gteq(Time.now.beginning_of_month))
                                            .take(1)

    query = notifications
                .join(bookings, Arel::Nodes::OuterJoin).on(notifications[:booking_id].eq(bookings[:id]))
                .join(users, Arel::Nodes::OuterJoin).on(bookings[current_user.client? ? :trainer_id : :client_id].eq(users[:id]))
                .join(announcements, Arel::Nodes::OuterJoin).on(announcements[:id].eq(notifications[:announcement_id]))
                .join(booking_sessions_2, Arel::Nodes::OuterJoin).on(notifications[:booking_session_id].eq(booking_sessions_2[:id]))
                .project(
                    "notifications.*",
                    booking_sessions_2[:to_time].as('session_to_time'),
                    users[:id].as('user_id'),
                    users[:avatar_file_name],
                    users[:avatar_content_type],
                    users[:avatar_file_size],
                    announcements[:id].as('announcement_id'),
                    announcements[:image_file_name],
                    announcements[:image_content_type],
                    announcements[:image_file_size],
                    users[:first_name],
                    users[:last_name],
                    bookings[:id].as('booking_id'),
                    "(SELECT COUNT(*)
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                      ) as sessions_count",

                    "(SELECT COUNT(*)
                    FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                      ) as sessions_in_this_week_count",

                    "(SELECT COUNT(*)
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_month.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_month.strftime("%Y-%m-%d %H:%M:%S") }'
                      ) as sessions_in_this_month_count",

                    "(#{ booking_sessions
                             .project(booking_sessions[:from_time])
                             .where(booking_sessions[:booking_id].eq(bookings[:id]))
                             .where(booking_sessions[:paid_up].eq(true))
                             .order(booking_sessions[:from_time].asc)
                             .take(1)
                             .to_sql
                      }) as first_session_date",

                    "(#{ booking_sessions
                             .project(booking_sessions[:from_time])
                             .where(booking_sessions[:booking_id].eq(bookings[:id]))
                             .where(booking_sessions[:paid_up].eq(true))
                             .order(booking_sessions[:from_time].desc)
                             .take(1)
                             .to_sql
                      }) as last_session_date",

                    "(SELECT from_time
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                      ORDER BY from_time ASC
                      LIMIT 1
                      ) as first_session_in_this_week_date",
                    "(SELECT from_time
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                      ORDER BY from_time DESC
                      LIMIT 1
                      ) as last_session_in_this_week_date",

                    "(#{ first_session_in_this_month_query.to_sql }) as first_session_in_this_month_date",
                    "(#{ last_session_in_this_month_query.to_sql }) as last_session_in_this_month_date",

                    "(SELECT COUNT(*)
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_week.strftime("%Y-%m-%d %H:%M:%S") }'
                      ) as sessions_in_this_week_count",
                    "(SELECT COUNT(*)
                      FROM booking_sessions
                      WHERE
                        booking_sessions.booking_id = bookings.id
                        AND from_time > '#{ Time.now.beginning_of_month.strftime("%Y-%m-%d %H:%M:%S") }'
                        AND from_time < '#{ Time.now.end_of_month.strftime("%Y-%m-%d %H:%M:%S") }'
                      ) as sessions_in_this_month_count",
                )
                .where(notifications[:user_id].eq(current_user.id))
                .group(notifications[:id], users[:id], bookings[:id], booking_sessions_2[:to_time], announcements[:id])
                .order(notifications[:id].desc)
    query
  end

  private

  def message
    user_name = self.user&.full_name
    case notification_type
      when 'trainer_aproved'
        "Hello #{user_name}. Your trainer application has approved."
      when 'trainer_canceled_once_booking', 'trainer_canceled_renewable_booking', 'trainer_canceled_free_booking', 'trainer_canceled_consultation_booking'
        if self.user.trainer?
          "Your booking canceled!"
        else
          "Trainer has cancelled booking!"
        end
      when 'client_approved_once_booking', 'client_approved_free_booking', 'client_approved_consultation_booking'
        "Your session was accepted. Schedule has been updated."
      when 'client_approved_renewable_booking'
        "Your subscription was accepted. Schedule has been updated."
      when 'client_rejected_once_booking'
        "Proposed session was declined. Find out why here."
      when 'client_canceled_once_booking', 'client_canceled_free_booking', 'client_canceled_consultation_booking'
        self.user.client? ? "Your booking canceled!" : "Client has cancelled booking!"
      when 'trainer_created_renewable_booking', 'trainer_created_once_booking', 'trainer_created_free_booking', 'trainer_created_consultation_booking'
        "Check out your training package!"
      when 'rate_session'
        "How do you feel? Rate session from #{(self.data[:to_time] + self.user.try(:time_zone).to_i).strftime('%b, %d')}."
      when 'client_rejected_renewable_booking', 'client_rejected_free_booking', 'client_rejected_consultation_booking'
        "Your subscription was declined. Find out why here."
      when 'client_canceled_renewable_booking'
        "A subscription was cancelled. Find out why here."
      when 'trainer_created_booking_change_request_for_renewable_booking',
           'trainer_created_booking_change_request_for_once_booking',
           'trainer_created_booking_change_request_for_free_booking',
           'trainer_created_booking_change_request_for_consultation_booking'
        "Booking change requested."
      when 'client_rejected_booking_change_request_for_renewable_booking',
           'client_rejected_booking_change_request_for_once_booking',
           'client_rejected_booking_change_request_for_free_booking',
           'client_rejected_booking_change_request_for_consultation_booking'
        "Booking change request rejected."
      when 'client_approved_booking_change_request_for_renewable_booking',
           'client_approved_booking_change_request_for_once_booking',
           'client_approved_booking_change_request_for_free_booking',
           'client_approved_booking_change_request_for_consultation_booking'
        "Booking change request approved."
      when 'client_created_change_request_for_renewable_booking',
           'client_created_change_request_for_once_booking',
           'client_created_change_request_for_free_booking',
           'client_created_change_request_for_consultation_booking',
           'trainer_created_change_request_for_renewable_booking',
           'trainer_created_change_request_for_once_booking',
           'trainer_created_change_request_for_free_booking',
           'trainer_created_change_request_for_consultation_booking'
        "Session change requested."
      when 'client_approved_change_request_for_renewable_booking',
           'client_approved_change_request_for_once_booking',
           'client_approved_change_request_for_free_booking',
           'client_approved_change_request_for_consultation_booking',
           'trainer_approved_change_request_for_renewable_booking',
           'trainer_approved_change_request_for_once_booking',
           'trainer_approved_change_request_for_free_booking',
           'trainer_approved_change_request_for_consultation_booking'
        "Session change approved."
      when 'client_rejected_change_request_for_renewable_booking',
           'client_rejected_change_request_for_once_booking',
           'client_rejected_change_request_for_free_booking',
           'client_rejected_change_request_for_consultation_booking',
           'trainer_rejected_change_request_for_renewable_booking',
           'trainer_rejected_change_request_for_once_booking',
           'trainer_rejected_change_request_for_free_booking',
           'trainer_rejected_change_request_for_consultation_booking'
        "Session change rejected."
      when 'client_charge_failed'
        "Failed to renew subscription."
      when 'client_charge_failed_after_two_days'
        "Subscription payment failed."
      when 'announcement'
        { body: announcement.title, title: 'Announcement from Peach'}
    end
  end

  def notify
    self.user&.notify message, data
  end

  def add_info
    user_name = self.user&.full_name
    case notification_type
      when 'trainer_aproved'
        self.data[:title] = "Hello #{user_name}."
        self.data[:description] =  "Your trainer application has approved."
      when 'client_approved_once_booking', 'client_approved_free_booking', 'client_approved_consultation_booking'
        self.data[:title] = 'Your session was accepted.'
        self.data[:description] = 'Schedule has been updated.'
      when 'client_rejected_once_booking'
        self.data[:title] = 'Proposed session was declined.'
        self.data[:description] = 'Find out why here.'
      when 'trainer_created_renewable_booking', 'trainer_created_once_booking', 'trainer_created_free_booking', 'trainer_created_consultation_booking'
        self.data[:title] = 'Your training package is here!'
        self.data[:description] = 'Check out the new schedule.'
      when 'rate_session'
        self.data[:title] = 'How do you feel?'
        self.data[:description] = "Rate session from #{Time.at(self.data[:to_time]).strftime('%b, %d')}."
      when 'client_approved_renewable_booking'
        self.data[:title] = 'Your subscription was accepted!'
        self.data[:description] = 'Schedule is updated.'
      when 'client_rejected_renewable_booking'
        self.data[:title] = 'Your subscription was declined.'
        self.data[:description] = 'Find out why here.'
      when 'client_rejected_free_booking', 'client_rejected_consultation_booking'
        self.data[:title] = 'Your session was declined.'
        self.data[:description] = 'Find out why here.'
      when 'trainer_canceled_once_booking', 'trainer_canceled_renewable_booking', 'trainer_canceled_free_booking', 'trainer_canceled_consultation_booking',
           'client_canceled_once_booking', 'client_canceled_renewable_booking', 'client_canceled_free_booking', 'client_canceled_consultation_booking'
        self.data[:title] = 'A subscription was cancelled.'
        self.data[:description] = 'Find out why here.'
      when 'client_created_change_request_for_renewable_booking',
           'client_created_change_request_for_once_booking',
           'client_created_change_request_for_free_booking',
           'client_created_change_request_for_consultation_booking',
           'trainer_created_change_request_for_renewable_booking',
           'trainer_created_change_request_for_once_booking',
           'trainer_created_change_request_for_free_booking',
           'trainer_created_change_request_for_consultation_booking'
        self.data[:title] = 'Session change requested!'
        self.data[:description] = 'Check out.'
      when 'client_approved_change_request_for_renewable_booking',
           'client_approved_change_request_for_once_booking',
           'client_approved_change_request_for_free_booking',
           'client_approved_change_request_for_consultation_booking',
           'trainer_approved_change_request_for_renewable_booking',
           'trainer_approved_change_request_for_once_booking',
           'trainer_approved_change_request_for_free_booking',
           'trainer_approved_change_request_for_consultation_booking'
        self.data[:title] = "Session change approved."
        self.data[:description] = "Session change approved."
      when 'client_rejected_change_request_for_renewable_booking',
           'client_rejected_change_request_for_once_booking',
           'client_rejected_change_request_for_free_booking',
           'client_rejected_change_request_for_consultation_booking',
           'trainer_rejected_change_request_for_renewable_booking',
           'trainer_rejected_change_request_for_once_booking',
           'trainer_rejected_change_request_for_free_booking',
           'trainer_rejected_change_request_for_consultation_booking'
        self.data[:title] = "Session change rejected."
        self.data[:description] = "Session change rejected."
      when 'client_rejected_booking_change_request_for_renewable_booking',
           'client_rejected_booking_change_request_for_once_booking',
           'client_rejected_booking_change_request_for_free_booking',
           'client_rejected_booking_change_request_for_consultation_booking'
        self.data[:title] = 'Booking change request rejected.'
        self.data[:description] = 'Booking change request rejected.'
      when 'client_approved_booking_change_request_for_renewable_booking',
          'client_approved_booking_change_request_for_once_booking',
          'client_approved_booking_change_request_for_free_booking',
          'client_approved_booking_change_request_for_consultation_booking'
        self.data[:title] = 'Booking change request approved.'
        self.data[:description] = 'Booking change request approved.'
      when 'trainer_created_booking_change_request_for_renewable_booking',
          'trainer_created_booking_change_request_for_once_booking',
          'trainer_created_booking_change_request_for_free_booking',
          'trainer_created_booking_change_request_for_consultation_booking'
        self.data[:title] = 'Booking change requested.'
        self.data[:description] = 'Booking change requested.'
      when 'client_charge_failed'
        self.data[:title] = "Failed to renew subscription."
        self.data[:description] = "Find out more."
      when 'client_charge_failed_after_two_days'
        self.data[:title] = "Subscription payment failed."
        self.data[:description] = "Card was declined. Sessions cancelled."
      when 'announcement'
        self.data[:title] = announcement.title
        self.data[:description] = announcement.subtitle
        self.data[:announcement_id] = announcement.id
    end
  end
end
