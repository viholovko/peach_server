class ScheduleHistory < ApplicationRecord

  belongs_to :user
  serialize :value, Array
end