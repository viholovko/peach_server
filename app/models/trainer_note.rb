class TrainerNote < ActiveRecord::Base
  belongs_to :trainer
  belongs_to :client
  validates :text, presence: true
end