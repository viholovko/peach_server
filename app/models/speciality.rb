class Speciality < ApplicationRecord

  has_and_belongs_to_many :goals
  validates :title, presence: true

  def self.search_query(params)
    params[:per_page] ||= 10

    specialities = Speciality.arel_table

    fields = [
        specialities[:id],
        specialities[:title],
        specialities[:created_at]
    ]

    query = specialities
                .group(fields)

    if params[:count]
      query.project('COUNT(*)')
    else
      if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
        query = query.order(specialities[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(specialities[:id].desc)
      end
      query.project(fields)
    end

    query.where(specialities[:title].matches("%#{ params[:title] }%")) if params[:title].present?

    query
  end
end
