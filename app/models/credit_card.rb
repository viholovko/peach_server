class CreditCard < ApplicationRecord
  belongs_to :user

  before_validation :create_stripe_customer
  after_destroy :destroy_stripe_customer

  attr_accessor :stripe_token

  def activate
    update_attribute(:active, true)
  end

  def deactivate
    update_attribute(:active, false)
  end

  private

  def create_stripe_customer
    begin
      customer = Stripe::Customer.create email: user.email, source: stripe_token

      self.email       = user.email
      self.customer_id = customer.id
      self.name        = customer.sources.data.first.name
      self.brand       = customer.sources.data.first.brand
      self.country     = customer.sources.data.first.country
      self.exp_month   = customer.sources.data.first.exp_month
      self.exp_year    = customer.sources.data.first.exp_year
      self.last4       = customer.sources.data.first.last4

    rescue Exception => e
      self.errors.add :base, e.message
    end
  end

  def destroy_stripe_customer
    begin
      cu = Stripe::Customer.retrieve(customer_id)
      cu.delete
    rescue

    end
  end
end