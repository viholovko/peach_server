class ProblemReport < ApplicationRecord

  # validates_presence_of :comment
  validates_presence_of :problem_report_reason_id

  belongs_to :user
  belongs_to :problem_report_reason


  def self.search_query(params = {})
    params[:per_page] ||= 10

    users = User.arel_table
    roles = Role.arel_table
    problem_reports = ProblemReport.arel_table
    problem_report_reasons = ProblemReportReason.arel_table

    fields = [
      problem_reports[:id],
      problem_reports[:comment],
      problem_reports[:created_at],
      problem_reports[:user_id],
      users[:first_name],
      users[:last_name],
      roles[:name],
      problem_report_reasons[:title]
    ]

    query = problem_reports
                .group(fields)
                .join(users, Arel::Nodes::OuterJoin)
                  .on(users[:id].eq(problem_reports[:user_id]))
                .join(roles, Arel::Nodes::OuterJoin)
                  .on(users[:role_id].eq(roles[:id]))
                .join(problem_report_reasons, Arel::Nodes::OuterJoin)
                  .on(problem_reports[:problem_report_reason_id].eq(problem_report_reasons[:id]))

    if params[:count]
      query.project('COUNT(*)')
    else
      if params[:sort_column]=='name' && %w[asc desc].include?(params[:sort_type])
        query = query.order(users[:first_name].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
          query = query.order(problem_reports[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
        else
          query = query.order(problem_reports[:id].desc)
        end
      end
    end

    query.project(fields)

    query.where(problem_report_reasons[:id].eq(params[:problem_report_reason_id])) if params[:problem_report_reason_id].present?
    query.where(users[:first_name].matches("%#{ params[:name] }%")
            .or(users[:last_name].matches("%#{ params[:name] }%"))) if params[:name].present?

    query
  end
end
