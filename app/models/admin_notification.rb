class AdminNotification < ApplicationRecord
  belongs_to :user

  before_save :prepare_data
  serialize :data, Hash

  enum notify_type: {
    new_user: 0,
    new_blocking: 1,
    charge_failed: 2
  }

  def self.search_query(params={})
    notifications = AdminNotification.arel_table

    if params[:count]
      query = notifications.project('COUNT(*)')
    else
      query = notifications.project(Arel.star)

      if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
        query.order(notifications[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query.order(notifications[:id].desc)
      end
    end

    query
  end

  private

  def prepare_data
    user = self.user

    case notify_type
      when 'new_user'
        self.data[:title] = "New #{user&.role&.name} registered."
        self.data[:description] =  "#{user&.full_name} (#{user&.email})."
        self.data[:url] =  "app#/clients/#{user&.id}/show" if user&.role&.name == "client"
        self.data[:url] =  "app#/trainers/#{user&.id}/show" if user&.role&.name == "trainer"
        self.data[:url] =  "app#/admins" if user&.role&.name == "admin"
      when 'new_blocking'
        self.data[:title] = 'Trainer blocking.'
        self.data[:description] = "Client #{user&.full_name} blocked trainer."
        self.data[:url] =  "app#/clients/#{user&.id}/show" if user&.role&.name == "client"
      when 'charge_failed'
        self.data[:title] = 'Charge failed.'
        self.data[:description] = "Client #{user&.full_name} charge failed."
        self.data[:url] =  "app#/clients/#{user&.id}/show" if user&.role&.name == "client"
    end
  end
end
