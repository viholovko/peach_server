class Feedback < ApplicationRecord
  validates :comment,length: { minimum: 10 }, presence: true, if: :negative_stars?
  validates :stars, numericality: { only_integer: true,greater_than_or_equal_to:1,less_than_or_equal_to:5 }
  belongs_to :user, optional: true

  def self.search(options={})
    feedbacks = Feedback.arel_table
    users = User.arel_table

    query= feedbacks
               .join(users,                 Arel::Nodes::OuterJoin).on(users[:id]                        .eq(feedbacks[:user_id]))

    query.group(users[:id],feedbacks[:id])

    searchingByName = options[:name].present?
    names = options[:name].split if searchingByName

    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      query.order("relevancy DESC") if options[:count].nil?
    end

    if options[:sort_column].present? && %w(asc desc).include?(options[:sort_type])
      if options[:sort_column]=="stars" || options[:sort_column]=="comment" || options[:sort_column]=="user_id" || options[:sort_column]=="created_at"
        query.order(feedbacks[options[:sort_column.to_sym]].send(options[:sort_type] == 'asc' ? :asc : :desc))
      elsif options[:sort_column]=="feedback_id"
        query.order("feedback_id #{options[:sort_type] == 'asc' ? :asc : :desc}")
      else
        query.order(users[options[:sort_column.to_sym]].send(options[:sort_type] == 'asc' ? :asc : :desc))
      end
    else
      query.order(feedbacks[:stars].desc) if !searchingByName
    end

    if options[:count]
      query.project("COUNT(*),feedbacks.id as feedback_id")
    else
      fields=[
               feedbacks[Arel.star],
               feedbacks[:id].as('feedback_id'),
               users[:id],
               users[:first_name],
               users[:last_name],
               users[:gender],
               users[:avatar_file_name],
               users[:avatar_file_size],
               users[:avatar_content_type],
               users[:avatar_updated_at]
      ]
      fields<< "ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy" if searchingByName
      query.project(*fields)
    end
    query
  end
  private
  def negative_stars?
    self.stars.try(:<,3)
  end
end
