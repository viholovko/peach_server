class BookingSession < ApplicationRecord
  belongs_to :booking
  belongs_to :feeling
  has_many :session_change_requests, dependent: :destroy

  self.skip_time_zone_conversion_for_attributes = [:from_time, :to_time]

  validates :from_time, :presence =>true, :allow_blank => false
  validates :to_time, :presence =>true, :allow_blank => false
  validate :from_time_lower_than_finish
  validates :feeling, presence: true, on: :update
  validates :rate, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }, on: :update
  after_update :check_rate_updates

  private

  def from_time_lower_than_finish
    if self.from_time && self.to_time
      if  self.from_time > self.to_time
        self.errors.add(:start_time, "has to be lower than finish time")
        false
      # elsif (30.minutes..6.hours).exclude? self.to_time - self.from_time # TODO strange, but this doesn't work ...
      elsif (to_time.to_i - from_time.to_i) != 1.hour.to_i
        self.errors.add(:duration, "of training should be equal to 1 hour!")
        false
      end
    end
  end

  def check_rate_updates
    if rate_changed?
      Notification.where(
          user_id: booking.client_id,
          notification_type: :rate_session,
          booking_id: booking_id,
          action_required: true
      ).each { |i| i.update_attributes(action_required: false) if i.data[:booking_session_id] == id }
    end
  end

end
