class BlockClientReason < ActiveRecord::Base


  validates :title, presence: true

  def self.search_query(params)
    params[:per_page] ||= 10

    reasons = BlockClientReason.arel_table

    fields = [
        reasons[:id],
        reasons[:title],
        reasons[:created_at]
    ]

    query = reasons
                .group(fields)

    if params[:count]
      query.project('COUNT(*)')
    else
      if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
        query = query.order(reasons[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(reasons[:id].desc)
      end
      query.project(fields)
    end

    query.where(reasons[:title].matches("%#{ params[:title] }%")) if params[:title].present?

    query
  end
end
