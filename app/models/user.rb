include ActionView::Helpers::DateHelper

class User < ApplicationRecord
  include ApplicationHelper

  belongs_to :role

  has_one    :bank_account

  has_many   :sessions, dependent: :destroy
  has_many   :credit_cards, dependent: :destroy
  has_many   :blockings, foreign_key: "from_user_id", dependent: :destroy
  has_many   :blocked_users, through: :blockings, source: :to_user
  has_many   :reverse_blocked_users, foreign_key: "to_user", class_name: "Blocking"
  has_many   :blocked_by_users, through: :reverse_blocked_users,  source: :from_user
  has_many   :notifications,   dependent: :destroy
  has_many   :admin_notifications,   dependent: :destroy
  has_many   :problem_reports, dependent: :destroy
  has_many   :feedbacks
  has_many   :schedules, dependent: :destroy
  has_many   :schedule_histories, dependent: :destroy

  has_and_belongs_to_many :goals,           join_table: :goals_clients,            foreign_key: :client_id
  has_and_belongs_to_many :specialities,    join_table: :specialities_trainers,    foreign_key: :trainer_id
  has_and_belongs_to_many :training_places

  accepts_nested_attributes_for :schedules, allow_destroy: true
  accepts_nested_attributes_for :schedule_histories

  has_attached_file :avatar,
                    styles: { medium: '500x500>', thumb: '100x100>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/users/:id/avatars/:style/:filename",
                    url: "/system/users/:id/avatars/:style/:filename"

  attr_accessor :password, :password_confirmation
  attr_accessor :working_hours
  attr_accessor :validate_password_after_restore

  validates :email,                 presence: true, format: { with: /.*\@.*\..*/, message: "is incorrect"}
  validates :last_name,             presence: true
  validates :first_name,            presence: true
  validates :password,              presence: true, confirmation: true, if: :validate_password?
  validates :password_confirmation, presence: true, if: :validate_password?
  validates :sessions_price,        numericality: {
      greater_than_or_equal_to: SystemSettings.min_price,
      less_than_or_equal_to: SystemSettings.max_price, allow_nil: true
  }


  validate :email_uniqueness
  validate :role_presence
  validate :training_place_should_be_valid
  validate :client_gender_should_be_valid
  validate :validate_bio_n_qualification

  validates_attachment_size :avatar, less_than: 20.megabytes, unless: Proc.new {|model| model.avatar }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  before_validation :check_working_hours
  before_validation :downcase_email

  before_save       :encrypt_password
  before_save       :create_qb_account

  after_create      :send_confirmation_email
  after_create      :notify_admin_about_new_user

  before_destroy    :validate_destroy
  before_destroy    :destroy_qb_account

  scope :admins, -> { where(role_id: Role.admin.id ) }
  scope :only_deleted, -> { unscope(where: :deleted_at).where.not(deleted_at: nil) }
  scope :with_deleted, -> { unscope(where: :deleted_at) }
  default_scope -> { where(deleted_at: nil) }

  alias_method :really_destroy!, :destroy

  enum gender: {
      male: 0,
      female: 1,
      other: 2
  }

  enum training_place: {
      mobile: 0,
      gym_based: 1,
      both: 3
  }

  enum client_gender: {
      client_accept_male: 1,
      client_accept_female: 2,
      client_accept_both: 3
  }

  def destroy
    update(deleted_at: Time.current)
  end

  def training_place=(value)
    super value
    @training_place_backup = nil
  rescue
    @training_place_backup = value
      super nil
  end

  def client_gender=(value)
    super value
    @client_gender_backup = nil
  rescue
    @client_gender_backup = value
    super nil
  end

  Role::NAMES.each do |name_constant|
    define_method("#{name_constant}?") { self.role.try(:name) == name_constant.to_s }
  end

  def authenticate(password)
    self.encrypted_password == encrypt(password)
  end

  def has_location
    if !self.latitude.nil? && !self.longitude.nil? && !self.location_radius.nil?
      true
    else
      false
    end
  end

  def has_credit_card
    if !self.credit_cards.where(active: true).blank?
      true
    else
      false
    end
  end

  def full_name
    [first_name, last_name].compact.join(' ')
  end

  def online
    isOnline = self.sessions.order(updated_at: :desc).first
    return true if isOnline && isOnline.updated_at > Time.now - 15.minutes
    false
  end

  def validate_destroy
    if self.admin? && User.admins.count == 1
      self.errors.add :base, 'Can not remove last admin.'
      throw(:abort)
      #false
    elsif User.count == 1
      self.errors.add :base, 'Can not remove last user.'
      throw(:abort)
      #false
    end
  end

  def to_json
    reload
    #update_qb_session

    if qb_app_id != SystemSettings.instance.qb_application_id
      create_qb_account force: true
    end

    result = {
        id: id,
        email: email,
        first_name: first_name,
        last_name: last_name,
        gender: gender,
        role: role.name,
        online: online,
        avatar: paperclip_url(avatar),
        qb_session_token: qb_token,
        qb_login: qb_login,
        qb_password: qb_password,
        qb_user_id: qb_user_id,
        notifications_enabled: notifications_enabled,
        registration_step: registration_step.to_i,
        address: address,
        post_code: post_code,
        latitude: latitude,
        longitude: longitude,
        location_radius: location_radius,
        training_places: training_places.map{|training_place|
          {
              id: training_place.id,
              title: training_place.title,
          }
        },
        bio: bio,
        working_hours: (0..6).map{|i|
          {
              day: i,
              ranges: schedules.select{|s| s.day == i }.map{|v|
                {
                    from_time: v.from_time.to_f,
                    to_time: v.to_time.to_f,
                }
              }
          }
        },
        sessions_price: sessions_price
    }

    if client?
      result.merge!(
          {
              goals: goals.map{ |goal|
                {
                    id: goal.id,
                    title: goal.title,
                }
              },
              trainers_gender:  User.genders.key(trainers_gender)
          }
      )
    elsif trainer?
      result[:qualification] = qualification
      result[:specialities] = specialities.map{|speciality|
        {
            id: speciality.id,
            title: speciality.title,
        }
      }
      result[:average_response_time] = distance_of_time_in_words(average_response_time)
    end

    result
  end

  def avatar_from_url(url)
    self.avatar = open(url) if url
  end

  def notify(message, data = {})
    session = sessions.last
    return unless session

    begin
      if session.device_type == 'ios'
        tokens = [ session.push_token ]
        password = SystemSettings.ios_push_password
        notification = RubyPushNotifications::APNS::APNSNotification.new tokens, { aps: { data: data, alert: message, sound: 'true', badge: 1 } }

        raise 'Push certificate missing.' unless SystemSettings.ios_push_certificate.present?

        pusher = RubyPushNotifications::APNS::APNSPusher.new(
            File.read(SystemSettings.ios_push_certificate.path),
            true, # SystemSettings.ios_push_environment_sandbox,
            password,
            { host: 'gateway.sandbox.push.apple.com' } #SystemSettings.ios_push_apns_host }
        )
        pusher.push [notification]

        pusher = RubyPushNotifications::APNS::APNSPusher.new(
            File.read(SystemSettings.ios_push_certificate.path),
            false, # SystemSettings.ios_push_environment_sandbox,
            password,
            { host: 'gateway.push.apple.com' } # SystemSettings.ios_push_apns_host }
        )

        pusher.push [notification]

      elsif session.device_type == 'android'

      end
    rescue Exception => e
      puts "==================push error============="
      puts e.message
      puts e.backtrace.join("\n")
    end
  end

  def send_password_reset
    self.update_attribute :reset_password_token, SecureRandom.random_number(999999)
    UserMailer.password_reset(self.id).deliver_now
  end

  def send_email_confirmation_instructions
    unless social
      self.update_attribute :confirmation_token, SecureRandom.random_number(999999)
      UserMailer.confirmation_instructions(self.id).deliver_now
    end
  end

  def send_confirmation_email
    return if self.email_confirmed
    self.update_attribute :confirmation_token, SecureRandom.random_number(999999)
    return if self.trainer?
    UserMailer.confirmation_instructions(self.id).deliver
  end

  def distance_to(other_user)
    return nil unless valid_coordinates? && other_user.try(:valid_coordinates?)
    rad = Math::PI/180
    lat1 = latitude
    lon1 = longitude
    lat2 = other_user.latitude
    lon2 = other_user.longitude
    2 * 3961 * Math.asin(
        Math.sqrt(
            (Math.sin(rad*((lat2 - lat1) / 2))) ** 2 + Math.cos(rad*(lat1)) *
                Math.cos(rad*(lat2)) * (Math.sin(rad*((lon2 - lon1) / 2))) ** 2))
  end

  def distance_to_coordinates(latitude:, longitude:)
    return nil unless valid_coordinates?

    rad = Math::PI/180

    2 * 3961 * Math.asin(
        Math.sqrt(
            (Math.sin(rad*((latitude - self.latitude) / 2))) ** 2 + Math.cos(rad*(self.latitude)) *
                Math.cos(rad*(latitude)) * (Math.sin(rad*((longitude - self.longitude) / 2))) ** 2))
  end

  def valid_coordinates?
    latitude && longitude
  end

  def as_client
    Client.find_by_id(id)
  end

  def as_trainer
    Trainer.find_by_id(id)
  end

  def blocked_users_with_comment params={}
    block_query params.merge(type: 'blocked')
  end

  def blocked_by_users_with_comment params={}
    block_query params.merge(type: 'blocked_by')
  end

  def my_sessions(options={})
    trainers              = User.arel_table.alias('trainers')
    opponents             = User.arel_table.alias('opponents')
    booking_sessions      = BookingSession.arel_table
    bookings              = Booking.arel_table
    specialities          = Speciality.arel_table
    goals_clients         = Arel::Table.new :goals_clients
    goals_specialities    = Arel::Table.new :goals_specialities
    feelings              = Feeling.arel_table
    specialities_trainers = Arel::Table.new :specialities_trainers

    query = booking_sessions
                .join(bookings,              Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id]                     .eq(bookings[:id]))
                .join(trainers,              Arel::Nodes::OuterJoin).on(bookings[:trainer_id]                             .eq(trainers[:id]))
                .join(opponents,             Arel::Nodes::OuterJoin).on(bookings[self.trainer? ? :client_id : :trainer_id].eq(opponents[:id]))
                .join(specialities_trainers, Arel::Nodes::OuterJoin).on(trainers[:id]                                     .eq(specialities_trainers[:trainer_id]))
                .join(specialities,          Arel::Nodes::OuterJoin).on(specialities[:id]                                 .eq(specialities_trainers[:speciality_id]))
                .join(feelings,              Arel::Nodes::OuterJoin).on(feelings[:id]                                     .eq(booking_sessions[:feeling_id]))
                .join(goals_clients,         Arel::Nodes::OuterJoin).on(goals_clients[:client_id]                         .eq(bookings[:client_id]))
                .join(goals_specialities,    Arel::Nodes::OuterJoin).on(goals_specialities[:goal_id]                      .eq(goals_clients[:goal_id]))

    if options[:count]
      query.project("COUNT(*)")
    else
      select_fields = [
          booking_sessions[:id],
          booking_sessions[:from_time],
          booking_sessions[:to_time],
          booking_sessions[:rate],
          booking_sessions[:feeling_id],
          booking_sessions[:booking_id],
          opponents[:id].as('opponent_id'),
          opponents[:first_name].as('opponent_first_name'),
          opponents[:last_name].as('opponent_last_name'),
          opponents[:avatar_file_name].as('opponent_avatar_file_name'),
          opponents[:avatar_content_type].as('opponent_avatar_content_type'),
          opponents[:avatar_file_size].as('opponent_avatar_file_size'),
          opponents[:avatar_updated_at].as('opponent_avatar_updated_at'),
          opponents[:location_radius].as('opponent_location_radius'),
          opponents[:gender].as('opponent_gender'),

          bookings[:address],
          bookings[:session_price],
          booking_sessions[:id].as('session_id'),
          feelings[:id].as('feeling_id'),
          feelings[:title].as('feeling_title'),
          booking_sessions[:id].as('booking_sessions_id'),
          bookings[:latitude],
          bookings[:longitude],
          bookings[:address],
          bookings[:booking_type],
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT specialities.id, specialities.title ) r )) FILTER (WHERE specialities.id IS NOT NULL), '[]') AS specialities"
      ]
      if self.latitude && self.longitude
        select_fields << "(gc_dist(#{ self.latitude }, #{ self.longitude }, opponents.latitude, opponents.longitude)) as distance"
      end
      query.project(*select_fields)
    end

    query.where(booking_sessions[:paid_up].eq(true))

    query.where(bookings[self.trainer? ? :trainer_id : :client_id].eq(self.id))
    query.where(bookings[:status].in([Booking.statuses[:finished], Booking.statuses[:approved]]))

    query.where(booking_sessions[:to_time].gteq(DateTime.now)) if options[:select].to_s.downcase == 'upcoming'
    query.where(booking_sessions[:to_time].lteq(DateTime.now)) if options[:select].to_s.downcase == 'completed'

    query.where(booking_sessions[:from_time].gteq(options[:from_time].to_datetime))  if options[:from_time].present?
    query.where(booking_sessions[:to_time].lteq(options[:to_time].to_datetime))      if options[:to_time].present?

    if options[:sort_column].present? && %w(asc desc).include?(options[:sort_type])
      query.order("#{options[:sort_column.to_sym]} #{options[:sort_type] == 'asc' ? :asc : :desc}")
    else
      if options[:select].present?
        query.order(booking_sessions[:to_time].asc)  if options[:select].to_s.downcase == 'upcoming'
        query.order(booking_sessions[:to_time].desc) if options[:select].to_s.downcase == 'completed'
      else
        query.order(booking_sessions[:from_time].asc)
      end
    end

    query.group(booking_sessions[:id], bookings[:id], trainers[:id], feelings[:id], opponents[:id])

    query
  end


  def get_qb_session
    begin
      qb_app_session = QuickBlox::Session.new
      qb_user_session = QuickBlox::UserSession.new qb_app_session, login: self.qb_login, password: self.qb_password
    rescue Exception => e
      # create_qb_account force: true
      # retry
    end
    qb_user_session.session
  end

  private

  def update_qb_session
    if qb_login
      if self.qb_token
        self.qb_token
      else
        qb_session = QuickBlox::Session.new
        QuickBlox::UserSession.new qb_session, login: self.qb_login, password: self.qb_password
        self.update_attribute :qb_token, qb_session.token
        qb_session
      end
    end
  end

  def create_qb_account options = {}
    if !qb_user_id || options[:force]
      begin
        self.salt = make_salt if salt.blank?
        self.qb_login = "#{ self.email }_#{ self.role.name }_#{ Time.now.to_i }"
        self.qb_password = encrypt("#{ self.email }_#{ Time.now.to_i }")[0...40]

        qb_session = QuickBlox::Session.new
        qb_user = QuickBlox::User.create qb_session, login: self.qb_login, password: self.qb_password

        self.qb_user_id = qb_user.id
        self.qb_app_id = SystemSettings.instance.qb_application_id

        qb_session.destroy

        self.save
      rescue Exception => e
        # raise
      end
    end
  end

  def destroy_qb_account
    if qb_user_id
      begin
        qb_session = QuickBlox::Session.new
        qb_user = QuickBlox::User.show qb_session, self.qb_user_id
        QuickBlox::UserSession.new qb_session, login: self.qb_login, password: self.qb_password
        qb_user.destroy qb_session

        qb_session.destroy
      rescue Exception => e
        # raise
      end
    end
  end

  def validate_password?
    (!social && ((password.present? || password_confirmation.present?) || new_record? )) || validate_password_after_restore
  end

  def downcase_email
    self.email = self.email.downcase if self.email
  end

  def encrypt_password
    self.salt = make_salt if salt.blank?
    self.encrypted_password = encrypt(self.password) if self.password
  end

  def encrypt(string)
    secure_hash("#{string}--#{self.salt}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{self.password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  def training_place_should_be_valid
    if @training_place_backup
      self.training_place ||= @training_place_backup
      error_message = "#{@training_place_backup} is not a valid training place"
      errors.add(:base, error_message)
    end
  end

  def client_gender_should_be_valid
    if @client_gender_backup
      self.client_gender ||= @client_gender_backup
      error_message = "#{@client_gender_backup} is not a valid client gender"
      errors.add(:base, error_message)
    end
  end

  def email_uniqueness
    if self.email_changed? or self.deleted_at_changed?
      if User.where(email: self.email, deleted_at: nil).where.not(id: self.id).count > 0
        errors.add(:email, "User with this email address is already exists.")
        false
      end
    end
  end

  def block_query params

    params[:per_page] ||= 10

    users = User.arel_table
    blokings = Blocking.arel_table
    blockings_reasons = Arel::Table.new :blockings_reasons
    block_client_reasons = BlockClientReason.arel_table
    block_trainer_reasons = BlockTrainerReason.arel_table

    searchingByName = params[:name].present?
    names = params[:name].split if searchingByName

    select_fields = [
        blokings[Arel.star],
        blokings[:created_at].as("block_date"),
        users[Arel.star]
    ]

  if params[:type] == 'blocked_by'
    query = users.join(blokings, Arel::Nodes::OuterJoin).on(blokings[:from_user_id].eq(users[:id]))
    query.where(blokings[:to_user_id].eq(self.id))
    query.join(blockings_reasons, Arel::Nodes::OuterJoin).on(blokings[:id].eq(blockings_reasons[:blocking_id]))
    if self.role_id == Role.client.id
      query.join(block_client_reasons, Arel::Nodes::OuterJoin).on(block_client_reasons[:id].eq(blockings_reasons[:reason_id]))
      select_fields.push('array_agg(block_client_reasons.title) as block_reasons')
    elsif self.role_id == Role.trainer.id
      query.join(block_trainer_reasons, Arel::Nodes::OuterJoin).on(block_trainer_reasons[:id].eq(blockings_reasons[:reason_id]))
      select_fields.push('array_agg(block_trainer_reasons.title) as block_reasons')
    end

  elsif params[:type] == 'blocked'
    query = users.join(blokings, Arel::Nodes::OuterJoin).on(blokings[:to_user_id].eq(users[:id]))
    query.where(blokings[:from_user_id].eq(self.id))
    query.join(blockings_reasons, Arel::Nodes::OuterJoin).on(blokings[:id].eq(blockings_reasons[:blocking_id]))
    if self.role_id == Role.client.id
      query.join(block_trainer_reasons, Arel::Nodes::OuterJoin).on(block_trainer_reasons[:id].eq(blockings_reasons[:reason_id]))
      select_fields.push('array_agg(block_trainer_reasons.title) as block_reasons')
    elsif self.role_id == Role.trainer.id
      query.join(block_client_reasons, Arel::Nodes::OuterJoin).on(block_client_reasons[:id].eq(blockings_reasons[:reason_id]))
      select_fields.push('array_agg(block_client_reasons.title) as block_reasons')
    end
  end

    if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
      if params[:sort_column] != "block_date"
        query = query.order(users[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(blokings[:created_at].send(params[:sort_type] == 'asc' ? :asc : :desc))
      end
    else
      query.order("block_date")
    end

    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      query.order("relevancy DESC") if params[:count].nil?
      select_fields.push("ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy")
    end
    query.where(users[:email].matches("%#{ params[:email] }%"))       if params[:email].present?

    query.group(users[:id], blokings[:id])

    query.project(*select_fields)

    query
  end

  def check_working_hours
    return if !self.working_hours.is_a?(Array) || self.working_hours.blank?
    
    schedule_attributes = []

    (0..6).each do |day|
      day_record = working_hours.find{|i| i[:day].to_i == day} || {}
      day_record[:ranges] ||= []

      day_schedule_attributes = []

      day_record[:ranges].each do |range|
        day_schedule_attributes << {
            day: day,
            from_time: range[:from_time].strftime("%H:%M"),
            to_time: range[:to_time].strftime("%H:%M")
        }
        self.errors.add :to_time, 'should be greater then from time' if range[:from_time].to_time >= range[:to_time].to_time
      end

      self.errors.add :working_hours, 'should not overlap' if day_schedule_attributes.sort_by{|i| i[:from_time]}.each_cons(2).any?{ |x,y| x[:to_time] > y[:from_time] }
      schedule_attributes += day_schedule_attributes
    end

    # schedule_attributes += self.schedules.map{|i| {id: i.id, _destroy: true }}.select{|i| !i[:id].nil?}
    self.schedules = []
    self.schedules_attributes = schedule_attributes
    make_sure_there_is_no_sessions_booked_for_old_working_hours

    self.schedule_histories_attributes = [{value: self.schedules.map(&:to_json), created_at: Time.now.utc.beginning_of_day}] unless self.schedules.blank?
  end

  def role_presence
    self.errors.add :base, "You haven't registered yet. Please select role before signing in." unless role
  end

  def make_sure_there_is_no_sessions_booked_for_old_working_hours
    return unless trainer?

    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    query = booking_sessions
                .project("COUNT(*)")
                .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                .where(bookings[:trainer_id].eq(self.id))
                .where(booking_sessions[:from_time].gt(Time.now))
                .where(bookings[:status].eq(Booking.statuses[:approved]))
                .where(booking_sessions[:paid_up].eq(true))

    conditions = []

    self.schedules.each do |schedule|
      conditions << "(extract(dow from booking_sessions.from_time) = #{ schedule.day } " +
          "AND booking_sessions.from_time::time >= '#{ schedule.from_time.hour }:#{ schedule.from_time.min }:00' " +
          "AND booking_sessions.to_time::time <= '#{ schedule.to_time.hour }:#{ schedule.to_time.min }:00')"
    end

    query.where(Arel::Nodes::SqlLiteral.new("( #{ conditions.join(" OR ")} )"))

    if BookingSession.find_by_sql(query.to_sql).first['count'] != BookingSession.where(bookings: {status: 'approved', trainer_id: self.id}, paid_up: true).where("from_time > ?", Time.now).joins(:booking).count
      self.errors.add :working_hours, "You have sessions for that time. To change working hours, please rearrange your sessions first."
    end
  end

  def notify_admin_about_new_user
    UserMailer.notify_admin_about_new_user(self.id).deliver_later
    AdminNotification.create(user_id: self.id, notify_type: "new_user")
  end

  def validate_bio_n_qualification
    system_settings = SystemSettings.instance

    if trainer? && system_settings.trainer_qualification_max_lenght && system_settings.trainer_qualification_max_lenght.to_i < qualification.to_s.size
      self.errors.add :qualification, "Maximum allowed lenght is #{ system_settings.trainer_qualification_max_lenght.to_i } characters."
    end

    if trainer? && system_settings.trainer_bio_max_lenght && system_settings.trainer_bio_max_lenght.to_i < bio.to_s.size
      self.errors.add :bio, "Maximum allowed lenght is #{ system_settings.trainer_bio_max_lenght.to_i } characters."
    end

  end
end