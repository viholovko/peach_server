class Blocking < ApplicationRecord
  belongs_to :to_user, class_name: "User"
  belongs_to :from_user, class_name: "User"

  attr_accessor :validate_trainer_reason
  attr_accessor :validate_client_reason

  has_and_belongs_to_many :block_trainer_reasons,
                          join_table: 'blockings_reasons',
                          class_name: 'BlockTrainerReason',
                          association_foreign_key: 'reason_id'
  has_and_belongs_to_many :block_client_reasons,
                          join_table: 'blockings_reasons',
                          class_name: 'BlockClientReason',
                          association_foreign_key: 'reason_id'
  validates :from_user_id, uniqueness: {scope: :to_user_id, message: "already blocked!"}
  validates :comment, presence: true, allow_blank: true
  validate :at_least_one_block_trainer_reason, if: :validate_trainer_reason
  validate :at_least_one_block_client_reason, if: :validate_client_reason
  after_create      :notify_admin_about_blocking

  private

  def at_least_one_block_trainer_reason
    self.errors.add :block_trainer_reasons, 'should have at least one reason.' if block_trainer_reasons.blank?
  end
  def at_least_one_block_client_reason
    self.errors.add :block_client_reasons, 'should have at least one reason.' if block_client_reasons.blank?
  end

  def notify_admin_about_blocking
    UserMailer.notify_admin_about_blocking(self.id).deliver_later
    AdminNotification.create(user: self.from_user, notify_type: "new_blocking")
  end

end
