class Client < User

  has_many :trainer_notes
  has_many :bookings
  has_many :booking_sessions, through: :bookings
  has_and_belongs_to_many :favorite_trainers, join_table: 'favorite_trainers', foreign_key: 'client_id', class_name: 'Trainer', association_foreign_key: 'trainer_id'
  has_many :payments, through: :bookings

  default_scope -> { where(role_id: Role.client.id) }

  def self.search_query(params={})
    params[:per_page] ||= 10
    searchingByName = params[:name].present?
    names = params[:name].split if searchingByName

    clients = User.arel_table

    query = clients
                .group(clients[:id])
                .where(clients[:role_id].eq(Role.client.id))

    if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
      query = query.order(clients[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      query = query.order(clients[:id].desc) if !searchingByName
    end

    if params[:show_deleted].present? then
      query.where(clients[:deleted_at].not_eq(nil))
    else
      query.where(clients[:deleted_at].eq(nil)) unless params[:show_all].present?
    end

    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      query.order("relevancy DESC")
      query.project(clients[Arel.star],
        "ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy"
      )
    else
      query.project(Arel.sql('*'))
    end

    query.where(clients[:email].matches("%#{ params[:email] }%"))       if params[:email].present?
    query
  end

  def self.my_trainers(current_user, params = {})
    trainers                 = Trainer.arel_table
    bookings                 = Booking.arel_table
    booking_sessions         = BookingSession.arel_table
    specialities_trainers    = Arel::Table.new :specialities_trainers
    goals_specialities       = Arel::Table.new :goals_specialities
    goals                    = Goal.arel_table
    specialities             = Speciality.arel_table
    training_places          = TrainingPlace.arel_table
    training_places_users    = Arel::Table.new :training_places_users

    searchingByName = params[:name].present?
    names = params[:name].split if searchingByName

    query = trainers
                .group(trainers[:id], bookings[:id], booking_sessions[:booking_id])
                .join(bookings, Arel::Nodes::OuterJoin).on(bookings[:trainer_id].eq(trainers[:id]).and(bookings[:id].eq(
                  bookings.join(booking_sessions, Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id].eq(bookings[:id]))
                    .where(bookings[:trainer_id].eq(trainers[:id]))
                    .where(bookings[:client_id].eq(current_user.id))
                    .where(bookings[:status].eq(Booking.statuses["approved"]))
                    .where(booking_sessions[:to_time].gt(Time.now))
                    .group(bookings[:id], booking_sessions[:id])
                    .order(booking_sessions[:from_time])
                    .take(1)
                    .project(bookings[:id]))))
                .join(booking_sessions,         Arel::Nodes::OuterJoin).on(booking_sessions[:booking_id]                .eq(bookings[:id]))
                .join(specialities_trainers,    Arel::Nodes::OuterJoin).on(specialities_trainers[:trainer_id]           .eq(trainers[:id]))
                .join(specialities,             Arel::Nodes::OuterJoin).on(specialities_trainers[:speciality_id]        .eq(specialities[:id]))
                .join(goals_specialities,       Arel::Nodes::OuterJoin).on(goals_specialities[:speciality_id]           .eq(specialities[:id]))
                .join(goals,                    Arel::Nodes::OuterJoin).on(goals_specialities[:goal_id]                 .eq(goals[:id]))
                .join(training_places_users,    Arel::Nodes::OuterJoin).on(training_places_users[:user_id]              .eq(trainers[:id]))
                .join(training_places,          Arel::Nodes::OuterJoin).on(training_places_users[:training_place_id]    .eq(training_places[:id]))

    query.where(bookings[:status].eq(Booking.statuses["approved"]))
    query.where(bookings[:client_id].eq(current_user.id))
    query.where(trainers[:role_id].eq(Role.trainer.id))
    query.where(trainers[:gender].eq(params[:gender]))                   if params[:gender].present?
    query.where(trainers[:confirmed].eq(params[:confirmed]))             if params[:confirmed].present?
    query.where(trainers[:email_confirmed].eq(params[:email_confirmed])) if params[:email_confirmed].present?
    query.where(goals[:id].in(params[:goal_ids]))                        if params[:goal_ids].is_a?(Array)
    query.where(training_places[:id].in(params[:training_place_ids]))    if params[:training_place_ids].is_a?(Array)
    query.where(trainers[:email].matches("%#{ params[:email] }%"))       if params[:email].present?

    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      query.order("relevancy DESC") if params[:count].nil?
    end

    if params[:show_deleted].present? then
      query.where(trainers[:deleted_at].not_eq(nil))
    else
      query.where(trainers[:deleted_at].eq(nil)) unless params[:show_all].present?
    end

    if params[:latitude].present? && params[:longitude].present? && params[:distance].present?
      # to_f should prevent sql injections
      query.where(Arel::Nodes::SqlLiteral.new("(gc_dist(#{ params[:latitude].to_f }, #{ params[:longitude].to_f }, users.latitude, users.longitude)) - users.location_radius").lteq( params[:distance].to_f ))
    end

    if params[:count]
      fields = ["COUNT(*)"]
    else
      fields = [
          trainers[Arel.star],
          "(gc_dist(#{ params[:latitude].to_f }, #{ params[:longitude].to_f }, users.latitude, users.longitude)) as distance",
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT specialities.id, specialities.title ) r )) FILTER (WHERE specialities.id IS NOT NULL), '[]') AS specialities",
          "COALESCE(json_agg(DISTINCT ( SELECT r FROM (SELECT training_places.id, training_places.title ) r )) FILTER (WHERE training_places.id IS NOT NULL), '[]') AS training_places",
          'MIN(booking_sessions.from_time) as next_session',
          bookings[:id].as("booking_id")
      ]
      fields.push("ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy") if searchingByName

      if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
        query = query.order(trainers[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(trainers[:id].desc) if !searchingByName
      end
      query.order("next_session") unless params[:sort_column].present?
    end
    query.project(*fields)
    query
  end

  def self.favorite_trainers(current_user, params={})
    users = User.arel_table
    favorite_trainers = Arel::Table.new :favorite_trainers
    query = users.join(favorite_trainers,Arel::Nodes::OuterJoin).on(favorite_trainers[:trainer_id].eq(users[:id]))

    searchingByName = params[:name].present?
    names = params[:name].split if searchingByName

    if params[:count].present?
        fields = ["COUNT(*)"]
    else
        fields = [
            users[Arel.star]
        ]
        if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
          if %w(id first_name gender).include?(params[:sort_column])
            query = query.order(users[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
          end
        else
          query.order(favorite_trainers[:id]) if !searchingByName
        end
        query.group(favorite_trainers[:id], users[:id])
    end
    query.where(favorite_trainers[:client_id].eq(Arel::Nodes::Quoted.new(current_user.id)))
    if searchingByName
      query.where(Arel.sql("(users.first_name ~* '#{names.join('|')}' OR users.last_name ~* '#{names.join('|')}')"))
      fields.push("ts_rank(to_tsvector(textcat(textcat(first_name, ' '), last_name)), to_tsquery('#{names.collect{|x| x+':*'}.join(' | ')}')) AS relevancy") if params[:count].nil?
      query.order("relevancy DESC") if params[:count].nil?
    end
    query.where(users[:email].matches("%#{ params[:email] }%"))             if params[:email].present?


    query.project(*fields)
    query

  end

  def self.booking_sessions(current_user, params = {})
    params[:per_page] ||= 10

    booking_sessions = BookingSession.arel_table
    bookings = Booking.arel_table
    feelings = Feeling.arel_table
    trainers = Trainer.arel_table

    fields = [
      booking_sessions[:id],
      booking_sessions[:booking_id],
      bookings[:booking_type],
      bookings[:session_price],
      bookings[:status],
      booking_sessions[:from_time],
      booking_sessions[:to_time],
      bookings[:trainer_id],
      trainers[:first_name],
      trainers[:last_name],
      feelings[:title],
      booking_sessions[:rate]
    ]

    query = booking_sessions
            .group(fields)
            .join(bookings, Arel::Nodes::OuterJoin).on(bookings[:id].eq(booking_sessions[:booking_id]))
            .join(feelings, Arel::Nodes::OuterJoin).on(feelings[:id].eq(booking_sessions[:feeling_id]))
            .join(trainers, Arel::Nodes::OuterJoin).on(bookings[:trainer_id].eq(trainers[:id]))

    query.where(bookings[:client_id].eq(current_user.id))
    query.where(bookings[:id].eq params[:booking_id]) if params[:booking_id].present?
    query.where(bookings[:status].eq params[:status]) if params[:status].present?
    query.where(trainers[:first_name].matches("%#{ params[:name] }%")
          .or(trainers[:last_name].matches("%#{ params[:name] }%"))) if params[:name].present?
    query.where(booking_sessions[:rate].eq params[:rate]) if params[:rate].present?

    case params[:sort_column]
    when 'name'
      query = query.order(trainers[:first_name].send(params[:sort_type] == 'asc' ? :asc : :desc))
    when 'booking_type', 'session_price', 'status'
      query = query.order(bookings[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    when 'feeling'
      query = query.order(booking_sessions[:feeling_id].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      if params[:sort_column].present? && %w[asc desc].include?(params[:sort_type])
        query = query.order(booking_sessions[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      end
    end

    query.project(*fields)
    query
  end

  def details_json(current_user)
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table
    query = bookings.join(booking_sessions, Arel::Nodes::OuterJoin).on(bookings[:id].eq(booking_sessions[:booking_id]))
    query.where(bookings[:client_id]          .eq(self.id))
    query.where(bookings[:trainer_id]         .eq(current_user.id))
    query.where(bookings[:status]             .eq(Booking.statuses["approved"]))
    query.where(booking_sessions[:from_time]  .gteq(Time.now))
    query.order(booking_sessions[:from_time])
    query.take(1)
    query.project(
      bookings[:id],
      booking_sessions[:id].as('booking_session_id'),
      booking_sessions[:from_time],
      booking_sessions[:to_time]
    )

    next_booking = Booking.find_by_sql(query.to_sql).first

    to_json.except(:qb_login, :qb_password, :registration_step).merge(
        {
            distance: distance_to(current_user),
            booking_id: next_booking.try(:id),
            booking_session_id: next_booking.try(:booking_session_id),
            next_session_date: next_booking.try(:from_time).try(:to_i),
            next_session_from_time: next_booking.try(:from_time).try(:to_i),
            next_session_to_time: next_booking.try(:to_time).try(:to_i)
        }
    )
  end
end