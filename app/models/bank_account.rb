class BankAccount < ApplicationRecord
  belongs_to :user
  validates :number, presence: true
  validates :sort_code, presence: true
  validates :name, :bank, presence: true
end