class RejectBookingReason < ApplicationRecord

  has_and_belongs_to_many :bookings
  validates :title, presence: true

  def self.search_query(params)
    params[:per_page] ||= 10

    reject_booking_reasons = RejectBookingReason.arel_table

    fields = [
        reject_booking_reasons[:id],
        reject_booking_reasons[:title],
        reject_booking_reasons[:created_at]
    ]

    query = reject_booking_reasons
                .group(fields)

    if params[:count]
      query.project('COUNT(*)')
    else
      if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
        query = query.order(reject_booking_reasons[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(reject_booking_reasons[:id].desc)
      end
      query.project(fields)
    end

    query.where(reject_booking_reasons[:title].matches("%#{ params[:title] }%")) if params[:title].present?

    query
  end
end
