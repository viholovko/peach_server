class Goal < ApplicationRecord

  has_and_belongs_to_many :specialities
  has_and_belongs_to_many :users, join_table: :goals_clients, associaion_foreign_key: :client_id

  validates :title, presence: true
  validate :at_least_one_speciality

  def self.search_query(params)
    params[:per_page] ||= 10

    goals = Goal.arel_table

    fields = [
        goals[:id],
        goals[:title],
        goals[:created_at]
    ]

    query = goals
                .group(fields)

    if params[:count]
      query.project('COUNT(*)')
    else
      if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
        query = query.order(goals[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(goals[:id].desc)
      end
      query.project(fields)
    end

    query.where(goals[:title].matches("%#{ params[:title] }%")) if params[:title].present?

    query
  end

  private

  def at_least_one_speciality
    self.errors.add :specialities, "should have at least one speciality" if specialities.blank?
  end
end
