class Schedule < ApplicationRecord

  belongs_to :user

  validates :from_time, :presence =>true, :allow_blank => false
  validates :to_time, :presence =>true, :allow_blank => false
  validate :from_time_lower_than_finish

  def to_json
    {
        day: day,
        from_time: from_time,
        to_time: to_time
    }
  end

  private

  def from_time_lower_than_finish
    if self.from_time && self.to_time
      if  self.from_time > self.to_time
        self.errors.add(:from_time, "has to be lower than finish time")
        false
      end
    end
  end

end
