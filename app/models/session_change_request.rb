class SessionChangeRequest < ActiveRecord::Base
  belongs_to :booking_session
  belongs_to :booking
  belongs_to :from_user, class_name: User
  belongs_to :to_user, class_name: User

  # self.skip_time_zone_conversion_for_attributes = [:from_time, :to_time]

  validates :from_time, presence: true
  validates :to_time, presence: true
  validate :inclusion_in_trainer_working_hours
  validate :exclusion_of_all_other_sessions
  validate :sessions_not_moved_to_next_month_or_in_the_past
  validate :from_time_lower_than_finish

  after_create :notify_trainer

  def self.query(user, options)
    users = User.arel_table
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table
    change_requests = SessionChangeRequest.arel_table

    q = change_requests
            .join(booking_sessions).on(change_requests[:booking_session_id].eq(booking_sessions[:id]))
            .join(bookings).on(booking_sessions[:booking_id].eq(bookings[:id]))
            .join(users).on(bookings[user.client? ? :client_id : :trainer_id].eq(users[:id]))

    if options[:count]
      q.project("COUNT(*)")
    else
      if options[:sort_column] == 'created_at' && %w(asc desc).include?(options[:sort_type])
        q.order(change_requests[:created_at].send(options[:sort_type] == 'asc' ? :asc : :desc))
      else
        q.order(change_requests[:id].desc)
      end

      q.project(
           change_requests[:id],
           change_requests[:from_time].as('requested_from_time'),
           change_requests[:to_time].as('requested_to_time'),
           booking_sessions[:from_time].as('current_from_time'),
           booking_sessions[:to_time].as('current_to_time'),
           bookings[:trainer_id],
           bookings[:client_id],
      )
    end

    q.where(change_requests[:to_user_id].eq(user.id))

    q
  end

  def reject
    Notification.create user: self.from_user,
                        notification_type: "#{ to_user.role.name }_rejected_change_request_for_#{ self.booking.booking_type }_booking",
                        data: {from_user: to_user.id},
                        booking_id: self.booking_id,
                        session_change_request_id: self.id
    set_notification_status
    self.update_attribute :active, false
  end

  def accept
    if valid?
      booking_session.assign_attributes from_time: self.from_time, to_time: self.to_time
      booking_session.save(validate: false)
      Notification.create user: self.from_user,
                          notification_type: "#{ to_user.role.name }_approved_change_request_for_#{ self.booking.booking_type }_booking",
                          data: {from_user: to_user.id},
                          booking_id: self.booking_id,
                          session_change_request_id: self.id
      set_notification_status
      self.update_attribute :active, false
      true
    else
      false
    end
  end

  private

  def inclusion_in_trainer_working_hours
    return unless from_time && to_time

    error = 'time do not fit with trainer working schedule'

    working_day = booking.trainer.schedules.where(day: from_time.wday)

    self.errors.add :from_time, error and return if working_day.blank?

    range = Time.new.change(hour: from_time.hour, min: from_time.min)..Time.new.change(hour: to_time.hour, min: to_time.min)

    session_fit = working_day
                      .map{|i| Time.new.change(hour: i.from_time.hour, min: i.from_time.min)..Time.new.change(hour: i.to_time.hour, min: i.to_time.min) }
                      .map{|working_range| working_range.include?(range)}
                      .include?(true)

    self.errors.add :booking_sessions, error unless session_fit
  end

  def exclusion_of_all_other_sessions
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    conditions = [
        booking_sessions[:from_time].gteq(self.from_time).and(booking_sessions[:from_time].lteq(self.to_time)),
        booking_sessions[:to_time].gteq(self.from_time).and(booking_sessions[:to_time].lteq(self.to_time))
    ]

    query = booking_sessions
                .project(bookings[:client_id], bookings[:trainer_id])
                .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                .where(bookings[:trainer_id].eq(booking.trainer&.id).or(bookings[:client_id].eq(booking.client&.id)))
                .where(booking_sessions[:id].not_eq(self.booking_session.id))
                .where(conditions.inject { |conds, cond| conds.or(cond) } )
                .take(1)

    result = BookingSession.find_by_sql(query.to_sql)

    if result.count > 0
      self.errors.add(:from_time, "time do not fit with other trainer sessions!") if result.first['trainer_id'] == booking.trainer&.id
      self.errors.add(:from_time, "time do not fit with other client sessions!") if result.first['client_id'] == booking.client&.id
    end
  end

  def notify_trainer
    Notification.create user: self.to_user,
                        notification_type: "#{ from_user.role&.name }_created_change_request_for_#{ self.booking.booking_type }_booking",
                        data: {from_user: from_user.id},
                        booking_id: self.booking_id,
                        action_required: true,
                        session_change_request_id: self.id
  end

  def set_notification_status
    Notification.where( user: self.to_user,
                        notification_type: "#{ from_user.role&.name }_created_change_request_for_#{ self.booking.booking_type }_booking",
                        booking_id: self.booking_id,
                        action_required: true,
                        session_change_request_id: self.id).update_all(action_required: false)
  end

  def sessions_not_moved_to_next_month_or_in_the_past
    if booking_session && from_time && (to_time > booking_session.to_time.end_of_month || from_time < booking_session.from_time.beginning_of_month)
      self.errors.add :subscription_session, "can't be moved from one month to the next. Select a date within the current calendar month."
      false
    elsif booking_session && from_time && (from_time < Time.now)
      self.errors.add :session, "can't be move to a date that has passed"
      false
    end
  end

  def from_time_lower_than_finish
    if from_time && to_time
      if from_time > to_time
        self.errors.add :from_time, "has to be lower than finish time"
        false
      elsif (to_time.to_i - from_time.to_i) != 1.hour.to_i
        self.errors.add(:duration, "of training should be equal to 1 hour!")
        false
      end
    end
  end
end
