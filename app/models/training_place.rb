class TrainingPlace < ApplicationRecord

  validates :title, presence: true

  def self.search_query(params)
    places = TrainingPlace.arel_table
    
    query = places.group(places[:id])

    if params[:count]
      query.project('COUNT(*)')
    else
      if params[:sort_column].present? && %w(asc desc).include?(params[:sort_type])
        query = query.order(places[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(places[:id].desc)
      end
      query.project(Arel.star)
    end

    query
  end

end
