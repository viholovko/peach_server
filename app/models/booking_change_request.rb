class BookingChangeRequest < ActiveRecord::Base
  belongs_to :booking
  belongs_to :from_user, class_name: User
  belongs_to :to_user, class_name: User

  serialize :booking_sessions_attributes, Array

  validate :session_days_should_be_in_future
  validate :inclusion_in_trainer_working_hours
  validate :exclusion_of_all_other_sessions

  after_create :notify_client

  def reject
    Notification.create user: self.from_user,
                        notification_type: "#{ to_user.role.name }_rejected_booking_change_request_for_#{ self.booking.booking_type }_booking",
                        data: {from_user: to_user.id},
                        booking_id: self.booking_id,
                        session_change_request_id: self.id
    set_notification_status
    self.update_attribute :active, false
  end

  def apply
    if valid?
      days_attributes = booking_sessions_attributes.map{ |session|
        {
            from_time: session[:from_time].strftime("%H:%M"),
            to_time: session[:to_time].strftime("%H:%M"),
            day: session[:from_time].wday,
        }
      }

      booking.booking_session_days_attributes = days_attributes + booking.booking_session_days.map{ |session| { id: session.id, '_destroy' => true }}

      booking.booking_sessions_attributes = Booking.future_sessions_attributes(Date.today.end_of_month, days_attributes) +
          booking.booking_sessions.where("from_time > ?", Date.today.end_of_month).map{ |session| { "id" => session.id, '_destroy' => true }}

      if booking.save
        self.update_attribute :applied, true
        true
      else
        booking.errors.full_messages.each do |e|
          self.errors.add :base, e
        end
        false
      end
    else
      false
    end
  end

  def accept
    return true unless booking.renewable?
    if valid?
      days_attributes = booking_sessions_attributes.map{ |session|
        {
            from_time: session[:from_time].strftime("%H:%M"),
            to_time: session[:to_time].strftime("%H:%M"),
            day: session[:from_time].wday,
        }
      }

      booking.booking_session_days_attributes = days_attributes + booking.booking_session_days.map{ |session| { id: session.id, '_destroy' => true }}

      booking.booking_sessions_attributes = Booking.future_sessions_attributes(Date.today.end_of_month, days_attributes) +
                                           booking.booking_sessions.where("from_time > ?", Date.today.end_of_month).map{ |session| { "id" => session.id, '_destroy' => true }}

      if booking.valid?
        Notification.create user: self.from_user,
                            notification_type: "#{ to_user.role.name }_approved_booking_change_request_for_#{ self.booking.booking_type }_booking",
                            data: {from_user: to_user.id},
                            booking_id: self.booking_id,
                            session_change_request_id: self.id
        set_notification_status
        self.assign_attributes active: false, date: (Time.now + 1.month).beginning_of_month
        true
      else
        booking.errors.full_messages.each do |e|
          self.errors.add :base, e
        end
        false
      end
    else
      false
    end
  end

  private

  def booking_sessions_should_not_overlap
    if booking_sessions_attributes.sort_by{|i| i[:from_time]}.each_cons(2).any?{ |x,y| x[:to_time] > y[:from_time] }
      self.errors.add(:booking_sessions, 'should not overlap!')
      false
    end
  end

  def inclusion_in_trainer_working_hours
    return unless booking.trainer

    error = 'time do not fit with trainer working schedule'

    booking_sessions_attributes.each do |session|
      working_day = booking.trainer.schedules.where(day: session[:from_time].wday)

      self.errors.add :booking_sessions, error and next if working_day.blank?

      range = Time.new.change(hour: session[:from_time].hour, min: session[:from_time].min)..Time.new.change(hour: session[:to_time].hour, min: session[:to_time].min)

      session_fit = working_day
                        .map{|i| Time.new.change(hour: i.from_time.hour, min: i.from_time.min)..Time.new.change(hour: i.to_time.hour, min: i.to_time.min) }
                        .map{|working_range| working_range.include?(range)}
                        .include?(true)

      self.errors.add :booking_sessions, error unless session_fit
    end
  end

  def exclusion_of_all_other_sessions
    bookings = Booking.arel_table
    booking_sessions = BookingSession.arel_table

    query = booking_sessions
                .project(bookings[:client_id], bookings[:trainer_id])
                .join(bookings).on(bookings[:id].eq(booking_sessions[:booking_id]))
                .where(bookings[:trainer_id].eq(booking.trainer&.id).or(bookings[:client_id].eq(booking.client&.id)))
                .where(bookings[:id].not_eq(booking.id))
                .where(bookings[:status].eq(Booking.statuses[:approved]))

    conditions = []

    self.booking_sessions_attributes.each do |sessions|
      conditions << booking_sessions[:from_time].gteq(sessions[:from_time]).and(booking_sessions[:from_time].lteq(sessions[:to_time]))
      conditions << booking_sessions[:to_time].gteq(sessions[:from_time]).and(booking_sessions[:to_time].lteq(sessions[:to_time]))
    end

    query.where(conditions.inject { |conds, cond| conds.or(cond) } ) if conditions.any?

    result = BookingSession.find_by_sql(query.to_sql)

    if result.count > 0
      self.errors.add(:from_time, "time do not fit with other trainer sessions!") if result.first['trainer_id'] == booking.trainer&.id
      self.errors.add(:from_time, "time do not fit with other client sessions!") if result.first['client_id'] == booking.client&.id
    end
  end

  def session_days_should_be_in_future
    booking_sessions_attributes.each do |session|
      self.errors.add :booking_sessions, 'time can\'t be blank or past' and next if !session[:from_time] || ! session[:to_time] || session[:from_time] < Time.now || session[:to_time] < Time.now
    end
  end

  def notify_client
    Notification.create user: self.to_user,
                        notification_type: "#{ from_user.role&.name }_created_booking_change_request_for_#{ self.booking.booking_type }_booking",
                        data: {from_user: from_user.id},
                        booking_id: self.booking_id,
                        action_required: true,
                        booking_change_request_id: self.id
  end

  def set_notification_status
    Notification.where( user: self.to_user,
                        notification_type: "#{ from_user.role&.name }_created_booking_change_request_for_#{ self.booking.booking_type }_booking",
                        booking_id: self.booking_id,
                        action_required: true,
                        booking_change_request_id: self.id).update_all(action_required: false)
  end
end
