class Payment < ApplicationRecord
  belongs_to :booking
  belongs_to :user
  has_many :booking_sessions, validate: false

  validates :booking, presence: true
  validates :user, presence: :true
  validates :date, uniqueness: { scope: :booking_id }
  validates :sessions_count, numericality: { greater_than: 0 }

  after_initialize :set_defaults
  before_validation :charge_client
  after_save :update_sessions_status

  serialize :stripe_response, Hash

  enum transaction_type: {
      charge_client: 0
  }

  def self.query(current_user, options = {})
    payments = Payment.arel_table

    page = options[:page].to_i
    page = 1 if page < 1
    per_page = options[:per_page].to_i
    per_page = 10 if per_page < 1

    q = nil

    if options[:upcoming]
      q = upcoming_billings(current_user, options).union(history_billings(current_user, options)).to_sql
    else
      q = history_billings(current_user, options).to_sql
    end

    options[:sort_type] ||= 'asc'
    options[:sort_column] ||= 'date'

    if options[:count]
      q = "SELECT COUNT(*) FROM (#{ upcoming_billings(current_user, options).to_sql } UNION ALL #{ history_billings(current_user, options).to_sql }) AS aggregated_payments"
    else
      q = "SELECT * FROM (#{ upcoming_billings(current_user, options).to_sql } UNION ALL #{ history_billings(current_user, options).to_sql }) AS aggregated_payments #{ ['asc', 'desc'].include?(options[:sort_type]) ? " ORDER BY 1 #{ options[:sort_type]}" : " ORDER BY 1 ASC" } LIMIT #{ per_page } OFFSET #{ (page - 1) * per_page }"
    end

    q
  end

  private

  def self.history_billings(current_user, options = {})
    payments  = Payment.arel_table
    bookings  = Booking.arel_table
    users     = User.arel_table
    opponents = User.arel_table.alias('opponent')

    q = payments
            .join(bookings).on(bookings[:id].eq(payments[:booking_id]))
            .join(users).on(bookings[current_user.client? ? :client_id : :trainer_id].eq(users[:id]))
            .join(opponents).on(bookings[current_user.client? ? :trainer_id : :client_id].eq(opponents[:id]))
            .group(payments[:id], opponents[:id], bookings[:id])
            .where(bookings[current_user.client? ? :client_id : :trainer_id].eq(current_user.id))
            .project(
              payments[:created_at].as('date'),
              opponents[:id].as('opponent_id'),
              '\'history\' as type',
              opponents[:first_name],
              opponents[:last_name],
              opponents[:avatar_file_name],
              opponents[:avatar_file_size],
              opponents[:avatar_content_type],
              bookings[:id].as('booking_id'),
              payments[:session_price],
              payments[:sessions_count],
              payments[:payment_method_last_four],
              payments[:card_brand]
            )

    q
  end

  def self.upcoming_billings(current_user, options = {})
    payments     = Payment.arel_table
    bookings     = Booking.arel_table
    users        = User.arel_table
    credit_cards = CreditCard.arel_table
    opponents    = User.arel_table.alias('opponent')

    q = bookings
        .join(users).on(bookings[current_user.client? ? :client_id : :trainer_id].eq(users[:id]))
        .join(opponents).on(bookings[current_user.client? ? :trainer_id : :client_id].eq(opponents[:id]))
        .join(credit_cards, Arel::Nodes::OuterJoin).on((current_user.client? ? users[:id] : opponents[:id]).eq(credit_cards[:user_id]).and(credit_cards[:active].eq(true)))
        .where(bookings[:status].eq(Booking.statuses[:approved]))
        .where(bookings[current_user.client? ? :client_id : :trainer_id].eq(current_user.id))
        .where(bookings[:booking_type].eq(Booking.booking_types[:renewable]))
        .where(bookings[current_user.client? ? :client_id : :trainer_id].eq(current_user.id))

    q.project(
      '(select date from payments where payments.booking_id = bookings.id order by date desc limit 1) + interval  \'1 month\' as date',
      opponents[:id].as('opponent_id'),
      '\'upcoming\' as type',
      opponents[:first_name],
      opponents[:last_name],
      opponents[:avatar_file_name],
      opponents[:avatar_file_size],
      opponents[:avatar_content_type],
      bookings[:id].as('booking_id'),
      bookings[:session_price],
      '(select COUNT(*)
          from booking_sessions
         where booking_sessions.booking_id = bookings.id
           AND date_trunc(\'month\', booking_sessions.from_time) = date_trunc(\'month\', (select date from payments where payments.booking_id = bookings.id order by date desc limit 1) + interval \'1 month\')
      ) as sessions_count',
      credit_cards[:last4].as('payment_method_last_four'),
      credit_cards[:brand].as('card_brand'),
    )

    q
  end

  def charge_client
    return unless charge_client?

    trainer = booking.trainer

    self.errors.add :trainer,          'not found'                                   and return unless trainer
    self.errors.add :base,             '^Please add a credit card to your profile'   and return if user.credit_cards.where(active: true).blank?
    self.errors.add :booking_sessions, 'should be at least one'                      and return if @sessions.count == 0

    card = user.credit_cards.where(active: true).first
    self.payment_method_last_four = card.last4
    self.card_brand = card.brand

    begin
      charge = Stripe::Charge.create(
        amount: session_price * sessions_count * 100,
        currency: 'gbp',
        customer: card.customer_id,
        description: "Charge client ##{ user.id } #{ user.full_name } for booking##{ booking.id }. Trainer: ##{trainer.id } #{ trainer.full_name}."
      )

      self.stripe_response = charge.to_hash
    rescue => e
      self.errors.add :base, e.message
    end

  end

  def set_defaults
    return unless new_record?

    if booking.payments.count == 0
      # if booking starts from next month
      first_session = booking.booking_sessions.order(from_time: :asc).first
      self.date = first_session.from_time if first_session
    end

    self.date = date.beginning_of_month

    @sessions = booking.booking_sessions.where "from_time >= ? AND from_time <= ?", self.date, self.date.to_datetime.end_of_month

    self.session_price = booking.session_price
    self.fee = SystemSettings.instance.fee

    self.sessions_count = @sessions.count

    case transaction_type
      when 'charge_client'
        self.user = booking.client
      else

    end
  end

  def update_sessions_status
    @sessions.update_all paid_up: true, payment_id: self.id
    self.booking.update_attribute :last_payment_date, self.date
  end
end