class Admin < User
  self.table_name = 'users'

  default_scope -> { where(role_id: Role.admin.id) }

  def self.search_query(params)
    params[:per_page] ||= 10

    admins = User.arel_table

    fields = [
        admins[:id],
        admins[:first_name],
        admins[:last_name],
        admins[:email],
        admins[:created_at]
    ]

    query = admins
                .project(fields)
                .group(fields)
                .where(admins[:role_id].eq(Role.admin.id))

    if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
      query = query.order(admins[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      query = query.order(admins[:id].desc)
    end

    query.where(
          admins[:first_name].matches("%#{ params[:name] }%")
      .or(admins[:last_name].matches("%#{ params[:name] }%")))         if params[:name].present?
    query.where(admins[:email].matches("%#{ params[:email] }%"))       if params[:email].present?

    query
  end
end
