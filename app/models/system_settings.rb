class SystemSettings < ActiveRecord::Base
  validates_numericality_of :fee, only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_numericality_of :min_radius, greater_than_or_equal_to: 0, less_than_or_equal_to: :max_radius
  validates_numericality_of :max_radius, greater_than_or_equal_to: :min_radius
  validates_numericality_of :min_price, greater_than_or_equal_to: 0,less_than_or_equal_to: :max_price
  validates_numericality_of :max_price, greater_than_or_equal_to: :min_price
  validates_numericality_of :package_expiration_time, only_integer: true, greater_than_or_equal_to: 1
  validates_numericality_of :min_sessions, greater_than_or_equal_to: 1, less_than_or_equal_to: :max_sessions
  validates_numericality_of :max_sessions, greater_than_or_equal_to: :min_sessions

  has_attached_file :ios_push_certificate, storage: :filesystem
  do_not_validate_attachment_file_type :ios_push_certificate

  before_save :configure_stripe
  before_save :configure_qb

  class << self
    SystemSettings.column_names.each do |column|
      define_method column do
        instance.send(column)
      end
    end

    def ios_push_certificate
      instance.ios_push_certificate
    end
  end

  def self.instance
    first_or_create do |s|
      s.fee = 0
      s.min_radius = 0
      s.max_radius = 150
      s.min_price = 1
      s.max_price = 150
      s.package_expiration_time = 24
      s.min_sessions = 1
      s.max_sessions = 4

      s.ios_push_apns_host = 'gateway.sandbox.push.apple.com'
      s.ios_push_password = ''
      s.ios_push_environment_sandbox = true
      # s.ios_push_certificate = ''
    end
  end

  def configure_stripe
    if stripe_publishable_key_changed? || stripe_secret_key_changed?
      Rails.configuration.stripe = {
          :publishable_key => stripe_publishable_key,
          :secret_key      => stripe_secret_key
      }

      Stripe.api_key = Rails.configuration.stripe[:secret_key]
    end
  end

  def configure_qb
    if qb_host_changed? || qb_application_id_changed? || qb_auth_key_changed? || qb_auth_secret_changed?
      QuickBlox.configure do |config|
        config.host = qb_host || "https://api.quickblox.com"
        config.application_id = qb_application_id.to_s
        config.auth_key = qb_auth_key.to_s
        config.auth_secret = qb_auth_secret.to_s
      end
    end
  end

end
