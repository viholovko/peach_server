class Feeling < ApplicationRecord

  has_many :booking_sessions

  validates :title, presence: true

  def self.search_query(params)
    feelings = Feeling.arel_table

    query = feelings.group(feelings[:id])

    if params[:count]
      query.project('COUNT(*)')
    else
      if params[:sort_column].present? && %w(asc desc).include?(params[:sort_type])
        query = query.order(feelings[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
      else
        query = query.order(feelings[:id].desc)
      end
      query.project(Arel.star)
    end

    query
  end

end
