class Announcement < ApplicationRecord

  has_attached_file :image,
                    styles: { medium: '500x500>' },
                    default_url: '/images/missing.png',
                    path: ":rails_root/public/system/announcements/:id/avatars/:style/:filename",
                    url: "/system/announcements/:id/avatars/:style/:filename"

  validates_attachment_size :image, less_than: 20.megabytes
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  validates :image, presence: true
  validates :title, presence: true
  validates :subtitle, presence: true
  validates :body, presence: true

  after_create :start_worker
  
  def self.search_query(user, params = {})
    announcements = Announcement.arel_table

    fields = [
        announcements[:id],
        announcements[:title],
        announcements[:subtitle],
        announcements[:body],
        announcements[:notify_trainers],
        announcements[:notify_clients],
        announcements[:notified_times],
        announcements[:image_file_name],
        announcements[:image_content_type],
        announcements[:image_file_size],
        announcements[:image_updated_at],
        announcements[:created_at]
    ]

    query = announcements.group(fields)

    if params[:count]
      query.project("COUNT(*)")
    else
      query.project(fields)
    end

    if !params[:sort_column].blank? && %w(asc desc).include?(params[:sort_type])
      query = query.order(announcements[params[:sort_column.to_sym]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      query = query.order(announcements[:id].desc)
    end

    query.where(announcements[:title].matches("%#{ params[:title] }%"))             if params[:title].present?
    query.where(announcements[:subtitle].matches("%#{ params[:subtitle] }%"))       if params[:subtitle].present?
    query.where(announcements[:body].matches("%#{ params[:body] }%"))               if params[:body].present?
    query.where(announcements[:notify_trainers].eq(params[:notify_trainers]))       if params[:notify_trainers].present?
    query.where(announcements[:notify_clients].eq(params[:notify_clients]))         if params[:notify_clients].present?
    query.where(announcements[:notify_clients].eq(true))                            if user.client?
    query.where(announcements[:notify_trainers].eq(true))                           if user.trainer?

    query
  end

  def notify
    if notify_trainers
      User.where(role_id: Role.trainer.id).find_each do |user|
        Notification.create user: user,
                            notification_type: "announcement",
                            data: {},
                            announcement_id: self.id

      end
    end

    if notify_clients
      User.where(role_id: Role.client.id).find_each do |user|
        Notification.create user: user,
                            notification_type: "announcement",
                            data: {},
                            announcement_id: self.id
      end
    end
  end

  def start_worker
    AnnouncementsWorker.perform_in(1.minute, id)
    self.update_attribute :notified_times, notified_times.to_i + 1
  end

  def body_attachments
    Attachment.where entity_id: self.id, entity_type: 'attachments_body'
  end

  after_destroy :destroy_attachments
  after_save :update_attachments

  private

  def destroy_attachments
    body_attachments.destroy_all
  end

  def update_attachments
    Attachment.where('created_at <= :day_ago AND entity_id IS NULL', :day_ago  => 1.day.ago ).destroy_all

    Attachment.where(entity_id: nil, entity_type: "attachments_body").each do |attachment|
      if content.include? attachment.file.url
        attachment.update_attribute :entity_id, id
      end
    end
  end

end