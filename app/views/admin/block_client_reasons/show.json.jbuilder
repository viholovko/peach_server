json.block_client_reason do
  json.id         @block_client_reason.id
  json.title      @block_client_reason.title
  json.created_at @block_client_reason.created_at.try(:strftime, "%d/%m/%Y")
end