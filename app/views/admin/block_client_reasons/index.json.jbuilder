json.block_client_reasons @block_client_reasons.each do |reasons|
  json.id         reasons.id
  json.title      reasons.title
  json.created_at reasons.created_at.try(:strftime, "%d/%m/%Y")
end
json.total @total