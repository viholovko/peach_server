json.announcement do
  json.id @announcement.id
  json.title @announcement.title
  json.subtitle @announcement.subtitle
  json.body @announcement.body
  json.notify_trainers @announcement.notify_trainers
  json.notify_clients @announcement.notify_clients
  json.image do
    json.url @announcement.image.url
  end
  json.created_at @announcement.created_at.try(:strftime, "%d/%m/%Y")
end