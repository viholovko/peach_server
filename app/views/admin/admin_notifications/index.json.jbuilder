json.notifications @notifications.each do |notification|
  json.id                    notification.id
  json.type                  notification.notify_type
  json.description           notification.data[:description]
  json.title                 notification.data[:title]
  json.url                   notification.data[:url]
  json.readed                notification.readed
  json.created_at            notification.created_at.try(:strftime, "%d/%m/%Y")
end
json.unread_count AdminNotification.where("readed = ?", false).count
json.total @total
