json.bookings @sessions do |session|
  json.id session['id']
  json.user_id session['user_id']
  json.status Booking.statuses.key(session['status'])
  json.full_name "#{ session['first_name'] } #{ session['last_name'] }"
  json.email session['email']
  json.gender session['gender']
  json.booking_type Booking.booking_types.key(session['booking_type'])
  json.session_price session['sessions_price']
  json.rate session['rate']
  json.from_time session['from_time']
  json.to_time session['to_time']
  user = User.new(
      id: session['user_id'],
      avatar_file_name: session['avatar_file_name'],
      avatar_file_size: session['avatar_file_size'],
      avatar_content_type: session['avatar_content_type'],
  )

  json.avatar user.avatar
end
json.total @count