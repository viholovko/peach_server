json.trainers @trainers.each do |trainer|
  json.id trainer.id
  json.full_name trainer.full_name
  json.email trainer.email
  json.email_confirmed trainer.email_confirmed
  json.confirmed trainer.confirmed
  json.gender trainer.gender
  json.social trainer.social
  json.avatar paperclip_url(trainer.avatar, :medium)
  json.has_credit_card trainer.has_credit_card
  json.created_at trainer.created_at.try(:strftime, "%d/%m/%Y")
  json.deactivated trainer.deleted_at.nil? ? false: true
end
json.total @total
