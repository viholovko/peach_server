json.trainer do
  json.id @trainer.id
  json.first_name @trainer.first_name
  json.last_name @trainer.last_name
  json.full_name @trainer.full_name
  json.email @trainer.email
  json.email_confirmed @trainer.email_confirmed
  json.gender @trainer.gender
  json.post_code @trainer.post_code
  json.confirmed @trainer.confirmed
  json.qb_token @trainer.qb_token
  json.social @trainer.social
  json.address @trainer.address
  json.has_location @trainer.has_location
  json.latitude @trainer.latitude
  json.longitude @trainer.longitude
  json.location_radius @trainer.location_radius
  json.sessions_price @trainer.sessions_price.to_f
  json.avatar do
    json.url @trainer.avatar.url
  end
  json.has_credit_card @trainer.has_credit_card
  json.created_at @trainer.created_at.try(:strftime, "%d/%m/%Y")
  json.deactivated @trainer.deleted_at.nil? ? false: true
  json.specialities @trainer.specialities.map(&:title).join(', ')
  json.training_places @trainer.training_places.map(&:title).join(', ')
  json.bio @trainer.bio
  json.qualification @trainer.qualification

  json.bank_account_name @trainer.bank_account&.name
  json.bank_account_number @trainer.bank_account&.number
  json.bank_account_bank @trainer.bank_account&.bank
  json.bank_account_sort_code @trainer.bank_account&.sort_code
  json.specialities Speciality.all do |speciality|
    json.id speciality.id
    json.title speciality.title
    json.selected !!@trainer.specialities.exists?(speciality.id)
  end
  json.average_response_time distance_of_time_in_words(@trainer.average_response_time)
end