json.trainer do
  json.specialities Speciality.all do |speciality|
    json.id speciality.id
    json.title speciality.title
    json.selected false
  end
end