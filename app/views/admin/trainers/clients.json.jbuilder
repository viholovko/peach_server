json.users @clients.each do |user|
  json.id user.id
  json.full_name user.full_name
  json.email user.email
  json.gender user.gender
  json.avatar paperclip_url(user.avatar, :medium)
end
json.total @total