json.deactivated_trainers @trainers.each do |trainer|
  json.id trainer.id
  json.full_name trainer.full_name
  json.email trainer.email
  json.deactivate_message trainer.deactivate_message
  json.deleted_at trainer.deleted_at.try(:strftime, "%d/%m/%Y")
end
json.total @total