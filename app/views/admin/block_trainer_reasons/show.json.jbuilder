json.block_trainer_reason do
  json.id         @block_trainer_reason.id
  json.title      @block_trainer_reason.title
  json.created_at @block_trainer_reason.created_at.try(:strftime, "%d/%m/%Y")
end