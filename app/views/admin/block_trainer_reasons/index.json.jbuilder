json.block_trainer_reasons @block_trainer_reasons.each do |reason|
  json.id         reason.id
  json.title      reason.title
  json.created_at reason.created_at.try(:strftime, "%d/%m/%Y")
end
json.total @total