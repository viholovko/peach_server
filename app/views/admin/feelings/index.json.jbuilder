json.feelings @feelings.each do |feeling|
  json.id         feeling.id
  json.title      feeling.title
  json.created_at feeling.created_at.try(:strftime, "%d/%m/%Y")
end
json.total @total
