json.deactivated_clients @clients.each do |client|
  json.id client.id
  json.full_name client.full_name
  json.email client.email
  json.deactivate_message client.deactivate_message
  json.deleted_at client.deleted_at.try(:strftime, "%d/%m/%Y")
end
json.total @total