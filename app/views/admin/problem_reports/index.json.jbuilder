json.problem_reports @problem_reports.each do |problem_report|
  json.id                    problem_report.id
  json.comment               problem_report.comment
  json.problem_report_reason problem_report.title
  json.created_at            problem_report.created_at.try(:strftime, "%d/%m/%Y")
  json.user_id               problem_report.user_id
  json.first_name            problem_report.first_name
  json.last_name             problem_report.last_name
  json.role_name             problem_report.name
end
json.options @options
json.total @total
