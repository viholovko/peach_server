json.reject_booking_reason do
  json.id         @reject_booking_reason.id
  json.title      @reject_booking_reason.title
  json.created_at @reject_booking_reason.created_at.try(:strftime, "%d/%m/%Y")
end