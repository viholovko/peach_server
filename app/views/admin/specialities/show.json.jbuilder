json.speciality do
  json.id         @speciality.id
  json.title      @speciality.title
  json.created_at @speciality.created_at.try(:strftime, "%d/%m/%Y")
  json.goals Goal.all do |goal|
    json.id goal.id
    json.title goal.title
    json.selected @speciality.goals.exists?(goal.id)
  end
end