json.specialities @specialities.each do |speciality|
  json.id         speciality.id
  json.title      speciality.title
  json.created_at speciality.created_at.try(:strftime, "%d/%m/%Y")
  json.goals      speciality.goals.map(&:title).join(', ')
end
json.total @total