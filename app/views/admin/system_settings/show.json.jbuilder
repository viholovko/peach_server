json.system_settings do
  json.fee @system_settings.fee
  json.min_radius @system_settings.min_radius.to_f
  json.max_radius @system_settings.max_radius.to_f
  json.min_price @system_settings.min_price.to_f
  json.max_price @system_settings.max_price.to_f
  json.package_expiration_time @system_settings.package_expiration_time
  json.min_sessions @system_settings.min_sessions
  json.max_sessions @system_settings.max_sessions
  json.ios_push_apns_host @system_settings.ios_push_apns_host
  json.ios_push_password @system_settings.ios_push_password
  json.ios_push_environment_sandbox @system_settings.ios_push_environment_sandbox
  json.ios_push_certificate do
    json.name @system_settings.ios_push_certificate_file_name
  end
  json.stripe_publishable_key @system_settings.stripe_publishable_key
  json.stripe_secret_key @system_settings.stripe_secret_key

  json.qb_host @system_settings.qb_host
  json.qb_application_id @system_settings.qb_application_id
  json.qb_auth_key @system_settings.qb_auth_key
  json.qb_auth_secret @system_settings.qb_auth_secret

  json.trainer_qualification_max_lenght @system_settings.trainer_qualification_max_lenght
  json.trainer_bio_max_lenght @system_settings.trainer_bio_max_lenght
end
