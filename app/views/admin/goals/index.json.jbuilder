json.goals @goals.each do |goal|
  json.id           goal.id
  json.title        goal.title
  json.created_at   goal.created_at.try(:strftime, "%d/%m/%Y")
  json.specialities goal.specialities.map(&:title).join(', ')
end
json.total @total