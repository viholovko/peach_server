json.goal do
  json.id         @goal.id
  json.title      @goal.title
  json.created_at @goal.created_at.try(:strftime, "%d/%m/%Y")
  json.specialities Speciality.all do |speciality|
    json.id speciality.id
    json.title speciality.title
    json.selected @goal.specialities.exists?(speciality.id)
  end
end