json.admins @admins.each do |admin|
  json.full_name admin.full_name
  json.id admin.id
  json.email admin.email
  json.created_at admin.created_at.try(:strftime, "%d/%m/%Y")
end
json.total @total