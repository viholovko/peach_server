json.client do
  json.id @client.id
  json.first_name @client.first_name
  json.last_name @client.last_name
  json.full_name @client.full_name
  json.email @client.email
  json.email_confirmed @client.email_confirmed
  json.gender @client.gender
  json.post_code @client.post_code
  json.qb_token @client.qb_token
  json.social @client.social
  json.address @client.address
  json.has_location @client.has_location
  json.latitude @client.latitude
  json.longitude @client.longitude
  json.location_radius @client.location_radius
  json.goals @client.goals.map(&:title).join(', ')
  json.avatar do
    json.url paperclip_url(@client.avatar, :medium)
  end
  json.has_credit_card @client.has_credit_card
  json.created_at @client.created_at.try(:strftime, "%d/%m/%Y")
  json.deactivated @client.deleted_at.nil? ? false: true
  json.bio @client.bio
  json.training_places @client.training_places.map(&:title).join(', ')
end