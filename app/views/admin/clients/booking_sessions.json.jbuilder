json.booking_sessions @booking_sessions.each do |booking_session|
  json.id             booking_session.id
  json.booking_id     booking_session.booking_id
  json.booking_type   Booking.booking_types.key(booking_session.booking_type)
  json.session_price  booking_session.session_price
  json.status         Booking.statuses.key(booking_session.status)
  json.from_time      booking_session.from_time.try(:strftime, '%a, %m/%d/%Y %H:%M')
  json.to_time        booking_session.to_time.try(:strftime, '%a, %m/%d/%Y %H:%M')
  json.trainer_id     booking_session.trainer_id
  json.trainer_name   [booking_session.first_name, booking_session.last_name].compact.join(' ')
  json.feeling        booking_session.title
  json.rate           booking_session.rate
end
json.total @total
json.options @options
