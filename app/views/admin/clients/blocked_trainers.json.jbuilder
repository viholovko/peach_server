json.users @blocked_users.each do |user|
  json.id user.id
  json.full_name user.full_name
  json.email user.email
  json.gender user.gender
  json.avatar paperclip_url(user.avatar, :medium)
  json.block_date user.block_date.try(:strftime, "%d/%m/%Y")
  json.comment user.comment
  json.block_reasons user.block_reasons.join(', ')
end
json.total @total
