json.clients @clients.each do |client|
  json.id client.id
  json.full_name client.full_name
  json.email client.email
  json.email_confirmed client.email_confirmed
  json.gender client.gender
  json.social client.social
  json.avatar json.avatar paperclip_url(client.avatar, :medium)
  json.has_credit_card client.has_credit_card
  json.created_at client.created_at.try(:strftime, "%d/%m/%Y")
  json.deactivated client.deleted_at.nil? ? false: true
end
json.total @total