json.users @trainers do |trainer|
  json.id trainer.id
  json.full_name trainer.full_name
  json.gender trainer.gender
  json.email trainer.email
  json.avatar paperclip_url(trainer.avatar, :medium)
  json.specialities trainer.specialities.all.map(&:title).join(", ")
end
json.total @total