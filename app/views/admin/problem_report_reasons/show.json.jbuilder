json.problem_report_reason do
  json.id         @problem_report_reason.id
  json.title      @problem_report_reason.title
  json.created_at @problem_report_reason.created_at.try(:strftime, "%d/%m/%Y")
end
