json.email_sender do
  json.address @email_sender.address
  json.port @email_sender.port
  json.domain @email_sender.domain
  json.authentication @email_sender.authentication
  json.user_name @email_sender.user_name
  json.enable_starttls_auto @email_sender.enable_starttls_auto
end