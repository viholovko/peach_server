json.training_place do
  json.id         @training_place.id
  json.title      @training_place.title
  json.created_at @training_place.created_at.try(:strftime, "%d/%m/%Y")
end