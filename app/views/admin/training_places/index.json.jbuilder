json.training_places @training_places.each do |place|
  json.id         place.id
  json.title      place.title
  json.created_at place.created_at.try(:strftime, "%d/%m/%Y")
end
json.total @total
