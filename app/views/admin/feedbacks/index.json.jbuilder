json.total @total
json.feedbacks @feedbacks.each do |feedback|

  user = User.new id: feedback['id'],
                  avatar_file_name: feedback['avatar_file_name'],
                  avatar_file_size: feedback['avatar_file_size'],
                  avatar_content_type: feedback['avatar_content_type'],
                  avatar_updated_at: feedback['avatar_updated_at']

  json.full_name    [feedback.first_name, feedback.last_name].compact.join(' ')
  json.gander       feedback.gender
  json.avatar       paperclip_url(user.avatar, :medium)
  json.stars        feedback['stars']
  json.comment      feedback['comment']
  json.id           feedback['feedback_id']
  json.user_id      feedback['id']
  json.time         feedback['created_at'].try(:strftime, "%d/%m/%Y %R")
end
