json.block_trainer_reason do
  json.id @block_trainer_reason.id
  json.created_at time_ago_in_words(@block_trainer_reason.created_at) + ' ' + t('datetime.ago') + ' ' + t('datetime.at') + ' ' + @block_trainer_reason.created_at.strftime("%H:%M")
  json.title @block_trainer_reason.title
  json.created_at @block_trainer_reason.created_at
  json.updated_at @block_trainer_reason.updated_at
end