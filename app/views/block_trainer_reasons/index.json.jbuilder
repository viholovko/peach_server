json.block_trainer_reasons @block_trainer_reasons.each do |block_trainer_reason|
  json.id block_trainer_reason.id
  json.created_at time_ago_in_words(block_trainer_reason.created_at) + ' ' + t('datetime.ago') + ' ' + t('datetime.at') + ' ' + block_trainer_reason.created_at.strftime("%H:%M")
  
  json.title block_trainer_reasons.title
  
  
end
json.count @count