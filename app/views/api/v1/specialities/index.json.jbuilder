json.array! @specialities.each do |speciality|
  json.id speciality.id
  json.title speciality.title
end