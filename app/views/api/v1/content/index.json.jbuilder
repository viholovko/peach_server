json.goals @goals.each do |goal|
  json.id goal.id
  json.title goal.title
end
json.specialities @specialities.each do |speciality|
  json.id speciality.id
  json.title speciality.title
end
json.block_trainer_reasons @block_trainer_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
json.block_client_reasons @block_client_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
json.training_places @training_places.each do |place|
  json.id place.id
  json.title place.title
end
json.feelings @feelings.each do |feeling|
  json.id feeling.id
  json.title feeling.title
end
json.problem_report_reasons @problem_report_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
json.reject_booking_reasons @reject_booking_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
json.system_settings do
  json.min_session_price @system_settings.min_price.to_f
  json.max_session_price @system_settings.max_price.to_f
  json.min_radius @system_settings.min_radius
  json.max_radius @system_settings.max_radius
  json.trainer_bio_max_lenght @system_settings.trainer_bio_max_lenght
  json.trainer_qualification_max_lenght @system_settings.trainer_qualification_max_lenght
end
