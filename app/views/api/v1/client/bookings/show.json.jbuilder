json.id @booking.id
json.type @booking.booking_type
json.status @booking.status
json.trainer_id @booking.trainer&.id
json.first_name @booking.trainer&.first_name
json.last_name @booking.trainer&.last_name
json.avatar paperclip_url(@booking.trainer.avatar, :medium)
json.online @booking.trainer&.online
json.price @booking.booking_price
json.session_price @booking.session_price
json.latitude @booking.latitude
json.longitude @booking.longitude
json.distance current_user.distance_to_coordinates(latitude: @booking.latitude, longitude: @booking.longitude)
json.address @booking.address
json.reject_reasons @booking.reject_booking_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
json.next_session_date @next_session&.from_time&.beginning_of_day.to_f
json.next_session_from_time @next_session&.from_time&.to_f
json.next_session_to_time @next_session&.to_time&.to_f

json.sessions_count                   @booking.renewable? ? (@booking.pending? || @booking.rejected? ? @booking.estimated_sessions_count_per_month : @booking.booking_sessions.where(paid_up: true).count) : @booking.booking_sessions.count

json.first_session_date               @booking.first_session_date.to_f
json.last_session_date                @booking.last_session_date.to_f

json.first_session_in_this_week_date  @booking.booking_sessions.where("from_time >= ? AND to_time <= ?", Time.now.beginning_of_week, Time.now.end_of_week).order(from_time: :asc).first&.from_time&.to_f
json.last_session_in_this_week_date   @booking.booking_sessions.where("from_time >= ? AND to_time <= ?", Time.now.beginning_of_week, Time.now.end_of_week).order(from_time: :desc).first&.from_time&.to_f

json.first_session_in_this_month_date @booking.first_session_in_this_month_date.to_f
json.last_session_in_this_month_date  @booking.last_session_in_this_month_date.to_f

json.sessions_in_this_month_count     @booking.renewable? ? (@booking.pending? || @booking.rejected? ? @booking.estimated_sessions_count_per_month : @booking.paid_sessions_in_this_month_count) : @booking.booking_sessions.count
json.sessions_in_this_week_count      @booking.renewable? ? @booking.booking_session_days.count : @booking.booking_sessions.where("from_time >= ? AND to_time <= ?", Time.now.beginning_of_week, Time.now.end_of_week).count

# ce jakasj xernja for ios guys
once_session = @booking.booking_sessions.first
json.once_session_id once_session&.id
json.once_session_from_time once_session&.from_time.to_f
json.once_session_to_time once_session&.to_time.to_f

session_change_request = SessionChangeRequest.find_by(id: params[:session_change_request_id])
json.change_request_id session_change_request&.id
json.requested_from_time session_change_request&.from_time&.to_f
json.requested_to_time session_change_request&.to_time&.to_f

booking_change_request = BookingChangeRequest.find_by(id: params[:booking_change_request_id])
if booking_change_request
  json.booking_session_days booking_change_request.booking_sessions_attributes.each do |session|
    json.day session[:from_time].wday
    json.from_time session[:from_time].to_f
    json.to_time session[:to_time].to_f
  end
  json.days_per_week booking_change_request.booking_sessions_attributes.count
else
  json.booking_session_days @booking.booking_session_days.each do |day|
    json.day day.day
    json.from_time Time.new.utc.change(hour: day.from_time.split(':').first, min: day.from_time.split(':').last).to_f
    json.to_time Time.new.utc.change(hour: day.to_time.split(':').first, min: day.to_time.split(':').last).to_f
  end
  json.days_per_week @booking.booking_session_days.count
end