json.trainers @trainers do |trainer|
  json.id trainer.id
  json.first_name trainer.first_name
  json.last_name trainer.last_name
  json.gender trainer.gender
  json.avatar paperclip_url(trainer.avatar, :medium)
  json.bio trainer.bio
  json.address trainer.address
  json.post_code trainer.post_code
  json.latitude trainer.latitude
  json.longitude trainer.longitude
  json.location_radius trainer.location_radius
  json.online trainer.online
  json.sessions_price trainer.sessions_price
  json.specialities trainer.specialities.each do |speciality|
    json.id speciality.id
    json.title speciality.title
  end
end
json.total @total