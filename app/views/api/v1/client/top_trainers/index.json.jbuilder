json.trainers @trainers do |trainer|
  json.id trainer.id
  json.first_name trainer.first_name
  json.last_name trainer.last_name
  json.gender trainer.gender
  json.online trainer.online
  json.sessions_price trainer.sessions_price
  json.avatar paperclip_url(trainer.avatar, :medium)
  json.bio trainer.bio
  json.address trainer.address
  json.post_code trainer.post_code
  json.latitude trainer.latitude
  json.longitude trainer.longitude
  json.location_radius trainer.location_radius
  json.specialities trainer['specialities']
  json.training_places trainer['training_places']
  json.average_response_time distance_of_time_in_words(trainer['average_response_time'])
end
json.total @total
json.top true