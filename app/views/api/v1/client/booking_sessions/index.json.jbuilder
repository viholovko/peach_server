json.booking_sessions @sessions do |session|
  opponent = User.new id:                  session['opponent_id'],
                      avatar_file_name:    session['opponent_avatar_file_name'],
                      avatar_content_type: session['opponent_avatar_content_type'],
                      avatar_file_size:    session['opponent_avatar_file_size'],
                      avatar_updated_at:   session['opponent_avatar_updated_at']

  json.id              session.id
  json.booking_id      session.booking_id

  json.trainer_id      session['opponent_id']
  json.first_name      session['opponent_first_name']
  json.last_name       session['opponent_last_name']
  json.gender          User.genders.key(session['opponent_gender'].to_i)
  json.avatar          paperclip_url(opponent.avatar, :medium)

  json.specialities    session['specialities']
  json.rate            session['rate']
  json.feeling_id      session['feeling_id']
  json.feeling_title   session['feeling_title']

  json.latitude        session['latitude']
  json.longitude       session['longitude']
  json.address         session['address']
  json.location_radius session['opponent_location_radius']
  json.distance        session['distance']

  json.from_time       session['from_time'].to_f
  json.to_time         session['to_time'].to_f
  json.completed       session['to_time'] < DateTime.now

  json.booking_type    Booking.booking_types.key(session.booking_type)
  json.session_price   session.session_price

end
json.total @count