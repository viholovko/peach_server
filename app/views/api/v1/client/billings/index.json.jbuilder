json.total @total
json.earnings @payments do |payment|
  opponent = User.new id: payment['opponent_id'],
                      avatar_file_name: payment['avatar_file_name'],
                      avatar_file_size: payment['avatar_file_size'],
                      avatar_content_type: payment['avatar_content_type']

  json.trainer_id payment['opponent_id']
  json.avatar paperclip_url(opponent.avatar, :medium)
  json.first_name payment['first_name']
  json.last_name payment['last_name']

  json.booking_id payment['booking_id']
  json.session_price payment['session_price']
  json.sessions_count payment['sessions_count']
  json.date payment['type'] == 'upcoming' ? payment['date'].beginning_of_month.to_datetime.to_f : payment['date'].to_datetime.to_f
  json.type payment['type']
  json.payment_method_last_four payment['payment_method_last_four']
  json.card_brand payment['card_brand']
end