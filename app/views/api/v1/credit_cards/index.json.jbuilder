json.array! @credit_cards.each do |credit_card|
  json.id credit_card.id
  json.customer_id credit_card.customer_id
  json.name credit_card.name
  json.email credit_card.email
  json.brand credit_card.brand
  json.country credit_card.country
  json.exp_month credit_card.exp_month
  json.exp_year credit_card.exp_year
  json.last4 credit_card.last4
  json.activated credit_card.active
end