json.array! @goals.each do |goal|
  json.id goal.id
  json.title goal.title
end