json.change_requests @requests do |request|
  json.id request.id
  json.requested_from_time request['requested_from_time'].to_i
  json.requested_to_time request['requested_to_time'].to_i
  json.current_from_time request['current_from_time'].to_i
  json.current_to_time request['current_to_time'].to_i
  json.trainer_id request['trainer_id']
  json.client_id request['client_id']
end
json.total @count