json.notifications @notifications.each do |notification|
  json.id notification.id
  json.booking_id notification['booking_id']
  json.announcement_id notification['announcement_id']
  json.notification_type  notification.notification_type
  json.title              notification.data[:title]
  json.description        notification.data[:description]
  json.from_user_id       notification.data[:from_user]
  json.booking_session_id notification.data[:booking_session_id]
  json.created_at         (notification['session_to_time'] ? notification['session_to_time'].to_f : notification.created_at.to_f)

  opponent = User.new(
      id: notification['user_id'],
      avatar_file_name: notification['avatar_file_name'],
      avatar_content_type: notification['avatar_content_type'],
      avatar_file_size: notification['avatar_file_size'],
  )

  announcement = Announcement.new(
      id: notification['announcement_id'],
      image_file_name: notification['image_file_name'],
      image_content_type: notification['image_content_type'],
      image_file_size: notification['image_file_size'],
  )

  json.avatar notification.notification_type == 'announcement' ? paperclip_url(announcement.image, :medium) : paperclip_url(opponent.avatar, :medium)
  json.last_name notification['last_name']
  json.first_name notification['first_name']

  json.sessions_count                   notification['sessions_count']
  json.sessions_in_this_week_count      notification['sessions_in_this_week_count']
  json.sessions_in_this_month_count     notification['sessions_in_this_month_count']
  json.first_session_date               notification['first_session_date'].to_i
  json.last_session_date                notification['last_session_date'].to_i
  json.first_session_in_this_week_date  notification['first_session_in_this_week_date'].to_i
  json.last_session_in_this_week_date   notification['last_session_in_this_week_date'].to_i
  json.first_session_in_this_month_date notification['first_session_in_this_month_date'].to_i
  json.last_session_in_this_month_date  notification['last_session_in_this_month_date'].to_i

  json.action_required !!notification.action_required

  json.session_change_request_id notification.session_change_request_id
  json.booking_change_request_id notification.booking_change_request_id
  json.read !!notification.read
end
json.total @total
