json.bookings @bookings do |booking|
  json.id booking.id
  json.client_id booking['opponent_id']
  json.first_name booking['opponent_first_name']
  json.last_name booking['opponent_last_name']
  json.booking_type booking.booking_type
  json.status booking.status
  json.session_price booking.session_price
  json.latitude booking.latitude
  json.longitude booking.longitude
  json.address booking.address
  json.online (booking['online'].present? && booking['online'] > Time.now - 15.minutes)

  opponent = User.new(
      id: booking['opponent_id'],
      avatar_file_name: booking['opponent_avatar_file_name'],
      avatar_content_type: booking['opponent_avatar_content_type'],
      avatar_file_size: booking['opponent_avatar_file_size'],
  )
  json.avatar paperclip_url(opponent.avatar, :medium)

  json.sessions_count                   booking.renewable? ? (booking.pending? ? booking.estimated_sessions_count_per_month : booking['paid_sessions_count']) : booking['sessions_count']
  json.sessions_in_this_week_count      booking.renewable? ? booking['session_days_count'] : booking['sessions_in_this_week_count']
  json.sessions_in_this_month_count     booking.renewable? ? (booking.pending? ? booking.estimated_sessions_count_per_month : booking['paid_sessions_in_this_month_count']) : booking['sessions_count']

  json.first_session_date               booking.renewable? ? (booking.pending? ? booking.estimated_first_session_date_in_month.to_f : booking['first_session_in_this_month_date'].to_f) : booking['first_session_date'].to_f
  json.last_session_date                booking.renewable? ? (booking.pending? ? booking.estimated_last_session_date_in_month.to_f : booking['last_session_in_this_month_date'].to_f) : booking['last_session_date'].to_f

  json.first_session_in_this_week_date  booking['first_session_in_this_week_date']&.to_f
  json.last_session_in_this_week_date   booking['last_session_in_this_week_date']&.to_f

  json.first_session_in_this_month_date booking.renewable? ? (booking.pending? ? booking.estimated_first_session_date_in_month.to_f : booking['first_session_in_this_month_date'].to_f) : booking['first_session_date'].to_f
  json.last_session_in_this_month_date  booking.renewable? ? (booking.pending? ? booking.estimated_last_session_date_in_month.to_f : booking['last_session_in_this_month_date'].to_f) : booking['last_session_date'].to_f

  # ce jakasj xernja for ios guys
  once_session = booking.booking_sessions.first
  json.once_session_id once_session&.id
  json.once_session_from_time once_session&.from_time.to_f
  json.once_session_to_time once_session&.to_time.to_f
end
json.total @count