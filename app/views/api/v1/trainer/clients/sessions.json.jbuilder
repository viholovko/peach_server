json.array! @bookings do|booking|
  json.booking do
    json.id booking.id
    json.client_id booking.client_id
    json.trainer_id booking.trainer_id
      json.reject_booking_reasons booking.reject_booking_reasons.each do |r|
        json.id r.id
        json.title r.title
      end
    json.booking_type booking.booking_type
    json.status booking.status
    json.latitude booking.latitude
    json.longitude booking.longitude
    json.address booking.address
    json.session_price booking.session_price
    json.created_at booking.created_at
    json.updated_at booking.updated_at
  end
  json.booking_sessions booking.booking_sessions do|session|
    json.id session.id
    json.created_at session.created_at
    json.updated_at session.updated_at
    json.from_time session.from_time.try(:strftime, "%FT%T.%L%:z")
    json.to_time session.to_time.try(:strftime, "%FT%T.%L%:z")
  end
end