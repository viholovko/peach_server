json.clients @clients.each do |client|
  json.id client.id
  json.first_name client.first_name
  json.last_name client.last_name
  json.specialities client['specialities']
  json.goals client['goals']
  json.gender client.gender
  json.online client.online
  json.avatar paperclip_url(client.avatar, :medium)
  json.address client.address
  json.post_code client.post_code
  json.latitude client.latitude
  json.longitude client.longitude
  json.location_radius client.location_radius ? number_with_precision(client.location_radius.to_f/1000 , precision: 2) : nil
  #  TODO move to sql query
  next_session = Booking.find(client.booking_id).booking_sessions.where("from_time > '#{Time.now}'").order(:from_time).first&.from_time
  json.next_session_date next_session.try(:utc).try(:to_f)
  json.distance client['distance']
  json.booking_id client.booking_id
end
json.total @count