json.id         @announcement.id
json.title      @announcement.title
json.subtitle   @announcement.subtitle
json.body       @announcement.body
json.image      paperclip_url(@announcement.image)
json.created_at @announcement.created_at.try(:strftime, "%d/%m/%Y")