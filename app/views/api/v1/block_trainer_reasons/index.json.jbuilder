json.array! @reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end