json.array! @training_places.each do |place|
  json.id place.id
  json.title place.title
end