json.array! @problem_report_reasons.each do |reason|
  json.id reason.id
  json.title reason.title
end
