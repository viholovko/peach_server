json.users @qb_users do |user|
  json.id user.id
  json.first_name user.first_name
  json.last_name user.last_name
  json.gender user.gender
  json.avatar paperclip_url(user.avatar, :medium)
  json.role user.role.name
  json.qb_user_id user.qb_user_id
  json.bio user.bio
  json.online user.online
end
json.total @qb_users.count