json.array! @feelings.each do |feeling|
  json.id feeling.id
  json.title feeling.title
end
