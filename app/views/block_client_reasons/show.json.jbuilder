json.block_client_reason do
  json.id @block_client_reason.id
  json.created_at time_ago_in_words(@block_client_reason.created_at) + ' ' + t('datetime.ago') + ' ' + t('datetime.at') + ' ' + @block_client_reason.created_at.strftime("%H:%M")
  json.title @block_client_reason.title
  json.created_at @block_client_reason.created_at
  json.updated_at @block_client_reason.updated_at
end