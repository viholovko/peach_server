json.block_client_reasons @block_client_reasons.each do |block_client_reason|
  json.id block_client_reason.id
  json.created_at time_ago_in_words(block_client_reason.created_at) + ' ' + t('datetime.ago') + ' ' + t('datetime.at') + ' ' + block_client_reason.created_at.strftime("%H:%M")
  
  json.title block_client_reasons.title
  
  
end
json.count @count