'use strict';

angular.module('formInput.file', []);

angular.module('formInput.file').directive('file', ['$filter', function($filter) {

    function link(scope, element, attributes, ctrl) {

        var droppable_area = element.find('.droppable-area');
        var file_select = element.find('.file-select');


        scope.removeImage = function(e){
            e.stopPropagation();
            e.preventDefault();
            scope.file = null;
            return false;
        };

        var addFile = function(file){
            scope.$apply(function(){
                scope.file = { file: file, name: file.name };
            })
        };

        if (window.File && window.FileList && window.FileReader) {

            file_select[0].addEventListener("change", function(e){
                FileDragHover(e);
                var files = e.target.files || e.dataTransfer.files;
                if(files.length > 0) {
                    addFile(files[0]);
                }
            }, false);

            var xhr = new XMLHttpRequest();
            if (xhr.upload) {

                var FileDragHover = function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    if(e.type == 'dragover') {
                        droppable_area.addClass('file-hover')
                    }else{
                        droppable_area.removeClass('file-hover')
                    }
                };

                var FileSelectHandler = function(e) {
                    FileDragHover(e);

                    var files = e.target.files || e.dataTransfer.files;

                    if(files.length > 0) {
                        addFile(files[0]);
                    }
                };

                droppable_area[0].addEventListener("dragover", FileDragHover, false);
                droppable_area[0].addEventListener("dragleave", FileDragHover, false);
                droppable_area[0].addEventListener("drop", FileSelectHandler, false);
            }
        }
    }

    return {
        link: link,
        restrict: 'A',
        require: 'ngModel',
        scope: {
            file: '=ngModel'
        },
        template: "<div class='droppable-area'>" +
        "<ul class='images-list' >" +
        '<li class="plus">' +
        '{{ file.name }}' +
        "<label>" +
        '<input type="file" class="file-select" />' +
        "</label>" +
        "</li>" +
        "<ul>" +
        "</div>"
    };
}]);