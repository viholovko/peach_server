'use strict';

angular.module('redactor', []);

angular.module('redactor').directive('redactor', ['$filter', function($filter) {

    function link(scope, element, attributes, ctrl) {

        var redactor_element = $('<div style="border: 1px solid lightgray; padding: 10px"></div>');

        element.append(redactor_element);

        redactor_element.redactor({
            callbacks: {
                keyup: function(){
                  var self = this;
                  scope.$apply(function(){
                    scope.redactorModel = self.code.get()
                  });
                },
              change: function(){
                var self = this;
                scope.$apply(function(){
                  scope.redactorModel = self.code.get()
                });
              }
            },
            buttonSource: true,
            imageUpload: element.attr('file-upload-path'),
            fileUpload: element.attr('file-upload-path'),
            plugins: ['table', 'fontsize', 'fontcolor']
        });

        var redactor_initialized = 0;
        scope.$watch('redactorModel', function(){
            if(!scope.redactorModel){
                scope.redactorModel = '';
            }
            if(redactor_initialized < 3){
                redactor_element.redactor('code.set', scope.redactorModel);
                redactor_initialized++;
            }
        });

    }

    return {
        link: link,
        restrict: 'A',
        require: 'ngModel',
        scope: {
            redactorModel: '=ngModel'
        }
    };
}]);